import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
    input: 'js/main.js',
    output: {
        file: 'build/bundle.js',
        format: 'iife',
    },
    plugins: [
        resolve(),
        commonjs()
    ]
};

