# Animation des paramètres d’un appareil photo linéaire

Cette animation interactive permet d’aider à comprendre le rôle de chaque paramètre de la matrice de projection d’un appareil photo linéaire~:

* distance focale ;
* cisaillement vertical ;
* décalage du point principal ;
* position de l’appareil dans l’espace ;
* rotation de l’appareil dans l’espace.

Elle est utilisée pour la soutenance de mi-projet.

## Instructions d’utilisation

Il faut d’abord installer les dépendances et compiler le projet, ce qui nécessite Node.JS et npm :

```sh
npm install
npm run build
```

Il suffit ensuite d’ouvrir le fichier `index.html` dans un navigateur web.

## Inspiration

Cette animation est inspirée de celle créée par Kyle Simek disponible à l’adresse <http://ksimek.github.io/perspective_camera_toy.html>. Elle utilise les bibliothèques :

* dat.GUI : pour la modification interactive des paramètres de l’appareil ;
* Three.js : comme interface à WebGL pour le rendu 3D.

