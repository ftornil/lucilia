import * as THREE from 'three';

/**
 * Crée une caméra linéaire avec les paramètres intrinsèques donnés.
 * (dérivé de https://ksimek.github.io/code.html)
 *
 * @param [focal=1] Distance focale.
 * @param [m=0] Déformation en y.
 * @param [shear=0] Cisaillement en y.
 * @param [principal=(0,0)] Décalage du point principal.
 * @param width Largeur du plan de l’image.
 * @param height Hateur du plan de l’image.
 * @param [near=0.1] Seuil de la distance de rendu.
 * @param [far=2000] Plafond de la distance de rendu.
 */
export const CalibratedCamera = function (
    focal, m, shear, principal,
    width, height,
    near, far
)
{
    THREE.Camera.call(this);

    this.type = 'CalibratedCamera';

    this.focal = focal !== undefined ? focal : 1;
    this.m = m !== undefined ? m : 0;
    this.shear = shear !== undefined ? shear : 0;
    this.principal = principal !== undefined ? principal : new THREE.Vector2();

    this.width = width;
    this.height = height;

    this.near = near !== undefined ? near : 0.1;
    this.far = far !== undefined ? far : 2000;

    // Propriétés de rotation en degrés
    Object.defineProperty(this.rotation, 'degX', {
        get: () => this.rotation.x * THREE.Math.RAD2DEG,
        set: value => this.rotation.x = value * THREE.Math.DEG2RAD
    });

    Object.defineProperty(this.rotation, 'degY', {
        get: () => this.rotation.y * THREE.Math.RAD2DEG,
        set: value => this.rotation.y = value * THREE.Math.DEG2RAD
    });

    Object.defineProperty(this.rotation, 'degZ', {
        get: () => this.rotation.z * THREE.Math.RAD2DEG,
        set: value => this.rotation.z = value * THREE.Math.DEG2RAD
    });

    this.updateProjectionMatrix();
};

CalibratedCamera.prototype = Object.assign(
    Object.create(THREE.Camera.prototype),
    {
        constructor: CalibratedCamera,
        isCalibratedCamera: true,

        copy(source, recursive)
        {
            THREE.Camera.prototype.copy.call(this, source, recursive);

            this.focal = source.focal;
            this.m = source.m;
            this.shear = source.shear;
            this.principal.copy(source.principal);

            this.width = source.width;
            this.height = source.height;

            this.near = source.near;
            this.far = source.far;
            return this;
        },

        updateProjectionMatrix()
        {
            this.projectionMatrix.makeOrthographic(
                -this.width / 2.0,
                this.width / 2.0,
                this.height / 2.0,
                -this.height / 2.0,
                this.near,
                this.far
            );

            const X = this.near + this.far;
            const Y = this.near * this.far;
            const intrinsicCameraMatrix = new THREE.Matrix4();

            intrinsicCameraMatrix.set(
                -this.focal, this.shear, this.principal.x, 0,
                0, -this.focal - this.m, this.principal.y, 0,
                0, 0, X, Y,
                0, 0, -1, 0
            );

            this.projectionMatrix.multiply(intrinsicCameraMatrix);
        }
    }
);

