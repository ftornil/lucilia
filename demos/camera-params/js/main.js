import dat from 'dat.gui';

import {scene} from './scene';
import * as image from './image';
import * as world from './world';
import {updateVirtualCamera} from './objects';

/**
 * Rendu
 */

// Réalise le rendu de la vue principale
const render = () =>
{
    world.renderer.render(scene, image.camera, image.buffer);
    world.renderer.render(scene, world.camera);
};

// Met à jour les paramètres de l’image projetée et réalise son rendu
const updateIntrinsics = () =>
{
    image.camera.updateProjectionMatrix();
    updateVirtualCamera();
    render();
};

// Redimensionne la vue principale avec le redimensionnement
// de la fenêtre
window.addEventListener('resize', () =>
{
    world.updateSize();
    render();
});

// Met à jour le rendu lors du déplacement de la caméra fixe
// par l’utilisateur
world.controls.addEventListener('change', render);

// Rendu initial
world.updateSize();
updateIntrinsics();

/**
 * GUI
 */

const gui = new dat.GUI({
    width: 450
});

const intrinsics = gui.addFolder('Paramètres intrinsèques');

intrinsics
    .add(image.camera, 'focal', 0, 800, 0.1)
    .onChange(updateIntrinsics)
    .name('<em>c</em>');

intrinsics
    .add(image.camera, 'm', -1000, 1000)
    .onChange(updateIntrinsics)
    .name('<em>m</em>');

intrinsics
    .add(image.camera, 'shear', -1000, 1000)
    .onChange(updateIntrinsics)
    .name('<em>s</em>');

intrinsics
    .add(image.camera.principal, 'x', -image.WIDTH, image.WIDTH)
    .onChange(updateIntrinsics)
    .name('<em>p<sub>x</sub></em>');

intrinsics
    .add(image.camera.principal, 'y', -image.HEIGHT / 2, image.HEIGHT / 2)
    .onChange(updateIntrinsics)
    .name('<em>p<sub>y</sub></em>');

const extrinsics = gui.addFolder('Paramètres extrinsèques');

extrinsics
    .add(image.camera.position, 'x', -300, 300, 0.1)
    .onChange(render)
    .name('<em>O<sub>x</sub></em>');

extrinsics
    .add(image.camera.position, 'y', -300, 300, 0.1)
    .onChange(render)
    .name('<em>O<sub>y</sub></em>');

extrinsics
    .add(image.camera.position, 'z', -300, 800, 0.1)
    .onChange(render)
    .name('<em>O<sub>z</sub></em>');

extrinsics
    .add(image.camera.rotation, 'degX', -180, 180, 0.1)
    .onChange(render)
    .name('<em>R<sub>&alpha;</sub></em>');

extrinsics
    .add(image.camera.rotation, 'degY', -180, 180, 0.1)
    .onChange(render)
    .name('<em>R<sub>&beta;</sub></em>');

extrinsics
    .add(image.camera.rotation, 'degZ', -180, 180, 0.1)
    .onChange(render)
    .name('<em>R<sub>&gamma;</sub></em>');

