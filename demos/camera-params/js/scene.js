import * as THREE from 'three';

/**
 * Scène globale.
 */
export const scene = new THREE.Scene();

/**
 * Calques des objets de la scène.
 */
export const LAYERS = Object.freeze({
    // Calque par défaut contenant tous les objets visibles
    DEFAULT: 0,

    // Calque contenant les objets représentant la caméra
    // ajustable, visibles uniquement depuis la caméra fixe
    // pour éviter une boucle infinie de rendu
    WORLD: 1
});

