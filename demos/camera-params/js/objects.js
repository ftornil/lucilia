/**
 * @module Insère les objets dans la scène.
 */

import * as THREE from 'three';
import * as image from './image';
import {scene, LAYERS} from './scene';

const redMaterial = new THREE.MeshLambertMaterial({color: 0x8c1934});
const greenMaterial = new THREE.MeshLambertMaterial({color: 0x107f24});
const blueMaterial = new THREE.MeshLambertMaterial({color: 0x156289});
const lineMaterial = new THREE.LineBasicMaterial({
    color: 0x333333,
    linewidth: 2,
    transparent: true,
    opacity: .2
});

// Boîte bleue
const cube = new THREE.Mesh(
    new THREE.BoxGeometry(100, 100, 100),
    blueMaterial
);

scene.add(cube);
cube.position.set(-100, 0, 0);

// Sphère rouge
const sphere = new THREE.Mesh(
    new THREE.SphereGeometry(50, 16, 16),
    redMaterial
);

scene.add(sphere);
sphere.position.set(100, 0, 0);

// Cône vert
const cone = new THREE.Mesh(
    new THREE.ConeGeometry(30, 100, 32),
    greenMaterial
);

scene.add(cone);
cone.position.set(0, 0, -200);

// Éclairage
const lights = [
    new THREE.PointLight(0xffffff),
    new THREE.PointLight(0xffffff),
    new THREE.PointLight(0xffffff)
];

lights[0].position.set(500, 500, -500);
lights[1].position.set(-500, 500, 500);
lights[2].position.set(-500, 500, 500);

lights.forEach(light => scene.add(light));

// Point symbolisant le centre de la caméra
const imageOpening = new THREE.Mesh(
    new THREE.SphereGeometry(5, 16, 16),
    new THREE.MeshBasicMaterial()
);

image.camera.add(imageOpening);
imageOpening.layers.set(LAYERS.WORLD);

// Plan de l’image
const imagePlane = new THREE.Object3D();

// Fond blanc
imagePlane.add(new THREE.Mesh(
    new THREE.PlaneGeometry(image.WIDTH, image.HEIGHT),
    new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide
    })
));

// Image projetée
imagePlane.add(new THREE.Mesh(
    new THREE.PlaneGeometry(image.WIDTH, image.HEIGHT),
    new THREE.MeshBasicMaterial({
        transparent: true,
        map: image.buffer.texture,
        side: THREE.DoubleSide
    })
));

image.camera.add(imagePlane);
imagePlane.layers.set(LAYERS.WORLD);

// Rayons de projection
const imageProjectionRays = [];

for (let i = 0; i < 4; ++i)
{
    const ray = new THREE.Line(new THREE.Geometry(), lineMaterial);

    image.camera.add(ray);
    ray.layers.set(LAYERS.WORLD);
    imageProjectionRays.push(ray);
}

// Rectangle de projection
const PROJECTION_RECTANGLE_DEPTH = -750;
const imageProjectionRect = new THREE.Line(new THREE.Geometry(), lineMaterial);
image.camera.add(imageProjectionRect);
imageProjectionRect.layers.set(LAYERS.WORLD);

// Met à jour la représentation virtuelle de la caméra
// en fonction de ses paramètres
export const updateVirtualCamera = () =>
{
    // Position du plan de l’image
    imagePlane.position.x = image.camera.principal.x;
    imagePlane.position.y = image.camera.principal.y;
    imagePlane.position.z = image.camera.focal;

    // Géométrie du tronc de projection
    let angle = 3 * Math.PI / 4;
    const angleIncrement = -Math.PI / 2;

    imageProjectionRect.geometry.vertices = [];

    imageProjectionRays.forEach(ray =>
    {
        const planeCorner = imagePlane.position.clone();
        planeCorner.x += image.WIDTH * Math.sign(Math.cos(angle)) / 2;
        planeCorner.y += image.HEIGHT * Math.sign(Math.sin(angle)) / 2;
        angle += angleIncrement;

        const otherEnd = planeCorner.clone().normalize().multiplyScalar(-1);
        otherEnd.multiplyScalar(PROJECTION_RECTANGLE_DEPTH / otherEnd.z);

        ray.geometry.vertices = [];
        ray.geometry.vertices.push(otherEnd);
        ray.geometry.vertices.push(planeCorner);
        imageProjectionRect.geometry.vertices.push(otherEnd);
        ray.geometry.verticesNeedUpdate = true;
    });

    imageProjectionRect.geometry.vertices.push(
        imageProjectionRect.geometry.vertices[0]
    );

    imageProjectionRect.geometry.verticesNeedUpdate = true;
};

