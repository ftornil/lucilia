/**
 * @module Rendu de la scène en perspective centrale vers un buffer mémoire
 * qui simule une image générée par la matrice de projection choisie.
 */

import * as THREE from 'three';

import {scene} from './scene';
import {CalibratedCamera} from './CalibratedCamera';

// Distance focale par défaut
const FOCAL = 400;

// Seuil de rendu (tout objet plus proche ne sera pas affiché)
const NEAR = 0.1;

// Plafond de rendu (tout objet plus éloigné ne sera pas affiché)
const FAR = 10000;

// Largeur de l’image
export const WIDTH = 300;

// Hauteur de l’image
export const HEIGHT = 200;

// Caméra en perspective centrale ajustable
export const camera = new CalibratedCamera(
    FOCAL, 0, 0, new THREE.Vector2(),
    WIDTH, HEIGHT, NEAR, FAR
);

scene.add(camera);
camera.position.set(0, 0, 500);

// Buffer de rendu
export const buffer = new THREE.WebGLRenderTarget(WIDTH, HEIGHT);

