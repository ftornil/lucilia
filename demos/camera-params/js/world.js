/**
 * @module Rendu de la scène en perspective cavalière vers l’écran,
 * incluant notamment l’image simulée.
 */

import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';
import {scene, LAYERS} from './scene';

// Taille du tronc de visualisation
const FRUSTUM_SIZE = 900;

// Seuil de rendu (tout objet plus proche ne sera pas affiché)
const NEAR = -4000;

// Plafond de rendu (tout objet plus éloigné ne sera pas affiché)
const FAR = 4000;

// Caméra fixe en perspective cavalière
export const camera = new THREE.OrthographicCamera(
    -1, 1, 1, -1,
    NEAR, FAR
);

scene.add(camera);
camera.position.set(300, 300, 1000);
camera.layers.enable(LAYERS.WORLD);

// Zone de rendu vers l’écran
export const renderer = new THREE.WebGLRenderer({
    antialias: true,
    alpha: true
});

const container = document.querySelector('.view');
container.appendChild(renderer.domElement);

// Contrôle de la caméra à la souris
export const controls = new OrbitControls(camera, container);

// Appelé à chaque redimensionnement pour mettre à
// jour la taille de la zone de rendu
export const updateSize = () =>
{
    const width = window.innerWidth;
    const height = window.innerHeight;
    renderer.setSize(width, height);

    const aspect = width / height;
    camera.left = -FRUSTUM_SIZE * aspect / 2;
    camera.right = FRUSTUM_SIZE * aspect / 2;
    camera.top = FRUSTUM_SIZE / 2;
    camera.bottom = -FRUSTUM_SIZE / 2;
    camera.updateProjectionMatrix();
};

