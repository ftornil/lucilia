#!/usr/bin/env gnuplot

set term gif enhanced \
    font "Fira Sans" \
    animate \
    size 800,600

set output "visualize.gif"

set title "Reconstruction de la trajectoire de l’appareil et des points observés"
set xlabel "x"
set ylabel "y"
set zlabel "z"

set xrange [-15:15]
set yrange [-15:15]
set zrange [-20:40]

do for [angle=0:119] {
    set view 60,(360-angle*3)
    splot "camera.dat" u 1:2:3 title "Trajectoire de l’appareil" with lines, \
        "cloud.dat" u 1:2:3 title "Nuage de points" with dots
}

