#!/usr/bin/env node

if (process.argv.length !== 3)
{
    console.error(`Usage: ${process.argv[1]} MATCHES`);
    console.error('Génère les fichiers SVG associés aux MATCHES.');
    process.exitCode = 1;
    return;
}

const fs = require('fs');
const path = require('path');
const StreamArray = require('stream-json/utils/StreamArray');
const randomColor = require('random-color');
const imageSize = require('image-size');

const jsonStream = StreamArray.make();

// Génération des images pour toutes les correspondances
const images = {};
let number = 0;

jsonStream.output.on('data', ({value: {matches: matchList}}) =>
{
    const color = randomColor().hexString();
    ++number;

    matchList.forEach(match =>
    {
        if (images[match.image] === undefined)
        {
            // Création d’une nouvelle image si jamais recontrée
            const innerSize = imageSize(match.image);
            const padding = 100;
            const outerSize = {
                width: innerSize.width + 2 * padding,
                height: innerSize.height + 2 * padding
            };

            images[match.image] = `\
<svg width="${outerSize.width}" height="${outerSize.height}" \
viewBox="0 0 ${outerSize.width} ${outerSize.height}" \
xmlns="http://www.w3.org/2000/svg" \
xmlns:xlink="http://www.w3.org/1999/xlink">
<rect width="${outerSize.width}" height="${outerSize.height}" fill="white" />
<g transform="translate(${padding}, ${padding})">
    <image width="${innerSize.width}" height="${innerSize.height}" \
xlink:href="file://${match.image}" />
`;
        }

        // Ajout du point coloré
        images[match.image] += `\
    <path d="M ${match.position.x - 25},${match.position.y - 25} a 5.0005,5.0005 0 0 0 -3.4843,8.5859 l 14.1426,14.1426 -14.1426,14.1426 a 5.0005,5.0005 0 1 0 7.0722,7.0703 l 14.1407,-14.1426 14.1425,14.1426 a 5.0005,5.0005 0 1 0 7.0704,-7.0703 l -14.1426,-14.1426 14.1426,-14.1426 a 5.0005,5.0005 0 1 0 -7.0704,-7.0703 l -14.1425,14.1426 -14.1407,-14.1426 a 5.0005,5.0005 0 0 0 -3.5879,-1.5156 z" \
stroke="white" stroke-width="5" fill="black" />
    <circle cx="${match.position.x + 50}" cy="${match.position.y - 50}" r="20" \
fill="${color}" stroke="black" stroke-width="5" />
    <text text-anchor="middle" x="${match.position.x + 50}" \
y="${match.position.y - 44}" font-family="sans-serif" font-weight="bold" \
font-size="20">${number}</text>
`;
    });
});

// À la fin, écriture des images générées
jsonStream.output.on('end', () =>
{
    Object.keys(images).forEach(imagePath =>
    {
        fs.writeFileSync(path.join(
            path.dirname(imagePath),
            path.basename(imagePath, '.jpg') + '-features.svg'
        ), images[imagePath] + '</g>\n</svg>');
    });
});

// Lecture du fichier JSON en mode flux
const matchesPath = path.join(process.cwd(), process.argv[2]);
fs.createReadStream(matchesPath).pipe(jsonStream.input);
