#!/bin/bash

# Vérifie que les commandes nécessaires sont installées
if ! [ -x "$(command -v clang-format)" ]
then
    echo "Erreur : clang-format n’est pas installé."
    exit 2
fi

if ! [ -x "$(command -v getopt)" ]
then
    echo "Erreur : getopt n’est pas installé."
    exit 2
fi

getopt --test > /dev/null

if [[ $? -ne 4 ]]
then
    echo "Erreur : enhanced getopt n’est pas disponible."
    exit 2
fi

# Affiche les instructions d’utilisation du script
function usage
{
    echo "Usage: $(basename "$0") [-hf] path"
    echo -e "\nVérifie le formattage du code situé dans le répertoire \
projet dont le chemin est path."
    echo -e "\nOptions:"
    echo " -f, --fixit       corrige automatiquement les erreurs"
    echo " -h, --help        affiche ce message d’aide"
    echo -e "\nCode de retour :"
    echo " - 0  le formattage est correct"
    echo " - 1  des erreurs de formattage ont été détectées"
    echo " - 2  une erreur interne s’est produite"
}

OPTIONS=hf
LONGOPTIONS=help,fixit
PARSED=$(\
    getopt \
        --options="$OPTIONS" \
        --longoptions="$LONGOPTIONS" \
        --name $(basename "$0") \
        -- "$@"\
)

if [[ $? -ne 0 ]]
then
    usage
    exit 2
fi

mode_fixit=n

eval set -- "$PARSED"

while true
do
    case "$1" in
    -h|--help)
        usage
        exit 0
        ;;
    -f|--fixit)
        mode_fixit=y
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "Erreur de programmation !"
        exit 2
        ;;
    esac
done

if [[ $# -ne 1 ]]
then
    usage
    exit 2
fi

ROOT_DIR=$(realpath "$1")
SRC_DIR="$ROOT_DIR/src"

# Affiche les erreurs de formattage détectées par clang-format
# dans un source de code.
#
# Arguments :
#   - fichier dont il faut vérifier le formattage
function show_formatting_errors
{
    file_to_format=$1
    display_name=$(realpath --relative-to="$ROOT_DIR" "$file_to_format")
    differences=$( \
        diff --color=always "$file_to_format" <( \
            clang-format \
                -style=file \
                "$file_to_format" \
        ) \
    )

    has_errors=$?

    if [[ $has_errors -ne 0 ]]
    then
        echo -e "$display_name\n\n$differences\n\n───\n"
    fi

    return $has_errors
}

# Corrige les erreurs de formattage détectées par clang-format
# dans un source de code.
#
# Arguments :
#   - fichier dont il faut corriger le formattage
function fix_formatting_errors
{
    file_to_format=$1
    display_name=$(realpath --relative-to="$ROOT_DIR" "$file_to_format")
    fixed_file=$( \
        clang-format \
            -style=file \
            "$file_to_format" \
    )

    diff "$file_to_format" <(echo "$fixed_file") > /dev/null
    has_errors=$?

    if [[ $has_errors -ne 0 ]]
    then
        echo "$fixed_file" > "$file_to_format"
        echo "Corrections appliquées dans $display_name."
    fi

    return $has_errors
}

echo "$(tput bold)Vérification du formattage (clang-format)$(tput sgr0)"

if [[ $mode_fixit = "y" ]]
then
    echo -e "Liste des corrections appliquées :"
else
    echo -e "Liste des erreurs de formattage :"
fi

echo -e "\n───\n"

error_count=0

# Pour chaque fichier source, lance clang-format
while IFS= read -r -d '' -u 9
do
    if [[ $mode_fixit = "y" ]]
    then
        fix_formatting_errors "$REPLY"
    else
        show_formatting_errors "$REPLY"
    fi

    if [[ $? -ne 0 ]]
    then
        ((++error_count))
    fi
done 9< <( find "$SRC_DIR" -type f -exec printf '%s\0' {} + )

# Récapitulatif
if [[ $error_count -ne 0 ]]
then
    if [[ $mode_fixit = "y" ]]
    then
        echo -e "\n$error_count erreur(s) de formattage corrigée(s)."
    else
        echo "$error_count erreur(s) de formattage à corriger."
    fi
    exit 1
else
    echo "Aucune erreur de formattage à corriger."
fi
