#ifndef FEATURE_HPP
#define FEATURE_HPP

#include <nlohmann/json.hpp>
#include <opencv2/core/matx.hpp>
#include <opencv2/core/types.hpp>

/**
 * Point caractéristique d’une capture.
 */
class Feature
{
public:
    using Descriptor = cv::Vec<float, 128>;
    using Position = cv::Point2f;
    using Color = cv::Vec3b;

    /**
     * Construit un point caractéristique vide.
     */
    Feature();

    /**
     * Construit un point caractéristique.
     *
     * @param descriptor Vecteur descripteur du voisinage du point.
     * @param position Position du point dans l’image.
     * @param color Couleur dominante du point dans l’image au format BVR.
     */
    Feature(
        Descriptor,
        Position,
        Color
    );

    /**
     * Récupère le vecteur descripteur associé au point caractéristique.
     *
     * @return Descripteur du point caractéristique.
     */
    const Descriptor& descriptor() const;

    /**
     * Récupère la position en deux dimensions du point dans son image source.
     *
     * @return Position 2D du point caractéristique.
     */
    const Position& position() const;

    /**
     * Récupère la couleur dominante du point dans l’image.
     *
     * @return Couleur dominante du point au format BVR.
     */
    const Color& color() const;

    /**
     * Convertit un point caractéristique en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param feature Point caractéristique source.
     */
    friend void to_json(nlohmann::json&, const Feature&);

    /**
     * Charge un point caractéristique depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param capture Point caractéristique à remplir.
     */
    friend void from_json(const nlohmann::json&, Feature&);

private:
    // Vecteur descripteur de ce point caractéristique
    Descriptor _descriptor;

    // Position en deux dimensions du point dans son image source
    Position _position;

    // Couleur dominante du point dans l’image au format BGR
    Color _color;
};

#endif // FEATURE_HPP
