#include "Point.hpp"
#include "Feature.hpp"
#include "utils/opencv_conversions.hpp"
#include <algorithm>
#include <cmath>
#include <stdexcept>

Point::Point(Position position, double error, Color color)
: _position{std::move(position)}
, _error{error}
, _color{std::move(color)}
{
}

namespace
{
double angle_between(
    const Capture::Direction& first,
    const Capture::Direction& second
)
{
    double dot = first.dot(second);
    double first_length = cv::norm(first);
    double second_length = cv::norm(second);

    double c = dot / first_length * second_length;

    if (c >= 1.)
    {
        return 0.;
    }
    else if (c <= -1.)
    {
        return M_PI;
    }
    else
    {
        return std::acos(c);
    }
}
}

std::optional<Point> Point::fromMatches(
    const std::vector<Capture>& captures,
    const Matching::Cluster& cluster,
    double reprojection_limit,
    double angle_threshold
)
{
    // Vérification que les captures dans lesquelles les points ont été
    // mesurés forment un angle suffisamment grand pour que la reconstruction
    // soit pertinente
    double angle_ok = false;

    for (
        auto first_pair = std::cbegin(cluster);
        first_pair != std::cend(cluster) && !angle_ok;
        ++first_pair
    )
    {
        for (
            auto second_pair = std::next(first_pair);
            second_pair != std::cend(cluster) && !angle_ok;
            ++second_pair
        )
        {
            const auto& [first_capture, first_feature] = *first_pair;
            const auto& [second_capture, second_feature] = *second_pair;

            double angle = angle_between(
                captures[first_capture].direction(),
                captures[second_capture].direction()
            );

            if (angle >= angle_threshold)
            {
                angle_ok = true;
            }
        }
    }

    if (!angle_ok)
    {
        return std::optional<Point>{};
    }

    // Construction du système d’équations déterminant la position du
    // point dans l’espace et calcul de la couleur moyenne
    cv::Mat_<double> constraints(
        3 * cluster.size(),
        4 + cluster.size(),
        0.
    );

    double sum_b = 0;
    double sum_g = 0;
    double sum_r = 0;

    std::size_t cur_row = 0;
    std::size_t cur_col = 4;

    std::vector<std::size_t> apparitions;

    for (const auto& [id_capture, id_feature] : cluster)
    {
        // Construction des équations spécifiques au point actuel
        cv::Matx34d projection = captures[id_capture].projection();

        for (std::size_t proj_row = 0; proj_row < 3; ++proj_row)
        {
            for (std::size_t proj_col = 0; proj_col < 4; ++proj_col)
            {
                constraints(cur_row + proj_row, proj_col)
                    = -projection(proj_row, proj_col);
            }
        }

        const auto& feature
            = captures[id_capture].image().features()[id_feature];
        constraints(cur_row, cur_col) = feature.position().x;
        constraints(cur_row + 1, cur_col) = feature.position().y;
        constraints(cur_row + 2, cur_col) = 1;

        cur_row += 3;
        ++cur_col;

        // Accumulation de la couleur moyenne
        sum_b += feature.color()[0];
        sum_g += feature.color()[1];
        sum_r += feature.color()[2];
    }

    // Nous cherchons idéalement un point qui annule le système d’équations
    // `constraints`. Comme les informations sont bruitées, nous nous
    // contentons de rechercher un vecteur X minimisant `constraints * X`
    // avec le SVD
    cv::Mat_<double> point_with_alphas;
    cv::SVD::solveZ(constraints, point_with_alphas);

    Point result{
        Point::Position{
            /* x = */ point_with_alphas(0, 0) / point_with_alphas(3, 0),
            /* y = */ point_with_alphas(1, 0) / point_with_alphas(3, 0),
            /* z = */ point_with_alphas(2, 0) / point_with_alphas(3, 0),
        },
        /* error = */ 0,
        Point::Color{
            /* b = */
            static_cast<unsigned char>(cvRound(sum_b / cluster.size())),
            /* g = */
            static_cast<unsigned char>(cvRound(sum_g / cluster.size())),
            /* r = */
            static_cast<unsigned char>(cvRound(sum_r / cluster.size()))
        }
    };

    // Vérifie qu’en reprojetant le point reconstruit sur chacune
    // des captures, on n’obtient pas un résultat trop éloigné
    for (const auto& [id_capture, id_feature] : cluster)
    {
        cv::Point2d projected
            = captures[id_capture].sceneToImage(result.position());
        cv::Point2d original
            = captures[id_capture].image().features()[id_feature].position();

        if (
            (projected.x - original.x) * (projected.x - original.x)
            + (projected.y - original.y) * (projected.x - original.y)
            >= reprojection_limit * reprojection_limit
        )
        {
            return std::optional<Point>{};
        }
    }

    return std::optional<Point>{std::move(result)};
}

const Point::Position& Point::position() const
{
    return _position;
}

double Point::error() const
{
    return _error;
}

const Point::Color& Point::color() const
{
    return _color;
}

void to_json(nlohmann::json& object, const Point& point)
{
    object = {
        {"position", point._position},
        {"error", point._error},
        {"color", point._color}
    };
}

void from_json(const nlohmann::json& object, Point& point)
{
    object.at("position").get_to(point._position);
    object.at("error").get_to(point._error);
    object.at("color").get_to(point._color);
}

Point::operator CGAL::Exact_predicates_inexact_constructions_kernel::Point_3() const {
    return {_position.x, _position.y, _position.z};
}
