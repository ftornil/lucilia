#define BOOST_SYSTEM_NO_DEPRECATED
#define BOOST_FILESYSTEM_NO_DEPRECATED

#include "utils.hpp"
#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>

namespace fs = boost::filesystem;

std::vector<fs::path> explorePaths(const std::vector<fs::path>& list)
{
    std::vector<fs::path> result;

    for (const fs::path& item : list)
    {
        boost::system::error_code err;
        auto dir = fs::directory_iterator{item, err};

        if (err.value() == boost::system::errc::not_a_directory)
        {
            // Si l’item est un fichier simple, ajout direct à la liste
            result.push_back(item);
        }
        else if (err)
        {
            throw std::runtime_error{"Impossible d’accéder à " + item.string()
                                     + " : " + err.message()};
        }
        else
        {
            // Sinon, parcours des fichiers fils et ajout dans l’ordre lexico
            std::vector<fs::path> item_result;

            for (const auto& entry : dir)
            {
                item_result.push_back(entry.path());
            }

            std::sort(begin(item_result), end(item_result));
            std::copy(
                begin(item_result),
                end(item_result),
                std::back_inserter(result));
        }
    }

    return result;
}
