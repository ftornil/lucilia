#ifndef UTILS_ENUM_HPP
#define UTILS_ENUM_HPP

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace detail
{
std::vector<std::string> create_map_from_enum(std::string options);
}

/**
 * Déclare une énumération compatible avec les flux.
 *
 * Cette macro (eh oui…) permet de déclarer une énumération « scopée »
 * (enum class) automatiquement munie d’opérateurs d’entrée et de sortie
 * sur flux qui lisent et écrivent les noms des éléments de l’énumération.
 *
 * Adapté de <https://stackoverflow.com/a/48820063>.
 *
 * @example
 * DECLARE_ENUM(Enum, A, B, C)
 * Enum x = Enum::A;
 * std::cout << x; // affiche « A »
 * std::cin >> x; // saisie de « B »
 * assert(x == Enum::B);
 *
 * @param name Nom de l’énumération.
 * @param elts... Liste des éléments de l’énumération.
 */
#define DECLARE_ENUM(Enum, ...)\
enum class Enum\
{\
    __VA_ARGS__\
};\
inline std::ostream& operator<<(std::ostream& out, Enum value)\
{\
    static const auto map = detail::create_map_from_enum(#__VA_ARGS__);\
    out << map[static_cast<std::size_t>(value)];\
    return out;\
}\
inline std::istream& operator>>(std::istream& in, Enum& value)\
{\
    static const auto map = detail::create_map_from_enum(#__VA_ARGS__);\
    std::string token;\
    in >> token;\
    auto item = std::find(begin(map), end(map), token);\
    if (item == std::end(map))\
    {\
        in.setstate(std::ios_base::failbit);\
    }\
    else\
    {\
        value = static_cast<Enum>(std::distance(begin(map), item));\
    }\
    return in;\
}

/**
 * Namespace regroupant quelques énumérations utiles pour la classe Model, depuis CGAL
 */
namespace cgalEnums {
    typedef enum frontMesherForScaleSpaceReconstruction {
        ALPHA_SHAPE,
        ADVANCING_FRONT
    } mesher;

    typedef enum smoothingMethodForScaleSpaceReconstruction {
        WEIGHTED_PCA,
        JET_SMOOTHER
    } smoother;
}

#endif // UTILS_ENUM_HPP
