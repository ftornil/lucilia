#ifndef UTILS_PROGRESS_HPP
#define UTILS_PROGRESS_HPP

#include <chrono>
#include <iostream>

/**
 * Outil pour communiquer à l’utilisateur la progression d’un calcul.
 */
class Progress
{
private:
    class Task;

public:
    using Step = long unsigned int;

    /**
     * Construit un suiveur de progression qui n’affiche rien.
     */
    Progress() = default;

    /**
     * Construit un suiveur de progression qui affiche les informations
     * sur un flux de sortie donné.
     *
     * @param output Flux de sortie sur lequel les informations sont affichées.
     * @param [update_rate=1s] Délai minimum qui doit s’écouler entre chaque
     * rafraîchissement d’une barre de progression
     */
    Progress(
        std::ostream&,
        std::chrono::milliseconds update_delay = std::chrono::milliseconds{1000}
    );

    /**
     * Démarre une nouvelle tâche.
     *
     * @param title Description de la tâche.
     * @param number_steps Nombre d’étapes de progression prévues.
     * @return Objet de suivi de la tâche. La tâche est considérée comme
     * terminée lorsque l’objet est détruit.
     */
    Task start(const std::string&, Step);

private:
    // Flux de sortie utilisé pour l’affichage du suivi de progression
    std::ostream* _output = nullptr;

    // Délai minimum qui doit s’écouler entre chaque rafraîchissement
    // d’une barre de progression
    std::chrono::milliseconds _update_delay;

    // Heure du dernier rafraîchissement
    std::chrono::steady_clock::time_point _last_update;

    // Largeur totale de la barre de progression dans le terminal
    unsigned int _progress_width = 80;
};

class Progress::Task
{
public:
    /**
     * Démarre une nouvelle tâche.
     *
     * @param parent Suiveur de progression parent.
     * @param title Titre de la tâche.
     * @param number_steps Nombre d’étapes de progression prévues.
     */
    Task(Progress&, const std::string&, Step);

    /**
     * Termine la tâche.
     */
    ~Task();

    /**
     * Fait progresser la tâche du nombre donné d’étapes.
     *
     * @param [increment=1] Nombre d’étapes réalisées.
     */
    void advance(Step = 1);

private:
    /**
     * Met à jour l’affichage du suiveur.
     *
     * @param [status=Progressing] État du suiveur.
     */
    void _refresh();

    // Suiveur de progression parent
    Progress& _parent;

    // Nombre d’étapes à effectuer
    Step _total_steps;

    // Nombre d’étapes réalisées jusqu’à présent
    Step _performed_steps = 0;
};

#endif // UTILS_PROGRESS_HPP
