template<typename Type, int Rows, int Cols>
void nlohmann::adl_serializer<cv::Matx<Type, Rows, Cols>>::from_json(
    const nlohmann::json& object,
    cv::Matx<Type, Rows, Cols>& matrix
)
{
    if (object.size() != Rows)
    {
        throw std::invalid_argument(
            "la matrice donnée a " + std::to_string(object.size())
            + " lignes au lieu de " + std::to_string(Rows));
    }

    for (std::size_t i = 0; i < object.size(); ++i)
    {
        if (object.at(i).size() != Cols)
        {
            throw std::invalid_argument(
                "la matrice donnée a " + std::to_string(object.at(i).size())
                + " colonnes au lieu de " + std::to_string(Cols));
        }

        if (object.at(i).type() == nlohmann::json::value_t::array)
        {
            for (std::size_t j = 0; j < object.at(i).size(); ++j)
            {
                object.at(i).at(j).get_to(matrix(i, j));
            }
        }
        else
        {
            object.at(i).get_to(matrix(i, 0));
        }
    }
}

template<typename Type, int Rows, int Cols>
void nlohmann::adl_serializer<cv::Matx<Type, Rows, Cols>>::to_json(
    nlohmann::json& object,
    const cv::Matx<Type, Rows, Cols>& matrix
)
{
    nlohmann::json result(nlohmann::json::value_t::array);

    for (std::size_t i = 0; i < Rows; ++i)
    {
        if (Cols == 1)
        {
            result.push_back(matrix(i, 0));
        }
        else
        {
            nlohmann::json row(nlohmann::json::value_t::array);

            for (std::size_t j = 0; j < Cols; ++j)
            {
                row.push_back(matrix(i, j));
            }

            result.push_back(row);
        }
    }

    object = result;
}

template<typename Type, int Rows>
void nlohmann::adl_serializer<cv::Vec<Type, Rows>>::from_json(
    const nlohmann::json& object,
    cv::Vec<Type, Rows>& vector
)
{
    adl_serializer<cv::Matx<Type, Rows, 1>>::from_json(object, vector);
}

template<typename Type, int Rows>
void nlohmann::adl_serializer<cv::Vec<Type, Rows>>::to_json(
    nlohmann::json& object,
    const cv::Vec<Type, Rows>& vector
)
{
    adl_serializer<cv::Matx<Type, Rows, 1>>::to_json(object, vector);
}

template<typename Type>
void nlohmann::adl_serializer<cv::Point_<Type>>::from_json(
    const nlohmann::json& object,
    cv::Point_<Type>& point
)
{
    object.at("x").get_to(point.x);
    object.at("y").get_to(point.y);
}

template<typename Type>
void nlohmann::adl_serializer<cv::Point_<Type>>::to_json(
    nlohmann::json& object,
    const cv::Point_<Type>& point
)
{
    object = {
        {"x", point.x},
        {"y", point.y},
    };
}

template<typename Type>
void nlohmann::adl_serializer<cv::Point3_<Type>>::from_json(
    const nlohmann::json& object,
    cv::Point3_<Type>& point
)
{
    object.at("x").get_to(point.x);
    object.at("y").get_to(point.y);
    object.at("z").get_to(point.z);
}

template<typename Type>
void nlohmann::adl_serializer<cv::Point3_<Type>>::to_json(
    nlohmann::json& object,
    const cv::Point3_<Type>& point
)
{
    object = {
        {"x", point.x},
        {"y", point.y},
        {"z", point.z},
    };
}

template<typename T> 
bool operator< (const cv::Point3_<T> lVal, const cv::Point3_<T> rVal) {
    if (lVal.x < rVal.x)
        if (lVal.y < rVal.y)
            if (lVal.z < rVal.z)
                return true;
    return false;
}

template<typename T> 
bool operator> (const cv::Point3_<T> lVal, const cv::Point3_<T> rVal) {
    return rVal < lVal;
}

template<typename T> 
bool operator<=(const cv::Point3_<T> lVal, const cv::Point3_<T> rVal) {
    return !(lVal > rVal);
}

template<typename T> 
bool operator>=(const cv::Point3_<T> lVal, const cv::Point3_<T> rVal) {
    return !(lVal < rVal);
}

template<typename T> 
cv::Point3_<T>& operator+ (cv::Point3_<T>&lVal, const cv::Point3_<T>&rVal) {
    lVal.x += rVal.x;
    lVal.y += rVal.y;
    lVal.z += rVal.z;
    return lVal;
}

template<typename T> 
cv::Point3_<T>& operator/ (cv::Point3_<T>&lVal, const double scalar) {
    T converted_scalar = (T)scalar;
    // Divise les coordonnées par le scalaire donné
    lVal.x = lVal.x / converted_scalar;
    lVal.y = lVal.y / converted_scalar;
    lVal.z = lVal.z / converted_scalar;
    return lVal;
}

template<typename T> inline 
cv::Vec<T,3> coordVector(const cv::Point3_<T>& lVal, const cv::Point3_<T>& rVal) {
    return cv::Vec<T,3>(rVal.x - lVal.x, rVal.y - lVal.y, rVal.z - lVal.z);
}

template<typename T> inline 
cv::Vec<T,3> normalCoordVector(const cv::Point3_<T>& lVal, const cv::Point3_<T>& rVal) {
    return normaliseCoordVector(cv::Vec<T,3>(rVal.x - lVal.x, rVal.y - lVal.y, rVal.z - lVal.z));
}

template<typename T>
cv::Vec<T,3> normalizeCoordVector(const cv::Vec<T,3>& vec) {
    T length = sqrt(pow(vec[0],2) + pow(vec[1],2) + pow(vec[2],2));
    return cv::Vec<T,3>((vec[0])/length, (vec[1])/length, (vec[2])/length);
}