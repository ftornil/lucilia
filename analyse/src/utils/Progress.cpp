#include "Progress.hpp"
#include <sstream>
#include <type_traits>

Progress::Progress(
    std::ostream& output,
    std::chrono::milliseconds update_delay
)
: _output(&output)
, _update_delay(update_delay)
, _last_update(std::chrono::steady_clock::now())
{
}

Progress::Task Progress::start(const std::string& title, Step total_steps)
{
    return {*this, title, total_steps};
}

Progress::Task::Task(
    Progress& parent,
    const std::string& title,
    Step total_steps
)
: _parent{parent}
, _total_steps{total_steps}
{
    if (_parent._output != nullptr)
    {
        *(_parent._output) << title << '\n';
        _refresh();
    }
}

Progress::Task::~Task()
{
    if (_parent._output != nullptr)
    {
        _refresh();
        *(_parent._output) << std::endl;
    }
}

void Progress::Task::advance(Step increment)
{
    if (_parent._output != nullptr && _performed_steps < _total_steps)
    {
        _performed_steps += increment;

        if (_performed_steps > _total_steps)
        {
            _performed_steps = _total_steps;
        }

        if (
            _parent._last_update + _parent._update_delay
            < std::chrono::steady_clock::now()
        )
        {
            _refresh();
        }
    }
}

namespace
{
/**
 * Détermine le nombre de chiffres d’un entier positif en base 10.
 *
 * @param number Entier positif.
 * @return Nombre de chiffres.
 */
template<typename T>
unsigned int numberOfDigits(T number)
{
    static_assert(
        std::is_unsigned<T>::value,
        "Le nombre ne doit pas être signé"
    );

    unsigned int result = 1;

    while (number >= 10)
    {
        number /= 10;
        ++result;
    }

    return result;
}
}

void Progress::Task::_refresh()
{
    // Format :
    // XX/XX [######         ] NNN %

    std::ostringstream output;

    unsigned int max_step_width = numberOfDigits(_total_steps);
    unsigned int inner_width = _parent._progress_width
        - 2 * max_step_width // nombre d’étapes
        - 1 // séparateur du nombre d’étapes
        - 4 // crochets et espaces de la barre de progression
        - 5 // pourcentage
        ;

    output << '\r';
    output.width(max_step_width);
    output << _performed_steps << '/' << _total_steps;

    output << " [";

    Step blocks = std::min(
        _total_steps == 0
            ? static_cast<Step>(inner_width)
            : (inner_width * _performed_steps) / _total_steps,
        static_cast<Step>(inner_width)
    );

    for (Step i = 0; i < blocks; ++i)
    {
        output << '#';
    }

    for (Step i = 0; i < inner_width - blocks; ++i)
    {
        output << ' ';
    }

    output << "] ";

    if (_performed_steps < _total_steps)
    {
        output.width(3);
        output << std::min(
            _total_steps == 0
                ? Step{100}
                : (100 * _performed_steps) / _total_steps,
            Step{100}
        ) << " %";
    }
    else
    {
        output << " OK !";
    }

    *(_parent._output) << output.str();
    _parent._output->flush();
    _parent._last_update = std::chrono::steady_clock::now();
}
