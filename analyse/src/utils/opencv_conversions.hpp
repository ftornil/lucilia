#ifndef UTILS_OPENCV_CONVERSIONS_HPP
#define UTILS_OPENCV_CONVERSIONS_HPP

#include <nlohmann/json.hpp>
#include <opencv2/core.hpp>

namespace nlohmann
{
    template<typename Type, int Rows, int Cols>
    struct adl_serializer<cv::Matx<Type, Rows, Cols>>
    {
        /**
         * Convertit un objet JSON en matrice.
         *
         * @param object Objet JSON source.
         * @param matrix Matrice à remplir.
         * @throws std::invalid_argument Si un problème d’interprétation
         * intervient.
         */
        static void from_json(const nlohmann::json&, cv::Matx<Type, Rows, Cols>&);

        /**
         * Convertit une matrice en objet JSON.
         *
         * @param object Objet JSON à remplir.
         * @param matrix Matrice source.
         */
        static void to_json(nlohmann::json&, const cv::Matx<Type, Rows, Cols>&);
    };

    template<typename Type, int Rows>
    struct adl_serializer<cv::Vec<Type, Rows>>
    {
        /**
         * Convertit un objet JSON en vecteur.
         *
         * @param object Objet JSON source.
         * @param vector Vecteur à remplir.
         * @throws std::invalid_argument Si un problème d’interprétation
         * intervient.
         */
        static void from_json(const nlohmann::json&, cv::Vec<Type, Rows>&);

        /**
         * Convertit un vecteur en objet JSON.
         *
         * @param object Objet JSON à remplir.
         * @param vector Vecteur source.
         */
        static void to_json(nlohmann::json&, const cv::Vec<Type, Rows>&);
    };

    template<typename Type>
    struct adl_serializer<cv::Point_<Type>>
    {
        /**
         * Convertit un objet JSON en point 2D.
         *
         * @param object Objet JSON source.
         * @param point Point 2D à remplir.
         * @throws std::invalid_argument Si un problème d’interprétation
         * intervient.
         */
        static void from_json(const nlohmann::json&, cv::Point_<Type>&);

        /**
         * Convertit un point 2D en objet JSON.
         *
         * @param object Objet JSON à remplir.
         * @param point Point 2D source.
         */
        static void to_json(nlohmann::json&, const cv::Point_<Type>&);
    };

    template<typename Type>
    struct adl_serializer<cv::Point3_<Type>>
    {
        /**
         * Convertit un objet JSON en point 3D.
         *
         * @param object Objet JSON source.
         * @param point Point 3D à remplir.
         * @throws std::invalid_argument Si un problème d’interprétation
         * intervient.
         */
        static void from_json(const nlohmann::json&, cv::Point3_<Type>&);

        /**
         * Convertit un point 3D en objet JSON.
         *
         * @param object Objet JSON à remplir.
         * @param point Point 3D source.
         */
        static void to_json(nlohmann::json&, const cv::Point3_<Type>&);
    };
}


// Opérateurs de comparaison
template<typename T> bool operator< (const cv::Point3_<T> lVal, const cv::Point3_<T> rVal);
template<typename T> bool operator> (const cv::Point3_<T> lVal, const cv::Point3_<T> rVal);
template<typename T> bool operator<=(const cv::Point3_<T> lVal, const cv::Point3_<T> rVal);
template<typename T> bool operator>=(const cv::Point3_<T> lVal, const cv::Point3_<T> rVal);

// Opérateurs opérations mathématiques
/**
 * Ajoute les coordonnées de 2 points ensembles.
 * 
 * @param lVal point "origine" (auquel on ajoute les coordonnées)
 * @param rVal point "scalaire" (duquel on prends les coordonnées)
 * @returns référence au point contenant les nouvelles coordonnées
 */
template<typename T> cv::Point3_<T>& operator+ (cv::Point3_<T>&lVal, const cv::Point3_<T>&rVal);

/**
 * Divise les coordonnées d'un point par un scalaire donné.
 * 
 * @param lVal point à modifier
 * @param scalar scalaire par lequel diviser les coordonnées du point donné
 * @return référence au point contenant les nouvelles coordonnées
 */
template<typename T> cv::Point3_<T>& operator/ (cv::Point3_<T>&lVal, const double scalar);

/**
 * Crée un vecteur entre les 2 points lVal et rVal.
 * 
 * @param lVal opérande gauche
 * @param rVal opérande droit
 * @return référence à un vecteur entre lVal et rVal
 */
template<typename T> inline cv::Vec<T,3> coordVector(const cv::Point3_<T>& lVal, const cv::Point3_<T>& rVal);

/**
 * Crée un vecteur NORMALISÉ entre deux points lVal et rVal
 * 
 * @param lVal opérande gauche
 * @param rVal opérande droit
 * @return référence à un vecteur entre lVal et rVal de norme 1
 */
template<typename T> inline cv::Vec<T,3> normalCoordVector(const cv::Point3_<T>& lVal, const cv::Point3_<T>& rVal);

/**
 * Normalise un vecteur donné en argument.
 * 
 * @param vec vecteur à normaliser
 * @returns une référence au vecteur normalisé
 */
template<typename T> cv::Vec<T,3> normalizeCoordVector(const cv::Vec<T,3>& vec);

#include "opencv_conversions.tpp"

#endif // UTILS_OPENCV_CONVERSIONS_HPP
