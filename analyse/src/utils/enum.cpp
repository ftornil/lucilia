#include "enum.hpp"

std::vector<std::string> detail::create_map_from_enum(std::string options)
{
    // Efface les éventuels espaces
    options.erase(
        std::remove(options.begin(), options.end(), ' '),
        options.end()
    );

    // Découpe la liste des valeurs autour des virgules
    std::vector<std::string> items;

    std::string item;
    std::stringstream stream(options);

    while (std::getline(stream, item, ','))
    {
        items.push_back(item);
    }

    return items;
}
