#ifndef UTILS_UTILS_HPP
#define UTILS_UTILS_HPP

#include <boost/filesystem/path.hpp>
#include <vector>

/**
 * Explore une liste de répertoires et fichiers pour en extraire une
 * liste de simples fichiers. Les répertoires présents dans la liste
 * sont parcourus non-récursivement pour récupérer les chemins de fichiers
 * qu’ils contiennent, dans l’ordre lexicographique.
 *
 * @param list Liste de chemins à explorer.
 * @return Liste de chemins résultante.
 */
std::vector<boost::filesystem::path>
explorePaths(const std::vector<boost::filesystem::path>& list);

#endif // UTILS_UTILS_HPP
