#include "Options.hpp"
#include "utils/utils.hpp"
#include "utils/opencv_conversions.hpp"
#include <boost/program_options.hpp>
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

Options::Options()
: _console_options{"Options pour la ligne de commande ", 80, 55}
, _common_options{"", 80, 55}
{
    // clang-format off
    _console_options.add_options()
        ("help,h", "Affiche ce message d’aide.")
        ("config,C",
         po::value<fs::path>()
            ->value_name("PATH"),
         "Fichier de configuration définissant des valeurs pour les options "
         "ci-dessous.")
    ;

    po::options_description general{"Options de contrôle général "};

    general.add_options()
        ("general.number-threads,j",
         po::value(&this->general.number_threads)
            ->value_name("NUMBER")
            ->default_value(1),
         "Nombre de tâches parallèles à utiliser au maximum pour les "
         "différents calculs. La valeur 1 désactive le parallélisme.")
    ;
    _common_options.add(general);

    po::options_description calibration{
        "Options d’étalonnage de l’appareil photographique "
    };

    calibration.add_options()
        ("calibration.input",
         po::value(&this->calibration.input)
            ->value_name("PATH"),
         "Fichier depuis lequel des informations d’étalonnage pré-existantes "
         "pour l’appareil sont lues.")

        ("calibration.source,c",
         po::value<std::vector<fs::path>>()
            ->composing()
            ->value_name("PATH")
            ->notifier([this](auto& sources) {
                this->calibration.source = explorePaths(sources);

                if (this->calibration.source.empty())
                {
                    throw std::runtime_error{
                        "Aucune image source trouvée pour l’étalonnage. "
                        "Vérifiez le chemin passé au paramètre "
                        "« calibration.source »"
                    };
                }
            }),
         "Répertoire source pour l’étalonnage automatique de l’appareil "
         "à partir d’images. Chaque image doit contenir l’intégralité de "
         "l’échiquier d’étalonnage sur une surface plane.")

        ("calibration.chessboard-width,W",
         po::value(&this->calibration.chessboard_width)
            ->value_name("NUMBER")
            ->default_value(9),
         "Nombre d’intersections horizontales dans l’échiquier d’étalonnage.")

        ("calibration.chessboard-height,H",
         po::value(&this->calibration.chessboard_height)
            ->value_name("NUMBER")
            ->default_value(6),
         "Nombre d’intersections verticales dans l’échiquier d’étalonnage.")

        ("calibration.output",
         po::value(&this->calibration.output)
            ->value_name("PATH"),
         "Fichier dans lequel les résultats de l’étalonnage automatique "
         "sont écrits.")
    ;
    _common_options.add(calibration);

    po::options_description detection{
        "Options de détection des points caractéristiques "
    };

    detection.add_options()
        ("detection.input",
         po::value(&this->detection.input)
            ->value_name("PATH"),
         "Fichier depuis lequel des points caractéristiques des images "
         "déjà renseignés sont lus.")

        ("detection.source,I",
         po::value<std::vector<fs::path>>()
            ->composing()
            ->value_name("PATH")
            ->notifier([this](auto& sources) {
                this->detection.source = explorePaths(sources);

                if (this->detection.source.empty())
                {
                    throw std::runtime_error{
                        "Aucune image source trouvée pour la reconstruction. "
                        "Vérifiez le chemin passé au paramètre "
                        "« detection.source »"
                    };
                }
            }),
         "Répertoire contenant les images source pour la reconstruction.")

        ("detection.algorithm,D",
         po::value(&this->detection.algorithm)
            ->value_name("ALGORITHM")
            ->default_value(Detection::Algorithm::SURF),
         "Algorithme utilisé pour la détection et la description des points "
         "caractéristiques des images. Valeurs possibles : SIFT, SURF.")

        ("detection.output",
         po::value(&this->detection.output)
            ->value_name("PATH"),
         "Fichier dans lequel les points caractéristiques détectés "
         "dans les images sont écrits.")
    ;
    _common_options.add(detection);

    po::options_description sift{"Options de l’algorithme SIFT "};

    sift.add_options()
        ("sift.layers-per-octave",
         po::value(&this->detection.sift.layers_per_octave)
            ->value_name("VALUE")
            ->default_value(3),
         "Nombre de couches par octave.")

        ("sift.minimum-contrast",
         po::value(&this->detection.sift.minimum_contrast)
            ->value_name("VALUE")
            ->default_value(0.04, "0.04"),
         "Seuil de contraste d’un point caractéristique pour que celui-ci "
         "soit conservé. Plus ce seuil est élevé, plus la quantité de points "
         "détectés diminue.")

        ("sift.edge-threshold",
         po::value(&this->detection.sift.edge_threshold)
            ->value_name("VALUE")
            ->default_value(10, "10"),
         "Seuil au delà duquel un point est considéré comme une bordure et "
         "donc éliminé. Plus ce seuil est élevé, plus la quantité de points "
         "détectés augmente.")

        ("sift.sigma",
         po::value(&this->detection.sift.sigma)
            ->value_name("VALUE")
            ->default_value(1.6, "1.6"),
         "Valeur de l’écart-type (sigma) du flou gaussien appliqué sur "
         "l’image à chaque couche.")
    ;
    _common_options.add(sift);

    po::options_description surf{"Options de l’algorithme SURF "};

    surf.add_options()
        ("surf.minimum-hessian",
         po::value(&this->detection.surf.minimum_hessian)
            ->value_name("VALUE")
            ->default_value(400, "400"),
         "Hessien minimal pour qu’un point caractéristique soit conservé.")

        ("surf.number-octaves",
         po::value(&this->detection.surf.number_octaves)
            ->value_name("VALUE")
            ->default_value(4, "4"),
         "Nombre d’octaves utilisées.")

        ("surf.layers-per-octave",
         po::value(&this->detection.surf.layers_per_octave)
            ->value_name("VALUE")
            ->default_value(3, "3"),
         "Nombre de couches par octave.")
    ;
    _common_options.add(surf);

    po::options_description matching{
        "Options d’association des points caractéristiques "
    };

    matching.add_options()
        ("matching.input",
         po::value(&this->matching.input)
            ->value_name("PATH"),
         "Fichier depuis lequel des informations d’association des points "
         "caractéristiqeues pré-existantes sont lues.")

        ("matching.algorithm,M",
         po::value(&this->matching.algorithm)
            ->value_name("ALGORITHM")
            ->default_value(Matching::Algorithm::None),
         "Algorithme utilisé pour l’association automatique des points "
         "caractéristiques. Valeurs possibles : None, Tracking, "
         "QuickMatch.")

        ("matching.output",
         po::value(&this->matching.output)
            ->value_name("PATH"),
         "Fichier dans lequel le résultat de l’association automatique des "
         "points caractéristiques est écrit.")

        ("matching.visualize",
         po::value(&this->matching.visualize)
            ->value_name("PATH"),
         "Génère un fichier SVG permettant de visualiser les associations "
         "de points caractéristiques calculées.")
    ;
    _common_options.add(matching);

    po::options_description tracking{
        "Options de l’algorithme Tracking "
    };

    tracking.add_options()
        ("tracking.match-ratio",
         po::value(&this->matching.tracking.match_ratio)
            ->value_name("VALUE")
            ->default_value(0.3, "0.3"),
         "Ratio maximum entre la dissemblance d’une paire de points et "
         "la distinction d’un des deux points de la paire pour que "
         "ceux-ci soient considérés comme homologues.")
    ;
    _common_options.add(tracking);

    po::options_description quick_match{
        "Options de l’algorithme QuickMatch "
    };

    quick_match.add_options()
        ("quick-match.density-ratio",
         po::value(&this->matching.quick_match.density_ratio)
            ->value_name("VALUE")
            ->default_value(0.25, "0.25"),
         "Proportion de la distinction d’un descripteur donnant la fenêtre "
         "utilisée pour estimer sa densité. La fenêtre de l’estimation de "
         "densité est un paramètre qui régit le lissage de l’estimation "
         "résultante. Plus la fenêtre est élevée, plus l’estimation est "
         "lissée et perd en détail. Dans l’algorithme QuickMatch, la fenêtre "
         "est exprimée comme une proportion de la distinction du descripteur, "
         "correspondant à la distance à son plus proche voisin dans l’espace "
         "des descripteurs.")

        ("quick-match.edge-ratio",
         po::value(&this->matching.quick_match.edge_ratio)
            ->value_name("RATIO")
            ->default_value(0.7, "0.7"),
         "Définit le plafond de la distance d’une arête relativement à "
         "la distinction des deux groupes qu’elle sépare pour que celle-ci "
         "soit contractée et que les deux groupes soient fusionnés. "
         "Un taux d’acceptation proche de zéro fragmente davantage les "
         "groupes alors qu’un taux d’acceptation proche de un augmente le "
         "risque de faux positifs.")
    ;
    _common_options.add(quick_match);

    po::options_description reconstruction{
        "Options de reconstruction de la scène "
    };

    reconstruction.add_options()
        ("reconstruction.output,o",
         po::value(&this->reconstruction.output)
            ->value_name("PATH"),
         "Fichier dans lequel le résultat de la reconstruction de la scène "
         "est écrit.")

        ("reconstruction.input",
	 po::value(&this->reconstruction.input)
	     ->value_name("PATH"),
	 "Fichier depuis lequel charger une scène reconstruite.")

        ("reconstruction.sample-confidence",
         po::value(&this->reconstruction.sample_confidence)
            ->value_name("VALUE")
            ->default_value(0.999, "0.999"),
         "Confiance attendue dans la transformation projective obtenue "
         "entre deux images consécutives par échantillonnage aléatoire "
         "(RANSAC) des correspondances.")

        ("reconstruction.epipolar-distance",
         po::value(&this->reconstruction.epipolar_distance)
            ->value_name("VALUE")
            ->default_value(1, "1"),
         "Distance maximum en pixels entre un point et la droite épipolaire "
         "pour que le point soit pris en compte dans le calcul de la "
         "transformation projective entre deux images consécutives.")

        ("reconstruction.reprojection-limit",
         po::value(&this->reconstruction.reprojection_limit)
            ->value_name("DISTANCE")
            ->default_value(15, "15"),
         "Limite supérieure sur la distance entre la reprojection d’un "
         "point reconstruit et sa position originelle dans chaque capture "
         "pour que ce point soit conservé.")

        ("reconstruction.angle-threshold",
         po::value(&this->reconstruction.angle_threshold)
            ->value_name("ANGLE")
            ->default_value(.01 * M_PI, "0,01π"),
         "Seuil sur l’angle en radians entre les rayons des captures depuis "
         "lesquelles un point est vu pour que ce point soit conservé.")
    ;
    _common_options.add(reconstruction);

    po::options_description surface_reconstruction (
        "Options de reconstruction de surface."
    );

    surface_reconstruction.add_options()
        (
            "surface_reconstruction.output,O",
            po::value(&this->surface_reconstruction.output_model_path)
            ->value_name("PATH")
            ->default_value("outputSurface.ply", "outputSurface.ply"),
            "Fichier de sortie du maillage final, si il existe."
        )

        (
            "surface_reconstruction.align_model",
            po::value(&this->surface_reconstruction.align_model)
            ->value_name("BOOLEAN")
            ->default_value(true, "True"),
            "Essaie d'aligner la surface acquise sur un hyperplan de "
            "dimension 2 afin de mieux estimer la courbure de l'objet "
            "réel."
        )

        (
            "surface_reconstruction.outliers_threshold",
            po::value(&this->surface_reconstruction.outliers_threshold)
            ->value_name("VALUE")
            ->default_value(0.20, "0.20"),
            "Seul de points marginaux considérés trop important pour le "
            "filtrage de ceux ci."
        )

        (
            "surface_reconstruction.remove_outliers",
            po::value(&this->surface_reconstruction.remove_outliers)
            ->value_name("BOOLEAN")
            ->default_value(true, "True"),
            "Autorise le filtrage des points considérés comme marginaux "
            "par estimation de voisinage moyen."
        )

        (
            "surface_reconstruction.scale_iterations",
            po::value(&this->surface_reconstruction.scale_iterations)
            ->value_name("SCALE_ITERATIONS")
	    ->default_value(4, ""),
	    "Détermine le nombre de dérivations dans l'espace d'échelle "
	    "la fonction ScaleSpaceReconstruction va effectuer. Doit être "
	    "supérieur ou égal à 1"
        )

        (
            "surface_reconstruction.reconstruction_method,M",
            po::value(&this->surface_reconstruction.reconstruction_method)
            ->value_name("SURFACE_RECONSTRUCTION_METHOD")
            ->default_value(SurfaceReconstruction::ReconstructionMethod::Poisson),
            "La méthode à utiliser pour tenter de reconstruire la surface "
            "acquise. Par défaut, l'algorithme va d'abord essayer de faire "
            "une surface de Poisson, puis va se réorienter vers une méthode "
            "dite de Scale-Space si il n'y arrive pas."
        )
    ;
    _common_options.add(surface_reconstruction);
    // clang-format on
}

void Options::usage(std::ostream& out)
{
    out << "Utilisation : ./lucilia-analyse [OPTION...]\n";
    out << "Suite d’outils permettant l’analyse d’images prises en "
           "séquence dans une scène\npour en recréer un modèle 3D.\n\n";
    out << _console_options;
    out << _common_options;
}

namespace
{
/**
 * Rend absolu un chemin éventuellement passé en option du programme,
 * depuis un chemin racine donné.
 *
 * @param values Dictionnaire de valeurs passées au programme.
 * @param key Clé de l’option dont le chemin valeur doit être rendu absolu.
 * @param root Chemin racine.
 */
void makeAbsolutePath(
    po::variables_map& values, const std::string& key, const fs::path& root)
{
    if (values.count(key) == 1)
    {
        auto& value = values.at(key).as<fs::path>();

        if (value != "-")
        {
            value = fs::absolute(value, root);
        }
    }
}

/**
 * Rend absolu un tableau de chemins éventuellement passé en option du
 * programme, depuis un chemin racine donné.
 *
 * @param values Dictionnaire de valeurs passées au programme.
 * @param key Clé de l’option dont le chemin valeur doit être rendu absolu.
 * @param root Chemin racine.
 */
void makeAbsolutePathArray(
    po::variables_map& values, const std::string& key, const fs::path& root)
{
    if (values.count(key) == 1)
    {
        for (auto& value : values.at(key).as<std::vector<fs::path>>())
        {
            value = fs::absolute(value, root);
        }
    }
}
}

bool Options::parse(int argc, const char* argv[])
{
    try
    {
        // Lecture des options passées en argument
        po::options_description all_options;
        all_options.add(_console_options);
        all_options.add(_common_options);

        po::variables_map values;
        po::store(po::parse_command_line(argc, argv, all_options), values);

        if (values.count("help") == 1)
        {
            this->help = true;
            return true;
        }

        // Transformation des chemins relatifs passés en ligne de commande en
        // chemins absolus par rapport au répertoire d’exécution courant
        const auto cwd = fs::current_path();

        if (values.count("config") == 1)
        {
            auto config_path
                = fs::absolute(values["config"].as<fs::path>(), cwd);
            auto config_dir = config_path.parent_path();

            std::ifstream config_file{config_path.string().c_str()};

            if (!config_file.is_open())
            {
                std::cerr
                    << "Erreur d’ouverture du fichier de configuration à "
                       "l’emplacement " << config_path << " : "
                    << std::strerror(errno) << ".\n";
                return false;
            }

            po::store(
                po::parse_config_file(config_file, _common_options),
                values
            );

            // Transformation des chemins relatifs passés dans le fichier de
            // configuration en chemins absolus par rapport au fichier de config
            makeAbsolutePath(values, "calibration.input", config_dir);
            makeAbsolutePathArray(values, "calibration.source", config_dir);
            makeAbsolutePath(values, "calibration.output", config_dir);
            makeAbsolutePath(values, "detection.input", config_dir);
            makeAbsolutePathArray(values, "detection.source", config_dir);
            makeAbsolutePath(values, "detection.output", config_dir);
            makeAbsolutePath(values, "matching.input", config_dir);
            makeAbsolutePath(values, "matching.output", config_dir);
            makeAbsolutePath(values, "reconstruction.output", config_dir);
        }

        po::notify(values);
    }
    catch (const po::error& err)
    {
        this->usage(std::cerr);
        std::cerr << "\nSyntaxe des options invalide : " << err.what() << ".\n";
        return false;
    }
    catch (const std::runtime_error& err)
    {
        std::cerr << err.what() << ".\n";
        return false;
    }

    return true;
}
