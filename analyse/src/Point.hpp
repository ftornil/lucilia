#ifndef POINT_HPP
#define POINT_HPP

#include "Capture.hpp"
#include "Matching.hpp"
#include <nlohmann/json.hpp>
#include <opencv2/core/matx.hpp>
#include <opencv2/core/types.hpp>
#include <ostream>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

/**
 * Point de la scène correspondant à un ensemble de points caractéristiques
 * repérés dans plusieurs captures.
 */
class Point
{
public:
    using Position = cv::Point3d;
    using Color = cv::Vec3b;

    /**
     * Construit un point à l’origine.
     */
    Point() = default;

    /**
     * Construit un point.
     *
     * @param position Position dans l’espace.
     * @param error Erreur de positionnement du point.
     * @param color Couleur dominante du point au format BVR.
     */
    Point(
        Position,
        double,
        Color
    );

    /**
     * Construit un point.
     *
     * @param x Abscisse du point dans l’espace.
     * @param y Ordonnée du point dans l’espace.
     * @param z Cote du point dans l’espace.
     * @param error Erreur de positionnement du point.
     * @param apparitions Indices des captures dans lequel ce point apparaît
     * @param normale Normale en ce point, calculée avec les captures depuis `Point::fromCapture`
     */
    Point(double, double, double, double, std::vector<std::size_t>, cv::Vec3d);

    /**
     * Reconstruit un point de la scène à partir d’un groupe de points
     * caractéristiques correspondants dans un ensemble d’images.
     *
     * @param captures Liste des captures.
     * @param cluster Groupe de correspondances.
     * @param reprojection_limit Limite supérieure sur la distance entre
     * la reprojection d’un point reconstruit et sa position originelle
     * dans chaque capture pour que ce point soit conservé.
     * @param angle_threshold Angle minimum que doivent former au moins
     * l’une des paires de captures pour que la reconstruction soit valide.
     * Dans le cas contraire, un point vide est renvoyé.
     * @return Point reconstruit.
     */
    static std::optional<Point> fromMatches(
        const std::vector<Capture>&,
        const Matching::Cluster&,
        double,
        double
    );

    /**
     * Récupère la position du point dans la scène.
     *
     * @return Position dans l’espace.
     */
    const Position& position() const;

    /**
     * Récupère l’erreur de positionnement du point.
     *
     * @return Erreur de positionnement.
     */
    double error() const;

    /**
     * Récupère la couleur dominante du point au format BVR.
     *
     * @return Couleur dominante du point.
     */
    const Color& color() const;

    /**
     * Permets de récupérer la normale en ce point.
     * 
     * @return La normale de ce point, calculé depuis `Point::fromMatches()`
     */
    cv::Vec3d normal() const;

    /**
     * Convertit un point en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param point Point source.
     */
    friend void to_json(nlohmann::json&, const Point&);

    /**
     * Charge un point depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param point Point à remplir.
     */
    friend void from_json(const nlohmann::json&, Point&);
    
    /**
     * Opérateur de conversion en point 3D pour
     * la librarie CGAL
     */
    operator CGAL::Exact_predicates_inexact_constructions_kernel::Point_3() const;

private:
    // Position dans la scène
    Position _position;

    // Erreur de positionnement
    double _error;

    // Couleur dominante du point
    Color _color;
};

#endif // POINT_HPP
