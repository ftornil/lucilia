#include "Feature.hpp"
#include "Capture.hpp"
#include "utils/opencv_conversions.hpp"

Feature::Feature()
: _descriptor{Descriptor::all(0)}
, _position{0, 0}
, _color{Color::all(0)}
{
}

Feature::Feature(
    Descriptor descriptor,
    Position position,
    Color color
)
: _descriptor{std::move(descriptor)}
, _position{std::move(position)}
, _color{std::move(color)}
{
}

const Feature::Descriptor& Feature::descriptor() const
{
    return _descriptor;
}

const Feature::Position& Feature::position() const
{
    return _position;
}

const Feature::Color& Feature::color() const
{
    return _color;
}

void to_json(nlohmann::json& object, const Feature& feature)
{
    object = nlohmann::json{
        {"descriptor", feature._descriptor},
        {"position", feature._position},
        {"color", feature._color}
    };
}

void from_json(const nlohmann::json& object, Feature& feature)
{
    object.at("descriptor").get_to(feature._descriptor);
    object.at("position").get_to(feature._position);
    object.at("color").get_to(feature._color);
}
