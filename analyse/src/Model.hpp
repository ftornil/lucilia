#ifndef MODEL_HPP
#define MODEL_HPP

#include "Options.hpp"
#include "Feature.hpp"
#include "Point.hpp"
#include "Scene.hpp"
#include "utils/enum.hpp"
#include <climits>

#include <boost/tuple/tuple.hpp>

/**
 * To read PLY points
 */
#include <CGAL/IO/read_ply_points.h>

/**
 * For average spacing
 */
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/compute_average_spacing.h>

/**
 * For scale estimation
 */
#include <CGAL/estimate_scale.h>
#include <CGAL/jet_smooth_point_set.h>
#include <CGAL/grid_simplify_point_set.h>

/**
 * For outlier removal :
 */
#include <CGAL/remove_outliers.h>
#include <CGAL/property_map.h>

/**
 * for normal estimation and orientation :
 */
#include <CGAL/jet_estimate_normals.h>
#include <CGAL/mst_orient_normals.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef Kernel::FT FT;
typedef Kernel::Point_3 KernelPoint3;
typedef Kernel::Vector_3 KernelVector3;

typedef boost::tuple<KernelPoint3, KernelVector3, unsigned char, unsigned char, unsigned char> pointColorTuple; 

using namespace cgalEnums;

/**
 * Modele 3D resultant de la scene capturee.
 */
class Model {
public:
	/**
	 * Construit un modèle 3d vide. Sans points, ni facettes.
	 */
	Model() = default;

	/**
	 * Model, loading a PLY file.
	 */
	Model(const std::string&);

	/**
	 * Crée un modèle 3d contenant tous les points reconnus par
	 * l'analyse en amont du flux d'images effectué dans `Scene`
	 * 
	 * Attention, ne construit pas le mesh. Ce constructeur 
	 * instancie juste le modèle. Il ne fait aucun calcul.
	 */
	Model(std::vector<Point>);

	/**
	 * Crée un modèle 3d utilisant les informations contenues
	 * dans la scène, contenant les infos obtenues a partir
	 * d'un flux d'images calculées en amont.
	 * 
	 * Attention, ne construit pas le mesh. Ce constructeur
	 * instancie juste le modèle. Il ne fait aucun calcul.
	 */
	Model(Scene);

	/**
	 * Calcule l'espacement moyen entre les points du nuage,
	 * selon un paramètre donné
	 */
	void computeAverageSpacing(unsigned int k = 6);

	/**
	 * Automagiquement calcule l'echelle du modèle
	 */
	void estimateScale(const char* pathOut);

	/**
	 * Enlève les points marginaux, grâce à l'espacement
	 * calculé dans la fonction `computeAverageSpacing()`
	 */
	void removeOutliers(const char* pathOut, unsigned int k = 6);

	/**
	 * automagiquement estime les normales aux points
	 */
	void estimateNormals(const char* pathOut);

	/**
	 * Converts the model to PLY for SOME FUCKING REASON
	 */
	void toPLY(const std::string);

	/**
	 * Construit une représentation surfacique utilisant la technique
	 * des surfaces de Poisson.
	 */
	void PoissonReconstruction(const char*);

	/**
	 * reconstructs a surface
	 */
	void scaleSpaceReconstruction(const char*, unsigned int = 4);

	/**
	 * Attempts to reconstruct a model, trying the Poisson 
	 * method first, and then if it fails, reverting to the 
	 * Scale Space Surface reconstruction method
	 */
	void reconstruct(const char*);

private:
	/**
	 * Boost tuple containing a point, and it's associated color
	 * (taken from the captures it was detected in)
	 */
	std::vector<pointColorTuple> _colored_points;

	/**
	 * Espacement moyen entre tous les points du modèle, tel que déterminé
	 * par les fonctions implémentées dans la librairie CGAL.
	 */
	CGAL::Epick::FT average_spacing;

	/**
	 * Taille estimée du k-voisinnage du nuage de points
	 */
	std::size_t global_scale;
};

/**
 * Interpréteur de fichier PLY pour les fichiers extraits de OpenSfM.
 */
class openSfMPLYInterpreter {
public:	std::vector<pointColorTuple> points;

public:
	openSfMPLYInterpreter(std::vector<pointColorTuple>& p) : points(p) {};

	bool is_applicable(CGAL::Ply_reader& reader) {
		return reader.does_tag_exist<FT>("x") &&
		reader.does_tag_exist<FT>("y") &&
		reader.does_tag_exist<FT>("z") &&
		reader.does_tag_exist<FT>("nx") &&
		reader.does_tag_exist<FT>("ny") &&
		reader.does_tag_exist<FT>("nz") &&
		reader.does_tag_exist<unsigned char>("diffuse_red") &&
		reader.does_tag_exist<unsigned char>("diffuse_green") &&
		reader.does_tag_exist<unsigned char>("diffuse_blue") && 
		reader.does_tag_exist<unsigned char>("class") &&
		reader.does_tag_exist<unsigned char>("detection") ;
	}

	void process_line(CGAL::Ply_reader& reader) {
		FT x = (FT)0.;
		FT y = (FT)0.;
		FT z = (FT)0.;
		FT nx = (FT)0.;
		FT ny = (FT)0.;
		FT nz = (FT)0.;
		unsigned char diffuse_red = 0;
		unsigned char diffuse_green = 0;
		unsigned char diffuse_blue = 0;

		reader.assign(x, "x");
		reader.assign(y, "y");
		reader.assign(z, "z");
		reader.assign(nx, "nx");
		reader.assign(ny, "ny");
		reader.assign(nz, "nz");
		reader.assign(diffuse_red, "diffuse_red");
		reader.assign(diffuse_green, "diffuse_green");
		reader.assign(diffuse_blue, "diffuse_blue");

		points.push_back(pointColorTuple(
			KernelPoint3(x,y,z), 
			KernelVector3(nx,ny,nz),
			(unsigned char)diffuse_red, 
			(unsigned char)diffuse_green, 
			(unsigned char)diffuse_blue)
		);
	}
};

#endif
