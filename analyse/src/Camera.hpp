#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "utils/Progress.hpp"
#include <boost/filesystem/path.hpp>
#include <nlohmann/json.hpp>
#include <opencv2/core/matx.hpp>
#include <optional>
#include <vector>

/**
 * Appareil photographique.
 */
class Camera
{
public:
    using Intrinsics = cv::Matx33d;
    using Distortion = cv::Vec<double, 5>;

    /**
     * Crée un appareil photographique avec les paramètres par défaut.
     */
    Camera();

    /**
     * Crée un appareil photographique.
     *
     * @param intrinsics Paramètres intrinsèques de l’appareil.
     * @param distorsion Coefficients de distorsion de l’appareil.
     */
    Camera(Intrinsics, Distortion);

    /**
     * Crée un appareil photographique étalonné automatiquement.
     *
     * À partir d’un ensemble de prises de vues d’un échiquier par un même
     * appareil photographique, cette procédure calcule automatiquement les
     * matrices de paramètres intrinsèques et de distorsion de cet appareil.
     *
     * L’échiquier visualisé doit être de mêmes dimensions sur chaque image.
     * Les dimensions de l’échiquier sont exprimées en nombre d’intersections.
     * Par exemple, l’échiquier suivant est de dimensions 3 × 3 :
     *
     * ```
     * +--+--+--+--+
     * |  |xx|  |xx|
     * +--+--+--+--+
     * |xx|  |xx|  |
     * +--+--+--+--+
     * |  |xx|  |xx|
     * +--+--+--+--+
     * |xx|  |xx|  |
     * +--+--+--+--+
     * ```
     *
     * Si le mode de débogage est activé, chaque image sera affichée
     * successivement dans une fenêtre. Les points de l’échiquier détectés
     * seront dessinés par dessus l’image, permettant ainsi de vérifier
     * manuellement la validité de la détection.
     *
     * @param paths Liste des chemins des images à utiliser pour l’étalonnage.
     * @param width Largeur de l’échiquier (intersections horizontales).
     * @param height Hauteur de l’échiquier (intersections verticales).
     * @param progress Suiveur de progression pour afficher un suivi
     * du calcul.
     * @return Appareil photographique étalonné et erreur moyenne de
     * reprojection des points suite à l’étalonnage (en pixels). L’erreur
     * doit être au plus de l’ordre de un pixel pour qu’un étalonnage soit
     * de bonne qualité.
     *
     * @throws std::invalid_argument Si toutes les images passées en argument
     * sont inexploitables.
     */
    static std::pair<Camera, double> calibrate(
        const std::vector<boost::filesystem::path>&,
        unsigned int,
        unsigned int,
        Progress&
    );

    /**
     * Récupère la matrice de paramètres intrinsèques de l’appareil.
     *
     * La matrice de paramètres intrinsèques est une matrice triangulaire
     * supérieure de dimensions 3 × 3 qui encode la transformation projective
     * que fait subir l’appareil photo aux points de la scène :
     *
     * ```
     * [[ λf   s   x_0]
     *  [  0   f   y_0]
     *  [  0   0    1 ]]
     * ```
     *
     * * $f$ est la longueur focale ;
     * * $λ$ est le ratio vertical/horizontal ;
     * * $(x_0, y_0)$ est la position du point principal ;
     * * $s$ est le coefficient de cisaillement.
     *
     * @return Matrice de paramètres intrinsèques de l’appareil.
     */
    const Intrinsics& intrinsics() const;

    /**
     * Récupère les coefficients de distorsion de l’appareil.
     *
     * @return Vecteur de coefficients de distorsion de l’appareil.
     */
    const Distortion& distortion() const;

    /**
     * Convertit un appareil photo en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param camera Appareil photo source.
     */
    friend void to_json(nlohmann::json&, const Camera&);

    /**
     * Charge un appareil photo depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param camera Appareil photo à remplir.
     */
    friend void from_json(const nlohmann::json&, Camera&);

private:
    // Paramètres intrinsèques de l’appareil
    Intrinsics _intrinsics;

    // Coefficients de distorsion de l’appareil
    Distortion _distortion;
};

#endif // CAMERA_HPP
