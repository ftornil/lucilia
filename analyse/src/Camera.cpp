#include "Camera.hpp"
#include "utils/opencv_conversions.hpp"
#include <algorithm>
#include <boost/filesystem/path.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <stdexcept>
#include <string>

Camera::Camera()
: _intrinsics(Intrinsics::eye())
, _distortion(Distortion::all(0))
{
}

Camera::Camera(
    Intrinsics intrinsics,
    Distortion distortion
)
: _intrinsics(std::move(intrinsics))
, _distortion(std::move(distortion))
{
}

namespace
{
/**
 * Détecte les points d’intersection d’un échiquier de dimensions fixées
 * dans une image.
 *
 * @param path Chemin vers l’image.
 * @param board_width Nombre d’intersections horizontales dans l’échiquier
 * recherché.
 * @param board_height Nombre d’intersections verticales.
 * @return Liste des points d’intersection trouvés.
 */
std::vector<cv::Point2f> detectChessboard(
    const boost::filesystem::path& path,
    int board_width,
    int board_height
)
{
    cv::Mat image = cv::imread(path.string(), cv::IMREAD_GRAYSCALE);

    if (image.data == nullptr)
    {
        throw std::runtime_error{
            "image d’étalonnage invalide "
            "(chemin : " + path.string() + ")"
        };
    }

    cv::Size size{board_width, board_height};
    std::vector<cv::Point2f> corners;

    cv::findChessboardCorners(image, size, corners);
    return corners;
}
}

std::pair<Camera, double> Camera::calibrate(
    const std::vector<boost::filesystem::path>& paths,
    unsigned int width,
    unsigned int height,
    Progress& progress
)
{
    auto task = progress.start(
        "Étalonnage automatique de l’appareil photographique",
        paths.size() + 1
    );

    // Génération des points 3D de l’échiquier
    std::vector<cv::Point3f> board_points_template;
    board_points_template.reserve(width * height);

    for (unsigned int j = 0; j < height; ++j)
    {
        for (unsigned int i = 0; i < width; ++i)
        {
            board_points_template.push_back(cv::Point3f{
                static_cast<float>(i),
                static_cast<float>(j),
                0.0f
            });
        }
    }

    std::vector<std::vector<cv::Point3f>> boards_points{
        paths.size(),
        board_points_template
    };

    // Détection des coins de l’échiquier dans chaque image
    std::vector<std::vector<cv::Point2f>> images_points;
    images_points.reserve(paths.size());

    std::transform(
        begin(paths),
        end(paths),
        std::back_inserter(images_points),
        [&task, &width, &height](const auto& path) {
            auto result = detectChessboard(path, width, height);
            task.advance();
            return result;
        }
    );

    // Suppression des images dans lesquelles on n’a pas tous les
    // points de l’échiquier
    auto images_it = std::begin(images_points);
    auto boards_it = std::begin(boards_points);

    while (images_it != std::end(images_points)
           && boards_it != std::end(boards_points))
    {
        if (images_it->size() != boards_it->size())
        {
            images_it = images_points.erase(images_it);
            boards_it = boards_points.erase(boards_it);
        }
        else
        {
            ++images_it;
            ++boards_it;
        }
    }

    if (boards_points.empty())
    {
        throw std::invalid_argument{
            "aucune image d’étalonnage exploitable identifiée"
        };
    }

    // Étalonnage sur l’ensemble des images
    Intrinsics intrinsics = Intrinsics::eye();
    Distortion distortion = Distortion::all(0);

    cv::Mat first_image = cv::imread(paths[0].string(), cv::IMREAD_GRAYSCALE);
    cv::Size image_size{first_image.cols, first_image.rows};

    double error = cv::calibrateCamera(
        boards_points,
        images_points,
        image_size,
        intrinsics,
        distortion,
        cv::noArray(),
        cv::noArray(),
        false
    );

    task.advance();
    return {Camera{intrinsics, distortion}, error};
}

const Camera::Intrinsics& Camera::intrinsics() const
{
    return _intrinsics;
}

const Camera::Distortion& Camera::distortion() const
{
    return _distortion;
}

void to_json(nlohmann::json& object, const Camera& camera)
{
    object = nlohmann::json{
        {"intrinsics", camera._intrinsics},
        {"distortion", camera._distortion},
    };
}

void from_json(const nlohmann::json& object, Camera& camera)
{
    object.at("intrinsics").get_to(camera._intrinsics);
    object.at("distortion").get_to(camera._distortion);
}
