#include "Camera.hpp"
#include "Feature.hpp"
#include "Scene.hpp"
#include "Options.hpp"
#include "Model.hpp"
#include "utils/Progress.hpp"
#include <fstream>
#include <functional>
#include <fcntl.h>
#include <iostream>
#include <nlohmann/json.hpp>
#include <omp.h>
#include <opencv2/core/utils/logger.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <optional>
#include <string>

/**
 * {{TESTS}}
 * 
 * Macro permettant de sauter l'étape de reconstruction
 * de scène à partir d'images, et passant directement à
 * la phase de calcul de modèle 3D (en chargeant une scene
 * depuis un fichier JSON en entrée)
 */
#define MODEL_DEBUGGING

/**
 * {{TESTS}}
 * 
 * Macro permettant de savoir si on souhaite charger un
 * fichier JSON sortant du pipeline de LUCILIA, ou bien
 * un fichier OFF/OBJ contenant des points.
 */
// #define GET_POINTS_FROM_JSON

#ifndef GET_POINTS_FROM_JSON
    #define GET_POINTS_FROM_OFF
#else
    // #define GET_JSON_FROM_LUCILIA
    #define GET_JSON_FROM_OPENSFM
#endif
namespace cvlog = cv::utils::logging;

/**
 * Fonction de gestion des erreurs OpenCV.
 */
int opencv_error_handler(int, const char*, const char*, const char*, int, void*)
{
    // Ne rien faire. Ça ne devrait pas être trop compliqué.
    return 0;
}

int main(int argc, const char* argv[])
{
    // Désactivation des messages d’OpenCV
    cv::redirectError(opencv_error_handler);
    cvlog::setLogLevel(cvlog::LogLevel::LOG_LEVEL_SILENT);

    // Lecture des options passées au programme
    Options opts;

    if (!opts.parse(argc, argv))
    {
        return EXIT_FAILURE;
    }

    if (opts.help)
    {
        opts.usage(std::cout);
        return EXIT_SUCCESS;
    }

    // Paramétrage de OpenMP avec le nombre de tâches demandées
    auto num_threads = opts.general.number_threads;
    omp_set_num_threads(num_threads);

    if (num_threads > 1)
    {
        std::cerr << "Calcul avec " << num_threads << " tâches parallèles.\n";
    }

    // Création du suiveur de progression
    Progress progress{std::cerr};

    // Étalonnage de l’appareil
    Camera taken_by;

    if (!opts.calibration.input.empty())
    {
        // Chargement des informations d’étalonnage depuis un fichier
        std::ifstream input{opts.calibration.input.string().c_str()};
        taken_by = nlohmann::json::parse(input).get<Camera>();
    }
    else if (!opts.calibration.source.empty())
    {
        try
        {
            // Étalonnage automatique
            auto [result, error] = Camera::calibrate(
                opts.calibration.source,
                opts.calibration.chessboard_width,
                opts.calibration.chessboard_height,
                progress
            );

            if (error >= 1.1)
            {
                std::cerr
                    << "\nAvertissement : erreur d’étalonnage trop élevée ("
                    << error << " pixels en moyenne). Pour un étalonnage de "
                    "bonne qualité, l’erreur doit être au plus de l’ordre de "
                    "un pixel.\n";
            }

            taken_by = std::move(result);
        }
        catch (const std::exception& err)
        {
            std::cerr
                << "\nErreur lors de l’étalonnage automatique : "
                << err.what() << "\n";
            return EXIT_FAILURE;
        }

        // Écriture des informations d’étalonnage dans un fichier
        if (!opts.calibration.output.empty())
        {
            std::ofstream output{opts.calibration.output.string().c_str()};
            output << static_cast<nlohmann::json>(taken_by);
        }
    }
    else
    {
        // Rien à faire : affiche l’aide
        opts.usage(std::cout);
        return EXIT_FAILURE;
    }

    // Détection des points
    std::vector<Image> images;

    if (!opts.detection.input.empty())
    {
        // Chargement des points détectés depuis un fichier
        std::ifstream input{opts.detection.input.string().c_str()};
        images = nlohmann::json::parse(input).get<std::vector<Image>>();
    }
    else if (!opts.detection.source.empty())
    {
        auto options = opts.detection;
        auto paths = options.source;
        images.reserve(paths.size());

        // Création du détecteur avec les paramètres demandés
        cv::Ptr<cv::Feature2D> detector;

        switch (options.algorithm)
        {
        case Options::Detection::Algorithm::SIFT:
            detector = cv::xfeatures2d::SIFT::create(
                0, // Aucune limite du nombre de points détectés
                options.sift.layers_per_octave,
                options.sift.minimum_contrast,
                options.sift.edge_threshold,
                options.sift.sigma
            );
            break;

        case Options::Detection::Algorithm::SURF:
            detector = cv::xfeatures2d::SURF::create(
                options.surf.minimum_hessian,
                options.surf.number_octaves,
                options.surf.layers_per_octave,
                true, // Descripteurs à 128 valeurs et non 64
                false // Normalise les descripteurs sur leur rotation
            );
            break;

        default:
            std::cerr
                << "\nAlgorithme de détection inconnu : "
                << std::to_string(static_cast<int>(options.algorithm)) << '\n';
            return EXIT_FAILURE;
        }

        auto task = progress.start(
            "Chargement des images et détection des points caractéristiques",
            paths.size()
        );

        for (const auto& path : paths)
        {
            images.emplace_back(Image::fromPath(
                path,
                taken_by,
                detector
            ));

            task.advance();
        }

        if (!opts.detection.output.empty())
        {
            // Écriture des points détectés dans un fichier
            std::ofstream output{opts.detection.output.string().c_str()};
            output << static_cast<nlohmann::json>(images);
        }
    }
    else
    {
        // Rien de plus à faire
        return EXIT_SUCCESS;
    }

    // Association des points
    Matching matching;

    if (!opts.matching.input.empty())
    {
        // Chargement des associations depuis un fichier
        std::ifstream input{opts.matching.input.string().c_str()};
        matching = nlohmann::json::parse(input).get<Matching>();
    }
    else if (opts.matching.algorithm != Options::Matching::Algorithm::None)
    {
        try
        {
            matching = Matching::fromFeatures(
                images,
                opts.matching,
                progress
            );
        }
        catch (const std::exception& err)
        {
            std::cerr
                << "\nErreur lors de l’association des points : "
                << err.what() << '\n';
            return EXIT_FAILURE;
        }

        if (!opts.matching.output.empty())
        {
            // Écriture des associations dans un fichier
            std::ofstream output{opts.matching.output.string().c_str()};
            output << static_cast<nlohmann::json>(matching);
        }
    }
    else
    {
        // Rien de plus à faire
        return EXIT_SUCCESS;
    }

    if (!opts.matching.visualize.empty())
    {
        std::ofstream output{opts.matching.visualize.string().c_str()};
        matching.toSVG(images, output);
    }

    Scene scene;

    // Reconstruction de la scène
    if (!opts.reconstruction.input.empty()) 
    {
        std::ifstream input{opts.reconstruction.input};
        scene = nlohmann::json::parse(input).get<Scene>();
	std::cerr << "Entered loading bay" << std::endl;
    } 
    else if (!opts.reconstruction.output.empty())
    {

        try
        {
            scene = Scene::fromMotion(
                images,
                taken_by,
                matching,
                opts.reconstruction,
                progress
            );
        }
        catch (const std::exception& err)
        {
            std::cerr
                << "\nErreur lors de la reconstruction : "
                << err.what() << "\n";
            return EXIT_FAILURE;
        }

        std::ofstream output{opts.reconstruction.output.string().c_str()};
        output << static_cast<nlohmann::json>(scene);
    }

    if (!opts.surface_reconstruction.output_model_path.empty()) {
	    opts.surface_reconstruction.output_model_path = "model.ply";
    }

    // create model :
    Model surfaceToReconstruct(scene);

    surfaceToReconstruct.computeAverageSpacing();

    if (opts.surface_reconstruction.align_model == true) {
        surfaceToReconstruct.estimateScale(opts.surface_reconstruction.output_model_path.c_str());
    }

    if (opts.surface_reconstruction.remove_outliers == true) {
        surfaceToReconstruct.removeOutliers(opts.surface_reconstruction.output_model_path.c_str());
    }

    surfaceToReconstruct.estimateNormals(opts.surface_reconstruction.output_model_path.c_str());

    if (opts.surface_reconstruction.reconstruction_method == Options::SurfaceReconstruction::ReconstructionMethod::Poisson) {
        try {
            surfaceToReconstruct.PoissonReconstruction(opts.surface_reconstruction.output_model_path.c_str());
        } catch (...) {
            surfaceToReconstruct.scaleSpaceReconstruction(opts.surface_reconstruction.output_model_path.c_str());
        }
    } else if (opts.surface_reconstruction.reconstruction_method == Options::SurfaceReconstruction::ReconstructionMethod::ScaleSpace) {
        surfaceToReconstruct.scaleSpaceReconstruction(opts.surface_reconstruction.output_model_path.c_str());
    }

    return EXIT_SUCCESS;
}
