#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include "Camera.hpp"
#include "utils/enum.hpp"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <ostream>
#include <string>
#include <vector>

/**
 * Gère le passage d’options au programme.
 */
class Options
{
public:
    /**
     * Crée une instance de gestion des options passées.
     */
    Options();

    /**
     * Analyse les arguments passés au programme pour en extraire les
     * options correspondantes.
     *
     * @param argc Nombre d’arguments.
     * @param argv Valeur des arguments.
     * @return Vrai si et seulement si l’interprétation s’est terminée
     * sans problème et le programme peut être poursuivi normalement.
     */
    bool parse(int argc, const char* argv[]);

    /**
     * Affiche les instructions d’utilisation du programme sur
     * le flux de sortie donné.
     *
     * @param out Flot de sortie à utiliser.
     */
    void usage(std::ostream& out);

    /**
     * Vrai si l’affichage de l’aide est demandé.
     */
    bool help = false;

    /**
     * Groupe d’options de contrôle général.
     */
    struct General
    {
        /**
         * Nombre de tâches parallèles à utiliser pour le calcul.
         */
        unsigned int number_threads;
    } general;

    /**
     * Groupe d’options pour le paramétrage de l’appareil et l’étalonnage.
     */
    struct Calibration
    {
        /**
         * Charge les informations d’étalonnage depuis un fichier.
         */
        boost::filesystem::path input;

        /**
         * Écrit les informations d’étalonnage dans un fichier.
         */
        boost::filesystem::path output;

        /**
         * Liste des images pour l’étalonnage de l’appareil photo.
         */
        std::vector<boost::filesystem::path> source;

        /**
         * Largeur de l’échiquier d’étalonnage.
         */
        int chessboard_width;

        /**
         * Hauteur de l’échiquier d’étalonnage.
         */
        int chessboard_height;
    } calibration;

    /**
     * Groupe d’options pour la détection des points caractéristiques.
     */
    struct Detection
    {
        /**
         * Charge les points caractéristiques depuis un fichier.
         */
        boost::filesystem::path input;

        /**
         * Écrit les points détectés dans un fichier.
         */
        boost::filesystem::path output;

        /**
         * Liste des images depuis lesquelles la scène doit être reconstituée.
         */
        std::vector<boost::filesystem::path> source;

        /**
         * Paramètres de l’algorithme de détection SIFT.
         */
        struct SIFT
        {
            int layers_per_octave;
            double minimum_contrast;
            double edge_threshold;
            double sigma;
        } sift;

        /**
         * Paramètres de l’algorithme de détection SURF.
         */
        struct SURF
        {
            double minimum_hessian;
            int number_octaves;
            int layers_per_octave;
        } surf;

        /**
         * Algorithme de détection des points.
         */
        enum class Algorithm;
        Algorithm algorithm;
    } detection;

    /**
     * Groupe d’options pour l’association des points.
     */
    struct Matching
    {
        /**
         * Charge les associations de points depuis un fichier.
         */
        boost::filesystem::path input;

        /**
         * Écrit les associations de points dans un fichier.
         */
        boost::filesystem::path output;

        /**
         * Génère un fichier SVG permettant de visualiser les
         * associations de points.
         */
        boost::filesystem::path visualize;

        /**
         * Paramètres de l’algorithme d’association Tracking.
         */
        struct Tracking
        {
            /**
             * Ratio maximum entre la dissemblance d’une paire de points et
             * la distinction d’un des deux points de la paire pour que
             * ceux-ci soient considérés comme homologues.
             */
            double match_ratio;
        } tracking;

        /**
         * Paramètres de l’algorithme d’association QuickMatch.
         */
        struct QuickMatch
        {
            /**
             * Proportion de la distinction donnant la fenêtre utilisée pour
             * estimer la fonction de densité des descripteurs.
             */
            double density_ratio;

            /**
             * Taux d’acceptation des arêtes.
             */
            double edge_ratio;
        } quick_match;

        /**
         * Algorithmes d’association de points caractéristiques.
         */
        enum class Algorithm;
        Algorithm algorithm;
    } matching;

    /**
     * Groupe d’options pour la reconstruction.
     */
    struct Reconstruction
    {
        /**
         * Charge les informations de Scene calculées
         * auparavant depuis un fichier.
         */
        boost::filesystem::path input;

        /**
         * Écrit le résultat de la reconstruction dans un fichier.
         */
        boost::filesystem::path output;

        /**
         * Confiance demandée pour RANSAC.
         */
        double sample_confidence;

        /**
         * Distance maximum entre un point et la droite épipolaire.
         */
        double epipolar_distance;

        /**
         * Limite sur la distance de reprojection pour la reconstruction
         * de chaque point.
         */
        double reprojection_limit;

        /**
         * Seuil sur l’angle entre les rayons des captures depuis lesquelles
         * un point est vu pour que ce point soit conservé.
         */
        double angle_threshold;
    } reconstruction;

    struct SurfaceReconstruction 
    {
        /**
         * Permet de savoir si l'utilisateur souhaite aligner
         * le modèle avec un hyperplan estimé grâce aux méthodes
         * décrites dans `Model::estimateScale()`.
         */
        bool align_model;

        /**
         * Détermine si l'utilisateur souhaite enlever les points
         * marginaux ou non. Peut être utile pour les nuages de points
         * issus de captures "parfaites" où le pipeline a réussi à
         * reconstruire fidèlement la surface acquise auparavant.
         */
        bool remove_outliers;

        /**
         * Détermine le nombre de points à enlever pour un nuage de points.
         * La méthode de filtrage des points marginaux se base sur un calcul
         * de l'espacement moyen entre points. Si le nuage de points est très
         * uniformément réparti, alors une grande partie des points peuvent
         * être considérés comme "marginaux" alors qu'ils sont juste légèrement
         * plus espacés que les autres, laissant donc des trous béants dans 
         * les modèles.
         */
        double outliers_threshold;

        /**
         * Fichier de sortie du modèle reconstruit, si il existe. Il se peut que
         * les méthodes de reconstruction de surfaces échouent, au quel cas le
         * fichier de sortie ne sera pas créé.
         */
        boost::filesystem::path output_model_path;

        /**
         * Détermine le nombre d'itérations pour passer aux espaces de dimension
         * supérieures dans la méthode de reconstruction par scale-space.
         */
        unsigned int scale_iterations;

        /**
         * Permets à l'utilisateur de donner la méthode préférée de reconstruction
         * de surface. Possède aussi l'option de combiner les méthodes (pour avoir
         * une méthode de secours au cas où la[les] première[s] ne marche[nt] pas)
         * Pour l'instant, si une méthode ne fonctionne pas, il se rabbatra sur une
         * méthode de scale space
         */
        enum class ReconstructionMethod;
        ReconstructionMethod reconstruction_method;
    } surface_reconstruction;

private:
    // Description des options pour la ligne de commande
    boost::program_options::options_description _console_options;

    // Description des options générales
    boost::program_options::options_description _common_options;
};

DECLARE_ENUM(
    Options::Detection::Algorithm,
    SIFT,
    SURF,
)

DECLARE_ENUM(
    Options::Matching::Algorithm,
    None,
    Tracking,
    QuickMatch
)

DECLARE_ENUM(
    Options::SurfaceReconstruction::ReconstructionMethod,
    Poisson,
    ScaleSpace
)

#endif // OPTIONS_HPP
