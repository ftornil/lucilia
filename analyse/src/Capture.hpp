#ifndef CAPTURE_HPP
#define CAPTURE_HPP

#include "Camera.hpp"
#include "Image.hpp"
#include "Options.hpp"
#include <nlohmann/json.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/matx.hpp>
#include <opencv2/core/types.hpp>

/**
 * Image prise par un appareil photographique dans une scène.
 */
class Capture
{
public:
    using Translation = cv::Vec3d;
    using Rotation = cv::Matx33d;
    using Position = cv::Vec3d;
    using Direction = cv::Vec3d;

    /**
     * Crée une capture vide.
     */
    Capture();

    /**
     * Crée une capture prise à l’origine de la scène.
     *
     * @param image Image résultant de la capture.
     * @param taken_by Appareil photographique ayant capturé l’image.
     */
    Capture(
        Image,
        Camera
    );

    /**
     * Crée une nouvelle capture.
     *
     * @param image Image résultant de la capture.
     * @param taken_by Appareil photographique ayant capturé l’image.
     * @param translation Translation du référentiel absolu des points
     * vers le référentiel de la capture.
     * @param rotation Rotation du référentiel absolu des points de la scène
     * vers le référentiel de la capture.
     */
    Capture(
        Image,
        Camera,
        Translation,
        Rotation
    );

    /**
     * Reconstruit la pose (position du point de capture et orientation
     * de l’appareil photographique lors de la capture) d’une capture
     * relativement à la capture précédente et renvoie le résultat sous
     * forme d’une nouvelle capture.
     *
     * @param image Image résultant de la capture.
     * @param taken_by Appareil photographique ayant capturé l’image.
     * @param previous Précédente capture.
     * @param matches Paires de points caractéristiques correspondants
     * entre la capture précédente et la nouvelle capture, sous forme
     * de paires d’indices. Au moins 5 correspondances sont nécessaires
     * pour reconstruire la pose.
     * @param options Options pour la reconstruction.
     * @return Nouvelle capture reconstruite.
     */
    static Capture fromMatches(
        const Image&,
        const Camera&,
        const Capture&,
        const std::vector<std::pair<std::size_t, std::size_t>>&,
        const Options::Reconstruction&
    );

    /**
     * Récupère l’image résultant de la capture.
     *
     * @return Image résultant de la capture.
     */
    const Image& image() const;

    /**
     * Récupère l’appareil photographique ayant capturé la scène.
     *
     * @return Appareil photographique ayant capturé la scène.
     */
    const Camera& takenBy() const;

    /**
     * Récupère le vecteur de translation du référentiel absolu des points
     * de la scène vers le référentiel de la capture.
     *
     * @return Vecteur de translation.
     */
    const Translation& translation() const;

    /**
     * Récupère la matrice de rotation du référentiel absolu des points de
     * la scène vers le référentiel de la capture.
     *
     * @return Matrice de rotation.
     */
    const Rotation& rotation() const;

    /**
     * Récupère la matrice de paramètres extrinsèques de la capture, constituée
     * de la matrice de rotation et du vecteur de translation concaténés.
     *
     * ```
     * Extrinsèques = [Rotation | Translation]
     * ```
     *
     * @return Matrice de paramètres extrinsèques.
     */
    cv::Matx34d extrinsics() const;

    /**
     * Récupère la matrice de projection de la capture, mettant en relation les
     * coordonnées des points dans l’espace avec leurs coordonnées dans l’image.
     * Les coordonnées sont exprimées en coordonnées homogènes.
     *
     * ```
     * Projection = Intrinsèques × Extrinsèques
     * ```
     *
     * @return Matrice de projection.
     */
    cv::Matx34d projection() const;

    /**
     * Récupère la position du point de capture dans la scène.
     *
     * @return Point de capture dans la scène.
     */
    Position position() const;

    /**
     * Récupère la direction avant de la capture.
     *
     * @return Vecteur décrivant la direction avant de la capture.
     */
    Direction direction() const;

    /**
     * Déplace un point du référentiel absolu de la scène vers le
     * référentiel de la capture.
     *
     * @param point Point de la scène dans le référentiel absolu.
     * @return Point dans le référentiel de la capture.
     */
    cv::Point3d sceneToCapture(const cv::Point3d&) const;

    /**
     * Projette un point de la scène dans le référentiel de la capture
     * dans l’espace de l’image.
     *
     * @param point Point de la scène dans le référentiel de la capture.
     * @return Point dans l’espace de l’image.
     */
    cv::Point2d captureToImage(const cv::Point3d&) const;

    /**
     * Projette un point de la scène dans le référentiel absolu
     * dans l’espace de l’image.
     *
     * @param point Point de la scène dans le référentiel absolu.
     * @return Point dans l’espace de l’image.
     */
    cv::Point2d sceneToImage(const cv::Point3d&) const;

    /**
     * Convertit une capture en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param capture Capture source.
     */
    friend void to_json(nlohmann::json&, const Capture&);

    /**
     * Charge une capture depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param capture Capture à remplir.
     */
    friend void from_json(const nlohmann::json&, Capture&);

private:
    // Image résultant de la capture
    Image _image;

    // Appareil photographique utilisé pour la capture
    Camera _taken_by;

    // Vecteur de translation du référentiel
    Translation _translation;

    // Matrice de rotation du référentiel
    Rotation _rotation;
};

#endif // CAPTURE_HPP
