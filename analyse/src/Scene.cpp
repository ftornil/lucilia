#include "Scene.hpp"
#include "Feature.hpp"
#include "Matching.hpp"
#include "utils/Progress.hpp"
#include <algorithm>
#include <boost/filesystem/path.hpp>
#include <cmath>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

Scene::Scene(
    std::vector<Capture> capture,
    std::vector<Point> points
)
: _captures(std::move(capture))
, _points(std::move(points))
{
}

Scene Scene::fromMotion(
    const std::vector<Image>& images,
    const Camera& taken_by,
    const Matching& matching,
    const Options::Reconstruction& options,
    Progress& progress
)
{
    if (images.empty())
    {
        return Scene{};
    }

    std::vector<Capture> captures;
    captures.reserve(images.size());

    {
        auto poses_task = progress.start(
            "Reconstruction de la pose de chaque capture",
            images.size()
        );

        captures.emplace_back(
            images.front(),
            taken_by
        );

        poses_task.advance();

        for (std::size_t i = 1; i < images.size(); ++i)
        {
            captures.emplace_back(Capture::fromMatches(
                images[i],
                taken_by,
                captures[i - 1],
                matching.matchesBetween(i - 1, i),
                options
            ));

            poses_task.advance();
        }
    }

    std::vector<Point> points;
    points.reserve(matching.clusters().size());

    {
        auto points_task = progress.start(
            "Reconstruction de la position de chaque point",
            matching.clusters().size()
        );

        for (const Matching::Cluster& cluster : matching.clusters())
        {
            std::optional<Point> result = Point::fromMatches(
                captures,
                cluster,
                options.reprojection_limit,
                options.angle_threshold
            );

            if (result.has_value())
            {
                points.emplace_back(std::move(*result));
            }

            points_task.advance();
        }
    }

    return Scene{captures, points};
}

const std::vector<Capture>& Scene::captures() const
{
    return _captures;
}

const std::vector<Point>& Scene::points() const
{
    return _points;
}

void to_json(nlohmann::json& object, const Scene& scene)
{
    object = nlohmann::json{
        {"captures", scene._captures},
        {"points", scene._points},
    };
}

void from_json(const nlohmann::json& object, Scene& scene)
{
    object.at("captures").get_to(scene._captures);
    object.at("points").get_to(scene._points);
}
