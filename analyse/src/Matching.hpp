#ifndef MATCHING_HPP
#define MATCHING_HPP

#include "Camera.hpp"
#include "Feature.hpp"
#include "Image.hpp"
#include "Options.hpp"
#include <map>
#include <vector>

/**
 * Ensemble $M = {C_i}$ de groupes de points caractéristiques tel que chaque
 * groupe représente un point de la scène correspondant à tous les points à
 * l’intérieur du groupe. Cet ensemble respecte les conditions de cohérence
 * suivantes :
 *
 * - les groupes $C_i$ sont disjoints ;
 * - chaque groupe $C_i$ contient au plus un point caractéristique par image ;
 * - tous les points d’un groupe $C_i$ se correspondent deux à deux.
 */
class Matching
{
public:
    using ImageId = std::size_t;
    using FeatureId = std::size_t;
    using ClusterId = std::size_t;

    /**
     * Groupe de points caractéristiques similaires, représenté sous forme
     * d’une association d’image vers point caractéristique.
     */
    using Cluster = std::map<ImageId, FeatureId>;

    /**
     * Crée une association vide de points caractéristiques.
     */
    Matching() = default;

    /**
     * Crée une association à partir d’un ensemble de groupes de points
     * caractéristiques cohérent.
     *
     * Note : aucune vérification de cohérence n’est effectuée pour des
     * raisons de performance. Il appartient à l’appelant de s’en assurer
     * si besoin. Les comportements ne sont pas garantis si l’ensemble
     * de groupes n’est pas cohérent.
     *
     * @param clusters Ensemble de groupes de points caractéristiques.
     */
    Matching(std::vector<Cluster>);

    /**
     * Associe automatiquement les points caractéristiques similaires.
     *
     * Détecte, parmi un ensemble d’images, des points correspondant en
     * réalité au même point physique de la scène. L’algorithme peut à cet
     * effet se baser par exemple sur les vecteurs descripteurs du voisinage
     * de chaque point caractéristique des images, qui lui sont fournis en
     * entrée.
     *
     * @param images Liste d’images.
     * @param options Options de l’algorithme d’association à utiliser.
     * @param progress Suiveur de progression pour afficher un suivi du calcul.
     * @return Association reconstruite.
     */
    static Matching fromFeatures(
        const std::vector<Image>&,
        const Options::Matching&,
        Progress&
    );

    /**
     * Récupère les groupes de points caractéristiques correspondants.
     *
     * @return Ensemble de groupes de points caractéristiques.
     */
    const std::vector<Cluster>& clusters() const;

    /**
     * Récupère les correspondances disponibles entre deux images données.
     *
     * @param image1 Indice de la première image.
     * @param image2 Indice de la seconde image.
     * @return Liste de paires de points caractéristiques qui se correspondent
     * deux à deux.
     */
    std::vector<std::pair<FeatureId, FeatureId>> matchesBetween(
        ImageId,
        ImageId
    ) const;

    /**
     * Écrit un fichier SVG permettant de visualiser les correspondances
     * de cet ensemble.
     *
     * @param images Ensemble d’images dont les correspondances sont
     * issues.
     * @param out Flux de sortie pour le fichier SVG
     */
    void toSVG(const std::vector<Image>&, std::ostream&) const;

    /**
     * Convertit un ensemble de groupes en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param matching Ensemble de groupes source.
     */
    friend void to_json(nlohmann::json&, const Matching&);

    /**
     * Charge un ensemble de groupes depuis objet JSON.
     *
     * @param object Objet JSON source.
     * @param matching Ensemble de groupes à remplir.
     */
    friend void from_json(const nlohmann::json&, Matching&);

private:
    /**
     * Recrée l’association entre chaque image et les groupes de
     * points caractéristiques auquels elle appartient.
     */
    void _createClustersByImage();

    // Groupes de points caractéristiques
    std::vector<Cluster> _clusters;

    // Association de chaque image vers les groupes de points
    // caractéristiques auquels elle appartient
    std::multimap<ImageId, ClusterId> _clusters_by_image;
};

#endif // MATCHING_HPP
