#include "Model.hpp"
#include "Point.hpp"
#include "utils/opencv_conversions.hpp"

#include <unordered_map>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <chrono>

#include <CGAL/Point_set_3.h>
#include <CGAL/Point_set_3/IO.h>

#include <CGAL/Lapack_svd.h>

/**
 * For Poisson surface reconstruction
 */
#include <CGAL/property_map.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/make_surface_mesh.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/Point_with_normal_3.h>
#include <CGAL/OpenNL/linear_solver.h>
#include <CGAL/OpenNL/sparse_matrix.h>
#include <CGAL/compute_average_spacing.h>
#include <CGAL/Poisson_reconstruction_function.h>
#include <CGAL/Polygon_mesh_processing/distance.h>
#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/IO/output_surface_facets_to_polyhedron.h>

/**
 * For scale space surface reconstruction
 */
#include <CGAL/Triangulation_3.h>
#include <CGAL/Regular_triangulation_3.h>
#include <CGAL/Scale_space_surface_reconstruction_3.h>
// #include <CGAL/Scale_space_reconstruction_3/Jet_smoother.h>
// #include <CGAL/Scale_space_reconstruction_3/Alpha_shape_mesher.h>
// #include <CGAL/Scale_space_reconstruction_3/Weighted_PCA_smoother.h>
// #include <CGAL/Scale_space_reconstruction_3/Advancing_front_mesher.h>

#ifdef CGAL_LINKED_WITH_TBB
typedef CGAL::Parallel_tag ConcurrencyTag;
#else
typedef CGAL::Sequential_tag ConcurrencyTag;
#endif

Model::Model(std::vector<Point> points) {
	for (int i = 0; i < points.size(); ++i) {
		this->_colored_points.push_back( 
			pointColorTuple( (KernelPoint3)(points.at(i)), KernelVector3(0.0,0.0,0.0), 0, 0, 0 )
		);
	}
}

Model::Model(const std::string& pathIn) {
	this->_colored_points = std::vector<pointColorTuple>();
	openSfMPLYInterpreter plyInterpreter(this->_colored_points);
	std::ifstream in(pathIn);
	if ( !in.good() ) {
		std::cerr << "Model : stream could not be read properly" << std::endl;
		exit(EXIT_FAILURE);
	}
	// if ( !CGAL::read_ply_custom_points(in, plyInterpreter, CGAL::Epick()) ) {
	// 	std::cerr << "Could not read files" << std::endl;
	// 	exit(EXIT_FAILURE);
	// }
	if ( !CGAL::read_ply_custom_points<openSfMPLYInterpreter, CGAL::Epick>(in, plyInterpreter, CGAL::Epick()) ) {
		exit(EXIT_FAILURE);
	}
	this->_colored_points = plyInterpreter.points;
	std::cout << "Model read from file " << pathIn << ". " << this->_colored_points.size() << " points read." << std::endl;
}

Model::Model(Scene computedScene) {
	std::vector<Point> m = computedScene.points();
	for(int i = 0; i < m.size(); ++i) {
		this->_colored_points.push_back(
			pointColorTuple( (KernelPoint3)(m.at(i)),  KernelVector3(0.0,0.0,0.0), 0, 0, 0 )
		);
	}
}

void Model::computeAverageSpacing(unsigned int k) {
	auto start = std::chrono::high_resolution_clock::now();
	this->average_spacing = CGAL::compute_average_spacing<ConcurrencyTag>(
		this->_colored_points.begin(), this->_colored_points.end(),
		CGAL::Nth_of_tuple_property_map<0,pointColorTuple>(), k);
	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "Average point spacing : " << this->average_spacing << std::endl;
	std::chrono::duration<double> elapsedTime = end-start;
	std::cout << "\nModel::computeAverageSpacing( k = " << k << ") achieved in " << elapsedTime.count() << " seconds.\n" << std::endl; 
}

void Model::estimateScale(const char* pathOut) {
	std::cout << "Computing the global scale ... " << std::endl;
	auto start = std::chrono::high_resolution_clock::now();
	this->global_scale = CGAL::estimate_global_k_neighbor_scale(
		this->_colored_points.begin(), this->_colored_points.end(),
		CGAL::Nth_of_tuple_property_map<0, pointColorTuple>()
	);
	auto middle = std::chrono::high_resolution_clock::now();
	std::cout << "Computed kGlobalScale : " << this->global_scale << std::endl;
	std::cout << "Starting a jet surface fitting : " << std::endl;
	std::cout.flush();
	CGAL::jet_smooth_point_set<
		ConcurrencyTag
#ifndef CGAL_EIGEN3_ENABLED
		, 
		std::vector<pointColorTuple>::iterator, 
		CGAL::Nth_of_tuple_property_map<0,pointColorTuple>, 
		Kernel, 
		CGAL::Epick
#endif
		>(
			this->_colored_points.begin(),
			this->_colored_points.end(), 
			CGAL::Nth_of_tuple_property_map<0, pointColorTuple>(),
			static_cast<unsigned int>(this->global_scale), 
			CGAL::Epick()
		);
	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "finished." << std::endl;
	
	std::chrono::duration<double> elapsedTime1 = middle-start;
	std::chrono::duration<double> elapsedTime2 = end-middle;
	std::cout << "\nModel::estimateScale() : \n\tScale estimation achieved in " << elapsedTime1.count() 
	<< " seconds.\n\tJet surface fitting done in " << elapsedTime2.count() << " seconds.\n" << std::endl; 
	
	std::string newpath{pathOut};
	newpath += "_01_estimated_scale.ply";
	this->toPLY(newpath);
}

void Model::removeOutliers(const char* pathOut, unsigned int k) {
	std::cout << "Removing outliers ..." << std::endl;
	
	auto start = std::chrono::high_resolution_clock::now();
	auto first_to_remove = CGAL::remove_outliers(
		this->_colored_points.begin(), 
		this->_colored_points.end(), 
		CGAL::Nth_of_tuple_property_map<0,pointColorTuple>(), 
		k, 
		100.0f, 
		2.0 * this->average_spacing
	);
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime = end-start;
	std::cout << "\nModel::removeOutliers( k = " << k << ") achieved in " << elapsedTime.count() << " seconds.\n" << std::endl; 
	
	std::cout << ( 100.0f * std::distance(first_to_remove, this->_colored_points.end()) / (double)this->_colored_points.size() ) <<
	" percent of the points should be removed when using a distance of " << this->average_spacing << std::endl;

	std::cout << "Do you want to continue ? {Y/N}" << std::endl;
	std::string userInput("");
	while (userInput != "Y" && userInput != "N") {
		std::cin >> userInput;
		if (userInput != "Y" && userInput != "N") {
			std::cout << "Do you want to continue ? {Y/N}" << std::endl;
		}
	}

	if (userInput == "Y") {
		// enlève les points à supprimer
		this->_colored_points.erase(first_to_remove, this->_colored_points.end());

		std::cout << "Outliers removed." << std::endl;
		std::cout << "Recomputing average spacing ..." << std::endl;
		this->computeAverageSpacing();
	}
	std::string newpath(pathOut);
	newpath+="_02_outliers_removed.ply";

	this->toPLY(newpath);
	return;
}

void Model::estimateNormals(const char* pathOut) {
	// start by estimating normals :
	std::cout << "Starting to estimate normals" << std::endl;
	auto start = std::chrono::high_resolution_clock::now();
	CGAL::jet_estimate_normals<
		ConcurrencyTag,
		std::vector<pointColorTuple>::iterator,
		CGAL::Nth_of_tuple_property_map<0,pointColorTuple>,
		CGAL::Nth_of_tuple_property_map<1,pointColorTuple>
		>(
			this->_colored_points.begin(),
			this->_colored_points.end(), 
			CGAL::Nth_of_tuple_property_map<0,pointColorTuple>(),
			CGAL::Nth_of_tuple_property_map<1,pointColorTuple>(),
			this->global_scale
		);
	auto middlestart = std::chrono::high_resolution_clock::now();
	std::cout << "Finished" << std::endl;

	std::cout << "Re-orienting them ..." << std::endl;

	auto middleend = std::chrono::high_resolution_clock::now();
	auto first_unoriented_normal = CGAL::mst_orient_normals<
		std::vector<pointColorTuple>::iterator,
		CGAL::Nth_of_tuple_property_map<0,pointColorTuple>,
		CGAL::Nth_of_tuple_property_map<1,pointColorTuple>,
		CGAL::Lapack_svd>(
			this->_colored_points.begin(),
			this->_colored_points.end(),
			CGAL::Nth_of_tuple_property_map<0,pointColorTuple>(),
			CGAL::Nth_of_tuple_property_map<1,pointColorTuple>(),
			this->global_scale,
			CGAL::Lapack_svd()
		);
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime1 = middlestart-start;
	std::chrono::duration<double> elapsedTime2 = end-middleend;
	std::cout << "\nModel::estimateNormals() : \n\tNormal estimation achieved in " << elapsedTime1.count() 
	<< " seconds.\n\tNormal orientation done in " << elapsedTime2.count() << " seconds.\n" << std::endl; 
	if (first_unoriented_normal == this->_colored_points.end()) {
		std::cout << "All normals successfully oriented ! " << std::endl;
	} else {
		std::cout << std::distance(first_unoriented_normal, this->_colored_points.end()) << " normals not oriented" << std::endl;
	}

	std::cout << "Finished" << std::endl;

	std::string newpath(pathOut);
	newpath+="_03_normals_estimated_oriented.ply";

	this->toPLY(newpath);
}

void Model::toPLY(const std::string path_name) {
	std::ofstream out(path_name.c_str(), std::ofstream::out);
	if (! out.good() ) {
		std::cerr << "File could not be written" << std::endl;
		return;
	}
	out << "ply" << std::endl;
	out << "format ascii 1.0" << std::endl;
	out << "comment " << this->_colored_points.size() << " points up in this bitch" << std::endl;
	out << "element vertex " << this->_colored_points.size() << std::endl;
	out << "property double x" << std::endl;
	out << "property double y" << std::endl;
	out << "property double z" << std::endl;
	out << "property double nx" << std::endl;
	out << "property double ny" << std::endl;
	out << "property double nz" << std::endl;
	out << "property ushort red" << std::endl;
	out << "property ushort green" << std::endl;
	out << "property ushort blue" << std::endl;
	out << "end_header" << std::endl;
	for (auto _cp = this->_colored_points.begin(); _cp != this->_colored_points.end(); ++_cp) {
		out << (*_cp).get<0>().x() << " " << (*_cp).get<0>().y() << " " << (*_cp).get<0>().z() <<
		" " << (*_cp).get<1>().x() << " " << (*_cp).get<1>().y() << " " << (*_cp).get<1>().z() << 
		" " << (unsigned short)(*_cp).get<2>() << " " << (unsigned short)(*_cp).get<3>() << " " << (unsigned short)(*_cp).get<4>() << std::endl;
	}
	out << std::endl;
	out.close();
	return;
}

void Model::PoissonReconstruction(const char* path_name_out) {
	typedef Kernel::Sphere_3					KernelSphere3;
	typedef CGAL::Polyhedron_3<Kernel>				Polyhedron;
	typedef CGAL::Poisson_reconstruction_function<Kernel>		Poisson_reconstruction_fn;
	typedef CGAL::Surface_mesh_default_triangulation_3		STr;
	typedef CGAL::Surface_mesh_complex_2_in_triangulation_3<STr>	C2t3;
	typedef CGAL::Implicit_surface_3<Kernel, Poisson_reconstruction_fn> Surface_3;

	FT surface_mesh_angle = 20.0;		// Min triangle angle in DEGS
	FT surface_mesh_radius = 30;		// Min triangle size related to point set avg spacing
	FT surface_mesh_distance = 0.375;	// Surface approx error

	/**
	 * Create the implicit function from the points !
	 * Using default solver
	 */
	Poisson_reconstruction_fn function(this->_colored_points.begin(), this->_colored_points.end(),
	CGAL::Nth_of_tuple_property_map<0, pointColorTuple>(),
	CGAL::Nth_of_tuple_property_map<1, pointColorTuple>());

	if (! function.compute_implicit_function() ) {
		std::cerr << "could not compute implicit function" << std::endl;
		exit(EXIT_FAILURE);
	}

	KernelPoint3 inner_point = function.get_inner_point();
	KernelSphere3 bounding_sphere = function.bounding_sphere();
	FT radius = std::sqrt(bounding_sphere.squared_radius());

	FT surface_mesh_sphere_radius = 5.0*radius;
	FT surface_mesh_dichotomy_error = surface_mesh_distance*this->average_spacing/1000.0;
	Surface_3 surface(function,KernelSphere3(inner_point, surface_mesh_sphere_radius*surface_mesh_sphere_radius),surface_mesh_dichotomy_error/surface_mesh_sphere_radius);

	CGAL::Surface_mesh_default_criteria_3<STr> criteria(surface_mesh_angle,surface_mesh_radius*this->average_spacing, surface_mesh_distance*this->average_spacing);

	STr tr;
	C2t3 c2t3(tr);
	CGAL::make_surface_mesh(c2t3, surface, criteria, CGAL::Manifold_with_boundary_tag());

	if (tr.number_of_vertices() == 0) {
		std::cerr << "no vertices in triangulation" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::string newpath = std::string(path_name_out);
	newpath+="_04_reconstructed_Poisson.off";

	std::ofstream out(newpath);
	Polyhedron output_mesh;
	CGAL::output_surface_facets_to_polyhedron(c2t3, output_mesh);
	out << output_mesh;

	return;

}

void Model::scaleSpaceReconstruction(const char* pathout, unsigned int nbIter) {
	// Create a reconstructor :
	auto superstart = std::chrono::high_resolution_clock::now();
	CGAL::Scale_space_surface_reconstruction_3<CGAL::Epick> model_reconstructor(6, 12);
	for (auto p = this->_colored_points.begin(); p != this->_colored_points.end(); ++p) {
		model_reconstructor.insert((*p).get<0>());
	}
	auto superend = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> ell = superend - superstart;
	std::cout << "\nScale space surface insertion done in " << ell.count() << " seconds.\n" << std::endl;
	for (unsigned int i = 1; i <= nbIter; ++i) {
		// Reconstruction parameters
		auto start = std::chrono::high_resolution_clock::now();
		model_reconstructor.increase_scale(1);
		model_reconstructor.reconstruct_surface();
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsedTime = end-start;
		std::cout << "\nScale space surface at scale " << i << " done in " << elapsedTime.count() << " seconds.\n" << std::endl;

		std::cerr << "File is being written" << std::endl;
		// Open the file given in argument
		std::string newPath = pathout;
		newPath += ("_05_scalespace_"+std::to_string(i)+=".off");
		std::ofstream out(newPath);
		// Write the model to the file given
		out << "OFF " << std::endl << model_reconstructor.number_of_points() << " " << model_reconstructor.number_of_triangles() << " 0 " << std::endl;
		for (auto it = this->_colored_points.begin(); it != this->_colored_points.end(); ++it) {
			out << (*it).get<0>().x() << " " << (*it).get<0>().y() << " " << (*it).get<0>().z() << std::endl;
		}
		for (auto it = model_reconstructor.surface_begin(); it != model_reconstructor.surface_end(); ++it) {
			out << "3 " << (*it)[0] << " " << (*it)[1] << " " << (*it)[2] << std::endl;
		}
		// for (int i = 0; i < model_reconstructor.number_of_shells(); ++i) {
		// 	for (auto it = model_reconstructor.shell_begin(i); it != model_reconstructor.shell_end(i); ++it) {
		// 		out << "3 " << *it << std::endl;
		// 	}
		// }
		out.close();
	}
	
	return;
}

void Model::reconstruct(const char* pathOut) {
	// first, try the poisson method :
	std::cout << "Attempting to reconstruct the surface with Poisson ..." << std::endl;
	auto start = std::chrono::high_resolution_clock::now();
	try {
		this->PoissonReconstruction(pathOut);
	} catch (...) {
		auto end = std::chrono::high_resolution_clock::now();
		std::cout << "\nPoisson method tried to reconstruct the surface, and failed in\n" << (std::chrono::duration<double>(end-start)).count() 
		<< " seconds. Switching to the Scale Space reconstruction method ..." << std::endl;
		this->scaleSpaceReconstruction(pathOut);
	}
}
