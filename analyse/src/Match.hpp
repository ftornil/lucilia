#ifndef MATCH_HPP
#define MATCH_HPP

#include <nlohmann/json.hpp>
#include <opencv2/core/matx.hpp>
#include <opencv2/core/types.hpp>
#include <ostream>
#include <vector>
#include <CGAL/Point_3.h>

class Feature;

/**
 * Correspondance de plusieurs points caractéristiques entre eux, dans
 * plusieurs images d’une même scène.
 */
class Match
{
public:
    using Id = int32_t;
    using Position = cv::Point3d;

    /**
     * Identifiant ne renvoyant vers aucune correspondance.
     */
    static constexpr Id Null = -1;

    /**
     * Construit une correspondance vide.
     */
    Match() = default;

    /**
     * Construit une correspondance.
     *
     * @param id Identifiant de la correspondance.
     */
    explicit Match(Id id) noexcept;

    /**
     * Récupère l’identifiant de la correspondance.
     *
     * @param Identifiant de la correspondance.
     */
    Id getId() const;

    /**
     * Récupère la position 3D dans la scène.
     *
     * @return Position 3D.
     */
    const Position& getPosition() const;

    /**
     * Modifie la position 3D dans la scène.
     *
     * @param position Nouvelle position 3D dans la scène.
     */
    void setPosition(const Position& position);

    /**
     * Récupère la liste des points caractéristiques correspondants.
     *
     * @return Liste des points caractéristiques correspondants.
     */
    const std::vector<Feature>& getMatchingFeatures() const;

    /**
     * Ajoute un point caractéristique correspondant.
     *
     * @param feature Nouveau point caractéristique correspondant.
     */
    void addMatchingFeature(Feature& feature);

    /**
     * Convertit une correspondance en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param match Correspondance source.
     */
    friend void to_json(nlohmann::json&, const Match&);

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a < b, faux sinon
     */
    bool operator<(const Match& rVar) const;

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a > b, faux sinon
     */
    bool operator>(const Match& rVar) const;

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a <= b, faux sinon
     */
    bool operator<=(const Match& rVar) const;

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a >= b, faux sinon
     */
    bool operator>=(const Match& rVar) const;

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a == b, faux sinon
     */
    bool operator==(const Match& rVar) const;

    /**
     * Compare deux points en fonction de leurs coordonnées spatiales X,Y,Z. 
     * Utilisé pour la premiere phase de l'algorithme de triangulation
     * 
     * @returns Vrai si a != b, faux sinon
     */
    bool operator!=(const Match& rVal) const;

    /**
     * Opérateur de conversion en cv::Point3d, pour les importer dans
     * le modèle 3d lors de la triangulation
     */
    operator cv::Point3d() const;

    /**
     * Opérateur de conversion en CGAL::Point3, pour utilisation dans
     * les fonctions de représentation surfaciques dans la classe `Model`.
     * 
     * Note : formé en tant que template, pour pouvoir l'utiliser avec
     * tous types de kernels.
     */
    template<typename _T> operator CGAL::Point_3<_T>() const {
        return CGAL::Point_3<_T>(_position.x, _position.y, _position.z);
    }

private:
    // Identifiant de la correspondance unique dans la scène
    Id _id = Null;

    // Position 3D dans la scène
    Position _position;

    // Liste des points caractéristiques correspondants
    std::vector<Feature> _matching_features;
};

#endif // MATCH_HPP
