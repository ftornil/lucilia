#include "Matching.hpp"
#include <algorithm>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>
#include <boost/container_hash/hash.hpp>
#include <boost/dynamic_bitset.hpp>
#include <fstream>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <queue>

Matching::Matching(std::vector<Cluster> clusters)
: _clusters(std::move(clusters))
{
    _createClustersByImage();
}

const std::vector<Matching::Cluster>& Matching::clusters() const
{
    return _clusters;
}

std::vector<std::pair<Matching::FeatureId, Matching::FeatureId>>
Matching::matchesBetween(ImageId image1, ImageId image2) const
{
    auto [from, to] = _clusters_by_image.equal_range(image1);
    std::vector<std::pair<FeatureId, FeatureId>> result;

    for (auto cluster_it = from; cluster_it != to; ++cluster_it)
    {
        auto feature1_it = _clusters[cluster_it->second].find(image1);
        auto feature2_it = _clusters[cluster_it->second].find(image2);

        if (feature2_it != std::cend(_clusters[cluster_it->second]))
        {
            result.emplace_back(
                feature1_it->second,
                feature2_it->second
            );
        }
    }

    return result;
}

namespace
{
using ImageId = Matching::ImageId;
using FeatureId = Matching::FeatureId;
using ClusterId = Matching::ClusterId;
using Cluster = Matching::Cluster;

/**
 * Empile un vecteur de points caractéristiques dans une matrice dans laquelle
 * chaque ligne correspond au descripteur d’un point caractéristique, dans
 * l’ordre du vecteur d’entrée.
 *
 * @param features Vecteur de points caractéristiques d’entrée.
 * @return Matrice résultante.
 */
cv::Mat stackDescriptors(const std::vector<Feature>& features)
{
    cv::Mat result{cv::Size{128, static_cast<int>(features.size())}, CV_32F};
    int row_index = 0;

    for (const Feature& feature : features)
    {
        cv::Mat row_data{feature.descriptor().t()};
        row_data.copyTo(result.row(row_index));
        ++row_index;
    }

    return result;
}

using FeatureMap = std::unordered_map<
    std::pair<ImageId, FeatureId>,
    ClusterId,
    boost::hash<std::pair<ImageId, FeatureId>>
>;

/**
 * Vérifie que tous les points caractéristiques d’un groupe
 * satisfont la condition de ressemblance.
 */
bool ratio_test(
    const std::vector<Image>& images,
    const Cluster& cluster,
    double ratio
)
{
    for (const auto& [image_id, feature_id] : cluster)
    {
        // Calcul de la distinction du point caractéristique
        const auto& image = images[image_id];
        double distinctiveness = std::numeric_limits<double>::infinity();

        for (
            FeatureId other_feature = 0;
            other_feature < image.features().size();
            ++other_feature
        )
        {
            if (feature_id != other_feature)
            {
                distinctiveness = std::min(distinctiveness, cv::norm(
                    image.features()[feature_id].descriptor(),
                    image.features()[other_feature].descriptor(),
                    cv::NORM_L2SQR
                ));
            }
        }

        for (const auto& [other_image, other_feature] : cluster)
        {
            if (image_id != other_image)
            {
                // Vérifie que la paire de points satisfait la condition
                // de ressemblance
                double distance = cv::norm(
                    image.features()[feature_id].descriptor(),
                    images[other_image].features()[other_feature].descriptor(),
                    cv::NORM_L2SQR
                );

                if (distance >= distinctiveness * ratio)
                {
                    return false;
                }
            }
        }
    }

    return true;
}

Matching tracking(
    const std::vector<Image>& images,
    const Options::Matching::Tracking& options,
    Progress& progress
)
{
    static auto matcher = cv::BFMatcher::create(
        cv::NormTypes::NORM_L2,
        /* cross_check = */ false
    );

    if (images.size() == 0)
    {
        return Matching{};
    }

    auto task = progress.start(
        "Association des points caractéristiques correspondants",
        images.size() - 1
    );

    // Liste des groupes
    std::vector<Cluster> clusters;

    // Associe à chaque point son groupe
    FeatureMap cluster_by_feature;

    // Ensemble des images auxquelles les points de chaque groupe appartiennent
    std::vector<boost::dynamic_bitset<>> cluster_images;

    for (ImageId image1 = 0; image1 + 1 < images.size(); ++image1)
    {
        ImageId image2 = image1 + 1;
        cv::Mat descs1 = stackDescriptors(images[image1].features());
        cv::Mat descs2 = stackDescriptors(images[image2].features());

        // Recherche exhaustive des points caractéristiques correspondants
        // parmi les points des deux images
        std::vector<cv::DMatch> matches;
        matcher->match(descs1, descs2, matches);

        // Création ou fusion de groupes pour les correspondantes
        for (std::size_t j = 0; j < matches.size(); ++j)
        {
            const cv::DMatch& match = matches[j];

            auto key1 = std::make_pair(image1, match.queryIdx);
            auto cluster1 = cluster_by_feature.find(key1);

            auto key2 = std::make_pair(image2, match.trainIdx);
            auto cluster2 = cluster_by_feature.find(key2);

            if (cluster1 == cluster_by_feature.end()
                    && cluster2 == cluster_by_feature.end())
            {
                // Création d’un nouveau groupe pour les deux points
                cluster_by_feature[key1] = clusters.size();
                cluster_by_feature[key2] = clusters.size();

                clusters.emplace_back(Cluster{
                    key1,
                    key2
                });

                boost::dynamic_bitset<> set(images.size());
                set[image1] = 1;
                set[image2] = 1;
                cluster_images.emplace_back(std::move(set));
            }
            else if (cluster1 != cluster_by_feature.end()
                    && cluster2 != cluster_by_feature.end())
            {
                // Fusion des deux groupes existants, si compatibles
                if ((cluster_images[cluster1->second]
                        & cluster_images[cluster2->second]).none())
                {
                    ClusterId new_cluster = cluster1->second;
                    ClusterId old_cluster = cluster2->second;

                    if (
                        clusters[new_cluster].size()
                        < clusters[old_cluster].size()
                    )
                    {
                        std::swap(new_cluster, old_cluster);
                    }

                    Cluster next_cluster = clusters[new_cluster];

                    for (const auto& pair : clusters[old_cluster])
                    {
                        next_cluster.emplace(pair);
                    }

                    if (ratio_test(images, next_cluster, options.match_ratio))
                    {
                        cluster_by_feature[key1]
                            = cluster_by_feature[key2]
                            = new_cluster;

                        clusters[new_cluster] = std::move(next_cluster);
                        clusters[old_cluster].clear();

                        cluster_images[new_cluster]
                            = cluster_images[new_cluster]
                            | cluster_images[old_cluster];
                    }
                }
            }
            else
            {
                if (cluster2 == cluster_by_feature.end())
                {
                    std::swap(key1, key2);
                    std::swap(cluster1, cluster2);
                }

                // Ajout du premier point dans le second groupe si compatibles
                if (!cluster_images[cluster2->second].test(key1.first))
                {
                    Cluster next_cluster = clusters[cluster2->second];
                    next_cluster.emplace(key1);

                    if (ratio_test(images, next_cluster, options.match_ratio))
                    {
                        cluster_by_feature[key1] = cluster2->second;
                        clusters[cluster2->second] = std::move(next_cluster);
                        cluster_images[cluster2->second].set(key1.first);
                    }
                }
            }
        }

        task.advance();
    }

    // Filtrage des groupes contenant un point ou moins
    std::vector<Cluster> final_clusters;

    for (Cluster& cluster : clusters)
    {
        if (cluster.size() > 1)
        {
            final_clusters.emplace_back(std::move(cluster));
        }
    }

    return Matching{final_clusters};
}

std::vector<std::vector<double>>
compute_distinctiveness(
    std::size_t total_features_count,
    const std::vector<Image>& images,
    Progress& progress
)
{
    auto task = progress.start(
        "Calcul de la distinction des points caractéristiques",
        total_features_count
    );

    std::vector<std::vector<double>> distinctiveness(images.size());

    #pragma omp parallel for
    for (
        ImageId image = 0;
        image < images.size();
        ++image
    )
    {
        std::size_t features_count = images[image].features().size();
        std::vector<double> current_distinctiveness(
            features_count,
            std::numeric_limits<double>::infinity()
        );

        for (
            FeatureId feature1 = 0;
            feature1 < features_count;
            ++feature1
        )
        {
            for (
                FeatureId feature2 = feature1 + 1;
                feature2 < features_count;
                ++feature2
            )
            {
                double distance = cv::norm(
                    images[image].features()[feature1].descriptor(),
                    images[image].features()[feature2].descriptor(),
                    cv::NORM_L2SQR
                );

                current_distinctiveness[feature1] = std::min(
                    current_distinctiveness[feature1],
                    distance
                );

                current_distinctiveness[feature2] = std::min(
                    current_distinctiveness[feature2],
                    distance
                );
            }

            #pragma omp critical
            task.advance();
        }

        distinctiveness[image] = std::move(current_distinctiveness);
    }

    return distinctiveness;
}

std::vector<std::vector<double>>
compute_density(
    std::size_t total_features_count,
    const std::vector<Image>& images,
    const std::vector<std::vector<double>>& distinctiveness,
    double density_ratio,
    Progress& progress
)
{
    auto task = progress.start(
        "Estimation de la densité des points caractéristiques",
        total_features_count
    );

    std::vector<std::vector<double>> density(images.size());

    #pragma omp parallel for
    for (
        ImageId image1 = 0;
        image1 < images.size();
        ++image1
    )
    {
        std::size_t image1_features_count = images[image1].features().size();
        std::vector<double> current_density(image1_features_count, 0);

        for (
            FeatureId feature1 = 0;
            feature1 < image1_features_count;
            ++feature1
        )
        {
            for (
                ImageId image2 = 0;
                image2 < images.size();
                ++image2
            )
            {
                std::size_t image2_features_count
                    = images[image2].features().size();

                for (
                    FeatureId feature2 = 0;
                    feature2 < image2_features_count;
                    ++feature2
                )
                {
                    current_density[feature1]
                       += std::log(1 + std::sqrt(distinctiveness[image2][feature2]))
                        * std::exp(
                            -cv::norm(
                                images[image1].features()[feature1].descriptor(),
                                images[image2].features()[feature2].descriptor(),
                                cv::NORM_L2SQR
                            )
                            / 2
                            / density_ratio / density_ratio
                            / distinctiveness[image2][feature2]
                        );
                }
            }

            #pragma omp critical
            task.advance();
        }

        density[image1] = std::move(current_density);
    }

    return density;
}

struct Edge
{
    double distance;

    ImageId child_image;
    FeatureId child_feature;

    ImageId parent_image;
    FeatureId parent_feature;

    bool operator>(const Edge& rhs) const
    {
        return this->distance > rhs.distance;
    }
};

#pragma omp declare reduction ( \
    MergeEdges : \
    std::vector<Edge> : \
    omp_out.insert(std::end(omp_out), std::begin(omp_in), std::end(omp_in)) \
)

// Construction de l’arbre de densité
std::vector<Edge> build_density_tree(
    std::size_t total_features_count,
    const std::vector<Image>& images,
    const std::vector<std::vector<double>>& density,
    Progress& progress
)
{
    auto task = progress.start(
        "Construction de l’arbre de densité",
        total_features_count
    );

    std::vector<Edge> edges;

    #pragma omp parallel for reduction (MergeEdges : edges)
    for (
        ImageId image1 = 0;
        image1 < images.size();
        ++image1
    )
    {
        std::size_t image1_features_count = images[image1].features().size();

        for (
            FeatureId feature1 = 0;
            feature1 < image1_features_count;
            ++feature1
        )
        {
            ImageId parent_image = -1;
            FeatureId parent_feature = -1;
            double distance = std::numeric_limits<double>::infinity();

            for (
                ImageId image2 = 0;
                image2 < images.size();
                ++image2
            )
            {
                if (image1 != image2)
                {
                    std::size_t image2_features_count = images[image2].features().size();

                    for (
                        FeatureId feature2 = 0;
                        feature2 < image2_features_count;
                        ++feature2
                    )
                    {
                        if (density[image2][feature2] > density[image1][feature1])
                        {
                            double candidate_distance = cv::norm(
                                images[image1].features()[feature1].descriptor(),
                                images[image2].features()[feature2].descriptor(),
                                cv::NORM_L2SQR
                            );

                            if (candidate_distance < distance)
                            {
                                parent_image = image2;
                                parent_feature = feature2;
                                distance = candidate_distance;
                            }
                        }
                    }
                }
            }

            if (distance < std::numeric_limits<double>::infinity())
            {
                Edge edge;
                edge.distance = distance;
                edge.child_image = image1;
                edge.child_feature = feature1;
                edge.parent_image = parent_image;
                edge.parent_feature = parent_feature;

                edges.emplace_back(edge);
            }

            #pragma omp critical
            task.advance();
        }
    }

    // Création d’un tas binaire à partir des éléments du tableau
    std::make_heap(
        begin(edges),
        end(edges),
        std::greater<Edge>{}
    );

    return edges;
}

std::vector<Cluster> split_tree(
    std::size_t total_features_count,
    const std::vector<Image>& images,
    const std::vector<std::vector<double>>& distinctiveness,
    double edge_ratio,
    std::vector<Edge>& edges,
    Progress& progress
)
{
    auto task = progress.start(
        "Découpage de l’arbre en groupes",
        edges.size()
    );

    // Liste des groupes
    std::vector<Cluster> clusters;
    clusters.reserve(total_features_count);

    // Associe à chaque point son groupe
    std::vector<std::vector<ClusterId>> cluster_by_feature;
    cluster_by_feature.reserve(images.size());

    // Ensemble des images auxquelles les points de chaque groupe appartiennent
    std::vector<boost::dynamic_bitset<>> cluster_images;
    cluster_images.reserve(total_features_count);

    // Plus petite distinction du groupe
    std::vector<double> cluster_distinctiveness;
    cluster_distinctiveness.reserve(total_features_count);

    // Initialement, chaque point est dans un groupe singleton
    for (
        ImageId image = 0;
        image < images.size();
        ++image
    )
    {
        std::size_t features_count = images[image].features().size();
        std::vector<ClusterId> current_cluster_by_feature;
        current_cluster_by_feature.reserve(features_count);

        for (
            FeatureId feature = 0;
            feature < features_count;
            ++feature
        )
        {
            boost::dynamic_bitset<> set(images.size());
            set[image] = 1;

            current_cluster_by_feature.push_back(clusters.size());
            clusters.emplace_back(Cluster{{image, feature}});
            cluster_images.emplace_back(std::move(set));
            cluster_distinctiveness.push_back(distinctiveness[image][feature]);
        }

        cluster_by_feature.emplace_back(std::move(current_cluster_by_feature));
    }

    // Contraction des groupes reliés par des arêtes valides
    while (!edges.empty())
    {
        std::pop_heap(begin(edges), end(edges), std::greater<Edge>{});
        const Edge& edge = edges.back();

        auto child_cluster
            = cluster_by_feature[edge.child_image][edge.child_feature];
        auto parent_cluster
            = cluster_by_feature[edge.parent_image][edge.parent_feature];

        if (
            edge.distance <= edge_ratio * edge_ratio * std::min(
                cluster_distinctiveness[child_cluster],
                cluster_distinctiveness[parent_cluster]
            )
            && (cluster_images[child_cluster]
                & cluster_images[parent_cluster]).none()
        )
        {
            // Contraction des deux groupes (on déplace tous les points du plus
            // petit groupe dans le plus grand)
            ClusterId new_cluster = child_cluster;
            ClusterId old_cluster = parent_cluster;

            if (clusters[new_cluster].size() < clusters[old_cluster].size())
            {
                std::swap(new_cluster, old_cluster);
            }

            cluster_by_feature[edge.child_image][edge.child_feature]
                = cluster_by_feature[edge.parent_image][edge.parent_feature]
                = new_cluster;

            cluster_images[new_cluster]
                = cluster_images[new_cluster]
                | cluster_images[old_cluster];

            cluster_distinctiveness[new_cluster] = std::min(
                cluster_distinctiveness[new_cluster],
                cluster_distinctiveness[old_cluster]
            );

            for (const auto& [image, feature] : clusters[old_cluster])
            {
                clusters[new_cluster][image] = feature;
            }

            clusters[old_cluster].clear();
        }

        edges.pop_back();
        task.advance();
    }

    return clusters;
}

Matching quick_match(
    const std::vector<Image>& images,
    const Options::Matching::QuickMatch& options,
    Progress& progress
)
{
    std::size_t total_features_count = 0;

    for (const auto& image : images)
    {
        total_features_count += image.features().size();
    }

    auto distinctiveness = compute_distinctiveness(
        total_features_count,
        images,
        progress
    );

    auto density = compute_density(
        total_features_count,
        images,
        distinctiveness,
        options.density_ratio,
        progress
    );

    auto edges = build_density_tree(
        total_features_count,
        images,
        density,
        progress
    );

    density.clear();

    std::vector<Cluster> working_clusters = split_tree(
        total_features_count,
        images,
        distinctiveness,
        options.edge_ratio,
        edges,
        progress
    );

    distinctiveness.clear();

    // Filtrage des groupes contenant un point ou moins
    std::vector<Cluster> clusters;

    for (Cluster& cluster : working_clusters)
    {
        if (cluster.size() > 1)
        {
            clusters.emplace_back(std::move(cluster));
        }
    }

    return Matching{clusters};
}
}

Matching Matching::fromFeatures(
    const std::vector<Image>& images,
    const Options::Matching& options,
    Progress& progress
)
{
    switch (options.algorithm)
    {
    case Options::Matching::Algorithm::Tracking:
        return tracking(
            images,
            options.tracking,
            progress
        );

    case Options::Matching::Algorithm::QuickMatch:
        return quick_match(
            images,
            options.quick_match,
            progress
        );

    default:
        throw std::runtime_error{
            "Algorithme de mise en correspondance inconnu : "
            + std::to_string(static_cast<int>(options.algorithm))
        };
    }
}

void Matching::_createClustersByImage()
{
    _clusters_by_image.clear();

    for (std::size_t i = 0; i < _clusters.size(); ++i)
    {
        for (const auto& [image, feature] : _clusters[i])
        {
            _clusters_by_image.emplace(image, i);
        }
    }
}

void to_json(nlohmann::json& object, const Matching& matching)
{
    object = static_cast<nlohmann::json>(matching._clusters);
}

void from_json(const nlohmann::json& object, Matching& matching)
{
    object.get_to(matching._clusters);
    matching._createClustersByImage();
}

void Matching::toSVG(
    const std::vector<Image>& images,
    std::ostream& out
) const
{
    static const int image_height = 300;
    static const int image_margin = 30;

    out << R"SVG(<?xml version="1.0" standalone="no"?>
<svg width="200" height="250" version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink">
)SVG";

    // Insertion des images verticalement
    int current_y = 0;
    std::size_t image_index = 0;
    std::vector<double> scalings(images.size(), 1);

    for (const Image& image : images)
    {
        out << R"SVG(    <image xlink:href="data:image/jpeg;base64,)SVG";

        // Redimensionnement à la volée de l’image et intégration
        // au sein du fichier SVG au format Base64
        cv::Mat matrix = cv::imread(
            image.path().string(),
            cv::IMREAD_COLOR
        );

        if (matrix.size[0] != image_height)
        {
            cv::Mat resized_matrix;
            scalings[image_index] = static_cast<double>(image_height)
                / matrix.size[0];

            cv::resize(
                matrix,
                resized_matrix,
                cv::Size{
                    static_cast<int>(matrix.size[1] * scalings[image_index]),
                    image_height
                },
                0, 0,
                cv::INTER_AREA
            );

            matrix = std::move(resized_matrix);
        }

        std::vector<unsigned char> bytes;
        cv::imencode(".jpg", matrix, bytes);

        using namespace boost::archive::iterators;
        using base64_iterator = base64_from_binary<
            transform_width<
                const unsigned char*,
                6,
                8
            >
        >;

        std::copy(
            base64_iterator(bytes.data()),
            base64_iterator(bytes.data() + bytes.size()),
            std::ostream_iterator<char>(out)
        );

        out << R"SVG(" x="0" y=")SVG"
            << current_y
            << R"SVG(" height=")SVG"
            << image_height
            << R"SVG(" />
)SVG";

        current_y += image_height + image_margin;
        ++image_index;
    }

    // Ajout des points de pour chaque groupe
    auto get_point_x = [
        &images, &scalings
    ](ImageId image_id, FeatureId feature_id)
    {
        return scalings[image_id]
            * images[image_id].features()[feature_id].position().x;
    };

    auto get_point_y = [
        &images, &scalings
    ](ImageId image_id, FeatureId feature_id)
    {
        return scalings[image_id]
            * images[image_id].features()[feature_id].position().y
            + image_id * (image_height + image_margin);
    };

    for (const Cluster& cluster : _clusters)
    {
        out << "    <g>\n";

        for (
            auto pair = std::cbegin(cluster);
            pair != std::cend(cluster);
            ++pair
        )
        {
            double point_x = get_point_x(pair->first, pair->second);
            double point_y = get_point_y(pair->first, pair->second);

            auto next = std::next(pair);

            // Lien entre le point actuel et le suivant
            if (next != std::cend(cluster) && pair->first + 1 == next->first)
            {
                double next_point_x = get_point_x(next->first, next->second);
                double next_point_y = get_point_y(next->first, next->second);

                out << R"SVG(        <path d="M)SVG"
                    << point_x << " " << point_y
                    << R"SVG( L)SVG"
                    << next_point_x << " " << next_point_y
                    << R"SVG(" stroke="black" stroke-width="3")SVG"
                    << R"SVG( fill="none" />
)SVG";

                out << R"SVG(        <path d="M)SVG"
                    << point_x << " " << point_y
                    << R"SVG( L)SVG"
                    << next_point_x << " " << next_point_y
                    << R"SVG(" stroke="white" stroke-width="2")SVG"
                    << R"SVG( fill="none" />
)SVG";
            }

            // Cercle indiquant le point actuel
            out << R"SVG(        <circle cx=")SVG"
                << point_x
                << R"SVG(" cy=")SVG"
                << point_y
                << R"SVG(" r="5" stroke="black" stroke-width="3")SVG"
                << R"SVG( fill="none" />
)SVG";

            out << R"SVG(        <circle cx=")SVG"
                << point_x
                << R"SVG(" cy=")SVG"
                << point_y
                << R"SVG(" r="5" stroke="white" stroke-width="2")SVG"
                << R"SVG( fill="none" />
)SVG";
        }

        out << "    </g>\n";
    }

    out << R"SVG(</svg>
)SVG";
}
