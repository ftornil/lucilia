#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "Camera.hpp"
#include <boost/filesystem/path.hpp>
#include <opencv2/features2d.hpp>
#include <vector>

class Feature;

class Image
{
public:
    /**
     * Crée une image vide.
     */
    Image() = default;

    /**
     * Crée une nouvelle image.
     *
     * @param path Chemin absolu vers l’image.
     * @param features Ensemble de points caractéristiques de l’image.
     */
    Image(
        boost::filesystem::path,
        std::vector<Feature>
    );

    /**
     * Détecte automatiquement les points caractéristiques d’une image
     * à l’aide du détecteur donné.
     *
     * @param path Chemin vers l’image relativement au répertoire courant.
     * @param taken_by Appareil photo ayant capturé l’image.
     * @param detector Détecteur de points caractéristiques à utiliser.
     * @return Image créée.
     */
    static Image fromPath(
        const boost::filesystem::path&,
        const Camera&,
        cv::Ptr<cv::Feature2D>
    );

    /**
     * Récupère le chemin absolu vers l’image.
     *
     * @return Chemin absolu vers l’image.
     */
    const boost::filesystem::path& path() const;

    /**
     * Récupère l’ensemble de points caractéristiques de l’image.
     *
     * @return Points caractéristiques de l’image.
     */
    const std::vector<Feature>& features() const;

    /**
     * Convertit une image en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param image Image source.
     */
    friend void to_json(nlohmann::json&, const Image&);

    /**
     * Charge une image depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param capture Image à remplir.
     */
    friend void from_json(const nlohmann::json&, Image&);

private:
    // Chemin absolu vers l’image
    boost::filesystem::path _path;

    // Ensemble de points caractéristiques de l’image
    std::vector<Feature> _features;
};

#endif // IMAGE_HPP
