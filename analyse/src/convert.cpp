#include "Scene.hpp"
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <opencv2/core.hpp>

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cerr << "Utilisation : ./convert [fichier] captures|points\n";
        return EXIT_FAILURE;
    }

    std::string input_path = argv[1];
    std::string kind = argv[2];

    std::ifstream input{input_path};
    Scene scene = nlohmann::json::parse(input).get<Scene>();
    const auto& points = scene.points();
    const auto& captures = scene.captures();

    if (kind == "captures")
    {
        std::cout <<
            "ply\n"
            "format ascii 1.0\n"
            "element vertex " << captures.size() << "\n"
            "property double x\n"
            "property double y\n"
            "property double z\n"
            "property double nx\n"
            "property double ny\n"
            "property double nz\n"
            "end_header\n";

        for (const Capture& capture : captures)
        {
            Capture::Position position = capture.position();
            Capture::Direction direction = capture.direction();

            std::cout
                << position(0) << " "
                << position(1) << " "
                << position(2) << " "
                << direction(0) << " "
                << direction(1) << " "
                << direction(2) << "\n";
        }
    }
    else if (kind == "points")
    {
        // Filtre les points trop éloignés
        std::vector<Point> near_points;
        near_points.reserve(points.size());

        std::copy_if(
            cbegin(points),
            cend(points),
            back_inserter(near_points),
            [](const Point& point)
            {
                return cv::norm(point.position()) <= 50;
            }
        );

        std::cout <<
            "ply\n"
            "format ascii 1.0\n"
            "element vertex " << near_points.size() << "\n"
            "property double x\n"
            "property double y\n"
            "property double z\n"
            "property uchar diffuse_red\n"
            "property uchar diffuse_green\n"
            "property uchar diffuse_blue\n"
            "end_header\n";

        for (const Point& point : near_points)
        {
            std::cout
                << point.position().x << " "
                << point.position().y << " "
                << point.position().z << " "
                << static_cast<int>(point.color()(2)) << " "
                << static_cast<int>(point.color()(1)) << " "
                << static_cast<int>(point.color()(0)) << "\n";
        }
    }
}
