#ifndef SCENE_HPP
#define SCENE_HPP

#include "Capture.hpp"
#include "Matching.hpp"
#include "Point.hpp"
#include <boost/filesystem/path.hpp>
#include <nlohmann/json.hpp>
#include <opencv2/core.hpp>
#include <string>
#include <vector>

/**
 * Scène reconstruite à partir d’un flux d’images.
 */
class Scene
{
public:
    /**
     * Crée une scène vide.
     */
    Scene() = default;

    /**
     * Crée une scène.
     *
     * @param captures Flux d’images capturant la scène.
     * @param points Points de la scène.
     */
    Scene(
        std::vector<Capture>,
        std::vector<Point>
    );

    /**
     * Reconstruit une scène à partir d’un flux d’images.
     *
     * @param images Flux d’images à analyser.
     * @param taken_by Appareil photo ayant capturé le flux d’images.
     * @param matching Association des points caractéristiques.
     * @param options Options de reconstruction.
     * @param progress Suiveur de progression.
     * @return Scène reconstruite.
     */
    static Scene fromMotion(
        const std::vector<Image>&,
        const Camera&,
        const Matching&,
        const Options::Reconstruction&,
        Progress&
    );

    /**
     * Récupère le flux d’images capturant la scène.
     *
     * @return Liste des captures associées à la scène.
     */
    const std::vector<Capture>& captures() const;

    /**
     * Récupère les points de la scène.
     *
     * @return Liste des points dans la scène.
     */
    const std::vector<Point>& points() const;

    /**
     * Convertit une scène en objet JSON.
     *
     * @param object Objet JSON à remplir.
     * @param scene Scène source.
     */
    friend void to_json(nlohmann::json&, const Scene&);

    /**
     * Charge une scène depuis un objet JSON.
     *
     * @param object Objet JSON source.
     * @param scene Scène à remplir.
     */
    friend void from_json(const nlohmann::json&, Scene&);

private:
    // Liste des captures utilisées pour la reconstruction
    std::vector<Capture> _captures;

    // Liste des points reconstruits dans la scène
    std::vector<Point> _points;
};

#endif // SCENE_HPP
