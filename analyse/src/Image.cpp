#include "Image.hpp"
#include "Feature.hpp"
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>

Image::Image(
    boost::filesystem::path path,
    std::vector<Feature> features
)
: _path(std::move(path))
, _features(std::move(features))
{}

namespace
{
/**
 * Calcule la couleur d’un point à coordonnées non-entières par
 * interpolation bilinéaire.
 *
 * @param source Image source au format BVR.
 * @param point Coordonnées du point.
 * @return Couleur interpolée.
 */
Feature::Color interpolate_color(
    const cv::Mat_<unsigned char>& source,
    cv::Point2f point
)
{
    const auto border = cv::BORDER_REFLECT_101;

    int floor_x = static_cast<int>(point.x);
    int floor_y = static_cast<int>(point.y);

    auto dist_x = point.x - floor_x;
    auto dist_y = point.y - floor_y;

    int x_l = cv::borderInterpolate(floor_x, source.cols, border);
    int y_t = cv::borderInterpolate(floor_y, source.rows, border);

    int x_r = cv::borderInterpolate(floor_x + 1, source.cols, border);
    int y_b = cv::borderInterpolate(floor_y + 1, source.rows, border);

    const auto& tl = source.at<Feature::Color>(y_t, x_l);
    const auto& tr = source.at<Feature::Color>(y_t, x_r);
    const auto& bl = source.at<Feature::Color>(y_b, x_l);
    const auto& br = source.at<Feature::Color>(y_b, x_r);

    return Feature::Color{
        /* b = */
        static_cast<unsigned char>(cvRound(
            (tl[0] * (1 - dist_x) + tr[0] * dist_x) * (1 - dist_y)
            + (bl[0] * (1 - dist_x) + br[0] * dist_x) * dist_y
        )),
        /* g = */
        static_cast<unsigned char>(cvRound(
            (tl[1] * (1 - dist_x) + tr[1] * dist_x) * (1 - dist_y)
            + (bl[1] * (1 - dist_x) + br[1] * dist_x) * dist_y
        )),
        /* r = */
        static_cast<unsigned char>(cvRound(
            (tl[2] * (1 - dist_x) + tr[2] * dist_x) * (1 - dist_y)
            + (bl[2] * (1 - dist_x) + br[2] * dist_x) * dist_y
        ))
    };
}
}

Image Image::fromPath(
    const boost::filesystem::path& path,
    const Camera& taken_by,
    cv::Ptr<cv::Feature2D> detector
)
{
    // Lecture de l’image à partir du fichier source et correction de
    // la distorsion induite par l’appareil photographique
    cv::Mat source = cv::imread(
        path.string(),
        cv::IMREAD_COLOR
    );

    if (source.empty())
    {
        throw std::runtime_error{
            "impossible de lire l’image à partir du fichier "
                + path.string()
        };
    }

    auto data = source.clone();
    cv::undistort(
        source,
        data,
        taken_by.intrinsics(),
        taken_by.distortion()
    );

    // Détection des points caractéristiques de l’image et calcul
    // de leur vecteur descripteur
    std::vector<cv::KeyPoint> cv_features;
    cv::Mat cv_descriptors;

    detector->detectAndCompute(
        data,
        cv::noArray(),
        cv_features,
        cv_descriptors
    );

    std::vector<Feature> res_features;
    res_features.reserve(cv_features.size());

    int index = 0;

    for (const auto& cv_feature : cv_features)
    {
        const cv::Point2f& point = cv_feature.pt;

        res_features.emplace_back(
            cv_descriptors.row(index),
            point,
            // Interpolation bilinéaire pour obtenir la couleur du point
            // caractéristique (le point peut se trouver à des coordonnées
            // non-entières)
            interpolate_color(data, point)
        );

        ++index;
    }

    return Image{
        path,
        std::move(res_features)
    };
}

const boost::filesystem::path& Image::path() const
{
    return _path;
}

const std::vector<Feature>& Image::features() const
{
    return _features;
}

void to_json(nlohmann::json& object, const Image& image)
{
    object = nlohmann::json{
        {"path", image._path.string()},
        {"features", image._features}
    };
}

void from_json(const nlohmann::json& object, Image& image)
{
    object.at("path").get_to(image._path);
    object.at("features").get_to(image._features);
}
