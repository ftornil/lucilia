#include "Capture.hpp"
#include "Feature.hpp"
#include "Options.hpp"
#include "utils/opencv_conversions.hpp"
#include <algorithm>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>

Capture::Capture()
: _translation(Translation::all(0))
, _rotation(Rotation::eye())
{
}

Capture::Capture(
    Image image,
    Camera taken_by
)
: _image(std::move(image))
, _taken_by(std::move(taken_by))
, _translation(Translation::all(0))
, _rotation(Rotation::eye())
{
}

Capture::Capture(
    Image image,
    Camera taken_by,
    Translation translation,
    Rotation rotation
)
: _image(std::move(image))
, _taken_by(std::move(taken_by))
, _translation(std::move(translation))
, _rotation(std::move(rotation))
{
}

Capture Capture::fromMatches(
    const Image& image,
    const Camera& taken_by,
    const Capture& previous,
    const std::vector<std::pair<std::size_t, std::size_t>>& matches,
    const Options::Reconstruction& options
)
{
    if (matches.size() < 5)
    {
        throw std::runtime_error{
            "pas assez de points communs pour trouvés entre l’image « "
            + image.path().string() + " » et l’image « "
            + previous.image().path().string() + " ». "
            + std::to_string(matches.size()) + " points communs "
              "trouvés et 5 points nécessaires pour reconstruire la pose "
              "de la capture."
        };
    }

    std::vector<cv::Point2f> prev_points, cur_points;
    prev_points.reserve(matches.size());
    cur_points.reserve(matches.size());

    for (const auto& [from, to] : matches)
    {
        prev_points.push_back(previous.image().features()[from].position());
        cur_points.push_back(image.features()[to].position());
    }

    // Calcule une transformation projective entre la précédente capture
    // et la capture actuelle avec la méthode des 5 points
    cv::Mat inliers;
    cv::Mat essential_mat = cv::findEssentialMat(
        prev_points,
        cur_points,
        taken_by.intrinsics(),
        cv::RANSAC,
        options.sample_confidence,
        options.epipolar_distance,
        inliers
    );

    // Extraction du positionnement relatif du second appareil par rapport
    // au premier grâce à la matrice essentielle et élimination des
    // correspondances aberrantes par le test de chiralité
    Rotation relative_rotation;
    Translation relative_translation;

    cv::Mat scene_points_homogeneous;
    cv::recoverPose(
        essential_mat,
        prev_points,
        cur_points,
        taken_by.intrinsics(),
        relative_rotation,
        relative_translation,
        inliers
    );

    return Capture{
        image,
        taken_by,
        relative_rotation * previous.translation() + relative_translation,
        relative_rotation * previous.rotation()
    };
}

const Image& Capture::image() const
{
    return _image;
}

const Camera& Capture::takenBy() const
{
    return _taken_by;
}

const Capture::Translation& Capture::translation() const
{
    return _translation;
}

const Capture::Rotation& Capture::rotation() const
{
    return _rotation;
}

cv::Matx34d Capture::extrinsics() const
{
    cv::Mat_<double> result(3, 4);
    auto result_subleft = result(cv::Rect{0, 0, 3, 3});
    auto result_subright = result(cv::Rect{3, 0, 1, 3});

    cv::Mat_<double>{_translation}.copyTo(result_subright);
    cv::Mat_<double>{_rotation}.copyTo(result_subleft);

    return result;
}

cv::Matx34d Capture::projection() const
{
    return _taken_by.intrinsics() * this->extrinsics();
}

Capture::Position Capture::position() const
{
    return this->rotation().t() * (-this->translation());
}

Capture::Direction Capture::direction() const
{
    return this->rotation() * Capture::Direction(0, 0, 1);
}

cv::Point3d Capture::sceneToCapture(const cv::Point3d& from) const
{
    cv::Point3d rotated = _rotation * from;
    return cv::Point3d{
        rotated.x + _translation(0),
        rotated.y + _translation(1),
        rotated.z + _translation(2)
    };
}

cv::Point2d Capture::captureToImage(const cv::Point3d& from) const
{
    // Application de la distorsion radiale et tangentielle
    double center_x = from.x / from.z;
    double center_y = from.y / from.z;

    double k1 = _taken_by.distortion()[0];
    double k2 = _taken_by.distortion()[1];
    double p1 = _taken_by.distortion()[2];
    double p2 = _taken_by.distortion()[3];
    double k3 = _taken_by.distortion()[4];

    double radius = center_x * center_x + center_y * center_y;
    double radial_dist = 1. + radius * (k1 + radius * (k2 + radius * k3));

    double tangent_dist_x
        = 2. * p1 * center_x * center_y
        + p2 * (radius + 2. * center_x * center_x);
    double distort_x = center_x * radial_dist + tangent_dist_x;

    double tangent_dist_y
        = p1 * (radius + 2. * center_y * center_y)
        + 2. * p2 * center_x * center_y;
    double distort_y = center_y * radial_dist + tangent_dist_y;

    // Projection
    double focal_x = _taken_by.intrinsics()(0, 0);
    double focal_y = _taken_by.intrinsics()(1, 1);
    double princ_x = _taken_by.intrinsics()(0, 2);
    double princ_y = _taken_by.intrinsics()(1, 2);

    return cv::Point2d{
        focal_x * distort_x + princ_x,
        focal_y * distort_y + princ_y
    };
}

cv::Point2d Capture::sceneToImage(const cv::Point3d& from) const
{
    return this->captureToImage(this->sceneToCapture(from));
}

void to_json(nlohmann::json& object, const Capture& capture)
{
    object = nlohmann::json{
        {"image", capture._image.path().string()},
        {"translation", capture._translation},
        {"rotation", capture._rotation},
        {"taken_by", capture._taken_by}
    };
}

void from_json(const nlohmann::json& object, Capture& capture)
{
    std::string path;
    object.at("image").get_to(path);
    capture._image = Image{path, std::vector<Feature>{}};

    object.at("translation").get_to(capture._translation);
    object.at("rotation").get_to(capture._rotation);
    object.at("taken_by").get_to(capture._taken_by);
}
