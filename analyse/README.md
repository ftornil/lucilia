# Traitement d’images pour le calcul d’un modèle 3D d’une scène

## Pré-requis

* Compilateur C++ récent
* CMake 3.6
* Boost 1.66
* Bibliothèque JSON de nlohmann 3.5
* OpenCV 4.0

## Compilation

### Mode développement

```
mkdir -p build/Debug
cd build/Debug
cmake -DCMAKE_BUILD_TYPE=Debug ../..
make
```

### Mode production

```
mkdir -p build/Release
cd build/Release
cmake ../..
make
```
