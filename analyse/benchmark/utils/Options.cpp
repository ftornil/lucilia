#include "Options.hpp"
#include "../../src/utils/utils.hpp"
#include "../../src/utils/opencv_conversions.hpp"
#include <boost/program_options.hpp>
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <opencv2/xfeatures2d.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

Options::Options()
: _console_options{"Options pour la ligne de commande ", 80, 55}
, _common_options{"", 80, 55}
{
    // clang-format off
    // Options réservées à la ligne de commande
    _console_options.add_options()
        ("help,h", "Affiche ce message d’aide.")
        ("config,C",
         po::value<fs::path>()
            ->value_name("PATH"),
         "Fichier de configuration définissant des valeurs pour les options "
         "ci-dessous.")
    ;

    po::options_description io{"Options d’entrée/sortie "};

    io.add_options()
        ("io.output-file,o",
         po::value(&this->io.output_file)
            ->value_name("PATH")
            ->default_value("-", "-"),
         "Fichier dans lequel stocker le résultat au format "
         "JSON. Le fichier sera créé s’il n’existe pas, et écrasé sinon. "
         "Le fichier spécial - désigne la sortie standard.")
    ;
    _common_options.add(io);

    po::options_description calibration{
        "Options de l’appareil et d’étalonnage automatique "};

    calibration.add_options()
        ("calibration.source,c",
         po::value<std::vector<fs::path>>()
            ->composing()
            ->value_name("PATH")
            ->notifier([this](auto& sources) {
                this->calibration.source = explorePaths(sources);
            }),
         "Répertoire source pour l’étalonnage automatique de "
         "l’appareil photo à partir d’images. Chaque image doit contenir "
         "l’intégralité de l’échiquier d’étalonnage sur une surface plane. "
         "Si aucune source n’est précisée, l’étalonnage automatique n’est pas "
         "réalisé.")

        ("calibration.chessboard-width,W",
         po::value(&this->calibration.chessboard_width)
            ->value_name("NUMBER")
            ->default_value(9),
         "Nombre d’intersections horizontales dans l’échiquier d’étalonnage.")

        ("calibration.chessboard-height,H",
         po::value(&this->calibration.chessboard_height)
            ->value_name("NUMBER")
            ->default_value(6),
         "Nombre d’intersections verticales dans l’échiquier d’étalonnage.")

        ("calibration.intrinsics,i",
         po::value<std::string>()
            ->value_name("MATRIX")
            ->notifier([this](const std::string& input) {
                this->calibration.intrinsics = nlohmann::json::parse(input);
                this->calibration.has_intrinsics = true;
            }),
         "Matrice 3 × 3 des paramètres intrinsèques de l’appareil photo "
         "utilisé pour capturer les images. Si l’étalonnage automatique "
         "est demandé, ces paramètres intrinsèques servent de valeur "
         "initiale pour l’étalonnage.")

        ("calibration.distortion,d",
         po::value<std::string>()
            ->value_name("VECTOR")
            ->notifier([this](const std::string& input) {
                this->calibration.distortion = nlohmann::json::parse(input);
            }),
         "Liste des 5 paramètres de distorsion de l’appareil photo "
         "utilisé pour capturer les images. Ignoré si l’étalonnage automatique "
         "a été demandé.")

        ("calibration.debug",
         po::bool_switch(&this->calibration.debug),
         "Active le mode de débogage pour l’étalonnage automatique. En mode "
         "débogage, la détection de l’échiquier est retracée sur chaque image "
         "l’une après l’autre.")
    ;
    _common_options.add(calibration);

    po::options_description reconstruction{"Options de reconstruction "};

    reconstruction.add_options()
        ("reconstruction.source,I",
         po::value<std::vector<fs::path>>()
            ->composing()
            ->value_name("PATH")
            ->notifier([this](auto& sources) {
                this->reconstruction.source = explorePaths(sources);
            }),
         "Répertoire source pour les images utilisées pour la "
         "reconstruction de la scène. Si aucune source n’est précisée, la "
         "reconstruction n’est pas réalisée.")

        ("reconstruction.detector,D",
         po::value(&this->reconstruction.detector_name)
            ->value_name("ALGORITHM")
            ->default_value("surf"),
         "Algorithme utilisé pour la détection et la description des points "
         "caractéristiques des images. Valeurs possibles : \"sift\", "
         "\"surf\".")

        ("reconstruction.debug",
         po::bool_switch(&this->reconstruction.debug),
         "Active le mode de débogage pour la reconstruction. En mode débogage, "
         "la détection des points caractéristiques est retracée sur chaque "
         "capture l’une après l’autre.")
    ;
    _common_options.add(reconstruction);

    po::options_description sift{"Options de l’algorithme SIFT "};

    sift.add_options()
        ("sift.layers-per-octave",
         po::value(&this->reconstruction.sift.layers_per_octave)
            ->value_name("VALUE")
            ->default_value(3),
         "Nombre de couches par octave.")

        ("sift.minimum-contrast",
         po::value(&this->reconstruction.sift.minimum_contrast)
            ->value_name("VALUE")
            ->default_value(0.04, "0.04"),
         "Seuil de contraste d’un point caractéristique pour que celui-ci "
         "soit conservé. Plus ce seuil est élevé, plus la quantité de points "
         "détectés diminue.")

        ("sift.edge-threshold",
         po::value(&this->reconstruction.sift.edge_threshold)
            ->value_name("VALUE")
            ->default_value(10, "10"),
         "Seuil au delà duquel un point est considéré comme une bordure et "
         "donc éliminé. Plus ce seuil est élevé, plus la quantité de points "
         "détectés augmente.")

        ("sift.sigma",
         po::value(&this->reconstruction.sift.sigma)
            ->value_name("VALUE")
            ->default_value(1.6, "1.6"),
         "Valeur de l’écart-type (sigma) du flou gaussien appliqué sur "
         "l’image à chaque couche.")
    ;
    _common_options.add(sift);

    po::options_description surf{"Options de l’algorithme SURF "};

    surf.add_options()
        ("surf.minimum-hessian",
         po::value(&this->reconstruction.surf.minimum_hessian)
            ->value_name("VALUE")
            ->default_value(400, "400"),
         "Hessien minimal pour qu’un point caractéristique soit conservé.")

        ("surf.number-octaves",
         po::value(&this->reconstruction.surf.number_octaves)
            ->value_name("VALUE")
            ->default_value(4, "4"),
         "Nombre d’octaves utilisées.")

        ("surf.layers-per-octave",
         po::value(&this->reconstruction.surf.layers_per_octave)
            ->value_name("VALUE")
            ->default_value(3, "3"),
         "Nombre de couches par octave.")
    ;
    _common_options.add(surf);

    po::options_description orb{"Options de l'algorithme ORB "};

    orb.add_options()
        ("orb.nb_features_to_retain", po::value(&this->reconstruction.orb.max_features_to_retain)->
        value_name("VALUE")->default_value(500, "500"), "Nombre maximal de points caractéristiques à retenir.")
        ("orb.scale_factor", po::value(&this->reconstruction.orb.scale_factor)->
        value_name("VALUE")->default_value(1.2f, "01.20"), "Ratio de décimation de la pyramide construite par le descripteur BRIEF.")
        ("orb.nb_levels", po::value(&this->reconstruction.orb.nb_levels)->
        value_name("VALUE")->default_value(8, "8"), "Nombre de niveaux de la pyramide.")
        ("orb._edge_threshold", po::value(&this->reconstruction.orb.edge_threshold)->
        value_name("VALUE")->default_value(31, "31"), "Taille de la bordure où les points caractéristiques ne seront pas détectés (doit être proche de orb.patch_size).")
        ("orb.first_level", po::value(&this->reconstruction.orb.first_level)->
        value_name("VALUE")->default_value(0, "0"), "Premier niveau de la pyramide. Les niveaux infèrieurs seront remplis avec une copie de l'image suréchantillonée.")
        ("orb.wta_k", po::value(&this->reconstruction.orb.wta_k)->
        value_name("VALUE")->default_value(2, "2"), "Nombre de points aléatoires pris pour calculer la différence de luminosité. Peut être égal à 2, 3, ou 4.")
        ("orb.patch_size", po::value(&this->reconstruction.orb.patch_size)->
        value_name("VALUE")->default_value(31, "31"), "Taille des patches pour le descripteur BRIEF.")
        ("orb.fast_threshold", po::value(&this->reconstruction.orb.fast_threshold)->
        value_name("VALUE")->default_value(20, "20"), "Valeur de seuil pour le détecteur de coins FAST.");
    _common_options.add(orb);

    po::options_description brisk{"Options de l'algorithme BRISK "};

    brisk.add_options()
        ("brisk.thresh", po::value(&this->reconstruction.brisk.thresh)->value_name("VALUE")->default_value(30, "30"), "Desc")
        ("brisk.nb_octaves", po::value(&this->reconstruction.brisk.nb_octaves)->value_name("VALUE")->default_value(3, "3"), "Desc")
        ("brisk.pattern_scale", po::value(&this->reconstruction.brisk.pattern_scale)->value_name("VALUE")->default_value(1.0f, "1.0"), "Desc");

    _common_options.add(brisk);

    po::options_description freak{"Paramètres de l'algorithme FREAK "};

    freak.add_options()
        ("freak.orientation_normalised", po::value(&this->reconstruction.freak.orientation_normalised)->value_name("VALUE")->default_value(true, "Vrai"), "Normalise les orientations.")
        ("freak.scale_normalised", po::value(&this->reconstruction.freak.scale_normalised)->value_name("VALUE")->default_value(true, "Vrai"), "Normalise les échelles.")
        ("freak.pattern_scale", po::value(&this->reconstruction.freak.pattern_scale)->value_name("VALUE")->default_value(22.0f, "22.0"), "Scalaire pour le modèle de description.")
        ("freak.nb_octaves", po::value(&this->reconstruction.freak.nb_octaves)->value_name("VALUE")->default_value(3, "3"), "Nombre d'octaves couverts par les points caractéristiques détectés.");

    _common_options.add(freak);

    po::options_description brief{"Paramètres de l'extracteur de descripteurs BRIEF "};

    brief.add_options()
        ("brief.bytes", po::value(&this->reconstruction.brief.bytes)->value_name("VALUE")->default_value(32, "32"), "Nombre d'octets à utiliser pour le descripteur.")
        ("brief.use_orientation", po::value(&this->reconstruction.brief.use_orientation)->value_name("VALUE")->default_value(true, "Vrai"), "Le descripteur possède une orientation.");

    _common_options.add(brief);
    // clang-format on
}

void Options::usage(std::ostream& out)
{
    out << "Utilisation : ./lucilia-analyse [OPTION...]\n";
    out << "Suite d’outils permettant l’analyse d’images prises en "
           "séquence dans une scène\nen vue de recréer un modèle 3D.\n\n";
    out << _console_options;
    out << _common_options;
}

namespace
{
/**
 * Rend absolu un chemin éventuellement passé en option du programme,
 * depuis un chemin racine donné.
 *
 * @param values Dictionnaire de valeurs passées au programme.
 * @param key Clé de l’option dont le chemin valeur doit être rendu absolu.
 * @param root Chemin racine.
 */
void makeAbsolutePath(
    po::variables_map& values, const std::string& key, const fs::path& root)
{
    if (values.count(key) == 1)
    {
        auto& value = values.at(key).as<fs::path>();

        if (value != "-")
        {
            value = fs::absolute(value, root);
        }
    }
}

/**
 * Rend absolu un tableau de chemins éventuellement passé en option du
 * programme, depuis un chemin racine donné.
 *
 * @param values Dictionnaire de valeurs passées au programme.
 * @param key Clé de l’option dont le chemin valeur doit être rendu absolu.
 * @param root Chemin racine.
 */
void makeAbsolutePathArray(
    po::variables_map& values, const std::string& key, const fs::path& root)
{
    if (values.count(key) == 1)
    {
        for (auto& value : values.at(key).as<std::vector<fs::path>>())
        {
            value = fs::absolute(value, root);
        }
    }
}
}

bool Options::parse(int argc, const char* argv[])
{
    try
    {
        // Lecture des options passées en argument
        po::options_description all_options;
        all_options.add(_console_options);
        all_options.add(_common_options);

        po::variables_map values;
        po::store(po::parse_command_line(argc, argv, all_options), values);

        if (values.count("help") == 1)
        {
            this->help = true;
            return true;
        }

        // Transformation des chemins relatifs passés en ligne de commande en
        // chemins absolus par rapport au répertoire d’exécution courant
        const auto cwd = fs::current_path();
        makeAbsolutePath(values, "io.output-file", cwd);
        makeAbsolutePathArray(values, "calibration.source", cwd);
        makeAbsolutePathArray(values, "reconstruction.source", cwd);

        if (values.count("config") == 1)
        {
            auto config_path
                = fs::absolute(values["config"].as<fs::path>(), cwd);
            auto config_dir = config_path.parent_path();

            std::ifstream config_file{config_path.string().c_str()};

            if (!config_file.is_open())
            {
                std::cerr << "Erreur d’ouverture du fichier de configuration "
                             "au chemin "
                        + config_dir.string() + " : "
                          << std::strerror(errno) << ".\n";
                return false;
            }

            po::store(
                po::parse_config_file(config_file, _common_options),
                values);

            // Transformation des chemins relatifs passés dans le fichier de
            // configuration en chemins absolus par rapport au fichier de config
            makeAbsolutePath(values, "io.output-file", config_dir);
            makeAbsolutePathArray(values, "calibration.source", config_dir);
            makeAbsolutePathArray(values, "reconstruction.source", config_dir);
        }

        po::notify(values);

        if (this->reconstruction.detector_name == "sift")
        {
            this->reconstruction.detector = cv::xfeatures2d::SIFT::create(
                0, // Nombre de points caractéristiques détectés illimité
                this->reconstruction.sift.layers_per_octave,
                this->reconstruction.sift.minimum_contrast,
                this->reconstruction.sift.edge_threshold,
                this->reconstruction.sift.sigma);
        }
        else if (this->reconstruction.detector_name == "surf")
        {
            this->reconstruction.detector = cv::xfeatures2d::SURF::create(
                this->reconstruction.surf.minimum_hessian,
                this->reconstruction.surf.number_octaves,
                this->reconstruction.surf.layers_per_octave,
                true, // Descripteurs à 128 valeurs et non 64
                false); // Normalise les descripteurs sur leur rotation
        } else if (this->reconstruction.detector_name == "orb") {
            this->reconstruction.detector = cv::ORB::create(
                this->reconstruction.orb.max_features_to_retain,
                this->reconstruction.orb.scale_factor,
                this->reconstruction.orb.nb_levels,
                this->reconstruction.orb.edge_threshold,
                this->reconstruction.orb.first_level,
                this->reconstruction.orb.wta_k,
                this->reconstruction.orb.score_type,
                this->reconstruction.orb.patch_size,
                this->reconstruction.orb.fast_threshold
            );
        } else if (this->reconstruction.detector_name == "brisk") {
            this->reconstruction.detector = cv::BRISK::create(
                this->reconstruction.brisk.thresh,
                this->reconstruction.brisk.nb_octaves,
                this->reconstruction.brisk.pattern_scale
            );
        } else if (this->reconstruction.detector_name == "freak") {
            this->reconstruction.detector = cv::xfeatures2d::FREAK::create(
                this->reconstruction.freak.orientation_normalised,
                this->reconstruction.freak.scale_normalised,
                this->reconstruction.freak.pattern_scale,
                this->reconstruction.freak.nb_octaves
            );
        } else if (this->reconstruction.detector_name == "brief") {
            this->reconstruction.detector = cv::xfeatures2d::BriefDescriptorExtractor::create(
                this->reconstruction.brief.bytes,
                this->reconstruction.brief.use_orientation
            );
        }
        else if (this->reconstruction.detector_name == "orb") {
            this->reconstruction.detector = cv::ORB::create(
                this->reconstruction.orb.max_features_to_retain,
                this->reconstruction.orb.scale_factor,
                this->reconstruction.orb.nb_levels,
                this->reconstruction.orb.edge_threshold,
                this->reconstruction.orb.first_level,
                this->reconstruction.orb.wta_k,
                this->reconstruction.orb.score_type,
                this->reconstruction.orb.patch_size,
                this->reconstruction.orb.fast_threshold
            );
        } else if (this->reconstruction.detector_name == "brisk") {
            this->reconstruction.detector = cv::BRISK::create(
                this->reconstruction.brisk.thresh,
                this->reconstruction.brisk.nb_octaves,
                this->reconstruction.brisk.pattern_scale
            );
        } else if (this->reconstruction.detector_name == "freak") {
            this->reconstruction.detector = cv::xfeatures2d::FREAK::create(
                this->reconstruction.freak.orientation_normalised,
                this->reconstruction.freak.scale_normalised,
                this->reconstruction.freak.pattern_scale,
                this->reconstruction.freak.nb_octaves
            );
        } else if (this->reconstruction.detector_name == "brief") {
            this->reconstruction.detector = cv::xfeatures2d::BriefDescriptorExtractor::create(
                this->reconstruction.brief.bytes,
                this->reconstruction.brief.use_orientation
            );
        }
        else
        {
            this->usage(std::cerr);
            std::cerr << "\nAlgorithme de détection inconnu : "
                      << this->reconstruction.detector_name << ".\n";
            return false;
        }
    }
    catch (const po::error& err)
    {
        this->usage(std::cerr);
        std::cerr << "\nSyntaxe des options invalide : " << err.what() << ".\n";
        return false;
    }

    return true;
}
