#ifndef UTILS_OPTIONS_HPP
#define UTILS_OPTIONS_HPP

#include "../../src/Camera.hpp"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <opencv2/features2d.hpp>
#include <ostream>
#include <string>
#include <vector>

/**
 * Gère l’interprétation des options passées au programme.
 */
class Options
{
public:
    /**
     * Crée une instance de gestion des options passées.
     */
    Options();

    /**
     * Analyse les arguments passés au programme pour extraire les
     * options correspondantes.
     *
     * @param argc Nombre d’arguments.
     * @param argv Valeur des arguments.
     * @return Vrai si et seulement si l’interprétation s’est terminée
     * sans problème et le programme peut être poursuivi normalement.
     */
    bool parse(int argc, const char* argv[]);

    /**
     * Affiche les instructions d’utilisation du programme sur le flot
     * de sortie donné.
     *
     * @param out Flot de sortie à utiliser.
     */
    void usage(std::ostream& out);

    /**
     * Vrai si l’affichage de l’aide est demandé.
     */
    bool help = false;

    /**
     * Groupe d’options pour les entrées/sorties.
     */
    struct
    {
        /**
         * Chemin vers le fichier de sortie, ou "-" pour la sortie standard.
         */
        boost::filesystem::path output_file;
    } io;

    /**
     * Groupe d’options pour le paramétrage de l’appareil et l’étalonnage.
     */
    struct
    {
        /**
         * Liste des images pour l’étalonnage de l’appareil photo.
         */
        std::vector<boost::filesystem::path> source;

        /**
         * Largeur de l’échiquier d’étalonnage.
         */
        int chessboard_width = 0;

        /**
         * Hauteur de l’échiquier d’étalonnage.
         */
        int chessboard_height = 0;

        /**
         * Paramètres intrinsèques initiaux de l’appareil photo.
         */
        Camera::Intrinsics intrinsics = Camera::Intrinsics::eye();

        /**
         * Vrai si des paramètres intrinsèques initiaux ont été spécifiés.
         */
        bool has_intrinsics = false;

        /**
         * Paramètres de distorsion initiaux de l’appareil photo.
         */
        Camera::Distortion distortion = Camera::Distortion::all(0);

        /**
         * Vrai si le mode de débogage de l’étalonnage est activé.
         */
        bool debug = false;
    } calibration;

    /**
     * Groupe d’options pour la reconstruction.
     */
    struct
    {
        /**
         * Liste des images depuis lesquelles la scène doit être reconstituée.
         */
        std::vector<boost::filesystem::path> source;

        /**
         * Paramètres de l’algorithme de détection SIFT.
         */
        struct
        {
            int layers_per_octave = 0;
            double minimum_contrast = 0;
            double edge_threshold = 0;
            double sigma = 0;
        } sift;

        /**
         * Paramètres de l’algorithme de détection SURF.
         */
        struct
        {
            double minimum_hessian = 0;
            int number_octaves = 0;
            int layers_per_octave = 0;
        } surf;

        /**
         * Paramètres pour le descripteur ORB.
         * (contient les valeurs par défaut de 
         * cv::ORB::create() pour l'instant)
         */
        struct {
            int max_features_to_retain = 500;
            float scale_factor = 1.2f;
            int nb_levels = 8;
            int edge_threshold = 31;
            int first_level = 0;
            int wta_k = 2;
            cv::ORB::ScoreType score_type = cv::ORB::HARRIS_SCORE;
            int patch_size = 31;
            int fast_threshold = 20;
        } orb;

        /**
         * Paramètres pour le descripteur BRISK
         * (contient les valeurs par défaut de 
         * cv::BRISK::create() pour l'instant)
         */
        struct {
            int thresh = 30;
            int nb_octaves = 3;
            float pattern_scale = 1.0f;
        } brisk;

        /**
         * Paramètres du descripteur FREAK
         * (contient les valeurs par défaut de 
         * cv::FREAK::create() pour l'instant)
         */
        struct {
            bool orientation_normalised = true;
            bool scale_normalised = true;
            float pattern_scale = 22.0f;
            int nb_octaves = 4;
        } freak;

        /**
         * Paramètres du descripteur BRIEF [[[DEPRECATED]]]
         */
        struct {
            int bytes = 32;
            bool use_orientation = false;
        } brief;

        /**
         * Nom de l’algorithme de détection utilisé pour la reconstruction.
         */
        std::string detector_name;

        /**
         * Algorithme de détection utilisé pour la reconstruction.
         */
        cv::Ptr<cv::Feature2D> detector;

        /**
         * Vrai si le mode de débogage de la reconstruction est activé.
         */
        bool debug = false;
    } reconstruction;

private:
    // Description des options pour la ligne de commande
    boost::program_options::options_description _console_options;

    // Description des options générales
    boost::program_options::options_description _common_options;
};

#endif // UTILS_OPTIONS_HPP
