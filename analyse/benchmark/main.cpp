#include "../src/Camera.hpp"
#include "../src/Feature.hpp"
#include "../src/Scene.hpp"
#include "./utils/Options.hpp"
#include "../src/utils/Progress.hpp"
#include <fstream>
#include <functional>
#include <iostream>
#include <nlohmann/json.hpp>
#include <opencv2/core/utils/logger.hpp>
#include <optional>
#include <string>


namespace cvlog = cv::utils::logging;

/**
 * Crée l’appareil photo à partir des paramètres initiaux fournis par
 * l’utilsiateur et procède à un éventuel étalonnage si demandé pour
 * ajuster les paramètres.
 *
 * @param opts Options passées par l’utilisateur.
 * @return Appareil photo résultant, si aucune erreur ne s’est produite.
 */
std::optional<Camera> calibrate(Progress&, const Options&);

/**
 * Reconstruit la scène à partir du flux d’image fourni par l’utilisateur.
 *
 * @param opts Options passées par l’utilisateur.
 * @param camera Appareil photo utilisé pour la capture.
 * @return Scène résultante, si aucune erreur ne s’est produite.
 */
std::optional<Scene> reconstruct(Progress&, const Options&, const Camera&);

/**
 * Écrit une représentation textuelle de la scène reconstruite sur le
 * fichier de sorti choisi par l’utilisateur.
 *
 * @param opts Options passées par l’utilisateur.
 * @param callback Fonction de rappel permettant de réaliser l’opération de
 * sortie.
 * @return Code de retour à renvoyer.
 */
int output(const Options& opts, std::function<void(std::ostream&)> callback);

/**
 * Fonction de gestion des erreurs OpenCV.
 */
int opencv_error_handler(int, const char*, const char*, const char*, int, void*)
{
    // Ne rien faire. Ça ne devrait pas être trop compliqué.
    return 0;
}

/**
 * Actions prises : (* sinifie le bloc est en commentaire, pas supprimé)
 * 
 *      - enlever le message d'aide, on veut uniquement lancer l'utilitaire sur les jeux de données 
 *      * enlever la fenetre de debug pour calibration camera
 *      - 
 */

int main(int argc, const char* argv[])
{
    // Désactivation des messages d’OpenCV
    cv::redirectError(opencv_error_handler);
    cvlog::setLogLevel(cvlog::LogLevel::LOG_LEVEL_SILENT);

    // Interprétation des options passées au programme
    Options opts;

    if (!opts.parse(argc, argv))
    {
        return EXIT_FAILURE;
    }

    // Création du suiveur de progression
    Progress progress{std::cerr};

    // Étalonnage de l’appareil
    Camera camera;
    auto camera_result = calibrate(progress, opts);

    if (!camera_result)
    {
        return EXIT_FAILURE;
    }

    camera = *camera_result;

    // Reconstruction de la scène
    if (opts.reconstruction.source.empty())
    {
        if (opts.calibration.source.empty())
        {
            opts.usage(std::cerr);
            std::cerr << "\nVeuillez spécifier au moins une phase de calcul à "
                "réaliser.\n";
            return EXIT_FAILURE;
        }
        else
        {
            return output(opts, [&camera](std::ostream& out) {
                out << static_cast<nlohmann::json>(camera);
            });
        }
    }

    Scene scene;
    auto scene_result = reconstruct(progress, opts, camera);

    if (!scene_result)
    {
        return EXIT_FAILURE;
    }

    scene = *scene_result;
    return output(opts, [&scene](std::ostream& out) {
        out << static_cast<nlohmann::json>(scene);
    });
}

std::optional<Camera> calibrate(
    Progress& progress,
    const Options& opts
)
{
    if (opts.calibration.source.empty())
    {
        // Si aucune source n’est donnée pour l’étalonnage automatique,
        // utilisation des paramètres intrinsèques et des coefficients
        // de distorsion fournis en entrée
        return std::make_optional<Camera>(
            opts.calibration.intrinsics,
            opts.calibration.distortion
        );
    }

    // // // // // // // // // // // // // // // // // // // // // // if (opts.calibration.debug)
    // // // // // // // // // // // // // // // // // // // // // // {
    // // // // // // // // // // // // // // // // // // // // // //     cv::namedWindow("calibration", cv::WINDOW_GUI_EXPANDED);
    // // // // // // // // // // // // // // // // // // // // // // }

    try
    {
        auto [camera, error] = Camera::calibrate(
            opts.calibration.source,
            opts.calibration.chessboard_width,
            opts.calibration.chessboard_height,
            opts.calibration.has_intrinsics
                ? std::make_optional(opts.calibration.intrinsics)
                : std::optional<Camera::Intrinsics>{},
            progress,
            opts.calibration.debug ? "calibration" : ""
        );

        // // // // // // // // // // // // // // // // // // // // // // if (opts.calibration.debug)
        // // // // // // // // // // // // // // // // // // // // // // {
        // // // // // // // // // // // // // // // // // // // // // //     cv::destroyWindow("calibration");
        // // // // // // // // // // // // // // // // // // // // // // }

        if (error >= 1.1)
        {
            std::cerr << "\nAvertissement : erreur d’étalonnage trop élevée ("
                      << error << " pixels en moyenne). Pour un étalonnage de "
                      "bonne qualité, l’erreur doit être au plus de l’ordre de "
                      "un pixel.\n";
        }

        return std::make_optional(std::move(camera));
    }
    catch (const std::exception& err)
    {
        std::cerr << "\nErreur lors de l’étalonnage : " << err.what() << "\n";
        return std::optional<Camera>{};
    }
}

std::optional<Scene> reconstruct(
    Progress& progress,
    const Options& opts,
    const Camera& camera
)
{
    try
    {
        auto scene = Scene::fromMotion(
            camera,
            opts.reconstruction.source,
            opts.reconstruction.detector,
            Matching::Algorithm::GreedyTracking,
            progress
        );

        return std::make_optional(std::move(scene));
    }
    catch (const std::exception& err)
    {
        std::cerr << "\nErreur lors de la reconstruction : " << err.what()
                  << "\n";
        return std::optional<Scene>{};
    }
}

int output(const Options& opts, std::function<void(std::ostream&)> callback)
{
    if (opts.io.output_file == "-")
    {
        callback(std::cout);
        return EXIT_SUCCESS;
    }

    std::ofstream outfile{opts.io.output_file.string().c_str()};
    callback(outfile);

    if (outfile.fail())
    {
        std::cerr << "Erreur lors de l’écriture des résultats : "
                  << std::strerror(errno) << "\n";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
