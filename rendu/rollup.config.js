import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import alias from 'rollup-plugin-alias';
import vue from 'rollup-plugin-vue';

export default {
    input: 'src/main.js',
    output: {
        file: 'build/bundle.js',
        format: 'iife',
    },
    plugins: [

        vue(),
        alias({
            vue: 'node_modules/vue/dist/vue.esm.js'
        }),
        resolve(),
        commonjs(),
        replace({
            'process.env.NODE_ENV': JSON.stringify('development'),
            'process.env.VUE_ENV': JSON.stringify('browser')
        })
    ]
};
