﻿# Jeudi 15 février 2018

**Objectifs du jour :**
-   Modélisation du projet;
-   Découverte et prise en main de *Three.js*.
-   Étude et découverte de *NPM*;
-   Mise en place de l'environnement de travail au LIRMM.

## Modélisation

Nous avons commencé par lister les fonctionnalités que l'on souhaite intégrer à notre application.

Nous avons réalisé un premier schéma qui résume ce que doit être capable de faire l'utilisateur :

![Liste de ce que doit pourvoir faire l'utilisateur](./2018-02-15/feature.jpg)

Nous avons ensuite réalisé un début de diagramme d’activité.

![diagramme d'activité](./2018-02-15/activite.jpg)

# Prise en main de *Three.js*

Prise en main du *Three.js* à l'aide de la documentation.

[Creating a scene](https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene).

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>My first three.js app</title>
		<style>
			body { margin: 0; }
			canvas { width: 100%; height: 100% }
		</style>
	</head>
	<body>
		<script src="bundle.js"></script>
	</body>
</html>
```

```js
import * as THREE from 'three';

// scène
let scene = new THREE.Scene();


// caméra
let camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000);

camera.position.z = 5;


// rendu
let renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.shadowMap.enabled = true;

// cube
var geometry = new THREE.BoxGeometry(1, 1, 1);
var material = new THREE.MeshBasicMaterial({color: 0x00a0aa});
var cube = new THREE.Mesh(geometry, material);
scene.add(cube);


// render loop
function animate()
{
    requestAnimationFrame(animate);
    renderer.render(scene, camera);

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    cube.rotation.z += 0.01;
}
animate();
```

![Premier cube avec Three.js](./2018-02-15/hello-word.png)

Etude de la documentation concernant le chargement d'un modèle au format .obj

# étude de *NPM*

Voici les explications sur un tableau.

![Premier cube avec Three.js](./2018-02-15/npm1.jpg)
![Premier cube avec Three.js](./2018-02-15/npm2.jpg)

## Préparation environnement de travail au LIRMM
-   Mise en place d'un PC
-   Création d'une session
-   Installation des outils et clonage du repos *git*

#jeudi 22 février

## Explication de NVM
Node Version Manage (NVM) est le gestionnaire de versions de node. Il permet d'installer, supprimer et changer de version de node très facilement.

[github](https://github.com/creationix/nvm)

Nous avons utilisé cet outil afin d'installer node.js

## Chargement d'objet .obj

Découverte et utilisation de 2 bibliothèques JavaScript afin d'ouvrir un fichier `.obj` : OBJloader2, loadersupport, MTLloader.

![chargement de l'objet house.obj](./2018-02-22/house.png)


## Test sur les lumières et ombres


```js
import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

// scène
let scene = new THREE.Scene();


// caméra
let camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000);

camera.position.set(0, 10, 4);
camera.up = new THREE.Vector3(0, 0, 1);


// rendu
let renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.shadowMap.enabled = true;
// renderer.shadowMapSoft = true; // anti aliasing
// renderer.shadowMap.type = THREE.PCFSoftShadowMap;


// ambiant light
var ambiantLight = new THREE.AmbientLight(0x303030);
scene.add(ambiantLight);


//Create a DirectionalLight and turn on shadows for the light
var light = new THREE.DirectionalLight(0xffffff, 1, 100);
light.position.set(6, 6, 8);
light.target.position.set(0, 0, 0);
light.castShadow = true;

light.shadowDarkness = 0.5;

light.shadowCameraNear = 0;
light.shadowCameraFar = 20;

light.shadowCameraLeft = -5;
light.shadowCameraRight = 5;
light.shadowCameraTop = 5;
light.shadowCameraBottom = -5;

scene.add(light);

// plan
var planeGeometry = new THREE.PlaneGeometry(20, 10);
var planeMaterial = new THREE.MeshPhongMaterial({
    color: 0x00e676,
    side: THREE.DoubleSide
});
var plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.position.set(0, 0, 0);
plane.receiveShadow = true;
scene.add(plane);


// cube
var cubeGeometry = new THREE.BoxGeometry(2, 2, 2);
var cubeMaterial = new THREE.MeshLambertMaterial({color: 0x0000ff});
var cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.set(0, 0, 2);
cube.castShadow = true;
cube.receiveShadow = true;
scene.add(cube);

// cone
var ConeGeometry = new THREE.ConeGeometry(1.5, 4);
ConeGeometry.openEnded = true;
var ConeMaterial = new THREE.MeshLambertMaterial({color: 0xffff00});
var cone = new THREE.Mesh(ConeGeometry, ConeMaterial);
cone.position.set(4, 1, 2);
cone.rotation.x = Math.PI / 2;
cone.castShadow = true;
cone.receiveShadow = true;
scene.add(cone);

//Permet de contrôler la caméra à l'aide de la souris
//clic gauche pour "rotate"
//clic droit pour "pan"
var controls = new OrbitControls(camera);

// render loop
function animate()
{
    controls.update();
    requestAnimationFrame(animate);
    renderer.render(scene, camera);

    // cube.rotation.x += 0.01;
    // cube.rotation.y += 0.01;
    cube.rotation.z += 0.01;
}
animate();
```
![Test sur les ombres et lumières.obj](./2018-02-22/shadow.png)


## Contrôle de caméra

Utilisation de la bibliothèque `trackballcontrols.js`. Cependant le résultat obtenu ne correspond pas à nos attentes.

On observe une déformation du rendu et le mouvement de caméra est dificile.


#Jeudi 1 Mars


## Explication du fonctionnement de npm
npm est un gestionnaire de paquets officiel pour node.js. Il permet de gérer les dépendances de l'application et d'installer des applications Node.js disponibles sur le dépôt [npm](https://www.npmjs.com/).

fichier de configuration :
```json
{
  "name": "rendu",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "lint": "eslint .",
    "build": "rollup -c"json
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "dat.gui": "^0.7.0",
    "three": "^0.89.0",
    "three-orbitcontrols": "^2.0.0",
    "wwobjloader2": "^2.3.1"
  },
  "devDependencies": {
    "eslint": "^4.17.0",
    "rollup": "^0.55.5",
    "rollup-plugin-commonjs": "^8.3.0",
    "rollup-plugin-node-resolve": "^3.0.3",
    "tape": "^4.8.0"
  }
}
```

Nous avons fusionné les différents tests réalisés. Nous avons également unifié le style syntaxique et défini nos conventions de code.Nous avons installé et configuré le linter `eslint`.

Les biliotheque que l'on utilisait était jusque positionnées de manière statique dans le dépot. Afin de simplifier la gestion des dépendances, nous avons cherché et mis en place les bibliothèque équivalentes disponibles sur NPM.

## Chargement d'objet au format .json

Nous n'avons pas trouver de bibliothèque correspandant a nos attente permettant d'ouvrire un objet au format .obj sur npm.

Nous avons pour le moment mise en place un systeme permettant d'ouvrir les objet au format .json géré nativement par three.js. Nous avons discuter de ce problème avec l'équipe 2 qui sera capable de nous fournir le modèle dans le format .json.

Cepandant nous réfléchissons a un moyen pour notre application d'ouvrir un fihier .obj car c'est un format universelle, alors que le format .json n'est compatible que avec three.

## prochain objectif

-   Séparation des fichiers
-   Outils lumière
-   Outils mesure (si réalisable)
-   Commencer la rédaction du rapport

# Jeudi 8 mars

Présentation du travail réaliser, explication des premier schéma de modélisation (sur le tableau).

A faire :
-   explorer l'API de dat.gui voir si on peut faire du dynamique
-   Formaliser la modélisation actuelle
-   bien choisir, formaliser les différente structure de données pour les lumières et les objet notament.
-   séparé le code proprement

