﻿
# Modélisation

Jusqu'à présent nous avons réalisé différents tests avec Threejs. Cependant l'ensemble de notre code était regroupé en un seul fichier.

Cela nous pose un probleme de lecture, de clarté du code et ne nous permet pas de le rendre facilement dynamique pour réaliser l'interface graphique.

A ce moment-là nous ne savions plus comment avancer dans le projet. Nous avons donc imaginé, découpé et organisé notre code en différents fichiers.

Nous avons dans un premier temps listé les différents objets manipulés par notre application :
-   modèles
-   caméra
-   scène
-   directionalLight
-   ambiantLight

L'objectif est de séparer les reponsablités de chacun. Voici le schéma non détaillé de notre modélisation.

![Premier brouillon de modélisation](./2018-04/brouM.jpg)

## Caméra

Encapsule la caméra de three et est responsable de son intéraction avec l'utilisateur.

Voici le debut de *Camera.js*

```js
import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

const DEFAULT_VIEW_ANGLE = 50;
const DEFAULT_FAR = 100;
const DEFAULT_NEAR = 0.1;

/**
 * Crée une caméra.
 *
 * @param {Float} ratio de l'affichage.
 * @return {Camera} une caméra.
 */
const Camera = (ratio) => {

    // Caméra THREE
    const threeCamera = new THREE.PerspectiveCamera(
        DEFAULT_VIEW_ANGLE,
        ratio,
        DEFAULT_FAR,
        DEFAULT_NEAR
    );

    // contrôle de la caméra
    const orbitControls = new OrbitControls(threeCamera);

    return {

        /**
         * Met à jour la position de la caméra selon les interactions
         * de l'utilisateur
         */
        controlsUpdate() {
            orbitControls.update();
        },

        /**
         * Met à jour le ratio de la caméra
         */
        cameraUpdateRatio(newRatio) {
            threeCamera.aspect = newRatio;
            threeCamera.updateProjectionMatrix();
        }
    };

};

export default Camera;
```

## Modèle

Cette classe représente un modèle. Elle contient le modèle mais aussi toutes les informations qui lui sont rattachées, telles que son titre ou une description.

C'est également cette classe qui est responsable du chargement du modèle à partir d'un fichier.

## Lumière

Responsable de la lumière et des helpers associés. Il nous reste encore du travail de modélisation pour cette classe, notamment au niveau de l'héritage.

## Scène

Représente une scène et est reponsable du rendu.

Elle contient une caméra, un modèle ainsi qu'un tableau de lumières.

C'est également cette classe qui gère la redimension de la fenêtre.

Voici le début de *Scène.js*

```JS
import * as THREE from 'three';
import Camera from './Camera.js';

/**
 * Crée une scène.
 *
 * @param {HtmlElement} dom Élement dans lequel est calculé le rendu.
 * @return {Scene} une scène.
 */
const Scene = (dom) => {

    /**
     * Calcule le ratio de l'élement HTML.
     *
     * @return {Float} ratio de l'élement HTML.
     */
    const ratio = () => {
        const rect = dom.getBoundingClientRect();
        return rect.width / rect.height;
    };

    const camera = Camera(ratio());

    const scene = new THREE.Scene();

    const renderer = new THREE.WebGLRenderer();
    dom.appendChild(renderer.domElement);

    /**
     * Met à jour le rendu et la caméra à partir de la taille de dom.
     */
    const resizeListener = () => {
        camera.cameraUpdateRatio(ratio());
        const rect = dom.getBoundingClientRect();
        renderer.setSize(rect.width, rect.height);
    };


    resizeListener();
    // Redimentionne le rendu lors de la mise a jour de la taille de dom
    window.addEventListener('resize', resizeListener, false);



    return {
        /**
         * Obtient la caméra de la scène.
         *
         * @return {Camera} Caméra de la scène.
         */
        get camera() {
            return camera;
        },

        /**
         * Met à jour le rendu et la caméra de la scène
         */
        update() {
            renderer.render(scene, camera);
            camera.controlsUpdate();
        },

        /**
         * Détruit les écouteurs d'évènements de la scène
         */
        dispose() {
            window.removeEventListener('resize', resizeListener);
        }
    };
};

export default Scene;
```



La tâche de modélisation fut assez complexe et nous a demandé un certain temps, mais elle fut nécessaire pour nous permettre d'avancer dans le projet.

Ce modèle nous permet d'ajouter facilement une interface graphique et simplifie grandement notre *main.js*.

```js
import Scene from './Scene';

const scene = Scene(document.body);

/**
 * Boucle de rendu
 */
const animate = () => {
    scene.update();
    requestAnimationFrame(animate);
};
```

# La programmation en JS

Afin de pouvoir réaliser les pseudo-classes que nous avons modélisées, il nous a fallu nous renseigner sur concept d'objet propre JS.

![Résumé exemple d'une pseodo classe avec prototype en JS](./2018-04/classe.jpg)

![Héritage simple JS](./2018-04/simHer.jpg)

![Héritage avec prototype JS](./2018-04/comHer.jpg)

# Dat.gui

Nous avons réalisé nos premiers tests avec dat.gui. Cependant cette bibliothèque ne nous offre pas assez de libertés pour réaliser l'interface que l'on souhaite. 

Nous avons donc decidé de tenter de réaliser l'interface des outils avec vue.js.

De plus Dat.gui ne possède pas de documentation clairement définie ce qui nous complique le travail.

# Chargement des objet .json

Nous n'avons pas réussi à charger des fichiers JSON avec leurs textures. Ce problème est maintenant résolu.

Voici un exemple :

![Modèle de Mario chargé à partir d'un fichier JSON](./2018-04/mario.png)

# Test vue.js

Nous avons réalisé les premiers tests avec vue.js. Il nous reste encore du travail à ce niveau.

Voici notre premier test mettant en avant les concepts de base de vue.js.

Voici le fichier HTML
```HTML
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Test vue</title>
    </head>
    <body>

        <div id="app">
            {{ message }}
        </div>

        <div id="app2">
            <span v-bind:title="message">
                Passez votre souris sur moi pendant quelques secondes
                pour voir mon titre lié dynamiquement !
                {{ test }}
            </span>
        </div>

        <div id="app3">
            <p v-if="seen"> Maintenant vous me voyez</p>
        </div>

        <div id="app4">
            <li v-for="todo in todos">
                {{ todo.text }}
            </li>
        </div>

        <div id="app5">
            <p>{{ message }}</p>
            <button v-on:click="reverseMessage">Message retourné</button>
        </div>

        <div id="app6">
            <p>{{ message }}</p>
            <input type="text" v-modeljjk ">
        </div>

        <script src="build/bundle.js"></script>
    </body>
</html>
```

Voici le fichier JS

```js
import Vue from 'vue';

const app = new Vue({
    el: '#app',
    data: {
        message : 'Hello word !'
    }
});

const app2 = new Vue({
    el: '#app2',
    data: {
        message: 'Vous avez affiché cette page le '
            + new Date().toLocaleString(),
        test: 'test'
    }
});

const app3 = new Vue({
    el: '#app3',
    data: {
        seen: true
    }
});

const app4 = new Vue({
    el: '#app4',
    data: {
        todos: [
            { text: 'apprendre javascript' },
            { text: 'apprendre vue' },
            { text: 'créer quelque chose de génial'}
        ]
    }
});

const app5 = new Vue({
    el: '#app5',
    data: {
        message: 'hello vue.js !'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('');
        }
    }
});

const app6 = new Vue({
    el: '#app6',
    data: {
        message: 'hello test'
    }
});
```

![Premier test de vueJS](./2018-04/vue.png)

## Prochains objectifs

-   Terminer la mise en place des classes
-   Adapter notre modélistion a vueJs
-   Mise en place de l'interface graphique
