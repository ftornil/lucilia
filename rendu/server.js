const express = require('express');
const serveStatic = require('serve-static');
const fs = require('fs');
const path = require('path');

const app = express();

console.log('http://127.0.0.1:3000');

// Chemin spécial pour obtenir la liste des fichiers
app.get('/test', (req, res) => {
    walk(path.join(__dirname, 'objects'), function (err, results) {

        // Cas d'erreur
        if (err) {
            res.status(500);
            res.end(JSON.stringify({
                erreur: err.message
            }));
        }

        // Cas de succès
        results = results.filter(el => el.split('.').pop() == 'json');

        console.log(results);
        res.end(JSON.stringify(results));
    });
});

// Si non chemin spécial alors renvoyer les fichiers statiques
app.use(serveStatic(__dirname));

app.listen(3000);

/*
 * Renvoie récursivement les chemins de fichier dans un dossier
 *
 * @entry dir dossier à explorer
 * @entry done fonction de callback à appeler une fois l'exploration terminée
 */
var walk = function (dir, done) {
    var results = [];

    fs.readdir(dir, function (err, list) {
        if (err) return done(err);

        var pending = list.length;

        if (!pending) {
            return done(null, results);
        }

        list.forEach(function (file) {
            file = path.resolve(dir, file);

            fs.stat(file, function (err2, stat) {

                if (stat && stat.isDirectory()) {
                    walk(file, function (err3, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};
