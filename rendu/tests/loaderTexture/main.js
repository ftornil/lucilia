import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

// Création de scène et de caméra
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
    100,
    window.innerWidth / window.innerHeight,
    0.1,
    1000);
camera.position.set(0, 0, 8);


//Permet de contrôler la caméra à l'aide de la souris
const controls = new OrbitControls(camera);
controls.autoRotate = false;

// Création de lumières pour éclairer la scène
const ambientLight = new THREE.AmbientLight(0x303030);
scene.add(ambientLight);

const directionalLight1 = new THREE.DirectionalLight(0xC0C090);
directionalLight1.position.set(0, 0, 10);
scene.add(directionalLight1);

const directionalLight2 = new THREE.DirectionalLight(0xC0C090);
directionalLight2.position.set(100, 50, -100);
scene.add(directionalLight2);

//Création du renderer et initialisation de sa taille
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.shadowMap.enabled = true;

//Permet de charger un objet .json
var objectLoader = new THREE.ObjectLoader();

// Initiate load of model
objectLoader.load(
    'objects/mario/mario.json',
    function (obj) {
        // commenté les 3 lignes suivante si ce n'est pas mario
        // car mario est gros^^
        obj.scale.set(0.1, 0.1, 0.1);
        obj.up.set(1, 1, 1);
        window.obj = obj;
        scene.add(obj);
    },
    function() {},
    function (err) {
        console.log('erreur de chargement : ', err);
    });


// cube
const cubeGeometry = new THREE.BoxGeometry(2, 2, 2);
const cubeMaterial = new THREE.MeshLambertMaterial({color: 0x0000ff});
const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.set(0, 0, -6);
cube.castShadow = true;
cube.receiveShadow = true;
scene.add(cube);

// helper
const helper = new THREE.DirectionalLightHelper(directionalLight1);
scene.add(helper);
const helper2 = new THREE.DirectionalLightHelper(directionalLight2);
scene.add(helper2);

// rendu
function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();
}
animate();
