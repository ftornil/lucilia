import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

// scène
let scene = new THREE.Scene();


// caméra
let camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000);

camera.position.set(0, 10, 4);
camera.up = new THREE.Vector3(0, 0, 1);


// rendu
let renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.shadowMap.enabled = true;
// renderer.shadowMapSoft = true; // anti aliasing
// renderer.shadowMap.type = THREE.PCFSoftShadowMap;


// ambiant light
var ambiantLight = new THREE.AmbientLight(0x303030);
scene.add(ambiantLight);


//Create a DirectionalLight and turn on shadows for the light
var light = new THREE.DirectionalLight(0xffffff, 1, 100);
light.position.set(6, 6, 8);
light.target.position.set(0, 0, 0);
light.castShadow = true;

light.shadowDarkness = 0.5;

light.shadowCameraNear = 0;
light.shadowCameraFar = 20;

light.shadowCameraLeft = -5;
light.shadowCameraRight = 5;
light.shadowCameraTop = 5;
light.shadowCameraBottom = -5;

scene.add(light);
window.light = light;
window.scene = scene;

// plan
var planeGeometry = new THREE.PlaneGeometry(20, 10);
var planeMaterial = new THREE.MeshPhongMaterial({
    color: 0x00e676,
    side: THREE.DoubleSide
});
var plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.position.set(0, 0, 0);
plane.receiveShadow = true;
scene.add(plane);


// cube
var cubeGeometry = new THREE.BoxGeometry(2, 2, 2);
var cubeMaterial = new THREE.MeshLambertMaterial({color: 0x0000ff});
var cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.set(0, 0, 2);
cube.castShadow = true;
cube.receiveShadow = true;
scene.add(cube);

// cone
var ConeGeometry = new THREE.ConeGeometry(1.5, 4);
ConeGeometry.openEnded = true;
var ConeMaterial = new THREE.MeshLambertMaterial({color: 0xffff00});
var cone = new THREE.Mesh(ConeGeometry, ConeMaterial);
cone.position.set(4, 1, 2);
cone.rotation.x = Math.PI / 2;
cone.castShadow = true;
cone.receiveShadow = true;
scene.add(cone);

//Create a helper for the shadow camera (optional)
var helper = new THREE.CameraHelper(light.shadow.camera);
scene.add(helper);

//Permet de contrôler la caméra à l'aide de la souris
//clic gauche pour "rotate"
//clic droit pour "pan"
var controls = new OrbitControls(camera);

// render loop
function animate()
{
    controls.update();
    requestAnimationFrame(animate);
    renderer.render(scene, camera);

    // cube.rotation.x += 0.01;
    // cube.rotation.y += 0.01;
    cube.rotation.z += 0.01;
}
animate();
