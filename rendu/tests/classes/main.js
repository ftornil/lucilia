import Scene from './Scene';

const scene = Scene(
    document.body,
    'objects/tea.json'
    // 'objects/mario/mario.json'
);

scene.addDirectionalLight(2, 2, 2);
scene.directionalLights[0].changeIntensity(3);
scene.directionalLights[0].changeColor(1, 0, 0);
scene.directionalLights[0].enableHelper(true);

/**
 * Boucle de rendu
 */
const animate = () => {
    scene.update();
    requestAnimationFrame(animate);
};

animate();

window.scene = scene;
