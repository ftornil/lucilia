import * as THREE from 'three';

const DEFAULT_COLOR = 0x303030;
const DEFAULT_INTENSITY = 0.8;

/**
 * Crée une lumière ambiante
 *
 * @param {Scene} scene dans laquelle sera ajoutee la lumière
 * @return {AmbientLight} une lumière ambiante
 */

const AmbientLight =  (scene) => {

    // AmbiantLight THREE
    const threeAmbientLight = new THREE.AmbientLight(
        DEFAULT_COLOR,
        DEFAULT_INTENSITY
    );
    scene.add(threeAmbientLight);

    return {

        /**
        * Change la couleur de la lumière.
        *
        * @param {Integer} r Quantité de couleur rouge.
        * @param {Integer} g Quantité de couleur vert.
        * @param {Integer} b Quantité de couleur bleu.
        */
        changeColor(r, g, b) {
            threeAmbientLight.color.r = r;
            threeAmbientLight.color.g = g;
            threeAmbientLight.color.b = b;
        },

        /**
        * Change l'intensité de la lumière.
        *
        * @param {Integer} intensity Intensité de la lumière.
        */
        changeIntensity(intensity) {
            threeAmbientLight.intensity = intensity;
        }
    };

};

export default AmbientLight;
