import * as THREE from 'three';
import Camera from './Camera.js';
import AmbientLight from './AmbientLight.js';
import DirectionalLight from './DirectionalLight.js';
import Model from './Model.js';

/**
 * Créé un scène.
 *
 * @param {HtmlElement} dom Élement dans lequel est calculé le rendu.
 * @param {String} path Chemin du modèle à charger.
 * @return {Scene} une scène.
 */
const Scene = (dom, path) => {

    /**
     * Calcule le ratio de l'élement HTML.
     *
     * @return {Float} ratio de l'élement HTML.
     */
    const ratio = () => {
        const rect = dom.getBoundingClientRect();
        return rect.width / rect.height;
    };

    const scene = new THREE.Scene();

    const camera = Camera(ratio(), dom);
    camera.setCameraPosition(0, 0, 20);

    const ambientLight = AmbientLight(scene);

    const directionalLights = [];

    const renderer = new THREE.WebGLRenderer();
    dom.appendChild(renderer.domElement);

    const modele = Model(scene, path);

    /**
     * Met a jour le rendu est la caméra à partir de la taille de dom.
     */
    const resizeListener = () => {
        camera.cameraUpdateRatio(ratio());
        const rect = dom.getBoundingClientRect();
        renderer.setSize(rect.width, rect.height);
    };


    resizeListener();
    // Redimentionne le rendu lors de la mise a jour de la taille de dom
    window.addEventListener('resize', resizeListener, false);

    return {
        /**
         * Obtient la caméra de la scène.
         *
         * @return {Camera} Caméra de la scène.
         */
        get camera() {
            return camera;
        },

        /**
         * Obtient la lumière ambiante de la scène.
         *
         * @return {AmbientLight} lumière ambiante de la scène.
         */
        get ambientLight() {
            return ambientLight;
        },

        /**
         * Obtient la liste des lumières directionelle de la scène.
         *
         * @return {DirectionalLights} lumière directionelle de la scène.
         */
        get directionalLights() {
            return directionalLights;
        },

        /**
         * Obtient le modèle de la scène.
         *
         * @return {Modele} Modèle de la scène.
         */
        get modele() {
            return modele;
        },

        /**
         * Ajoute une lumière directionnelle
         *
         * @param {Integer} x la position sur l'axe des x.
         * @param {Integer} y la position sur l'axe des y.
         * @param {Integer} z la position sur l'axe des z.
         */
        addDirectionalLight(x, y, z) {
            const light = DirectionalLight(scene);
            light.setPosition(x, y, z);
            directionalLights.push(light);
        },

        /**
         * Suprime une lumière directionnelle
         *
         * @param {Integer} light lumière a supprimer
         */
        removeDirectionalLight(light) {
            const indice = directionalLights.indexOf(light);
            if (indice == -1) {
                console.error(light, 'n\'appartient pas à la scène');
            } else {
                directionalLights[indice].remove();
                directionalLights.splice(indice, 1);
            }
        },

        /**
         * Met a jour le rendu et la camera de la scène
         */
        update() {
            renderer.render(scene, camera.threeCamera);
            camera.controlsUpdate();
        },

        /**
         * Détruit les écouteurs d'évènements de la scène
         */
        dispose() {
            window.removeEventListener('resize', resizeListener);
        }
    };
};

export default Scene;
