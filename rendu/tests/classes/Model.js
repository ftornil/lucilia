import * as THREE from 'three';

/**
 * Créé un modèle.
 *
 * @param {scene} ThreeScene Scène à laquelle appartient le modèle.
 * @param {String} pathFile Chemin du modèle à charger.
 * @return {Model} un model.
 */
const Model = (scene, pathFile) => {

    // loader THREE
    const objectLoader = new THREE.ObjectLoader();
    let object;

    /**
     * Charger un modèle.
     *
     * @param {String} pathFile Chemin du modèle à charger.
     */
    const loadModel = (path) => {
        objectLoader.load(
            path,
            function (obj) {
                scene.remove(object);
                object = obj;
                scene.add(object);
            },
            console.log('chargement de : ' + path),
            function (err) {
                console.log('erreur de chargement : ', err);
            }
        );
    };

    loadModel(pathFile);

    return {
        load(path) {
            loadModel(path);
        }
    };

};

export default Model;

// TODO gere le chargement et l'erreur proprement
