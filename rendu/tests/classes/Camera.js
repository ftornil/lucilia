import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

const DEFAULT_VIEW_ANGLE = 50;
const DEFAULT_NEAR = 0.1;
const DEFAULT_FAR = 200;

/**
 * Créé un caméra.
 *
 * @param {Float} ratio de l'affichage.
 * @return {Camera} une caméra.
 */
const Camera = (ratio, dom) => {

    // Caméra THREE
    const threeCamera = new THREE.PerspectiveCamera(
        DEFAULT_VIEW_ANGLE,
        ratio,
        DEFAULT_NEAR,
        DEFAULT_FAR
    );

    // controle de la caméra
    const orbitControls = new OrbitControls(threeCamera, dom);

    return {

        /**
         * Met a jour la position de la caméra selon les interactions
         * de l'uilisateur
         */
        controlsUpdate() {
            orbitControls.update();
        },

        /**
         * Met a jour le ration de la caméra
         */
        cameraUpdateRatio(newRatio) {
            threeCamera.aspect = newRatio;
            threeCamera.updateProjectionMatrix();
        },

        /**
         * Modifier la position de la caméra
         */
        setCameraPosition(x, y, z) {
            threeCamera.position.set(x, y, z);
        },

        /**
         * Getter de la threeCamera
         **/
        get threeCamera() {
            return threeCamera;
        }
    };

};

export default Camera;
