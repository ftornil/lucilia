import * as THREE from 'three';

const DEFAULT_COLOR = 0x303030;
const DEFAULT_INTENSITY = 0.5;

const DEFAULT_HELPER_SIZE = 1;

/**
 * Crée une lumière directionnelle
 *
 * @param {Scene} scene dans laquelle sera ajoutée la lumière
 * @return {DirectionalLight} une lumière directionnelle
 */

const DirectionalLight = (scene) => {

    // DirectionalLight THREE
    const threeDirectionalLight = new THREE.DirectionalLight(
        DEFAULT_COLOR,
        DEFAULT_INTENSITY
    );

    // Permet de visualiser plus facilement la lumière
    let threeHelper = new THREE.DirectionalLightHelper(
        threeDirectionalLight,
        DEFAULT_HELPER_SIZE);

    scene.add(threeDirectionalLight);
    scene.add(threeDirectionalLight.target);

    return {
        /**
        * Change la couleur de la lumière.
        *
        * @param {Integer} r Quantité de couleur rouge.
        * @param {Integer} g Quantité de couleur vert.
        * @param {Integer} b Quantité de couleur bleu.
        */
        changeColor(r, g, b) {
            threeDirectionalLight.color.r = r;
            threeDirectionalLight.color.g = g;
            threeDirectionalLight.color.b = b;
            threeHelper.update();
        },

        /**
        * Change l'intensité de la lumière.
        *
        * @param {Float} intensity l'intensité de la lumière.
        */
        changeIntensity(intensity) {
            threeDirectionalLight.intensity = intensity;
        },

        /**
        * Modifie la position de la lumière.
        *
        * @param {Integer} x la position sur l'axe des x.
        * @param {Integer} y la position sur l'axe des y.
        * @param {Integer} z la position sur l'axe des z.
        */
        setPosition(x, y, z) {
            threeDirectionalLight.position.set(x, y, z);
            threeDirectionalLight.updateMatrixWorld();
            threeHelper.update();
        },

        // TODO : Les ombre n'ont pas encoré été tester
        /**
        * Active les ombres si enable = true, les désactive sinon.
        *
        * @param {Boolean} enable pour activer/desactiver les ombres.
        */
        enableShadows(enable) {
            threeDirectionalLight.castShadow = enable;
        },

        /**
        * Active le helper.
        *
        * @param {Boolean} enable pour activer/désactiver le helper.
        */
        enableHelper(enable) {
            if (enable) {
                scene.add(threeHelper);
                threeHelper.update();
            } else {
                scene.remove(threeHelper);
            }
        },

        /**
        * modifier la cible de a lumière.
        *
        * @param {Integer} x la position sur l'axe des x.
        * @param {Integer} y la position sur l'axe des y.
        * @param {Integer} z la position sur l'axe des z.
        */
        target(x, y, z) {
            threeDirectionalLight.target.position.set(x, y, z);
            threeDirectionalLight.target.updateMatrixWorld();
            threeHelper.update();
        },

        /**
        * Supprimer la lumière de la scène.
        */
        remove() {
            threeHelper.dispose();
            scene.remove(threeHelper);
            scene.remove(threeDirectionalLight);
        }
    };
};

export default DirectionalLight;
