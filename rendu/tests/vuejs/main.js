import Vue from 'vue';

const app = new Vue({
    el: '#app',
    data: {
        message : 'Hello word !'
    }
});

const app2 = new Vue({
    el: '#app2',
    data: {
        message: 'Vous avez affiché cette page le '
            + new Date().toLocaleString(),
        test: 'test2'
    }
});

const app3 = new Vue({
    el: '#app3',
    data: {
        seen: true
    }
});

const app4 = new Vue({
    el: '#app4',
    data: {
        todos: [
            { text: 'apprendre javascript' },
            { text: 'apprendre vue' },
            { text: 'créer quelque chose de génial'}
        ]
    }
});

const app5 = new Vue({
    el: '#app5',
    data: {
        message: 'hello vue.js !'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('');
        }
    }
});

const app6 = new Vue({
    el: '#app6',
    data: {
        message: 'hello test'
    }
});

window.app = app;
window.app2 = app2;
window.app3 = app3;
window.app4 = app4;
window.app5 = app5;
window.app6 = app6;


app;
app2;
