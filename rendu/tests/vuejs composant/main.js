import Vue from 'vue';

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li> {{todo.text}} -- {{todo.id}} </li>'
});

var app7 = new Vue({
    el: '#app7',
    data: {
        groceryList:[
            {id: 0, text: 'Légumes'},
            {id: 1, text: 'Fromage'},
            {id: 2, text: 'Tout ce que les humains mange'}
        ]
    }
});

app7;
