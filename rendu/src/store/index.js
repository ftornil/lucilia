import * as light from './light';
import * as dirLight from './dirLight';
import debug from './debug';

export const state = {
    light: light.state,
    directionalLights: [],
    createLight: 0
};

export const methods = {
    light: light.methods,

    createLight() {
      state.createLight++;
    },

    addLight(newLight) {
      const lightState = dirLight.state;
      lightState.intensity = newLight.intensity;
      lightState.color.red = newLight.color.red;
      lightState.color.green = newLight.color.green;
      lightState.color.blue = newLight.color.blue;
      lightState.sphere = newLight.sphere;

      state.directionalLights.push(state);
    }
};
