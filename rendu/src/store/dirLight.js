import debug from './debug';

export const state = {
    intensity: 0.8,
    color: {
        redValue: 255,
        greenValue: 255,
        blueValue: 255
    },
    sphere: null,
    castShadow: false
};

export const methods = {
    setColor(next) {
        const {red, green, blue} = next;
        console.log('AV :', state.color, 'AP : ', next);
        state.color.redValue = red;
        state.color.greenValue = green;
        state.color.blueValue = blue;
    },

    setIntensity(next) {
      state.intensity = next.intensity;
    },

    setSphere(lightSphere) {
      state.sphere = lightSphere;
    },

    enableShadow(enable) {
      state.castShadow = enable;
    }
};
