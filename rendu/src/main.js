import Vue from 'vue';
import App from './App.vue';


// Création de la vue de l'application, avec le composant principal App
new Vue({
    el: '#test',
    components: App,
    template: '<App/>'
});
