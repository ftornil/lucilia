# Lucilia — Numérisation automatique d’une scène en trois dimensions

_Lucilia_ (du genre de diptères éponyme) est un ensemble de projets ayant pour objectifs l’automatisation de la reconstruction d’un modèle en trois dimensions d’une scène à partir de photographies prises par un drone et la création d’une application pour visualiser et réaliser des mesures sur ce modèle.

![Vue d’ensemble du projet](docs/overview.png)

## Projets

### Programmation d’un drone en vue de récolter des données

Afin de numériser la scène observée, le processus de reconstruction utilise un ensemble de photographies dont la distance séparant chaque couple est connue.
Ce processus est similaire à la façon dont un observateur humain fait le tour d’un objet pour en apprécier la profondeur.
L’objectif de ce projet est d’automatiser cette récolte d’informations à l’aide d’un drone, en calculant une trajectoire lui permettant d’avoir une vue sur l’ensemble de la scène et en le programmant pour prendre des photographies et mesurer la distance parcourue.

[Voir le projet »](recolte)

| Équipes ayant travaillé sur le projet                            | Année       |
| ---------------------------------------------------------------- | ----------- |
| Amandine Paillard, Alexandre Rouyer, _Ahmed Chemori (encadrant)_ | 2017 – 2018 |

### Traitement d’images pour le calcul d’un modèle 3D d’une scène

À partir d’une séquence d’images dont les champs de vue se chevauchent deux à deux et dont la distance parcourue de l’une à l’autre est connue, l’objectif de ce projet est de reconstruire de façon automatisée un modèle en trois dimensions fidèle à la scène qui a été observée, sous la forme d’un maillage de polygones.

[Voir le projet »](analyse)

| Équipes ayant travaillé sur le projet                              | Année       |
| ------------------------------------------------------------------ | ----------- |
| Mattéo Delabre, Thibault De Villèle, _Hinde Bouziane (encadrante)_ | 2018 – 2019 |
| Mattéo Delabre, Florent Tornil, _Hinde Bouziane (encadrante)_      | 2017 – 2018 |

### Construction d’une application pour exploiter les modèles

Dans ce projet, l’objectif est de permettre l’exploitation des modèles en trois dimensions en construisant une application web.
Cette application permet de visualiser de façon interactive l’ensemble du modèle et de prendre certains types de mesures.

[Voir le projet »](rendu)

| Équipes ayant travaillé sur le projet                            | Année       |
| ---------------------------------------------------------------- | ----------- |
| Maëlle Beuret, Rémi Cérès, _Nancy Rodriguez (encadrante)_        | 2017 – 2018 |
