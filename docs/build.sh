#!/usr/bin/env bash

##
# Construction des documents LaTeX du projet
##

root="docs"
build="build"
file_ext=".tex"
file_header="%! TEX program"

# Rend les commandes pushd/popd silencieuses
function pushd
{
    command pushd "$@" > /dev/null
}

function popd
{
    command popd > /dev/null
}

# Obtient la taille sur le disque d’un fichier
function get_file_size
{
    stat --printf="%s" "$1"
}

# Réduit la taille d’un fichier PDF avec Ghostscript
function optimize_pdf
{
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
        -dPDFSETTINGS=/printer -dDetectDuplicateImages=true \
        -dNOPAUSE -dQUIET -dBATCH \
        -sOutputFile="$2" "$1"
}

if [ ! -d "$root" ]
then
    echo "La racine des documents n’existe pas !"
    echo "Le script doit être lancé à la racine du projet."
    exit
fi

rm -rf "${root:?}/$build"
mkdir -p "$root/$build"

# Recherche tous les documents LaTeX pouvant être compilés
grep -rlZ "$root" -e "$file_header" --include "*$file_ext" \
| while read -rd '' file
do
    dir="$(dirname "$file")"
    name="$(basename "$file" "$file_ext")"
    id="${dir//\//:}:$name.pdf"

    output="$name.pdf"
    opti_output="__opti__.pdf"

    pushd "$dir" || exit
    rm -rf "$build"

    echo "[$id] Début de la compilation XeLaTeX"
    latexmk "$name" -outdir="$build" -xelatex || {
        echo "[$id] Erreur de compilation"
        exit 1
    }

    pushd "$build" || exit
    echo "[$id] Optimisation du PDF"
    optimize_pdf "$output" "$opti_output"

    # Conserve la version la plus légère entre la version
    # optimisée et la version originale
    size="$(get_file_size "$output")"
    opti_size="$(get_file_size "$opti_output")"

    if (( size > opti_size ))
    then
        mv "$opti_output" "$output"
    else
        echo "[$id] Impossible de réduire la taille du PDF"
        rm "$opti_output"
    fi

    popd || exit
    popd || exit

    # Déplace le PDF résultant dans le répertoire de construction
    cp "$dir/$build/$output" "$root/$build/$id"
done
