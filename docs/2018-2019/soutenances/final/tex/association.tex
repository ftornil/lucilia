\section{Association des points caractéristiques correspondants}
\frame{\tableofcontents}

\begin{frame}[fragile]{Points caractéristiques}
\begin{minipage}{.45\textwidth}
    \begin{itemize}
        \item \textbf{Point caractéristique~:} points possédant une localisation précise dans l’image.
        \item \enquote{Coins} de l’image, où l’intensité varie localement dans 2~directions.
        \item Algorithmes de détection~:\\Harris, SIFT, SURF, …
    \end{itemize}
\end{minipage}\hfill%
\begin{minipage}{.5\textwidth}
    \raggedleft
    \input{fig/association/table}
\end{minipage}
\end{frame}

\begin{frame}{Décider si deux points se correspondent (1/2)}
\begin{minipage}{.55\textwidth}
    \textbf{Heuristique~:} deux points se correspondent si leurs voisinages se ressemblent.

    \vspace{1em}
    \begin{itemize}
        \item Différence pixel par pixel~?
    \onslide<2->{
        \begin{itemize}
            \item[\xmark] Variation d’intensité et de contraste.
        \end{itemize}
        \item Corrélation des intensités~?
    }
    \onslide<3->{
        \begin{itemize}
            \item[\xmark] Variation de rotation.
            \item[\xmark] Variation d’échelle.
        \end{itemize}
        \item[\cmark] Intégrer toutes ces possibilités dans un vecteur~: \textbf{descripteur}.
    }
    \end{itemize}
\end{minipage}\hfill%
\begin{minipage}{.4\textwidth}
    \centering%
    \includegraphics{fig/association/association-ref.png}\hspace{5pt}%
    \includegraphics{fig/association/association-brightness.png}\\[5pt]%
\onslide<2->{%
    \includegraphics{fig/association/association-scale.png}\hspace{5pt}%
    \includegraphics{fig/association/association-rotate.png}%
}%
\end{minipage}
\end{frame}

\begin{frame}{Décider si deux points se correspondent (2/2)}
\begin{minipage}{.55\textwidth}
    \begin{itemize}
        \item Distance entre descripteurs $\equiv$\\\enquote{non ressemblance} de points.
        \item \textbf{En deçà de quelle distance y a-t-il correspondance~?}
    \end{itemize}
\only<1-2>{
    \begin{itemize}
        \item Fixer un plafond arbitraire ?
        \begin{itemize}
            \item[\xmark] Motifs répétés $\Rightarrow$ tous les points se correspondent~: aberration physique.
        \end{itemize}
    \onslide<2>{
        \item[\cmark] Éliminer les motifs répétés en relativisant à la distance au point le plus proche dans la même image.
    }
    \end{itemize}
}
\only<3>{
    \vspace{2em}
    \[
        \frac
            {\Dissimilarity(x, y)}
            {\min_{x' \in \var{image}(x)}{\Dissimilarity(x, x')}}
        \leq
        \ratioedge
    \]
}
\end{minipage}\hfill%
\begin{minipage}{.3\textwidth}
    \centering%
    \includegraphics[width=\linewidth]{fig/association/ceylan-1}

    \includegraphics[width=\linewidth]{fig/association/ceylan-2}
\end{minipage}
\end{frame}

\begin{frame}{Regrouper les points qui se correspondent}
\begin{minipage}{\dimexpr\textwidth-4cm}
    \begin{def-problem}
        \DefProblemName{\PbMatching}
        \DefProblemInput{Flux de $n$ images contenant chacune $m$ points}
        \DefProblemOutput{Partition en groupes de points correspondants}
    \end{def-problem}

    \textbf{Critères sur la partition~:}

    \begin{enumerate}
        \item Aucun groupe n’a deux points de la même image.
        \item Chaque point est dans exactement un groupe.
        \item Les points d’un groupe se correspondent.
        \item Le nombre de groupes est minimisé.
    \end{enumerate}
\end{minipage}\hfill%
\begin{minipage}{3cm}
    \input{fig/association/cluster-example}
\end{minipage}
\end{frame}

\begin{frame}{Première approche}{Recherche locale}
\begin{framed}
    \begin{enumerate}
        \item Initialement, chaque point est dans son groupe.
        \item Pour chaque paire d’images $(i, i+1)$ et chaque point $x$ de $i$~:
        \begin{enumerate}
            \item Soit $y$ le plus ressemblant à $x$ dans $j$.
            \item Fusionner les groupes de $x$ et $y$ s’ils sont compatibles~:
            \begin{itemize}
                \item s’ils concernent deux \textbf{ensembles d’images disjoints~;}
                \item si tous les points du groupe fusionné \textbf{se ressemblent deux à deux.}
            \end{itemize}
        \end{enumerate}
        \item Renvoyer les groupes ainsi formés.
    \end{enumerate}
\end{framed}
\begin{center}
    \textbf{Complexité~:} $\UpBound(nm^2)$
\end{center}
\end{frame}

\begin{frame}{Première approche}{Limites}
\begin{minipage}{.5\textwidth}
    \centering
    \input{fig/association/tracking-not-optimal}

    \vspace{1em}
    \textbf{Contre-exemple pour $\ratioedge = \frac{2}{3}$}
\end{minipage}\hfill%
\begin{minipage}{.4\textwidth}
    \begin{itemize}
        \item Fusionner une paire peut empêcher des choix plus avantageux par la suite.
        \item En cas d’occlusion, deux groupes distincts seront formés de part et d’autre.
    \end{itemize}
\end{minipage}
\end{frame}

\begin{frame}{Travail réalisé cette année}
    \begin{itemize}
        \item Étude d’une autre approche pour le regroupement des points.
        \item Implémentation du nouvel algorithme.
        \item Comparaison expérimentale des deux algorithmes.
    \end{itemize}
\end{frame}

\begin{frame}{Seconde approche}{Groupement générique~: QuickShift}
\begin{minipage}{.5\textwidth}
    \begin{itemize}
        \item Algorithme de groupement \emph{(clustering)} générique.
        \item \textbf{Idée~:} modes de la densité $=$\\points de référence.
        \item \textbf{Hiérarchie~:} parent d’un point $=$ plus proche de densité supérieure dans un rayon \emph{(hill-climbing).}
        \item Donne une forêt où chaque arbre est un groupe.
    \end{itemize}
\end{minipage}\hfill%
\begin{minipage}{.45\textwidth}
    \input{fig/association/quickshift-example}
\end{minipage}
\end{frame}

\begin{frame}{Seconde approche}{Adaptation à la correspondance~: QuickMatch}
    \begin{itemize}
        \item QuickShift dans l’espace des descripteurs.
        \item Voisinage non restreint contrairement à QuickShift.
        \item Après avoir calculé tous les parents on élimine les arêtes trop longues.
    \end{itemize}
    \begin{center}
        \textbf{Complexité~:} $\UpBound(n^2m^2)$ --- parallélisable
    \end{center}

    \slidecite{tron-clustering-2017}
\end{frame}

\begin{frame}{Évaluation expérimentale}
    \begin{minipage}{.475\textwidth}
        \begin{itemize}
            \item Jeu de 59~photographies prises autour du bâtiment~44.
            \item Difficultés~:
            \begin{itemize}
                \item végétation en fond~;
                \item ombres marquées~;
                \item structures répétées.
            \end{itemize}
            \item Méthodologie~:
            \begin{itemize}
                \item exécution de chaque algorithme~;
                \item échantillon aléatoire de 30~correspondances~;
                \item vérification manuelle.
            \end{itemize}
        \end{itemize}
    \end{minipage}\hfill%
    \begin{minipage}{.35\textwidth}%
        \hspace*{-6pt}
        \begin{tikzpicture}
            \node {\includegraphics[width=\textwidth]{fig/association/triolet-plan}};

            \node[
                circle,
                label={[fill=white,yshift=5pt]above:{\strut}Lieu des prises},
                draw=red!80!black,
                very thick,
            ] at (-.675, .6) {};

            \node[
                circle,
                label={[fill=white,yshift=5pt]above:{\strut}Vous êtes ici},
                draw=green!80!black,
                very thick,
            ] at (2, .3) {};
        \end{tikzpicture}

        \includegraphics[width=.3\textwidth]{fig/association/triolet-exemple-1}\hfill%
        \includegraphics[width=.3\textwidth]{fig/association/triolet-exemple-2}\hfill%
        \includegraphics[width=.3\textwidth]{fig/association/triolet-exemple-3}
    \end{minipage}
\end{frame}

\begin{frame}{Évaluation expérimentale}{Précision (\%)}
\begin{minipage}{.425\textwidth}
    \centering
    \textsc{Recherche locale}

    \vspace{1em}
    \begin{tikzpicture}
        \begin{axis}[
            xlabel=$\ratioedge$,
            width=\linewidth,
        ]
            \addplot table {fig/association/tracking-results-cluster-quality};
        \end{axis}
    \end{tikzpicture}
\end{minipage}\hfill%
\begin{minipage}{.575\textwidth}
    \centering
    \textsc{Segmentation densitaire}

    \vspace{1em}
    \begin{tikzpicture}
        \begin{axis}[
            axis on top,
            xlabel=$\ratiodensity$,
            xmin=0.2,
            xmax=1,
            ylabel=$\ratioedge$,
            ymin=0.2,
            ymax=2,
            width=.7\linewidth,
            height=5cm,
            point meta=explicit,
            colorbar,
            colormap/viridis,
            colorbar style={
                scaled ticks=false,
                tick label style={/pgf/number format/fixed},
            },
        ]
            \addplot [
                matrix plot*,
            ] table [meta index=2]
            {fig/association/quickmatch-results-cluster-quality};
        \end{axis}
    \end{tikzpicture}
\end{minipage}
\end{frame}

\begin{frame}{Évaluation expérimentale}{Trajectoire reconstruite}
\begin{minipage}{.33\textwidth}
    \centering
    \textsc{Recherche locale}

    \vspace{1em}
    \begin{tikzpicture}
        \begin{axis}[
            ticks=none,
            width=1.25\textwidth,
            scatter/classes={%
                start={fill=green, thick, mark size=3pt},%
                mid={fill=blue},%
                end={fill=red, thick, mark size=3pt}
            }
        ]
        \addplot[
            scatter,
            scatter src=explicit symbolic,
        ] table[meta=class]
                {fig/association/tracking-captures};
        \end{axis}
    \end{tikzpicture}
\end{minipage}\pause\hfill%
\begin{minipage}{.33\textwidth}
    \centering
    \textsc{Segmentation densitaire}

    \vspace{1em}
    \begin{tikzpicture}
        \begin{axis}[
            ticks=none,
            width=1.25\linewidth,
            scatter/classes={%
                start={fill=green, thick, mark size=3pt},%
                mid={fill=blue},%
                end={fill=red, thick, mark size=3pt}
            }
        ]
        \addplot[
            scatter,
            scatter src=explicit symbolic,
        ] table[meta=class]
                {fig/association/quickmatch-captures};
        \end{axis}
    \end{tikzpicture}
\end{minipage}\pause\hfill%
\begin{minipage}{.33\textwidth}
    \centering
    \textsc{OpenSfM}

    \vspace{1em}
    \begin{tikzpicture}
        \begin{axis}[
            ticks=none,
            width=1.25\textwidth,
            scatter/classes={%
                start={fill=green, thick, mark size=3pt},%
                mid={fill=blue},%
                end={fill=red, thick, mark size=3pt}
            }
        ]
        \addplot[
            scatter,
            scatter src=explicit symbolic,
        ] table[meta=class]
                {fig/association/opensfm-captures};
        \end{axis}
    \end{tikzpicture}
\end{minipage}
\end{frame}

\begin{frame}{Discussion}
    \begin{minipage}[t]{.45\linewidth}
        \textbf{Récapitulatif}

        \vspace{1em}
        \begin{itemize}
            \item Algorithme QuickMatch meilleur sur le jeu de test.
            \item Permet d’obtenir une \textbf{trajectoire plus cohérente.}
            \item Reste problème de \textbf{dérive} de la trajectoire.
            \item \textbf{Coût plus important} en complexité.
        \end{itemize}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.45\linewidth}
        \vspace*{-10pt}
        \textbf{Perspectives}

        \vspace{1em}
        \begin{itemize}
            \item Réduire la complexité~: recherche parent dans une fenêtre restreinte de photos voisines.
            \item Augmenter la précision~: segmentation des images (p.~ex.~élimination du ciel).
            \item Corriger la dérive~: ajustement par faisceaux.
        \end{itemize}
    \end{minipage}
\end{frame}
