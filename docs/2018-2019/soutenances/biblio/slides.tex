% vim: set spelllang=fr :
%! TEX program = xelatex

\documentclass[
    aspectratio=169,
    xcolor=table
]{beamer}
\usepackage{slides}

% Métadonnées
\title{Lucilia}
\subtitle[Traitement d’images pour le calcul d’un modèle~3D d’une scène]{Traitement d’images pour le calcul\\d’un modèle~3D d’une scène}
\author{Mattéo~\textsc{Delabre}}
\supervisor{\Madame~Hinde~\textsc{Bouziane}}
\institute{Soutenance de mi-projet CMI\\Projet annuel CMI M1 --- HMIN101I}
\date{25~janvier~2019}

% Sources importantes
\DeclareBibliographyCategory{major}
\addtocategory{major}{nister-2007-sfm-nphard}
\addtocategory{major}{lowe-2004-sift}
\addtocategory{major}{tron-clustering-2017}
\addtocategory{major}{yu-2016-pgsr}

\begin{document}

\frame[plain]{\titlepage}
\frame{\tableofcontents}

\section{Présentation du sujet}

\begin{frame}{Le projet Lucilia}
    \centering
    \input{fig/introduction/objectifs}

    \vspace{1em}
    \textbf{Applications}

    \vspace{-1em}
    \begin{framed}
    \begin{minipage}[t]{.5\linewidth}
        \begin{itemize}
            \item Réalité augmentée.
            \item Navigation terrestre et maritime.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}[t]{.5\textwidth}
        \begin{itemize}
            \item Reconnaissance d’objets.
            \item Sauvegarde du patrimoine.
        \end{itemize}
    \end{minipage}
    \end{framed}
\end{frame}

\begin{frame}{Numérisation automatique d’une scène}
    \begin{minipage}[t]{.5\textwidth}
        \begin{minipage}[t]{.6\linewidth}
            \centering
            \textbf{Avec contact}\\
            Bras de palpation

            \begin{itemize}
                \item Grande précision.
                \item Objet par objet.
                \item Risque d’altération.
            \end{itemize}
        \end{minipage}\hfill
        \begin{minipage}[t]{.35\linewidth}
            \vspace{-1em}
            \includegraphics[width=\linewidth]
            {fig/introduction/numerisation-contact}
        \end{minipage}
    \end{minipage}\pause\hfill%
    \begin{minipage}[t]{.5\textwidth}
        \begin{minipage}[t]{.35\linewidth}
            \vspace{-1em}
            \includegraphics[width=\linewidth]
            {fig/introduction/numerisation-sans-contact}
        \end{minipage}\hfill
        \begin{minipage}[t]{.6\linewidth}
            \centering
            \textbf{Sans contact}\\
            Ondes sonores ou électromagnétiques

            \begin{itemize}
                \item Scène complète.
                \item Moins sophistiqué.
                \item Moins de précision.
            \end{itemize}
        \end{minipage}
    \end{minipage}
\end{frame}

\begin{frame}{Résultats de l’an dernier dans le cadre de~Lucilia}
    \emph{Numérisation du bâtiment~44 avec une prise de photos manuelle.}
    \begin{minipage}{.4\textwidth}
        \includegraphics[width=\linewidth]{fig/introduction/bat-44-source}
    \end{minipage}\hfill%
    \begin{minipage}{.55\textwidth}
        \includegraphics[width=\linewidth]{fig/introduction/bat-44-results}
    \end{minipage}
\end{frame}

\begin{frame}{Limites et objectifs}
    \begin{itemize}
        \item Accroître la précision du modèle~:
        \begin{itemize}
            \item la trajectoire manque de continuité~;
            \item le bâtiment est déformé.
        \end{itemize}
        \item Estimer le taux d’erreur du modèle.
        \item Parallélisation et/ou distribution du calcul.
        \item Génération d’une surface avec texture.
    \end{itemize}

    \textbf{Dans cette présentation~:} amélioration de la précision du modèle.
\end{frame}

\section{Reconstruction de scène par recoupage de photographies}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Acquisition de structure par mouvement (\RefProblem{\PbSfM})}{Présentation}
    \begin{itemize}
        \item Transformer un flux d’images en modèle~3D.
        \item \textbf{Idée clef~:} parties chevauchantes des images $\Longrightarrow$ points communs.
        \item Points communs $\Longrightarrow$ \textbf{contraintes épipolaires}.
        \item Déduction de la trajectoire du drone et de la position des points observés.
    \end{itemize}
    \pause
    \begin{block}{Théorème}
        S’il existe des points dans la scène qui ne sont pas toujours visibles, alors \RefProblem{\PbSfM} est {\NPHardClass} (réduction de \RefProblem{\PbSAT} à \RefProblem{\PbSfM}).
    \end{block}
    \begin{center}
        \slidecite{nister-2007-sfm-nphard}
    \end{center}
\end{frame}

\begin{frame}{Acquisition de structure par mouvement (\RefProblem{\PbSfM})}{Processus}
    \centering

    \resizebox{.9\textwidth}{!}{\input{fig/reconstruction/pipeline}}

    \vfill
    \slidecite{forstner-2016-photogrammetric}
\end{frame}

\section{Couplage des points communs à deux images}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Présentation du problème}
    \begin{def-problem}
        \DefProblemName{\PbDM}
        \DefProblemInput{deux images et pour chacune un ensemble de points}
        \DefProblemOutput{ensemble de couples de points correspondants}
    \end{def-problem}
\end{frame}

\begin{frame}{Dissemblance entre points}
    \begin{minipage}{.75\textwidth}
        \begin{itemize}
            \item Points correspondants $\Longleftrightarrow$ \textbf{voisinages} ressemblants.
            \item Recherche d’une heuristique de dissemblance~:
            \begin{description}
                \item[sensible~:] deux voisinages distincts sont classifiés comme tels.
                \item[spécifique~:] deux voisinages ressemblants sont classifiés comme tels.
            \end{description}
            \item Trois approches~:
            \begin{itemize}
                \item distance pixel par pixel~;
                \item corrélation des intensités~;
                \item \textbf{distance de descripteurs.}
            \end{itemize}
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}{.2\textwidth}
        \centering
        \includegraphics{fig/association/neighborhood-1}
        \includegraphics{fig/association/neighborhood-2}

        \includegraphics{fig/association/neighborhood-3}
        \includegraphics{fig/association/neighborhood-4}

        \emph{Exemples de voisinages}
    \end{minipage}
    \vfill
    \begin{center}
        \slidecite{shakhnarovich-2005-similarity}
    \end{center}
\end{frame}

\begin{frame}{Comparatif des heuristiques}
    \centering
    \begingroup\midsepremove\footnotesize
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{%
            >{\raggedleft\arraybackslash}m{2.5cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}}
        &\textbf{Référence}
        &\textbf{Luminosité}
        &\textbf{Contraste}
        &\textbf{Point de vue}
        &\textbf{Autre objet}\\

        &\includegraphics{fig/association/patch-distance-ref}
        &\includegraphics{fig/association/patch-distance-lower-luminosity}
        &\includegraphics{fig/association/patch-distance-higher-contrast}
        &\includegraphics{fig/association/patch-distance-viewpoint-change}
        &\includegraphics{fig/association/patch-distance-other}\\[15pt]

        \toprule
        \TintMinValue{0} \TintMaxValue{2792120}
        \textbf{Distance pixel par pixel}
        & \Tint{0}
        & \Tint{1670544}
        & \Tint{356007}
        & \Tint{309174}
        & \Tint{2792120}\\

        \midrule
        \TintMinValue{1} \TintMaxValue{-0.081}
        \textbf{Corrélation des intensités}
        & \Tint{1.000}
        & \Tint{1.000}
        & \Tint{0.997}
        & \Tint{0.318}
        & \Tint{-0.081}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{621.436}
        \textbf{\SIFT}
        & \Tint{0.000}
        & \Tint{4.243}
        & \Tint{64.722}
        & \Tint{63.182}
        & \Tint{621.436}\\

	\midrule
        \TintMinValue{0} \TintMaxValue{1.008}
        \textbf{\SURF}
        & \Tint{0.000}
        & \Tint{0.023}
        & \Tint{0.021}
        & \Tint{0.124}
        & \Tint{1.008}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{150}
        \textbf{\ORB}
        & \Tint{0}
        & \Tint{4}
        & \Tint{10}
        & \Tint{8}
        & \Tint{150}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{180}
        \textbf{\BRISK}
        & \Tint{0}
        & \Tint{0}
        & \Tint{8}
        & \Tint{23}
        & \Tint{180}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{153}
        \textbf{\FREAK}
        & \Tint{0}
        & \Tint{4}
        & \Tint{30}
        & \Tint{26}
        & \Tint{153}\\

        \bottomrule\addlinespace[\belowrulesep]
    \end{tabular}
    \endgroup
\end{frame}

\begin{frame}{Formulation comme un problème d’affectation}
    \begin{itemize}
        \item Prenons une mesure de dissemblance quelconque.
        \item Problème vu comme un problème d’affectation~:
        \begin{itemize}
            \item Affecter à chaque point de la première image au plus un point de la seconde…
            \item …tout en minimisant la dissemblance totale de l’affectation.
        \end{itemize}
        \item Calculable en \textbf{temps polynomial~!}
    \end{itemize}
\end{frame}

\begin{frame}{Résolution du problème d’affectation}{Méthode~1~: lente mais exacte}
    \begin{itemize}
        \item \emph{On appelle $n$ le nombre de points à accoupler.}
        \item Recherche d’un couplage parfait de poids minimal.
        \item Algorithme de Kuhn-Munkres~: $\UpBound(n^3)$.
        \item Parallélisable~: $\UpBound\left(\frac{n^3}{t}\right)$ ($t$ = nombre de tâches).
    \end{itemize}

    \begin{center}
        \slidecite{kuhn-1955-hungarian}
    \end{center}
\end{frame}

\begin{frame}{Résolution du problème d’affectation}{Méthode~2~: approximative mais plus rapide}
    \begin{itemize}
        \item \textbf{Principe~:} affecte $B$ à $A$ $\Longleftrightarrow$ $B$ est le point qui ressemble le plus à $A$ et la dissemblance ne dépasse pas un seuil.
        \item Recherche rapide du \textbf{plus proche descripteur} d’un point.
        \pause
        \item Structures de données possibles~:
        \begin{itemize}
            \item simple tableau~: $\UpBound(n)$~;
            \item \textsc{$k$-d tree} et recherche avec \textsc{Best-bin-first}~:\\gain expérimental, même borne de complexité.
        \end{itemize}
        \item Complexité pire cas~: $\UpBound(n^2)$.
    \end{itemize}
    \vfill
    \begin{center}
        \slidecite{lowe-2004-sift}
    \end{center}
\end{frame}

\begin{frame}{Discussion}
    \begin{minipage}[t]{.45\linewidth}
        {\centering\textbf{Récapitulatif}\par}

        \begin{itemize}
            \item \RefProblem{\PbSfM} $\Leftarrow$ association de points correspondants.
            \item Heuristique de dissemblance~: descripteur de voisinage.
            \item Couplage d’images $\equiv$ problème d’affectation.
            \item Soluble en temps polynomial (exact ou approché).
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}[t]{.45\linewidth}
        {\centering\textbf{Perspectives}\par}

        \begin{itemize}
            \item Faire un banc d’essai pour tester spécificité/sensibilité des descripteurs.
            \item Étudier d’autres descripteurs.
            \begin{itemize}
                \item \emph{MALCOM~: descripteur binaire à motif basé sur l’apprentissage machine.}
            \end{itemize}
            \item L’utilisation de l’approche exacte en vaut-elle la peine~?
        \end{itemize}
    \end{minipage}
\end{frame}

\section{Généralisation de l’association de points à un ensemble d’images}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Problème de correspondance généralisé}
    \begin{def-problem}
        \DefProblemName{\PbKM}
        \DefProblemInput{flux d’images $(k > 2)$ et pour chacune un ensemble de points}
        \DefProblemOutput{ensemble cohérent de grappes de points correspondants}
    \end{def-problem}
\end{frame}

\begin{frame}{Approche par arbre couvrant}
    \begin{itemize}
        \item Calcul de couplages paire par paire d’images.
        \item Recherche d’un arbre couvrant dans le graphe.
        \item Problème~: pas de vue globale.
    \end{itemize}
\end{frame}

\begin{frame}{Approche par segmentation par densité}
    \begin{itemize}
        \item Hiérarchie de points~:
        \begin{itemize}
            \item parent d’un point~: point d’une autre image le plus similaire et de densité supérieure~;
            \item racine~: point le plus dense.
        \end{itemize}
        \item Construction itérative des grappes~:
        \begin{itemize}
            \item initialisation avec une grappe par point~;
            \item contraction de grappes disjointes reliées par une arête inférieure à un seuil.
        \end{itemize}
    \end{itemize}
    \vfill
    \begin{center}
        \slidecite{tron-clustering-2017}
    \end{center}
\end{frame}

\begin{frame}{Approche par optimisation non-convexe}
    \begin{itemize}
        \item Recherche de fonctions de permutations entre chaque ensemble de points et l’ensemble de tous les points.
        \begin{itemize}
            \item[$\Leftrightarrow$] matrices de permutations.
        \end{itemize}
        \item Relaxation matrices de permutations $\rightarrow$ bistochastiques (polytope convexe).
        \item Fonction objectif non-convexe~: minimisation de la dissemblance totale.
        \item Optimisation itérative avec Gauss–Seidel.
        \item Matrices initiales dérivées des couplages paire à paire.
    \end{itemize}
    \vfill
    \begin{center}
        \slidecite{yu-2016-pgsr}
    \end{center}
\end{frame}

\begin{frame}{Discussion}
    \begin{minipage}[t]{.45\linewidth}
        {\centering\textbf{Récapitulatif}\par}

        \begin{itemize}
            \item Généralisation de \RefProblem{\PbDM} difficile.
            \item Arbre couvrant des couplages~: seulement local.
            \item Segmentation par densité.
            \item Optimisation non-convexe de matrices de permutations.
        \end{itemize}
    \end{minipage}\hfill
    \begin{minipage}[t]{.45\linewidth}
        {\centering\textbf{Perspectives}\par}

        \begin{itemize}
            \item Implémenter la segmentation par densité.
            \item Utiliser un solveur de programmes non-convexes pour l’optimisation.
            \item Comparer les résultats et sélectionner la plus appropriée.
        \end{itemize}
    \end{minipage}
\end{frame}

\section{Conclusion et perspectives}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Conclusion}
    \begin{itemize}
        \item \textbf{Objectif~:} automatiser la numérisation d’une scène.
        \item Reconstruction de la scène à partir d’un flux d’images~: \textbf{problème~\NPHardClass.}
        \item Amélioration de l’étape d’association de points.
        \item \textbf{Heuristiques de dissemblance} de points~: descripteurs de voisinage.
        \item \textbf{Approximations du problème} de \RefProblem{\PbKM}~:
        \begin{itemize}
            \item segmentation par densité~;
            \item optimisation non-convexe.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Perspectives}
    \begin{itemize}
        \item Réalisation d’un banc d’essai pour \textbf{évaluer spécificité/sensibilité} des heuristiques de description.
        \item \textbf{Implémentation des approximations} de \RefProblem{\PbKM} identifiées et comparaison des résultats.
        \item Gestion de l’\textbf{incertitude dans la triangulation} des points et de la trajectoire.
        \item Mise en place de l’\textbf{ajustement de faisceaux.}
        \item Étude des méthodes de \textbf{génération de surface texturisée} à partir d’un maillage de points.
    \end{itemize}
\end{frame}

{\renewcommand{\pgfuseimage}[1]
{\hspace{2.6em}\includegraphics[scale=.75]{#1}}
\begin{frame}{}
    \centering
    {\Large Merci pour votre attention\\
    \textbf{Questions~?}}

    \usebeamertemplate{separator}

    \vfill
    \textbf{Principales références}

    \vspace{-.5em}
    \begin{multicols}{2}
        \AtNextBibliography{\tiny}
        \printbibliography[category=major]
    \end{multicols}
\end{frame}}

\end{document}
