* Égalisation d’histogramme en préprocessing de la détection.
10.1109/CISP.2013.6744035

* Faire tests de compilation sur Grid 5000. Vérifier la compatibilité des systèmes de compilation et de dépendances.
* Re-tester la précision GPS avec le Wi-Fi scanning désactivé.
