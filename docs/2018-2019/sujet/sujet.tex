%! TEX program = xelatex

\documentclass[a4paper]{article}

% Adjust line height
\usepackage{setspace}

% Change text color
\usepackage{xcolor}
\definecolor{deemph}{gray}{0.2}

% Load locale-specific typographic rules
\usepackage{polyglossia}
\usepackage[autostyle=true]{csquotes}
\setmainlanguage{french}
\usepackage{microtype}

% Maths
\usepackage{amsmath}

% Compose vector images in TeX
\usepackage{pgf}
\usepackage{tikz}
\usepackage{forest}

% Figures
\usepackage[subpreambles]{standalone}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{wrapfig}

% Text framing
\usepackage{framed}

% Lists formatting
\usepackage{enumitem}
\setlist[itemize]{itemsep=0pt}

% Bibliographie
\usepackage[autolang=hyphen]{biblatex}

\addbibresource{../../references.bib}
\setlength\bibitemsep{1em}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}

\DefineBibliographyStrings{french}{%
    urlseen = {consulté le},
    techreport = {Rapport technique},
}

\DeclareBibliographyCategory{important}
\AtEveryBibitem{\ifcategory{important}{\hspace{-8.25em}\textsc{\small Important~\textasteriskcentered}\hspace{2.75em}}{}}
\addtocategory{important}{forstner-2016-photogrammetric,hartley-2004-multiple}


% Marges
\usepackage[textwidth=13cm]{geometry}

% Automated hyperlink creation in the resulting PDF
% (this package must be loaded after all others)
\usepackage[
    bookmarks, % generate PDF index as bookmarks
    hidelinks, % do not put a colored frame around links
    colorlinks, % color links in dark blue
    linkcolor={blue!80!black},
    citecolor={blue!80!black},
    urlcolor={blue!80!black}
]{hyperref}

% Adjust hyperref anchors to link to the contents of the figure
% and not to its caption (must be loaded AFTER hyperref)
\usepackage[all]{hypcap}

% Document metadata
\title{\emph{Lucilia}~---~Numérisation automatique d’environnements en trois dimensions}
\date{1\textsuperscript{er}~octobre~2018}
\author{Mattéo~\textsc{Delabre}}

\begin{document}

\makeatletter\begingroup
\centering\noindent
\large\textsc{Sujet de projet annuel CMI}

\vspace{1em}
\LARGE\@title

\vspace{1em}
\large\@author\quad•\quad\@date

\vspace{1em}
\rule{.6\textwidth}{.4pt}

\endgroup\makeatother

\vspace{2em}
\noindent
\textsc{Encadrante~:} M\textsuperscript{me} Hinde \textsc{Bouziane}\\
\textsc{Mots-clefs~:} photogrammétrie, traitement d’images, étude d’algorithmes.

\vspace{1em}
\begin{wrapfigure}{R}{.25\textwidth}
    \raggedleft
    \vspace*{-1em}
    \includegraphics[width=.9\linewidth]{figures/illustration}
\end{wrapfigure}
\emph{Lucilia} est un ensemble de projets, démarrés durant l’année 2017~--~2018, ayant pour double objectif d’automatiser la reconstruction de modèles~3D d’environnements à partir de photographies prises par un drone et de créer une application pour visualiser ces modèles et y réaliser des mesures.

De nombreuses situations réelles peuvent tirer parti de tels modèles.
Chaque jour, plus d’un milliard d’utilisateurs utilisent les services \emph{Maps} et \emph{Earth} de Google pour découvrir et se repérer dans toutes les villes du monde~\cite{bi-2015-google-services}~: les données de ces services sont recoupées entre des informations cartographiques officielles et des modèles~3D construits avec des photos acquises par Google.
Ils peuvent aussi servir un intérêt plus historique en permettant de conserver une copie numérique d’un objet ou d’un bâtiment fragile du patrimoine~\cite[p.~11]{remondino-2016-3d}.
Enfin, ces techniques sont de plus en plus utilisées par les agriculteurs pour surveiller et préparer leurs récoltes sur des parcelles très étendues ou difficiles d’accès~\cite[p.~1]{grenzdorffer-2008-photogrammetric}.

Ce sujet propose de poursuivre le projet de mise au point d’un outil permettant de créer une représentation en trois dimensions sous la forme d’un maillage de polygones d’un environnement pour lequel on dispose d’une séquence de photographies (appelé flux d’images), avec la connaissance de la distance parcourue d’une prise à l’autre.

\section*{Travail réalisé l’an passé}

Florent~\textsc{Tornil} et moi-même avons exploré la littérature pour identifier différentes méthodes utilisées pour résoudre ce problème de reconstruction, appelé \emph{\enquote{structure from motion}} en anglais. Nous les avons synthétisés en un processus en cinq étapes~(cf. figure~\ref{fig:pipeline}) inspiré de \cite{forstner-2016-photogrammetric, hartley-2004-multiple}~:

\begin{enumerate}[align=left]
\item[\textbf{Phase 1 ---}] \textbf{Étalonnage de l’appareil photographique} utilisé pour capturer le flux d’images afin d’obtenir les paramètres géométriques de son objectif, tels que la distance focale ou la position du point principal.
\item[\textbf{Phase 2 ---}] \textbf{Détection de points caractéristiques dans chaque image,} c’est-à-dire de points dont le voisinage est unique et peut facilement être reconnu dans d’autres images, sous d’autres angles de vues et conditions d’éclairage.
\item[\textbf{Phase 3 ---}] Couplage des points similaires dans chaque paire d’images afin \textbf{d’estimer la trajectoire} suivie par l’appareil lors de la capture du flux d’images. Utilisation de cette trajectoire pour \textbf{reconstruire un nuage de points de la scène} en triangulant chaque point caractéristique dans l’espace.
\item[\textbf{Phase 4 ---}] \textbf{Ajustement global de la position des points et de la trajectoire} de l’appareil avec pour objectif de minimiser la distance entre la position originelle des points sur chaque image et la position obtenue par re-projection du nuage de points sur les images.
\item[\textbf{Phase 5 ---}] \textbf{Génération d’un maillage texturisé} à partir du nuage de points.
\end{enumerate}

\begin{figure}[htb!]
    \centering
    \makebox[\textwidth][c]{\input{figures/pipeline}}
    \caption{\textbf{Processus de numérisation d’un environnement en trois dimensions.}}
    \label{fig:pipeline}
\end{figure}

Pour chacune de ces phases, il existe de nombreux algorithmes qui trouvent un équilibre différent entre temps de calcul et qualité des résultats. Nous avons implementé un premier prototype du programme utilisant des algorithmes assez basiques pour réaliser les phases~1 à~3, disponible dans le dépôt Git du projet\footnote{\url{https://gitlab.com/ftornil/lucilia}}, qui donne des résultats similaires à la figure~\ref{fig:results}.

\begin{figure}[htb]
    \begin{minipage}{.4\textwidth}
        \includegraphics[width=\textwidth]{figures/input}
    \end{minipage}
    \hfill
    \begin{minipage}{.55\textwidth}
        \includegraphics[width=\textwidth]{figures/results}
    \end{minipage}
    \caption{\textbf{Nuage de points résultant de l’analyse} d’un flux d’images capturé autour du bâtiment à gauche. La forme cylindrique du bâtiment est correctement capturée par le nuage de points~(en vert) mais de sévères incohérences persistent notamment au niveau de la trajectoire de l’appareil~(en violet).}
    \label{fig:results}
\end{figure}

\section*{Déroulement du projet}

\paragraph*{Partie recherche.} Dans la première partie du projet, qui se déroulera du 1\textsuperscript{er}~octobre~2018 au 31~décembre~2018 (premier semestre, 3~mois), nous réaliserons des recherches sur les sujets ci-dessous.
Ces recherches feront l’objet d’un rapport, à rendre fin décembre, puis d’une soutenance orale à mi-projet en janvier~2019.

\begin{itemize}
    \item Comparer les performances et les résultats des algorithmes de détection des points dans les images, sélectionner un ou plusieurs algorithmes les plus adaptés à notre cas d’utilisation.
    \item Étudier les possibilités d’amélioration de l’algorithme de couplage des points et rechercher des algorithmes existants dans la littérature pouvant être adaptés à notre outil.
    \item Rechercher des méthodes permettant d’estimer l’erreur de mesure du modèle final, dérivée d’informations sur l’erreur de mesure dans les entrées de l’algorithme.
    \item Explorer les méthodes permettant de créer un maillage de polygones texturisé à partir du nuage de points et du flux d’images en vue d’implémenter la phase~5.
    \item Étudier la possibilité de paralléliser les calculs et d’en déplacer une partie sur la carte graphique avec CUDA ou OpenCL.
\end{itemize}

\paragraph*{Partie programmation.} Dans la seconde partie du projet, qui se déroulera du 1\textsuperscript{er}~janvier~2019 au 31~mars~2019 (second semestre, 3~mois), les recherches réalisées seront mises en application pour améliorer le prototype d’outil développé l’année précédente et se rapprocher de l’objectif du projet.
Ces développements feront l’objet d’un rapport final puis d’une soutenance orale de fin de projet en mai~2019.

\begin{itemize}
    \item Implémenter la phase~4~: ajuster la position des points du modèle.
    \item Implémenter la phase~5~: générer un maillage de polygones à partir du nuage.
    \item Mettre en place la parallélisation des calculs et le déplacement d’une partie des calculs vers la carte graphique.
    \item Transformer l’outil pour produire une bibliothèque réutilisable dans d’autres applications (actuellement, c’est une simple application dans le terminal).
    \item Tester l’outil sur des données (photographies et déplacement) capturées avec un drone ou, si ce n’est pas possible, à l’aide d’un téléphone et d’une application Android simpliste développée pour l’occasion.
\end{itemize}

\section*{Adéquation avec le parcours MIT}

Ce projet s’intéresse à la comparaison et à l’implémentation de méthodes de résolution approximatives pour \emph{structure from motion}, un problème $\mathcal{NP}$-difficile~\cite{nister-2007-sfm-nphard}.
Nous étudierons entre autres l’algorithme probabiliste \emph{random sample consensus}~(RANSAC)~\cite{fischler-1981-ransac} permettant d’éliminer les aberrations d’un jeu de données.
Cette étude se nourrira naturellement des enseignements en complexité algorithmique, méthodes et algorithmique probabiliste et méthodes approchées de la première année du master~MIT.
Elle permettra également aux étudiantes et étudiants d’approfondir leurs connaissances dans ces sujets et de découvrir des sujets connexes.

\section*{Modalités}

Les deux parties de ce projet annuel CMI font l’objet d’une convention entre la faculté des sciences, le laboratoire d’informatique, de robotique et de microélectronique de Montpellier (LIRMM), M\textsuperscript{me}~Hinde~\textsc{Bouziane}, encadrante, M\textsuperscript{me}~Anne-Elisabeth~\textsc{Baert}, responsable du CMI et les étudiants afin de leur permettre de travailler au sein du laboratoire.
Ce sujet est prévu pour une équipe de deux à quatre étudiantes et étudiants, qui ne doivent pas nécessairement être inscrits en CMI~; le cas échéant, le travail réalisé au premier semestre ne fera pas l’objet d’une évaluation pour lesdits étudiants.

\printbibliography

\end{document}
