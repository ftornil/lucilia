\chapter{Association des points d’une scène observés dans plusieurs images}
\label{chapter:association}

\noindent
\begin{minipage}[t]{\dimexpr\textwidth-6cm}
    \vspace*{0pt}
    Pour reconstruire un nuage de points de la scène, une possibilité est de s’appuyer sur les points caractéristiques détectés dans chaque image.
    Ce choix est pertinent en vue de la construction d’un maillage basé sur le nuage car, comme les points caractéristiques correspondent aux angles des images, les éléments de structure du modèle font partie du nuage ainsi reconstitué.

    Pour projeter les points caractéristiques de l’espace bidimensionnel des images sur les points de l’espace tridimensionnel de la scène qui leur correspondent, les techniques de \emph{reconstruction épipolaire} et d’\emph{ajustement par faisceaux} peuvent être utilisées~\parencite{hartley-2004-multiple}.

    Celles-ci requièrent d'avoir au préalable regroupé les points qui correspondent au même point de la scène ensemble~(cf. figure~\ref{fig:association-manual-tracking})~: il s'agit du problème de correspondance.

    \begin{framed}
        \noindent\textbf{Problème de correspondance~\parencite[sec.~4.3]{braunstein-empirical-1988}.} Parmi un ensemble de points caractéristiques d’images différentes, identifier un ensemble de groupes (en anglais, \emph{clusters}) de points homologues de telle sorte que : \begin{itemize}
            \item chaque groupe ne contienne pas deux points d’une même image~;
            \item chaque point appartienne à exactement un groupe~;
            \item chaque point d’un groupe corresponde à tous les autres points du groupe.
            \item le nombre total de groupes soit minimisé.
        \end{itemize}
    \end{framed}
\end{minipage}\hfill
\begin{minipage}[t]{5cm}
    \vspace*{0pt}
    \input{fig/association/manual-point-tracking}

    \captionof{figure}{Illustration d’une solution possible au problème de correspondance entre trois images. Chaque cercle numéroté indique un point caractéristique. Les points liés entre eux forment des groupes de points homologues.}
    \label{fig:association-manual-tracking}
\end{minipage}

La dissemblance des descripteurs est utilisée pour déterminer si deux points caractéristiques sont homologues.
Une première heuristique, naïve, consiste à déclarer qu’il y a correspondance entre deux points dès lors que leur dissemblance est sous un seuil fixé.
Le fait que le seuil soit fixe pose problème dans la plupart des configurations, où se trouvent souvent mélangés des points très distinctifs et des points qui se ressemblent.
Pour exclure les points qui se ressemblent, il est nécessaire d’abaisser le seuil, mais, ce faisant, les points distinctifs ne sont plus associés.
Pour pallier cela, le seuil limite peut être relativisé à la \enquote{distinction} d’un des deux points considérés~—~qui décrit dans quelle mesure un point est distinct des autres points de son image~—~pouvant être mesurée en calculant sa dissemblance avec le point qui lui ressemble le plus dans son image.
Avec cette seconde heuristique, une correspondance est déclarée lorsque le ratio entre la dissemblance des deux descripteurs et la distinction d’un des deux points est inférieure à un ratio $\ratioedge$ fixé.
Par exemple, \citeauthor{lowe-2004-sift} choisit dans son article de \citeyear{lowe-2004-sift} présentant l’algorithme SIFT un ratio de $\ratioedge = \num{0.8}$.

Dans ce chapitre, nous proposons une étude de deux algorithmes pour résoudre ce problème.

\section{Approche par recherche locale}

Ce premier algorithme (cf.~algorithme~\ref{algo:association-tracking}) de type glouton s’appuie sur une recherche locale des correspondances entre chaque paire consécutive d’images.
Initialement, chaque point caractéristique est seul dans son propre groupe.
Lorsqu’une correspondance entre deux points de deux images consécutives est identifiée, les groupes de ces deux points sont fusionnés si les conditions de compatibilité suivantes sont remplies~: \begin{enumerate}
    \item Les points des deux groupes concernent deux ensembles d’images disjoints.
    \item Le groupe résultant de la fusion respecte la condition de distinction relativisée.
\end{enumerate}

La première condition permet de maintenir l’invariant selon lequel un groupe ne peut pas contenir deux points d’une même image.
La seconde s’assure que les points au sein d’un des groupes ainsi formés se correspondent tous mutuellement.
Enfin, puisqu’initialement chaque point est dans un groupe et qu’au cours de l’algorithme, seules des fusions de groupes sont réalisées, l’algorithme garantit également que chaque point appartienne à exactement un groupe.

\begin{algorithm}
\begin{algorithmic}
    \Require\\\begin{itemize}
        \item $\FeatureSetAll$~: ensemble de points caractéristiques~;
        \item $\ratioedge$~: ratio maximum entre la dissemblance des descripteurs d’une paire de points et la distinction d’un des deux points pour que cette paire de points soit considérée homologue.
    \end{itemize}
    \Ensure ensemble de groupes de points similaires.
    \State créer un groupe par point caractéristique
    \ForAll{$i, i+1 \in \var{images}$}
        \ForAll{$j \in \var{points}(\var{images}[i])$}
            \State $\var{voisin}_j \gets$ point de l’image $i+1$ le plus ressemblant à $j$
            \If{le groupe de $j$ et de $\var{voisin}_j$ sont compatibles}
                \State fusionner le groupe de $j$ et de $\var{voisin}_j$
            \EndIf
        \EndFor
    \EndFor
    \Return les groupes ainsi formés
\end{algorithmic}
\caption{Calcul d’un ensemble de groupes par recherche locale}
\label{algo:association-tracking}
\end{algorithm}

Bien que cet algorithme satisfasse à trois des quatre conditions requises pour une solution au problème de correspondance, il ne garantit pas l’optimalité de la solution en termes du nombre de groupes obtenu.
Le choix fait par l’algorithme de fusionner les groupes d’une paire de points peut en effet s’avérer contre-productif en empêchant des fusions ultérieures plus avantageuses, comme dans l’exemple en figure~\ref{fig:association-tracking-not-optimal}.
Dans cet exemple, l’algorithme commence par regrouper les premiers points des images~1 et~2, ce qui rend impossible le regroupement du premier point de l’image~1 avec le dernier de l’image~2, mais aussi l’ajout du premier point de la dernière image dans le groupe ainsi formé car le premier point de la première image n’est pas homologue à celui de la troisième.
Or, en choisissant cette solution alternative, il est possible d’obtenir un nombre de groupes plus faible~: l’algorithme ne donne pas une solution optimale.

\begin{figure}[htb]
    \centering
    \input{fig/association/tracking-not-optimal}

    \caption{\textbf{Exemple mettant en défaut l’algorithme de recherche locale.} Chaque colonne de points correspond aux points caractéristiques d’une image. Les points sont accompagnés de leur descripteur, qui, pour des raisons de simplicité, est ici unidimensionnel. Les paires de points liées par des lignes pleines ou pointillées sont les homologues en prenant $\ratioedge = \frac{2}{3}$. Les lignes pleines sont celles qui lient les paires choisies par l’algorithme, ici, le résultat est une décomposition en huit groupes. Les lignes pointillées sont celles de la solution optimale qui est une décomposition en sept groupes.}
    \label{fig:association-tracking-not-optimal}
\end{figure}

Par ailleurs, cet algorithme traite le flux d’images de façon locale~: si un point subit une occlusion sur quelques images, celui-ci formera au moins deux groupes disjoints de part et d’autre de l’occlusion.
Une possibilité pour remédier à ce problème d’occlusion est de demander à l’algorithme d’inspecter toutes les paires d’images, consécutives ou non.
Cependant, cette variante aura tendance à accentuer l’effet montré ci-dessus~: beaucoup de fusions seront réalisées avec la première image, empêchant davantage de fusions ultérieures potentiellement plus avantageuses.

Pour implémenter cet algorithme, nous avons choisi de stocker au fur et à mesure du calcul les groupes dans un vecteur de taille variable.
Chaque groupe est représenté sous forme d’un dictionnaire associant les images du groupe au point concerné de chaque image~: cette structure reflète la propriété selon laquelle un groupe ne peut pas contenir deux points de la même image.
Afin de permettre de tester rapidement la compatibilité de deux groupes en termes de l’ensemble d’images qu’ils couvrent, chaque groupe est associé à un masque de bits contenant autant de bits qu’il n’y a d’images et dans lequel un bit est à~1 si, et seulement si, le groupe couvre l’image en question.
Les ensembles d’images couverts par deux groupes sont alors compatibles lorsque le \enquote{et} binaire de leur masque associé est nul.

À chaque paire d’images inspectée par l’algorithme, une recherche exhaustive des couples de points correspondants est réalisée en comparant chacun des $\UpBound(m^2)$ couples possibles, où $m$ est le nombre de points caractéristiques dans chaque image.
Pour chaque paire de points homologues trouvée, la compatibilité est vérifiée entre les groupes des deux points, ce qui est réalisé en $\UpBound(1)$ en utilisant le masque associé à chaque groupe.
Le nombre total de paires d’images inspectées par l’algorithme est de l’ordre de $\UpBound(n)$.
Globalement, notre implémentation de l’algorithme atteint donc une complexité dans le pire cas de $\UpBound(nm^2)$.

\section{Approche par segmentation densitaire des descripteurs}

L’algorithme QuickShift~\parencite{vedaldi-2008-quickshift} est une méthode générique de résolution de problèmes de groupement de données ordinales (ou \emph{clustering}).
L’idée de l’algorithme est de former une forêt dans laquelle chaque point de l’ensemble des points à grouper est lié à son plus proche voisin de densité supérieure, c’est-à-dire son plus proche voisin qui possède un nombre de points plus important de valeur similaire, s’il existe.
Il s’agit donc d’une méthode de type \emph{hill-climbing} de la fonction de densité.
Les voisins sont considérés dans un rayon $r$ fixe autour de chaque point, ce qui fait que les points les plus éloignés des autres sont des feuilles de la forêt.
Enfin, les différentes composantes connexes de cette forêt forment les groupes donnés en sortie de l’algorithme.

\begin{figure}[htb]
    \centering
    \input{fig/association/quickshift-example}

    \caption{\textbf{Exemple d’utilisation de QuickShift pour grouper des données à deux dimensions.} À gauche, l’ensemble des données d’entrée. À droite, la forêt à densité ascendante calculée par QuickShift ainsi que les groupes résultants.}
    \label{fig:quickshift-exemple}
\end{figure}

La densité considérée est la densité de probabilité des valeurs numériques des points à regrouper.
Puisque la distribution statistique de ces données n’est généralement pas connue \emph{a priori,} la majorité des applications utilisent la méthode de Parzen--Rosenblatt pour estimer la densité.

\begin{framed}
    \setlength{\parindent}{0pt}
    \textbf{Méthode de Parzen--Rosenblatt~(\cite{parzen-1962-density-estimate}, \cite{rosenblatt-1956-density-estimate})}.
    Étant donné un échantillon indépendamment et identiquement distribué d’observations $(x_1, ..., x_n)$ d’une variable aléatoire $X$, calcule une estimation de la densité de probabilité en un point $x$.
    Cette estimation est calculée à partir du nombre d’observations se trouvant dans une fenêtre $h$ autour de $x$, lissée par un noyau $K$ permettant, dans le cas d’un noyau gaussien par exemple, d’assurer la continuité de l’estimation.

    \vspace{1em}
    \begin{minipage}{.45\textwidth}
        \[
            f(x) =
                \frac{1}{nh}
                \sum_{i=1}^{n}{
                    K\left(\frac{x - x_i}{h}\right)
                }
        \]
    \end{minipage}\hfill
    \begin{minipage}{.45\textwidth}
        \[
            K(x) =
                \frac{1}{\sqrt{2\pi}}
                \exp\left(
                    -\frac{x^2}{2}
                \right)
        \]
    \end{minipage}
    \vspace{1em}

    La dimension de la fenêtre, $h$, détermine le niveau de \enquote{lissage} de l’estimation.
    Le choix de ce paramètre est critique et dépend de la distribution réelle des données.
\end{framed}

Cet algorithme peut par exemple être appliqué au problème de la segmentation des images, qui a pour but de déterminer dans une image un ensemble de régions contigües de points dont la couleur est similaire, par exemple à des fins de détection d’objets (cf.~figure~\ref{fig:quickshift-segmentation}).
L’un des principaux avantages de cet algorithme est qu’il n’est pas nécessaire de connaître à l’avance le nombre de groupes à créer, contrairement par exemple à l’algorithme des $k$-moyennes~\parencite[p.~526]{duda-2012-pattern}.
Cependant, deux paramètres doivent être ajustés et déterminent la qualité du résultat~: le rayon du voisinage considéré autour de chaque point et $h$, le niveau de lissage.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.475\textwidth]{fig/association/quickshift-segmentation-input}\hfill%
    \includegraphics[width=.475\textwidth]{fig/association/quickshift-segmentation-output}

    \caption{\textbf{Exemple d’utilisation de QuickShift pour segmenter une image.} L’algorithme est ici utilisé dans un espace à cinq dimensions contenant les points de l’image, avec deux dimensions pour la position du pixel et trois dimensions pour la couleur. À gauche, l’image originelle et à droite, l’image segmentée. Tiré de la documentation de la bibliothèque \emph{VLFeat}, voir \url{http://www.vlfeat.org/overview/quickshift.html}.}
    \label{fig:quickshift-segmentation}
\end{figure}

En se basant sur l’algorithme QuickShift, \citeauthor{tron-clustering-2017} ont proposé en~\citeyear{tron-clustering-2017} dans l’article~\citetitle{tron-clustering-2017} un nouvel algorithme pour l’association des points caractéristiques correspondants, appelé QuickMatch~(cf.~algorithme~\ref{algo:association-quickmatch}).
L’algorithme réalise une segmentation densitaire dans l’espace des descripteurs des points et renvoie un ensemble de groupes de points dont les descripteurs sont proches.
Pour estimer la densité du descripteur d’un point caractéristique $j$ dans l’image $i$, la méthode de Parzen--Rosenblatt est utilisée avec un noyau gaussien ayant pour fenêtre $\ratiodensity \times \var{distinction}_i^j$, où $\ratiodensity$ est un paramètre ajustable et $\var{distinction}_i^j$ est la distinction du point.
L’utilisation d’une fenêtre proportionnelle à la distinction de chaque point permet de s’assurer que seuls les points qui leur sont similaires sont pris en compte dans l’estimation de leur densité, aussi bien pour les points peu distinctifs que pour les points isolés.

Lors de la phase de construction de la forêt de points caractéristiques, contrairement à l’algorithme QuickShift, l’algorithme QuickMatch prend en compte tous les points des autres images de l’ensemble $\FeatureSetAll$ et pas seulement celles situées dans un voisinage donné.
Puisque pour tout point, sauf un unique qui est la racine de l’arbre, il existe toujours un point de densité supérieure dans une autre image, le résultat est non plus seulement une forêt mais un arbre.
Cet arbre est ensuite divisé en sous-arbres, correspondant aux groupes de points résultants, en considérant les arêtes dans l’ordre croissant de leur longueur et en sélectionnant les arêtes qui ne créent pas d’incompatibilité d’association (c’est-à-dire les arêtes qui n’invalident pas les conditions d’exclusion mutuelle des groupes et de distinction relativisées définies précédemment).

\begin{algorithm}
\begin{algorithmic}
    \Require\\\begin{itemize}
        \item $\FeatureSetAll$~: ensemble de points caractéristiques~;
        \item $\ratioedge$~: ratio maximum entre la dissemblance des descripteurs d’une paire de points et la distinction d’un des deux points pour que cette paire de points soit considérée homologue~;
        \item $\ratiodensity$~: taux de lissage du noyau gaussien utilisé pour l’estimation de densité.
    \end{itemize}
    \Ensure ensemble de groupes de points similaires.
    \ForAll{$i \in \{1, ..., \ImageStreamSize\}$} \Comment{Calcul de la distinction de chaque point}
        \ForAll{$j \in \{1, ..., \FeatureSetSize\}$}
            \State $\var{distinction}_i^j \gets \min_{k \neq j}\left\{\Dissimilarity(\Descriptor(\Feature{i}{j}), \Descriptor(\Feature{i}{k}))\right\}$
        \EndFor
    \EndFor
    \ForAll{$i_1 \in \{1, ..., \ImageStreamSize\}$} \Comment{Estimation de la densité de chaque point}
        \ForAll{$j_1 \in \{1, ..., \FeatureSetSize\}$}
            \State $\var{densité}_{i_1}^{j_1} \gets$
            \[
                \sum_{i_2=1}^{\ImageStreamSize}
                \sum_{j_2=1}^{\FeatureSetSize}
                \log\left(
                    1 + \sqrt{\var{distinction}_{i_2}^{j_2}}
                \right)
                \exp\left(
                    -\frac
                        {\Dissimilarity(\Descriptor(\Feature{i_1}{j_1}), \Descriptor(\Feature{i_2}{j_2}))}
                        {2(\ratiodensity\var{distinction}_{i_2}^{j_2})^2}
                \right)
            \]
        \EndFor
    \EndFor
    \State $\var{arêtes} \gets \emptyset$ \Comment{Formation de l’arbre de densité}
    \ForAll{$i_1 \in \{1, ..., \ImageStreamSize\}$}
        \ForAll{$j_1 \in \{1, ..., \FeatureSetSize\}$}
        \State $\var{candidats} \gets \left\{(i_2, j_2) \in (\{1, ..., \ImageStreamSize\} - \{i_1\}) \times \{1, ..., \FeatureSetSize\} \mid \var{densité}_{i_1}^{j_1} < \var{densité}_{i_2}^{j_2}\right\}$
            \State $\var{parent} \gets \argmin_{\Feature{i_2}{j_2} \in \var{candidats}}\left\{\Dissimilarity(\Descriptor(\Feature{i_1}{j_1}), \Descriptor(\Feature{i_2}{j_2}))\right\}$
            \If{$\var{parent} \neq \emptyset$}
            \State $\var{arêtes} \gets \var{arêtes} \cup \{(\Feature{i_1}{j_1}, \Feature{i_2}{j_2})\}$
            \EndIf
        \EndFor
    \EndFor
    \State $\var{arêtes}' \gets \emptyset$ \Comment{Découpage de l’arbre en sous-arbres}
    \ForAll{$(\Feature{i_1}{j_1}, \Feature{i_2}{j_2}) \in \var{arêtes}$, dans l’ordre croissant des longueurs,}
        \State $C_1 \gets$ composante connexe de $\Feature{i_1}{j_1}$ dans $\var{arêtes}'$
        \State $C_2 \gets$ composante connexe de $\Feature{i_2}{j_2}$ dans $\var{arêtes}'$
        \If{$C_1$ et $C_2$ sont compatibles}
            \State $\var{arêtes}' \gets \var{arêtes}' \cup \{(\Feature{i_1}{j_1}, \Feature{i_2}{j_2})\}$
        \EndIf
    \EndFor
    \Return les groupes formés par les composantes connexes de $\var{arêtes}'$
\end{algorithmic}
\caption{Calcul d’un ensemble de groupes par segmentation densitaire}
\label{algo:association-quickmatch}
\end{algorithm}

Pour implémenter cet algorithme, nous avons repris les mêmes structures de données que pour l’algorithme de recherche locale.
Avec ces structures, le calcul de la distinction de chaque point caractéristique est réalisé en~$\UpBound(nm^2)$, celui de la densité en~$\UpBound(n^2m^2)$ et celui de l’arbre de densité en~$\UpBound(n^2m^2)$.
Sur ces trois premières étapes, chaque itération est indépendante des autres, donc, si le nombre de tâches à disposition est $k$ la complexité des trois premières étapes est~$\UpBound\left(\frac{1}{k}n^2m^2\right)$.
Pour tirer parti cette possibilité de parallélisation du calcul, nous avons utilisé la technologie OpenMP\footnote{OpenMP~: \url{https://www.openmp.org/}} permettant de répartir les calculs sur un nombre de tâches choisies par l’utilisateur.
Comme avec l’algorithme de recherche locale, les tests de compatibilité sont réalisés en temps constant en s’appuyant sur la structure de masque de bits.
Dans la dernière étape de l’algorithme, toutes les arêtes sont consultées, or, puisqu’il s’agit d’un arbre, il y a au plus $nm - 1$ telles arêtes, donc cette étape est réalisée en~$\UpBound(nm)$.
Au total, l’algorithme possède une complexité dans le pire cas de $\UpBound\left(\frac{1}{k}n^2m^2 + nm\right)$.

\section{Évaluation et comparaison des deux méthodes}

Pour comparer ces deux algorithmes en pratique, nous les avons testés sur le jeu de données obtenu en photographiant le bâtiment~44 du campus~Triolet de la faculté des sciences.
Ce jeu de données contient 59~photographies prises le long d’un cercle autour du bâtiment en question.
La présence de structures répétitives sur le bâtiment~44 (fenêtres, toiture, portes) ainsi que de végétation à l’arrière-plan rend la tâche d’association non triviale.
Ce bâtiment possède cependant suffisamment de détails pour que cette tâche soit possible.
Au préalable, une détection de points caractéristiques a été réalisée avec l’algorithme SURF et les descripteurs de chaque points ont déjà été calculés.
Pour chacune des configurations de test, les paramètres suivants ont été mesurés dans le résultat~: \begin{itemize}
    \item nombre total de groupes contenant au moins deux points. En effet, les groupes ne contenant qu’un seul point ne sont pas utiles pour la reconstruction du nuage de points~;
    \item taille moyenne d’un groupe~;
    \item écart-type moyen des descripteurs dans un groupe. Il s’agit de la moyenne des écarts-types des descripteurs calculés dans chaque groupe de l’ensemble de groupes~;
    \item taux de correspondances correctes. Sur un échantillon aléatoire de 30~correspondances par ensemble, un dénombrement manuel des correspondances correctes et incorrectes a été réalisé.
\end{itemize}

Les résultats pour l’algorithme de recherche locale sont présentés sur la figure~\ref{fig:association-eval-tracking}, ceux pour l’algorithme de segmentation densitaire sont présentés sur la figure~\ref{fig:association-eval-quickmatch}.

\begin{figure}[htb]
    \begin{tikzpicture}
        \begin{axis}[
            xlabel=$\ratioedge$,
            ylabel=Nombre de groupes de taille $\geq 2$,
            width=.5\linewidth,
        ]
            \addplot table {fig/association/tracking-results-nb-clusters};
        \end{axis}
    \end{tikzpicture}\hfill%
    \begin{tikzpicture}
        \begin{axis}[
            xlabel=$\ratioedge$,
            ylabel=Taille moyenne d’un groupe,
            width=.5\linewidth,
        ]
            \addplot table {fig/association/tracking-results-cluster-size};
        \end{axis}
    \end{tikzpicture}

    \begin{tikzpicture}
        \begin{axis}[
            xlabel=$\ratioedge$,
            ylabel=Écart-type moyen des groupes,
            yticklabel style={/pgf/number format/precision=4},
            width=.5\linewidth,
        ]
            \addplot table {fig/association/tracking-results-cluster-deviation};
        \end{axis}
    \end{tikzpicture}\hfill%
    \begin{tikzpicture}
        \begin{axis}[
            xlabel=$\ratioedge$,
            ylabel=Taux de groupes corrects (\%),
            width=.5\linewidth,
        ]
            \addplot table {fig/association/tracking-results-cluster-quality};
        \end{axis}
    \end{tikzpicture}

    \caption{\textbf{Résultats de l’évaluation de l’algorithme de recherche locale.} L’algorithme a été testé sur le jeu de données du bâtiment~44 en faisant varier le paramètre $\ratioedge$ entre $\num{0.3}$ et $\num{1}$.}
    \label{fig:association-eval-tracking}
\end{figure}

\begin{figure}[htb]
    \begin{tikzpicture}
        \begin{axis}[
            axis on top,
            xlabel=$\ratiodensity$,
            xmin=0.2,
            xmax=1,
            ylabel=$\ratioedge$,
            ymin=0.2,
            ymax=2,
            width=.5\linewidth,
            point meta=explicit,
            colorbar,
            colormap/viridis,
            colorbar horizontal,
            colorbar style={
                xlabel=Nombre de groupes de taille $\geq 2$,
                scaled ticks=false,
                tick label style={/pgf/number format/fixed},
            },
        ]
            \addplot [
                matrix plot*,
            ] table [meta index=2]
            {fig/association/quickmatch-results-nb-clusters};
        \end{axis}
    \end{tikzpicture}\hfill
    \begin{tikzpicture}
        \begin{axis}[
            axis on top,
            xlabel=$\ratiodensity$,
            xmin=0.2,
            xmax=1,
            ylabel=$\ratioedge$,
            ymin=0.2,
            ymax=2,
            width=.5\linewidth,
            point meta=explicit,
            colorbar,
            colormap/viridis,
            colorbar horizontal,
            colorbar style={
                xlabel=Taille moyenne d’un groupe,
                scaled ticks=false,
                tick label style={/pgf/number format/fixed},
            },
        ]
            \addplot [
                matrix plot*,
            ] table [meta index=2]
            {fig/association/quickmatch-results-cluster-size};
        \end{axis}
    \end{tikzpicture}

    \begin{tikzpicture}
        \begin{axis}[
            axis on top,
            xlabel=$\ratiodensity$,
            xmin=0.2,
            xmax=1,
            ylabel=$\ratioedge$,
            ymin=0.2,
            ymax=2,
            width=.5\linewidth,
            point meta=explicit,
            colorbar,
            colormap/viridis,
            colorbar horizontal,
            colorbar style={
                xlabel=Écart-type moyen des groupes,
                scaled ticks=false,
                tick label style={/pgf/number format/fixed},
            },
        ]
            \addplot [
                matrix plot*,
            ] table [meta index=2]
            {fig/association/quickmatch-results-cluster-deviation};
        \end{axis}
    \end{tikzpicture}\hfill
    \begin{tikzpicture}
        \begin{axis}[
            axis on top,
            xlabel=$\ratiodensity$,
            xmin=0.2,
            xmax=1,
            ylabel=$\ratioedge$,
            ymin=0.2,
            ymax=2,
            width=.5\linewidth,
            point meta=explicit,
            colorbar,
            colormap/viridis,
            colorbar horizontal,
            colorbar style={
                xlabel=Taux de groupes corrects (\%),
                scaled ticks=false,
                tick label style={/pgf/number format/fixed},
            },
        ]
            \addplot [
                matrix plot*,
            ] table [meta index=2]
            {fig/association/quickmatch-results-cluster-quality};
        \end{axis}
    \end{tikzpicture}

    \caption{\textbf{Résultats de l’évaluation de l’algorithme de segmentation densitaire (QuickMatch).} L’algorithme a été testé sur le jeu de données du bâtiment~44 en faisant varier le paramètre~$\ratioedge$ entre $\num{0.2}$ et $\num{2}$ et le paramètre~$\ratiodensity$ entre $\num{0.2}$ et $\num{1}$.}
    \label{fig:association-eval-quickmatch}
\end{figure}

Sur le jeu de données considéré, l’algorithme de recherche locale a une faible performance, atteignant une précision maximale de \num{36.67}~\% pour $\ratioedge = \num{0.3}$, valeur du paramètre à laquelle les groupes sont de petite taille~---~en moyenne de 2.
Un deuxième maximum local de précision est atteint pour $\ratioedge = \num{0.8}$, avec une précision de \num{33.33}~\%, des groupes de taille légèrement plus grande~---~en moyenne de $\num{2.02}$~---~et un total de \num{59000}~groupes.
Quelle que soit la valeur du ratio, l’écart-type moyen dans les groupes varie très peu autour de $\num{0.19}$, avec une tendance à augmenter de façon proportionnelle au paramètre.
La valeur du ratio offrant le meilleur compromis entre taille des groupes et précision est donc $\ratioedge = \num{0.8}$.

L’algorithme de segmentation densitaire, quant à lui, montre une bien meilleure performance, atteignant toujours une précision supérieure à \num{76.67}~\% lorsque $\ratioedge < 1$.
Les résultats montrent également une très nette chute de performance lorsque le ratio $\ratioedge$ dépasse $1$ tandis que le paramètre de lissage de l’estimateur de densité, $\ratiodensity$, semble avoir une influence modérée sur les résultats.
Que ce soit en terme du nombre de groupes, de leur taille, de leur écart-type moyen ou de la précision du résultat, l’algorithme de segmentation densitaire montre systématiquement de meilleurs résultats que l’algorithme de recherche locale.
Ici encore, la valeur du ratio offrant le meilleur compromis entre les différentes mesures semble être $\ratioedge = \num{0.8}$.

Des tests supplémentaires doivent être réalisés pour déterminer dans quelle mesure le réglage de ces paramètres dépend du type de jeu de données d’entrée.

La supériorité des résultats de l’algorithme de segmentation densitaire s’obtient au prix d’une complexité plus élevée d’un facteur $n$, ce qui est une différence importante sur les jeux de données qui contiennent généralement de l’ordre de cent photographies.
Des améliorations sont envisageables pour améliorer cette complexité~; par exemple, le voisinage considéré autour du point lors de la recherche de son parent dans l’arbre de densité pourrait être limité aux points situés dans une fenêtre d’images autour de celui-ci.
Cette extension de l’algorithme pourrait également avoir une influence sur la qualité du résultat, en limitant la possibilité d’associations aberrantes avec des points situés sur des images très éloignées de l’image courante.
