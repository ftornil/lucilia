\chapter{Mise en contexte du projet}
\label{chapter:contexte}

\section{Applications pratiques de la reconstruction automatisée}

\begin{figure}[bht]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{contexte/application-chambord-nuage}
        \caption{\textbf{Nuage de points issu de la numérisation du château de Chambord} réalisée à l’École Nationale des Sciences Géographiques illustrant l’intérêt de la numérisation tridimensionnelle pour la sauvegarde du patrimoine.}
        \label{fig:contexte-appli-chambord}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{contexte/application-earth}
        \caption{\textbf{Rendu d’un modèle du cœur de Montpellier issu du logiciel Google Earth.} Google utilise des procédés photogrammétriques pour construire des modèles en trois dimensions de la surface de la~Terre.}
        \label{fig:contexte-appli-google}
    \end{minipage}
\end{figure}

\section{Méthodes de reconstruction et photogrammétrie}

Le besoin d’automatiser la numérisation en trois dimensions a donné lieu à la mise au point d’une variété de techniques de reconstruction qui se divisent en deux grands groupes~: les techniques avec contact ou sans contact.
Les procédés de numérisation avec contact emploient des appareils munis de bras palpant les objets à mesurer.
Bien qu’ils requièrent d’acquérir chaque objet individuellement et leur fassent courir le risque d’une altération, ces procédés permettent d’obtenir des modèles très précis, ce qui les rend utiles notamment dans l’industrie pour vérifier les défauts de production.
Pour des objets fragiles ou des scènes étendues, ces méthodes sont toutefois incommodes, ce qui a justifié le développement de techniques sans contact qui s’affranchissent de ce type d’appareil et acquièrent l’information nécessaire en captant ou en émettant des ondes électromagnétiques (captant ainsi un signal de lumière visible ou infrarouge, par exemple) ou sonores.
Ces procédés sont au cœur d’applications telles que la télédétection, la réalité augmentée, la reconnaissance d’objets ou la sauvegarde du patrimoine~\parencite{remondino-2016-3d,wei-2013-applications} illustrées en figures~\ref{fig:contexte-appli-chambord}~et~\ref{fig:contexte-appli-google}.

\section{Processus d’acquisition de structure par mouvement}

\begin{wrapfigure}{r}{.5\textwidth}
    \fbox{\centering\input{fig/contexte/scanning-techniques}}
    \caption{\textbf{Principales techniques de numérisation tridimensionnelle actuelles.} Les procédés d’acquisition sans contact actifs ou passifs sont distingués, selon qu’ils émettent des ondes dans l’environnement ou qu’ils se contentent de les capter.}
    \label{fig:contexte-techniques}
\end{wrapfigure}

Ce problème d’acquisition de structure par mouvement~(appelé dans la littérature anglophone~\emph{\enquote{structure from motion}}) fait partie de l’ensemble des techniques de numérisation photogrammétriques (cf.~figure~\ref{fig:contexte-techniques}), c’est-à-dire des techniques qui permettent d’obtenir de l’information sur un environnement physique à partir d’images de cet environnement~\parencite{forstner-2016-photogrammetric}. Son objectif est, selon \cite{waxman-1985-sfm}, «~l’inversion du processus de création d’un flux d’images afin de déterminer la structure de l’objet visualisé et le déplacement relatif dans l’espace de l’observateur par rapport à l’objet~»\footnote{\emph{\enquote{The goal is then to invert the image flow process along the line of sight and thereby to determine the local structure of the object under view and the relative motion in space between the object and the observer}}~\parencite[p.~1, traduction personnelle]{waxman-1985-sfm}.}.

\citeauthor{nister-2007-sfm-nphard} ont démontré en~\citeyear{nister-2007-sfm-nphard} que ce problème est \NPHardClass{} dès lors qu’il existe des points visibles dans certaines images du flux et masqués dans d’autres.
Dans le cadre de ce projet, et de la plupart des situations concrètes de reconstruction, cette condition est vérifiée\footnote{Par exemple, en tournant autour d’un cube, il n’est pas possible d’en voir les six faces en même temps.}.
Par conséquent, nous nous intéressons dans ce projet aux heuristiques qui permettent de fournir un modèle se rapprochant au mieux de la réalité dans une majorité de situations.
Ces heuristiques pouvant se révéler coûteuses en calculs, une attention particulière est prêtée à la réduction du temps de calcul et aux possibilités de distribution du calcul.

Pour un être humain comme pour un ordinateur, l’observation d’un objet complexe et inconnu selon un seul point de vue ne permet pas à elle seule d’appréhender sa structure tridimensionnelle.
En effet, si, dans une image, la direction dans laquelle se trouve un point par rapport à l’observateur est directement lisible, il est en général difficile d’estimer sa distance.
En revanche, lorsqu’un même point est reconnu dans plusieurs images, l’espace des positions plausibles à laquelle il pourrait se trouver est réduit, ce qui rend possible l’estimation de la structure de la scène en utilisant un appareil photographique (en l’occurrence un drone) en mouvement en son sein.
La mise au point d’algorithmes efficaces pour ce problème~\NPHardClass{} est un domaine de recherche actif depuis le début des années~1980~\parencite[p.~487]{wei-2013-applications} et nous avons décidé d’utiliser le processus suivant, inspiré des travaux de \cite{forstner-2016-photogrammetric}~(chapitre~11) et~\cite{szeliski-2011-computervision}~(section~4.1).
Ce processus est également illustré en figure~\ref{fig:contexte-pipeline}.

\paragraph{Étalonnage de l’appareil.} Chaque appareil photographique projette les rayons lumineux de façon différente sur son capteur.
Cette projection peut être modélisée mathématiquement par une transformation projective de l’espace de la scène vers l’espace de l’image, suivi d’une distortion radiale liée à la lentille utilisée.
Les paramètres de projection et de distorsion propres à l’appareil utilisé pour capturer le flux d’images sont initialement estimés~\parencite{zhang-2000-calibration} puis sont supposés être invariants tout au long de la capture.

\paragraph{Correction de la distorsion des images.} Les traitements réalisés sur les images dans les étapes suivantes requièrent que celles-ci soient exemptes de distorsions radiales.
Les images sont donc rectifiées en utilisant les paramètres déterminés précédemment pour annuler la distorsion.

\paragraph{Détection de points caractéristiques.} Certains points des images sont plus facilement reconnaissables sous une variété de points de vue grâce à la structure de leur voisinage.
Ces points sont dits \emph{caractéristiques} des images.
À cette étape, les images sont traitées individuellement pour y reconnaître des points susceptibles d’en être caractéristiques.
Dans la suite, ces points seront utilisés comme base pour reconstruire le modèle tridimensionnel~\parencite{lowe-2004-sift,bay-2008-surf}.

\paragraph{Association des points caractéristiques similaires.} L’enjeu de cette étape du processus est d’identifier des groupes de points caractéristiques qui, à travers le flux d’images, correspondent à l’observation du même point de la scène.
Ces groupes de correspondances doivent être formés de façon cohérente~: deux points d’une même image ne peuvent pas coïncider avec le même objet physique, un point d’une image ne peut pas être la projection de deux points de la scène et les correspondances les plus plausibles doivent être favorisées.

\paragraph{Reconstruction d’un nuage de points caractéristiques et de la trajectoire de l’appareil.} Lorsqu’un point est reconnu sur deux images différentes, sa position dans l’espace peut être estimée par le procédé de \emph{triangulation}\footnote{Ce procédé ne doit pas être confondu avec la notion homonyme de triangulation en géométrie qui consiste à partitionner un objet à $n$ dimensions en un ensemble de $n$-simplexes.} en exploitant les propriétés de la géométrie projective.~\parencite[p.~262]{hartley-2004-multiple}.
De même, lorsqu’au moins cinq points sont associés entre deux images, le déplacement relatif de l’appareil d’une image à l’autre peut être estimé.
Cette estimations n’exploitent cependant pas toute l’information du flux d’images car elles ne sont basées que sur des paires d’images.

\paragraph{Ajustement des positions des points et de la trajectoire de l’appareil.} Lorsque des points sont reconnus sur au moins trois images, les estimations de position et de trajectoire peuvent être encore affinées avec l’\emph{ajustement de faisceaux}~(connu dans la littérature anglophone comme le \emph{\enquote{bundle adjustment}}), un algorithme en programmation non-linéaire~\parencite[sections~18.1 et~18.6]{hartley-2004-multiple}.

\paragraph{Reconstruction de la surface tridimensionnelle.} À partir du nuage de points caractéristiques et des positions estimées de l’appareil, cette dernière étape consiste à estimer une surface passant par le nuage et texturisée par les photographies du flux~\parencite[section~4]{pollefeys-2001-image-based}.

\begin{figure}[htb]
    \hspace{-1.75em}
    \input{fig/contexte/pipeline}
    \caption{\textbf{Vue d’ensemble du processus d’acquisition de structure par mouvement} utilisé pour ce projet.}
    \label{fig:contexte-pipeline}
\end{figure}

