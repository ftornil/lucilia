\chapter{Détection de points caractéristiques dans des images d’une scène}
\label{chapter:detection}

\TK{Recherche de points qui peuvent être facilement repérés sous d’autres angles de vue (reproducibilité) et ce en vue de la phase suivante. Quels sont ces points~? -> contour, coin cf. figure 3.1}

\begin{figure}[htb]
    \centering
    \input{fig/detection/neighborhood-structures}
    \caption{\textbf{Types de structures locales observables dans une image.} Image adaptée d’une photographie d’Ildar Sagdejev sous licence CC-BY-SA.}
\end{figure}

\begin{figure}[htb]
    \input{fig/detection/image-as-matrix}
    \hfill
    \input{fig/detection/image-as-signal}
    \caption{\textbf{Deux modélisations équivalentes d’une image.} Les images sont généralement stockées en mémoire d’un ordinateur comme une matrice d’intensités lumineuses captées sur un ensemble fini de points~(à gauche). De façon équivalente, les images peuvent être vues comme un signal bidimensionnel d’intensité lumineuse~(à droite).}
\end{figure}

\section{Localisation des contours avec l’opérateur de Sobel}

\TK{Modélisation de l’image comme un signal bidimensionnel. Contours = disparité d’intensité. Disparités = amplitude maximale du gradient du signal ou annulation du laplacien. Approximations du gradient ou du laplacien par des différences finies (développement limité). Filtre de Sobel et problèmes de localité d’un contour.}

\section{Caractérisation des coins avec le détecteur de Harris}

\TK{Détecteur de Harris et problèmes d’échelle.}

\section{Classification de l’échelle et de l’orientation d’un point caractéristique}

\TK{Notion d’espace d’échelles pour détecter les coins à petite et grande échelle. Algorithme de détection SIFT par différence de Gaussiennes.}

\section{Mesures de la similarité de points caractéristiques}

Pour déterminer si deux points caractéristiques sont homologues, la seule information à disposition est celle des pixels avoisinants dans leur image respective.
Intuitivement, au plus la \enquote{forme} du voisinage des deux points est différente, au moins ceux-ci ont de chance de correspondre au même point de la scène.
Ce voisinage peut être pris de taille arbitraire~: un voisinage plus grand permettra d’augmenter l’information disponible pour la comparaison et donc d’améliorer la spécificité au détriment de la sensibilité, et inversement pour un voisinage plus petit.
Par exemple, le contenu d’un voisinage étendu change rapidement lorsque le point de vue évolue en rotation autour d’un objet, durant laquelle seul le voisinage proche reste comparable d’une image à l’autre.
En revanche, un voisinage trop restreint a une forte probabilité de se répéter à plusieurs endroits de l’autre image et de causer des faux positifs.

Pour une taille de voisinage $n \times n$ donnée, la différence de forme~—~ou dissemblance~—~des voisinages $\Neighborhood{i}{j}$ et $\Neighborhood{i'}{j'}$ de deux points caractéristiques $\Feature{i}{j}$ et $\Feature{i'}{j'}$ peut être mesurée de plusieurs façons.
Une mesure de dissemblance est d’autant meilleure qu’elle tend vers zéro lorsque deux vues du même point de la scène lui sont données~\parencite[p.~117]{shakhnarovich-2005-similarity}.

\paragraph{Distance pixel par pixel.} Somme des carrés des différences d’intensité des pixels deux à deux.

\[
    \PixelDissimilarity\left(\Feature{i}{j}, \Feature{i'}{j'}\right)
    = \sum_{x=1}^n\sum_{y=1}^n
    \left(\Neighborhood{i}{j}(x,y)-\Neighborhood{i'}{j'}(x,y)\right)^2
\]

Avec cette mesure basique, deux voisinages identiques sont à distance nulle et les voisinages sont d’autant plus éloignés que leur contenu diffère. En revanche, il suffit d’une variation d’éclairage d’une image à l’autre pour que deux vues du même point soient classifiées comme différentes~: la fonction $\PixelDissimilarity$ n’est pas \emph{robuste} aux variations de luminosité ou de contraste~(cf.~première ligne de la table~\ref{table:association-patch-distance}).

\paragraph{Corrélation des intensités.} Covariance normalisée des intensités dans chaque voisinage. En notant respectivement $\bar N$ et $\sigma_N$ l’intensité moyenne et l’écart-type des intensités dans un voisinage,

\[
    \CorSimilarity\left(\Feature{i}{j}, \Feature{i'}{j'}\right)
    = \frac{\sum_{x=1}^n\sum_{y=1}^n
    \left(\Neighborhood{i}{j}(x,y)-\bar\Neighborhood{i}{j}\right)
    \left(\Neighborhood{i'}{j'}(x,y)-\bar\Neighborhood{i'}{j'}\right)}
    {\sigma_{\Neighborhood{i}{j}}\sigma_{\Neighborhood{i'}{j'}}}.
\]

Ce coefficient de corrélation varie dans l’intervalle $[-1;1]$.
Il vaut $1$ lorsque les intensités des pixels des deux voisinages sont linéairement corrélés et sont donc de forme similaire.
Cette fonction est robuste aux variations de luminosité et de contraste grâce à la normalisation par rapport à l’intensité moyenne et à l’écart-type de l’intensité de chaque voisinage.
Cependant, si le point de vue sur l’objet évolue d’une image à l’autre, causant par exemple localement une rotation du voisinage, par exemple suite à un changement de point de vue, la corrélation faiblit~(cf.~deuxième ligne de la table~\ref{table:association-patch-distance}).

\paragraph{Distance de descripteurs.} Un \emph{descripteur} du voisinage d’un point est un vecteur qui encode sa forme de façon robuste aux variations de luminosité, de contraste ou de point de vue qu’il peut subir d’une image à l’autre.
La similarité de deux voisinages $\Neighborhood{i}{j}$ et $\Neighborhood{i'}{j'}$ pour lesquels des descripteurs $\Descriptor(\Feature{i}{j})$ et $\Descriptor(\Feature{i}{j})$ ont été calculés est évaluée en mesurant la distance entre ces vecteurs~\parencite[section~4.1.2]{szeliski-2011-computervision}.

\begin{tocome}
Le descripteur SIFT fournit une description indépendante de l’échelle, de l’orientation et des variations de luminosité et de contraste. Chaque point caractéristique est paramétré par $\left< (x, y), \sigma, \theta, f\right >$ où $(x, y)$ est la position du pixel central dans l’image, $\sigma$ est l’écart-type du flou gaussien utilisé pour détecter le point considéré, $\theta$ est l’orientation du point et $f$ est un vecteur décrivant le voisinage du point.

Dans l’algorithme SIFT, le voisinage d’un point caractéristique est considéré comme étant un carré de $16 \times 16$ pixels autour du pixel central. Il est divisé en seize blocs de $4 \times 4$ pixels. Le gradient de l’image est évalué pour chaque point de ce voisinage selon les fonctions approchées \begin{equation}
    \begin{cases}
        m\left(x,y\right) &= {\sqrt {\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)^{2}+\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)^{2}}}\\
        \theta\left(x,y\right) &= {\mathrm{tan}^{-1}}\left(\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)/\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)\right),
    \end{cases}
\end{equation} donnant respectivement la norme et la direction du vecteur gradient d’un pixel situé aux coordonnées $(x, y)$ de l’image. Dans ces équations, $L\left(x,y\right)$ désigne l’image obtenue en appliquant un lissage gaussien d’écart-type $\sigma$ sur l’image originelle, et $\mathrm{tan}^{-1}$ désigne la fonction arc tangente.

Pour chaque bloc du voisinage, un histogramme des gradients des points qu’il contient est calculé. Celui-ci est discrétisé sur 8~directions possibles. Chaque vecteur gradient du bloc considéré contribue à l’histogramme de façon proportionnelle à sa norme et inversement proportionnelle à sa distance au point d’intérêt~\parencite[p.~13]{lowe-2004-sift}. Enfin, les normes de chaque vecteur de l’histogramme de chaque zone sont stockées dans le vecteur $f$ de dimension $16 \times 8 = 128$, qui est le descripteur du point. La figure~\ref{fig:sift-feature-description} illustre ce processus.

\begin{figure}[htb]
    \centering
    \input{fig/association/sift-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SIFT d’un point caractéristique d’une image.} Le gradient de chaque pixel dans le voisinage du point est évalué, puis un histogramme est calculé pour chaque bloc de taille $4 \times 4$. L’ensemble des valeurs des histogrammes constitue le descripteur. Ici, seulement $8 \times 8$ pixels du voisinage sont illustrés par souci de clarté.}
    \label{fig:sift-feature-description}
\end{figure}

Les descripteurs calculés par l’algorithme SIFT sont normalisés et donc invariants par changement de luminosité. Ils sont uniquement basés sur le motif de variation d’intensité autour des points d’intérêt et sont donc invariants par rotation ou changement d’échelle~\parencite[p.~16]{lowe-2004-sift}.

\cite{bay-2008-surf}, ont également proposé une modification du descripteur SIFT~: le descripteur SURF, dont le calcul est effectué plus rapidement en utilisant l'ondelette de Haar ou \emph{«~Haar wavelet~»}, décrite par la formule \ref{equ:haar-wavelet-def}~\parencite{cheng-2011-haar}. Cette fonction permet de détecter des changements brutaux dans les valeurs. Dans le cadre de l'algorithme SURF, ces valeurs correspondent aux valeurs des pixels dans un cercle alentour au point détecté, en x et en y.

\begin{equation}
    \psi (t)=
        \begin{cases}
            1\quad &0\leq t<{\frac {1}{2}},\\
            -1&{\frac {1}{2}}\leq t<1,\\
            0&{\mbox{sinon.}}
        \end{cases}
    \label{equ:haar-wavelet-def}
\end{equation}

Pour déterminer l'orientation générale d'un point, la somme des valeurs est effectuée et la direction générale indiquée est affectée à la direction du point. La description du voisinage est quand à elle effectuée en calculant la somme du résultat de l'ondelette de Haar dans des sous-régions autour du point, comme illustré dans l'image \ref{fig:surf-feature-description}.

\begin{figure}[htb]
    \centering
    \input{fig/association/surf-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SURF d’un point caractéristique d’une image.} Au lieu de calculer un histogramme des directions de gradient possible dans chaque bloc, la somme de tous les vecteurs d’un bloc est choisie comme étant son représentant.}
    \label{fig:surf-feature-description}
\end{figure}
\end{tocome}

\TK{Présentation des descripteurs binaires utilisant un motif de comparaison fixe. Avantages d’un descripteur binaire en temps de calcul. Descripteurs \ORB, \BRISK{} et \FREAK.}

\TK{Analyse des résultats de comparaison des descripteurs en table 4.1.}

Dans la suite, la fonction de dissemblance choisie pour déterminer la distance entre le voisinage de deux points caractéristiques est notée $\Dissimilarity$.

\begin{table}[htb]
    \centering
    \begingroup\midsepremove
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{%
            >{\raggedleft\arraybackslash}m{2.5cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}}
        &\textbf{Référence}
        &\textbf{Baisse de la luminosité}
        &\textbf{Augmentation du contraste}
        &\textbf{Changement point de vue}
        &\textbf{Autre objet}\\[7.5pt]

        &\includegraphics{association/patch-distance-ref}
        &\includegraphics{association/patch-distance-lower-luminosity}
        &\includegraphics{association/patch-distance-higher-contrast}
        &\includegraphics{association/patch-distance-viewpoint-change}
        &\includegraphics{association/patch-distance-other}\\[15pt]

        \toprule
        \TintMinValue{0} \TintMaxValue{2792120}
        \textbf{Distance pixel par pixel~($\PixelDissimilarity$)}
        & \Tint{0}
        & \Tint{1670544}
        & \Tint{356007}
        & \Tint{309174}
        & \Tint{2792120}\\

        \midrule
        \TintMinValue{1} \TintMaxValue{-0.081}
        \textbf{Corrélation des intensités~($\CorSimilarity$)}
        & \Tint{1.000}
        & \Tint{1.000}
        & \Tint{0.997}
        & \Tint{0.318}
        & \Tint{-0.081}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{621.436}
        \textbf{\SIFT~($\EuclDissimilarity$)}
        & \Tint{0.000}
        & \Tint{4.243}
        & \Tint{64.722}
        & \Tint{63.182}
        & \Tint{621.436}\\

	\midrule
        \TintMinValue{0} \TintMaxValue{1.008}
        \textbf{\SURF~($\EuclDissimilarity$)}
        & \Tint{0.000}
        & \Tint{0.023}
        & \Tint{0.021}
        & \Tint{0.124}
        & \Tint{1.008}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{150}
        \textbf{\ORB~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{4}
        & \Tint{10}
        & \Tint{8}
        & \Tint{150}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{180}
        \textbf{\BRISK~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{0}
        & \Tint{8}
        & \Tint{23}
        & \Tint{180}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{153}
        \textbf{\FREAK~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{4}
        & \Tint{30}
        & \Tint{26}
        & \Tint{153}\\

        \bottomrule\addlinespace[\belowrulesep]
    \end{tabular}
    \endgroup
    \caption{\textbf{Comparaison de la spécificité et de la sensibilité des fonctions de dissemblance étudiées.} Chaque image est comparée à l’image de référence de la première colonne à l’aide de chaque fonction de dissemblance. Les couleurs sont attribuées proportionnellement à la valeur de similarité ou de dissemblance, avec en vert les scores les plus proches de la valeur de similarité maximale et en rouge les scores les plus proches de la valeur de dissemblance maximale.}
    \label{table:association-patch-distance}
\end{table}

\TK{Pistes de poursuite~:
\begin{itemize}
    \item Mise au point d’un descripteur à l’aide d’apprentissage machine~? Génération de données d’apprentissage par projection de modèles 3D aléatoires.
\end{itemize}}
