\subsection{La triangulation de Delaunay}
\label{subsection:sr_delaunay_and_cgal_1_delaunay}

\vspace{2em}

Comme vu auparavant, Delaunay est une méthode extrêmement populaire de reconstruction de modèle 3D à partir de nuages de points. En plus
d'être une des méthodes les plus anciennes de reconstruction, c'est aussi une des plus utilisées dans les domaines comme les 
télécommunications, où une triangulation de Delaunay est parfois utilisée pour recouvrir le plus efficacement une ville afin d'obtenir une
couverture réseau adéquate sur la totalité d'un quartier. 

\vspace{2em}

Dans n'importe quel nuage de points, la triangulation de Delaunay possède l'avantage d'être l'unique triangulation pavant 
l'espace de façon régulière, et surtout de façon \textit{équiangulaire}. En effet, la triangulation de Delaunay d'un nuage de points
est définie comme suit : 

\begin{align}
	\parbox[h][][c]{\textwidth}{Pour tout nuage de points $\mathcal{P}$ dans un espace $\mathbb{R}^{\mathcal{d}}$, la triangulation 
	de Delaunay est le dual du diagramme de Voronoï (qui est lui-même unique). Elle est définie comme suit : si aucune hypersphère de 
	l'espace ne contient $\mathcal{d}+2$ points de $\mathcal{P}$, alors $\textit{Del}(\mathcal{P})$ est une triangulation de $\mathcal{P}$.}
\end{align}
\label{definition:delaunay_triangulation}

Nous avons donc choisi en premmier lieu d'implémenter un algorithme de triangulation comme décrit dans~\cite{joe-1990-triangulation}.
Il permet d'obtenir la triangulation de Delaunay de n'importe quel nuage de points en trois dimensions en utilisant uniquement des
transformations locales des polytopes créés incrémentalement par l'algorithme. L'algorithme présenté par Joe est en fait une 
adaptation en trois dimensions de la triangulation de Delaunay par visibilité décrite en deux dimensions. 

\vspace{2em}

En effet, l'algorithme commence d'abord par ordonner les points selon l'ordre lexicographique de leurs coordonnées. Ensuite, il crée 
un tétraèdre à partir des quatre premiers points non-coplanaires du nuage, notée $\mathcal{T}_0$, et parcourt les points, en créant des
polytopes avec toutes les faces de la bordure de la triangulation intermédiaire à l'étape $i$ notée $\mathcal{T}_{i}$ qui sont visibles
depuis ce point, pour créer la triangulation suivante $\mathcal{T}_{i+1}$. Il vérifie ensuite récursivement sur les faces de chaque 
polytope créé (et les faces des polytopes adjacents à ceux ci) qu'ils ne violent pas la condition définie dans le paragraphe ci-dessus.

\vspace{2em}

Conformément aux conditions déclarées par la définition de la triangulation de Delaunay, on peut donc déterminer qu'en dimension 3, il
ne faut pas que pour tout tétraèdres $(A,B,C,D)$ et $(A,B,C,E)$, la sphère circonscrite à $(A,B,C,D)$ ne contienne le point $E$ (et 
inversement). Auquel cas, on devra effectuer un changement de la topologie locale des deux tétraèdres afin de remplir la condition de
la triangulation de Delaunay. Joe décrit donc cinq configurations possibles pour les procédures de transformations locales, comme
décrites ci-après :

\vspace{1em}

\begin{itemize}
	\item \textit{Configuration 1} : les points $A$, $B$, $C$, $D$ et $E$ font tous partie de l'enveloppe convexe de \\
	$(A,B,C,D,E)$. Alors si $DE$ intersecte le triangle $ABC$, deux triangulations sont possibles. Une première triangulation 
	serait possible avec les tétraèdres $ABCD$ et $ABCE$, une autre possiblité serait d'avoir les tétraèdres $ABDE$, $ACDE$, et $BCDE$ : \\ 
	\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.40\linewidth]{fig/surface_reconstruction/delaunay_01_config1a.png}
			\subcaption{Configuration 1 - Première triangulation possible}
		\end{subfigure}
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.40\linewidth]{fig/surface_reconstruction/delaunay_01_config1b.png}
			\subcaption{Configuration 1 - Seconde triangulation possible}
		\end{subfigure}
	\end{figure}

	\item \textit{Configuration 2} : l'enveloppe convexe ne contient que quatre des cinq points. Le dernier point ($A$ dans cet exemple) est 
	donc dans le tétraèdre $BCDE$. Dans ce cas là, une seule triangulation est possible : $ABCD$, $ABCE$, $ABDE$, $ACDE$ : 
	\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.40\linewidth]{fig/surface_reconstruction/delaunay_02_config2.png}
			\subcaption{Configuration 2 - seule triangulation possible}
		\end{subfigure}
	\end{figure}

	\item \textit{Configuration 3} : quatre des cinq points sont coplanaires (par exemple, $A$, $B$, $D$, et $E$). Alors si $ABDE$ 
	est strictement convexe (ne peut pas être dégénéré en triangle) alors il existe deux triangulations possibles.\\La première 
	contient les tétraèdres $ABCD$ et $ACDE$, alors que la seconde contient les tétraèdres $ACDE$, $BCDE$ : 
	\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.4\linewidth]{fig/surface_reconstruction/delaunay_03_config3a.png}
			\subcaption{Configuration 3 - Première triangulation possible}
		\end{subfigure}
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.4\linewidth]{fig/surface_reconstruction/delaunay_03_config3b.png}
			\subcaption{Configuration 3 - Seconde triangulation possible}
		\end{subfigure}
	\end{figure}

	\item \textit{Configuration 4} : quatre des cinq points sont coplanaires (par exemple, $A$, $B$, $D$, et $E$). Si $ABDE$ peut se dégénérer en un triangle, alors une seule triangulation est possible : elle contient $ABCD$ et $BCDE$ : 
	\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.40\linewidth]{fig/surface_reconstruction/delaunay_04_config4.png}
			\subcaption{Configuration 4 - seule triangulation possible}
		\end{subfigure}
	\end{figure}

	\item \textit{Configuration 5} : quatre des cinq points sont coplanaires (par exemple, $A$, $B$, $D$, et $E$). Alors si un point ($A$ par exemple) est à l'intérieur de $BDE$, alors $ABDE$ n'est pas convexe, et une seule triangulation est possible : $ABCD$, $ABCE$, et $ACDE$ : 
	\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
		\begin{subfigure}{0.4\linewidth}
		\centering
			\includegraphics[width=0.40\linewidth]{fig/surface_reconstruction/delaunay_05_config5.png}
			\subcaption{Configuration 5 - seule triangulation possible}
		\end{subfigure}
	\end{figure}
\end{itemize}

Parmi ces configurations possibles, on remarque que certaines proposent deux triangulations valables et légales (on dit d'un polytope qu'il
est légal s'il respecte la condition de Delaunay donnée auparavant \ref{definition:delaunay_triangulation}). Joe propose donc une preuve que
son algorithme peut créer une triangulation respectant le critère de Delaunay grâce à une suite finie de transformations locale, qui sont des
transformations de configurations quand deux triangulations d'un polytope sont valables et légales (comme dans les configurations 1 et 3).

Cet algorithme construit donc incrémentalement une triangulation de Delaunay en trois dimensions, mais possède quelques particulrités qui sont
considérées comme néfastes pour l'utilisation que l'on compte en faire. En
effet, cette façon de créer une triangulation d'un espace construit aussi une enveloppe convexe du nuage de points, ce qui n'est pas du
tout adapté à ce projet. Prenons le cas suivant, en deux dimensions (le même principe est applicable en trois dimensions) :

% Note : mettre l'exemple de Delaunay pour polygone concave ici (see http://www.cs.cmu.edu/~quake/triangle.delaunay.html)
% see also : https://travellermap.com/tmp/delaunay.htm
\begin{figure}[H]
	\captionsetup[subfigure]{justification=centering}
	\centering
	\begin{subfigure}{0.4\linewidth}
		\centering
		\includegraphics[width=0.49\linewidth]{fig/surface_reconstruction/delaunay_ch_cloudpoint.png}
		\subcaption{Nuage de points non convexe}
	\end{subfigure}
	\begin{subfigure}{0.4\linewidth}
		\centering
		\includegraphics[width=0.49\linewidth]{fig/surface_reconstruction/delaunay_ch_meshed.png}
		\subcaption{Triangulation de Delaunay résultante}
	\end{subfigure}
\end{figure}

On peut voir ici que lors de la construction de la triangulation par Delaunay, l'enveloppe convexe a aussi été crée, dû au fait que
$\textit{Del}(\mathcal{P}) = \textit{Dual}(\textit{Voronoï}(\mathcal{P}))$. La triangulation de Delaunay, en plus d'être assez complexe
à implémenter seule (comme il était prévu au début du projet), ne convient pas pour ce projet. Heureusement, la triangulation
par Delaunay n'est pas la seule façon d'extraire un modèle 3D à partir d'un nuage de points.
