\ProvidesPackage{biblio}

%%
%% Paquetages
%%

% Inclusion de figures TikZ (doit être chargé en premier)
\usepackage{standalone}

% Mise en page
\usepackage{geometry}
\usepackage{framed}

% Langue
\usepackage{polyglossia}
\usepackage[autostyle=true]{csquotes}
\setmainlanguage{french}
\usepackage{microtype}
\usepackage[
    output-decimal-marker={,},
    group-separator={\,},
]{siunitx}

% Police
\usepackage{fontspec}
\usepackage{xunicode}

\setmainfont[Mapping=tex-text]{Linux Libertine}
\setsansfont[Mapping=tex-text]{XCharter}

\usepackage[raggedright, sf]{titlesec}
\titleformat{\paragraph}[runin]{\bfseries\normalsize}{\theparagraph}{1em}{}

% Couleurs
\usepackage[table]{xcolor}

% Maths
\usepackage{amsmath}
\usepackage{unicode-math}
\usepackage{calc}

% Figures
\usepackage{graphicx}
\graphicspath{{fig/}}

\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes}

\usepackage{forest}

\usepackage{pgfplots}
\pgfplotsset{compat=1.13}

\usepackage{wrapfig}
\usepackage[font=rm]{caption}

% Tableaux
\usepackage{tabularx}
\usepackage{booktabs}
\newcommand{\midsepremove}{\aboverulesep = 0mm \belowrulesep = 0mm}

% Listes
\usepackage{enumitem}
\setlist[itemize]{parsep=0pt,label=—}
\setlist[enumerate]{parsep=0pt}

% Bibliographie
\usepackage[
    style=authortitle,
    citestyle=authoryear,
    maxbibnames=99,
    maxcitenames=3,
    backend=biber
]{biblatex}

\addbibresource{../../../references.bib}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}

\DefineBibliographyStrings{french}{%
    urlseen = {consulté le},
    techreport = {Rapport technique},
}

% Ajustements d’espacements
\setlength\columnsep{.5cm}
\setlength\bibitemsep{1em}

% Création automatique des hyperliens dans le PDF
% (doit être chargé en dernier)
\usepackage[
    bookmarks, % génère une table des matières sous forme de marque pages
    hidelinks, % pas de cadre coloré autour des liens
    colorlinks, % coloration des liens en bleu marine
    linkcolor={blue!80!black},
    citecolor={blue!80!black},
    urlcolor={blue!80!black}
]{hyperref}

% Déplace les ancrages de hyperref pour amener sur le contenu
% d’une figure et non pas sur sa légende (à charger après hyperref)
\usepackage[all]{hypcap}

%%
%% Commandes
%%

% Civilités
\newcommand{\Professeur}{P\textsuperscript{r}}
\newcommand{\Professeurs}{P\textsuperscript{rs}}
\newcommand{\Docteur}{D\textsuperscript{r}}
\newcommand{\Docteurs}{D\textsuperscript{rs}}
\newcommand{\Madame}{M\textsuperscript{me}}
\newcommand{\Mesdames}{M\textsuperscript{mes}}
\newcommand{\Monsieur}{M.}
\newcommand{\Messieurs}{MM.}

% Superviseur
\newcommand{\supervisor}[1]{\newcommand{\@supervisor}{#1}}

% Acronymes et abbréviations
\newcommand{\acronym}[1]{\textsc{#1}}
\newcommand{\CMI}{\acronym{Cmi}}
\newcommand{\SIFT}{\acronym{Sift}}
\newcommand{\SURF}{\acronym{Surf}}
\newcommand{\ORB}{\acronym{Orb}}
\newcommand{\BRISK}{\acronym{Brisk}}
\newcommand{\FREAK}{\acronym{Freak}}

% À faire
\newcommand{\TK}[1]{\textcolor{red}{\textbf{[[#1]]}}}
\newenvironment{tocome}%
    {\begingroup\color{red}\bfseries[[}%
    {]]\endgroup}

% Définition et référence à un problème
\newcommand{\RefProblem}[1]{\textsc{#1}}
\newcommand{\ProblemName}[1]{\def\@ProblemName{#1}}
\newcommand{\ProblemInput}[1]{\def\@ProblemInput{#1}}
\newcommand{\ProblemOutput}[1]{\def\@ProblemOutput{#1}}
\newenvironment{problem}{\begingroup}{\addvspace{.5\baselineskip}
    \begin{tabularx}{\dimexpr\linewidth-\parindent}{@{} r @{\enskip} X}
        \textbf{Problème~:} & \RefProblem{\@ProblemName}\\
        \textbf{Entrée~:} & \@ProblemInput\\
        \textbf{Sortie~:} & \@ProblemOutput
    \end{tabularx}%
\par\addvspace{.5\baselineskip}\endgroup}

% Figure en deux colonnes avec légende sur le côté
\newsavebox\@DuoBox
\newcommand{\DuoContent}[1]{\def\@DuoContent{#1}}
\newcommand{\DuoCaption}[1]{\def\@DuoCaption{#1}}
\newcommand{\DuoFigure}[1]{\def\@DuoFigure{#1}}
\newenvironment{duo}[1][5cm]{%
\begingroup%
\edef\savedwidth{#1}%
\edef\savedindent{\the\parindent}%
}{%
% Mesure de la hauteur de la figure
\begin{lrbox}{\@DuoBox}
\begin{minipage}[t]{\savedwidth}%
    \mbox{}\\[-\baselineskip]%
    \resizebox{\savedwidth}{!}{\@DuoFigure}
\end{minipage}
\end{lrbox}%
% Rendu des deux colonnes
\noindent%
\begin{minipage}[t][\ht\@DuoBox+\dp\@DuoBox]{\dimexpr\textwidth-\savedwidth-\columnsep}%
    \mbox{}\\[-\baselineskip]%
    \setlength\parindent{\savedindent}%
    \hspace*{\parindent}%
    \@DuoContent

    \vfill
    \footnoterule\par%
    \captionsetup[figure]{labelformat=side}%
    \@DuoCaption%
    \captionsetup[figure]{labelformat=default}
\end{minipage}\hfill\usebox\@DuoBox
\endgroup%
}

% Style de légende pour signifier que la figure est située ci-contre
\DeclareCaptionLabelFormat{side}{\textsc{#1 #2}~(ci-contre)}

% Coloration d’une valeur en fonction de son amplitude
\definecolor{TintMinRed}{HTML}{FF7973}
\def\@TintMinColor{TintMinRed}
\newcommand{\TintMinColor}[1]{\global\def\@TintMinColor{#1}}

\definecolor{TintMaxGreen}{HTML}{7ACC84}
\def\@TintMaxColor{TintMaxGreen}
\newcommand{\TintMaxColor}[1]{\global\def\@TintMaxColor{#1}}

\def\@TintMinValue{0}
\newcommand{\TintMinValue}[1]{\global\def\@TintMinValue{#1}}
\def\@TintMaxValue{255}
\newcommand{\TintMaxValue}[1]{\global\def\@TintMaxValue{#1}}

\newcommand{\Tint}[1]{%
    % Calcul de la proportion de la valeur donnée entre la valeur min et max
    % (il est nécessaire d’utiliser le moteur de précision étendue FPU)
    \pgfkeys{/pgf/fpu}%
    \pgfmathparse{100.0*(#1-\@TintMinValue)/(\@TintMaxValue-\@TintMinValue)}%
    \pgfmathfloattoint{\pgfmathresult}%
    \edef\proportion{\pgfmathresult}%
    \pgfkeys{/pgf/fpu=false}%
    % Application de la proportion de couleur entre la couleur min et max
    \edef\x{\noexpand\cellcolor{\@TintMinColor!\proportion!\@TintMaxColor}}%
    \x\num{#1}%
}

%%
%% Notations
%%

% Ensembles
\newcommand{\NaturalSet}{\mathbb{N}}
\newcommand{\RealSet}{\mathbb{R}}

% Points caractéristiques
\newcommand{\ImageStreamSize}{n}
\newcommand{\Feature}[2]{x_{#1}^{#2}}
\newcommand{\FeatureSet}[1]{X_{#1}}
\newcommand{\FeatureSetSize}{m}
\newcommand{\FeatureSetAll}{X}

% Images et voisinages
\newcommand{\Image}[1]{I_{#1}}
\newcommand{\Neighborhood}[2]{N_{#1}^{#2}}

% Descripteurs
\newcommand{\Descriptor}{\vec{d}}
\newcommand{\Similarity}{\Sigma}
\newcommand{\Dissimilarity}{\Delta}
\newcommand{\PixelDissimilarity}{\Dissimilarity_P}
\newcommand{\CorSimilarity}{\Sigma_C}
\newcommand{\EuclDissimilarity}{\Dissimilarity_E}
\newcommand{\HammDissimilarity}{\Dissimilarity_H}

% Vocabulaire des graphes
\newcommand{\Graph}{G}
\newcommand{\GraphVertices}{V}
\newcommand{\Vertex}{v}
\newcommand{\GraphEdges}{E}
\newcommand{\Edge}[2]{#1#2}
\newcommand{\GraphDirectedEdges}{A}
\newcommand{\DirectedEdge}[2]{\overrightarrow{\Edge{#1}{#2}}}

% Problèmes
\newcommand{\PClass}{\mathcal{P}}
\newcommand{\NPClass}{\mathcal{NP}}
\newcommand{\NPHardClass}{$\NPClass$-difficile}
\newcommand{\PbDM}{2-correspondance}
\newcommand{\PbKM}{k-correspondance}
\newcommand{\PbNNS}{Plus-proche-voisin}

% Bornes inf, sup, exact
\newcommand{\LoBound}{\Omega}
\newcommand{\ExBound}{\Theta}
\newcommand{\UpBound}{\mathcal{O}}
