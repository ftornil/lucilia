\chapter{Association des points d’une scène observés dans plusieurs images}
\label{chapter:association}

\begin{duo}[4.75cm]
\DuoContent{
    Afin d’être en mesure de positionner dans l’espace de la scène les points caractéristiques détectés dans le flux d’images, les données des images peuvent être exploitées pour faire correspondre entre eux les points caractéristiques qui résultent de l’observation d’un même point de la scène.
    Avec une telle correspondance, il est possible de positionner les points en utilisant la \emph{triangulation} et l’\emph{ajustement de faisceaux}~\parencite{hartley-2004-multiple}.

    Le \emph{problème de correspondance} consiste à rechercher des groupes qui, parmi les points caractéristiques d’un ensemble d’images, ont le plus de chance d’être équivalents~\parencite[section~4.3]{braunstein-empirical-1988}.
    La figure~\ref{fig:association-manual-tracking} est représentative d’une solution possible à ce problème~: remarquons que certains points peuvent être seuls (points~4 et~10) et que les groupes peuvent ne pas s’étendre sur tout l’ensemble d’images (groupes~8 et~11).

    Dans ce chapitre, nous étudions ce problème réduit dans un premier temps à une paire d’images puis généralisé à un ensemble d’images.
}
\DuoCaption{
    \captionof{figure}{Illustration d’une solution possible au problème de correspondance entre trois images. Chaque cercle numéroté indique un point caractéristique. Les points liés entre eux forment des groupes de points équivalents.}
    \label{fig:association-manual-tracking}
}
\DuoFigure{\input{fig/association/manual-point-tracking}}
\end{duo}

\section{Correspondance dans une paire d’images}

Considérons tout d’abord le problème de correspondance ramené à une paire d’images du flux.
Pour simplifier, nous supposons qu’un même point ne peut pas être observé à plusieurs endroits d’une même image, ce qui exclut la présence de surfaces réfléchissantes dans la scène.
Le problème consiste alors à affecter à chaque point de la première image au plus un point de la seconde, en favorisant l’association des points susceptibles de correspondre à l’observation du même point de la scène.
Comment déterminer si deux points caractéristiques sont ainsi homologues~?

\subsection{Mesures de la similarité de points caractéristiques}

Pour déterminer si deux points caractéristiques sont homologues, la seule information à disposition est celle des pixels avoisinants dans leur image respective.
Intuitivement, au plus la \enquote{forme} du voisinage des deux points est différente, au moins ceux-ci ont de chance de correspondre au même point de la scène.
Ce voisinage peut être pris de taille arbitraire~: un voisinage plus grand permettra d’augmenter l’information disponible pour la comparaison et donc d’améliorer la spécificité au détriment de la sensibilité, et inversement pour un voisinage plus petit.
Par exemple, le contenu d’un voisinage étendu change rapidement lorsque le point de vue évolue en rotation autour d’un objet, durant laquelle seul le voisinage proche reste comparable d’une image à l’autre.
En revanche, un voisinage trop restreint a une forte probabilité de se répéter à plusieurs endroits de l’autre image et de causer des faux positifs.

Pour une taille de voisinage $n \times n$ donnée, la différence de forme~—~ou dissemblance~—~des voisinages $\Neighborhood{i}{j}$ et $\Neighborhood{i'}{j'}$ de deux points caractéristiques $\Feature{i}{j}$ et $\Feature{i'}{j'}$ peut être mesurée de plusieurs façons.
Une mesure de dissemblance est d’autant meilleure qu’elle tend vers zéro lorsque deux vues du même point de la scène lui sont données~\parencite[p.~117]{shakhnarovich-2005-similarity}.

\paragraph{Distance pixel par pixel.} Somme des carrés des différences d’intensité des pixels deux à deux.

\[
    \PixelDissimilarity\left(\Feature{i}{j}, \Feature{i'}{j'}\right)
    = \sum_{x=1}^n\sum_{y=1}^n
    \left(\Neighborhood{i}{j}(x,y)-\Neighborhood{i'}{j'}(x,y)\right)^2
\]

Avec cette mesure basique, deux voisinages identiques sont à distance nulle et les voisinages sont d’autant plus éloignés que leur contenu diffère. En revanche, il suffit d’une variation d’éclairage d’une image à l’autre pour que deux vues du même point soient classifiées comme différentes~: la fonction $\PixelDissimilarity$ n’est pas \emph{robuste} aux variations de luminosité ou de contraste~(cf.~première ligne de la table~\ref{table:association-patch-distance}).

\paragraph{Corrélation des intensités.} Covariance normalisée des intensités dans chaque voisinage. En notant respectivement $\bar N$ et $\sigma_N$ l’intensité moyenne et l’écart-type des intensités dans un voisinage,

\[
    \CorSimilarity\left(\Feature{i}{j}, \Feature{i'}{j'}\right)
    = \frac{\sum_{x=1}^n\sum_{y=1}^n
    \left(\Neighborhood{i}{j}(x,y)-\bar\Neighborhood{i}{j}\right)
    \left(\Neighborhood{i'}{j'}(x,y)-\bar\Neighborhood{i'}{j'}\right)}
    {\sigma_{\Neighborhood{i}{j}}\sigma_{\Neighborhood{i'}{j'}}}.
\]

Ce coefficient de corrélation varie dans l’intervalle $[-1;1]$.
Il vaut $1$ lorsque les intensités des pixels des deux voisinages sont linéairement corrélés et sont donc de forme similaire.
Cette fonction est robuste aux variations de luminosité et de contraste grâce à la normalisation par rapport à l’intensité moyenne et à l’écart-type de l’intensité de chaque voisinage.
Cependant, si le point de vue sur l’objet évolue d’une image à l’autre, causant par exemple localement une rotation du voisinage, par exemple suite à un changement de point de vue, la corrélation faiblit~(cf.~deuxième ligne de la table~\ref{table:association-patch-distance}).

\paragraph{Distance de descripteurs.} Un \emph{descripteur} du voisinage d’un point est un vecteur qui encode sa forme de façon robuste aux variations de luminosité, de contraste ou de point de vue qu’il peut subir d’une image à l’autre.
La similarité de deux voisinages $\Neighborhood{i}{j}$ et $\Neighborhood{i'}{j'}$ pour lesquels des descripteurs $\Descriptor(\Feature{i}{j})$ et $\Descriptor(\Feature{i}{j})$ ont été calculés est évaluée en mesurant la distance entre ces vecteurs~\parencite[section~4.1.2]{szeliski-2011-computervision}.

\begin{tocome}
Le descripteur SIFT fournit une description indépendante de l’échelle, de l’orientation et des variations de luminosité et de contraste. Chaque point caractéristique est paramétré par $\left< (x, y), \sigma, \theta, f\right >$ où $(x, y)$ est la position du pixel central dans l’image, $\sigma$ est l’écart-type du flou gaussien utilisé pour détecter le point considéré, $\theta$ est l’orientation du point et $f$ est un vecteur décrivant le voisinage du point.

Dans l’algorithme SIFT, le voisinage d’un point caractéristique est considéré comme étant un carré de $16 \times 16$ pixels autour du pixel central. Il est divisé en seize blocs de $4 \times 4$ pixels. Le gradient de l’image est évalué pour chaque point de ce voisinage selon les fonctions approchées \begin{equation}
    \begin{cases}
        m\left(x,y\right) &= {\sqrt {\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)^{2}+\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)^{2}}}\\
        \theta\left(x,y\right) &= {\mathrm{tan}^{-1}}\left(\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)/\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)\right),
    \end{cases}
\end{equation} donnant respectivement la norme et la direction du vecteur gradient d’un pixel situé aux coordonnées $(x, y)$ de l’image. Dans ces équations, $L\left(x,y\right)$ désigne l’image obtenue en appliquant un lissage gaussien d’écart-type $\sigma$ sur l’image originelle, et $\mathrm{tan}^{-1}$ désigne la fonction arc tangente.

Pour chaque bloc du voisinage, un histogramme des gradients des points qu’il contient est calculé. Celui-ci est discrétisé sur 8~directions possibles. Chaque vecteur gradient du bloc considéré contribue à l’histogramme de façon proportionnelle à sa norme et inversement proportionnelle à sa distance au point d’intérêt~\parencite[p.~13]{lowe-2004-sift}. Enfin, les normes de chaque vecteur de l’histogramme de chaque zone sont stockées dans le vecteur $f$ de dimension $16 \times 8 = 128$, qui est le descripteur du point. La figure~\ref{fig:sift-feature-description} illustre ce processus.

\begin{figure}[htb]
    \centering
    \input{fig/association/sift-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SIFT d’un point caractéristique d’une image.} Le gradient de chaque pixel dans le voisinage du point est évalué, puis un histogramme est calculé pour chaque bloc de taille $4 \times 4$. L’ensemble des valeurs des histogrammes constitue le descripteur. Ici, seulement $8 \times 8$ pixels du voisinage sont illustrés par souci de clarté.}
    \label{fig:sift-feature-description}
\end{figure}

Les descripteurs calculés par l’algorithme SIFT sont normalisés et donc invariants par changement de luminosité. Ils sont uniquement basés sur le motif de variation d’intensité autour des points d’intérêt et sont donc invariants par rotation ou changement d’échelle~\parencite[p.~16]{lowe-2004-sift}.

\cite{bay-2008-surf}, ont également proposé une modification du descripteur SIFT~: le descripteur SURF, dont le calcul est effectué plus rapidement en utilisant l'ondelette de Haar ou \emph{«~Haar wavelet~»}, décrite par la formule \ref{equ:haar-wavelet-def}~\parencite{cheng-2011-haar}. Cette fonction permet de détecter des changements brutaux dans les valeurs. Dans le cadre de l'algorithme SURF, ces valeurs correspondent aux valeurs des pixels dans un cercle alentour au point détecté, en x et en y.

\begin{equation}
    \psi (t)=
        \begin{cases}
            1\quad &0\leq t<{\frac {1}{2}},\\
            -1&{\frac {1}{2}}\leq t<1,\\
            0&{\mbox{sinon.}}
        \end{cases}
    \label{equ:haar-wavelet-def}
\end{equation}

Pour déterminer l'orientation générale d'un point, la somme des valeurs est effectuée et la direction générale indiquée est affectée à la direction du point. La description du voisinage est quand à elle effectuée en calculant la somme du résultat de l'ondelette de Haar dans des sous-régions autour du point, comme illustré dans l'image \ref{fig:surf-feature-description}.

\begin{figure}[htb]
    \centering
    \input{fig/association/surf-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SURF d’un point caractéristique d’une image.} Au lieu de calculer un histogramme des directions de gradient possible dans chaque bloc, la somme de tous les vecteurs d’un bloc est choisie comme étant son représentant.}
    \label{fig:surf-feature-description}
\end{figure}
\end{tocome}

\TK{Présentation des descripteurs binaires utilisant un motif de comparaison fixe. Avantages d’un descripteur binaire en temps de calcul. Descripteurs \ORB, \BRISK{} et \FREAK.}

\TK{Analyse des résultats de comparaison des descripteurs en table 4.1.}

Dans la suite, la fonction de dissemblance choisie pour déterminer la distance entre le voisinage de deux points caractéristiques est notée $\Dissimilarity$.

\begin{table}[htb]
    \centering
    \begingroup\midsepremove
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{%
            >{\raggedleft\arraybackslash}m{2.5cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}%
            >{\centering\arraybackslash}m{1.9cm}}
        &\textbf{Référence}
        &\textbf{Baisse de la luminosité}
        &\textbf{Augmentation du contraste}
        &\textbf{Changement point de vue}
        &\textbf{Autre objet}\\[7.5pt]

        &\includegraphics{association/patch-distance-ref}
        &\includegraphics{association/patch-distance-lower-luminosity}
        &\includegraphics{association/patch-distance-higher-contrast}
        &\includegraphics{association/patch-distance-viewpoint-change}
        &\includegraphics{association/patch-distance-other}\\[15pt]

        \toprule
        \TintMinValue{0} \TintMaxValue{2792120}
        \textbf{Distance pixel par pixel~($\PixelDissimilarity$)}
        & \Tint{0}
        & \Tint{1670544}
        & \Tint{356007}
        & \Tint{309174}
        & \Tint{2792120}\\

        \midrule
        \TintMinValue{1} \TintMaxValue{-0.081}
        \textbf{Corrélation des intensités~($\CorSimilarity$)}
        & \Tint{1.000}
        & \Tint{1.000}
        & \Tint{0.997}
        & \Tint{0.318}
        & \Tint{-0.081}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{621.436}
        \textbf{\SIFT~($\EuclDissimilarity$)}
        & \Tint{0.000}
        & \Tint{4.243}
        & \Tint{64.722}
        & \Tint{63.182}
        & \Tint{621.436}\\

	\midrule
        \TintMinValue{0} \TintMaxValue{1.008}
        \textbf{\SURF~($\EuclDissimilarity$)}
        & \Tint{0.000}
        & \Tint{0.023}
        & \Tint{0.021}
        & \Tint{0.124}
        & \Tint{1.008}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{150}
        \textbf{\ORB~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{4}
        & \Tint{10}
        & \Tint{8}
        & \Tint{150}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{180}
        \textbf{\BRISK~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{0}
        & \Tint{8}
        & \Tint{23}
        & \Tint{180}\\

        \midrule
        \TintMinValue{0} \TintMaxValue{153}
        \textbf{\FREAK~($\HammDissimilarity$)}
        & \Tint{0}
        & \Tint{4}
        & \Tint{30}
        & \Tint{26}
        & \Tint{153}\\

        \bottomrule\addlinespace[\belowrulesep]
    \end{tabular}
    \endgroup
    \caption{\textbf{Comparaison de la spécificité et de la sensibilité des fonctions de dissemblance étudiées.} Chaque image est comparée à l’image de référence de la première colonne à l’aide de chaque fonction de dissemblance. Les couleurs sont attribuées proportionnellement à la valeur de similarité ou de dissemblance, avec en vert les scores les plus proches de la valeur de similarité maximale et en rouge les scores les plus proches de la valeur de dissemblance maximale.}
    \label{table:association-patch-distance}
\end{table}

\subsection{Modélisation du problème en théorie des graphes}

Il s’agit exactement du problème d’affectation, qui peut être résolu par la programmation linéaire ou par une modélisation en théorie des graphes~\parencite[section~8.1]{fortz-2012-ro}.
Un graphe de correspondance est un couplage parfait de poids minimal du graphe biparti complet~$\Graph = (\FeatureSet{i}, \FeatureSet{i+1}, \GraphEdges = \FeatureSet{i} \times \FeatureSet{i+1})$ pondéré par~$\Dissimilarity$.
Le problème de correspondance entre deux images consécutives peut donc être formalisé comme suit~:

\begin{problem}
\ProblemName{\PbDM}
\ProblemInput{ensembles de points~$\FeatureSet{i}$ et~$\FeatureSet{i+1}$ détectés dans deux images consécutives.}
\ProblemOutput{couplage $M \subseteq \GraphEdges$ tel que $\sum_{\Edge{\Feature{i}{j_1}}{\Feature{i+1}{j_2}} \in M}{\Dissimilarity(\Feature{i}{j_1}, \Feature{i+1}{j_2})}$ est minimal.}
\end{problem}

Le problème d’affectation est un problème polynomial qui peut être résolu de façon exacte en temps~$\UpBound(\max\{|X_i|, |X_{i+1}|\}^3)$ par l’algorithme de Kuhn–Munkres~\parencite{kuhn-1955-hungarian}.

\TK{Présentation de l’algorithme de Kuhn–Munkres}

\TK{Présentation de l’algorithme itératif par enchères et possibilité de distribution du calcul}

\TK{Preuves de correction et comparaison des complexités}

\section{Généralisation à un ensemble d’images}

\TK{Présentation du problème généralisé}

\subsection{Recherche de matrices de permutation optimales}

\TK{Présentation de la modélisation du problème sous forme de matrices de permutation des points caractéristiques. Recherche de matrices bistochastiques (permutation) minimisant le coût formé par la somme des coûts des associations faites dans ces matrices. Relaxation de cette fonction de coût sur des nombres réels. Obtention d’un problème de programmation convexe.}

\subsection{Algorithme de segmentation par densité \textsc{(QuickMatch)}}

\TK{Clustering}

\section{Discussion}

\TK{Pistes de poursuite~:
\begin{itemize}
    \item Implémentation de Kuhn-Munkres en version distribuée et tests.
    \item Tests plus poussés des différences entre descripteurs (table 4.1) sur un jeu de données plus important.
    \item Mise au point d’un descripteur à l’aide d’apprentissage machine~? Génération de données d’apprentissage par projection de modèles 3D aléatoires.
    \item Implémentation de PGSR avec un solveur de programmation convexe.
    \item Implémentation de QuickMatch~?
\end{itemize}}
