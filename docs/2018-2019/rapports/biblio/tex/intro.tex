\chapter{Introduction}

De nombreuses applications voient dans la reconstruction d’un modèle numérique en trois dimensions d’une scène réelle un intérêt grandissant.
En effet, à l’inverse des photographies qui ne peuvent pas conserver la structure spatiale d’un environnement, la fixation numérique de l’information tridimensionnelle permet de rendre la scène pérenne et ouvre également des possibilités d’analyse et de transformation automatisées de son contenu.
Une telle reconstruction peut être réalisée manuellement par un ou plusieurs artistes, mais cela peut demander une quantité de travail trop importante selon l’envergure de la scène.

\begin{figure}[bht]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{intro/application-chambord-nuage}
        \caption{\textbf{Nuage de points issu de la numérisation du château de Chambord} réalisée à l’École Nationale des Sciences Géographiques illustrant l’intérêt de la numérisation tridimensionnelle pour la sauvegarde du patrimoine.}
        \label{fig:intro-appli-chambord}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{intro/application-earth}
        \caption{\textbf{Rendu d’un modèle du cœur de Montpellier issu du logiciel Google Earth.} Google utilise des procédés photogrammétriques pour construire des modèles en trois dimensions de la surface de la~Terre.}
        \label{fig:intro-appli-google}
    \end{minipage}
\end{figure}

Le besoin d’automatiser la numérisation en trois dimensions a donné lieu à la mise au point d’une variété de techniques qui se divisent en deux groupes~: les techniques avec contact ou sans contact.
Les procédés de numérisation avec contact emploient des appareils munis de bras palpant les objets à mesurer.
Bien qu’ils requièrent d’acquérir chaque objet individuellement et leur fassent courir le risque d’une altération, ces procédés permettent d’obtenir des modèles très précis, ce qui les rend utiles notamment dans l’industrie pour vérifier les défauts de production.
Pour des objets fragiles ou des scènes étendues, ces méthodes sont toutefois incommodes, ce qui a justifié le développement de techniques sans contact qui s’affranchissent de ce type d’appareil et acquièrent l’information nécessaire en captant ou en émettant des ondes électromagnétiques (captant ainsi un signal de lumière visible ou infrarouge, par exemple) ou sonores.
Ces procédés sont au cœur d’applications telles que la télédétection, la réalité augmentée, la reconnaissance d’objets ou la sauvegarde du patrimoine~\parencite{remondino-2016-3d,wei-2013-applications} illustrées en figures~\ref{fig:intro-appli-chambord}~et~\ref{fig:intro-appli-google}.

Dans le cadre du projet annuel du M1 cursus master en ingénierie (\CMI) informatique, j’ai souhaité poursuivre le projet entamé pendant l’année universitaire 2017–2018 avec pour objectif de reconstruire un modèle en trois dimensions d’une scène statique arbitraire en utilisant un drone pour en capturer des photographies, limitant ainsi autant que possible l’intervention manuelle.
Une partie importante du travail nécessaire pour mener à bien ce projet se situe dans la programmation d’un drone et de sa trajectoire afin que celui-ci puisse capturer un flux d’images couvrant suffisamment l’environnement observé pour pouvoir le reconstruire.
Le projet auquel je m’intéresse concerne le processus de traitement des images captées par ce drone pour calculer le modèle en trois dimensions résultant. Plus précisément, à partir d’un flux d’images issu d’un appareil en mouvement dans une scène, l’objectif est de reconstruire un modèle sous la forme d’un maillage de polygones texturisé et à retrouver la position de l’appareil pour chaque cliché.

\begin{wrapfigure}{r}{.5\textwidth}
    \fbox{\centering\input{fig/intro/scanning-techniques}}
    \caption{\textbf{Principales techniques de numérisation tridimensionnelle actuelles.} Les procédés d’acquisition sans contact actifs ou passifs sont distingués, selon qu’ils émettent des ondes dans l’environnement ou qu’ils se contentent de les capter.}
    \label{fig:intro-techniques}
\end{wrapfigure}

Ce problème d’acquisition de structure par mouvement~(appelé dans la littérature anglophone~\emph{\enquote{structure from motion}}) fait partie de l’ensemble des techniques de numérisation photogrammétriques (cf.~figure~\ref{fig:intro-techniques}), c’est-à-dire des techniques qui permettent d’obtenir de l’information sur un environnement physique à partir d’images de cet environnement~\parencite{forstner-2016-photogrammetric}. Son objectif est, selon \cite{waxman-1985-sfm}, «~l’inversion du processus de création d’un flux d’images afin de déterminer la structure de l’objet visualisé et le déplacement relatif dans l’espace de l’observateur par rapport à l’objet~»\footnote{\emph{\enquote{The goal is then to invert the image flow process along the line of sight and thereby to determine the local structure of the object under view and the relative motion in space between the object and the observer}}~\parencite[p.~1, traduction personnelle]{waxman-1985-sfm}.}.

\citeauthor{nister-2007-sfm-nphard} ont démontré en~\citeyear{nister-2007-sfm-nphard} que ce problème est \NPHardClass{} dès lors qu’il existe des points visibles dans certaines images du flux et masqués dans d’autres.
Dans le cadre de ce projet, et de la plupart des situations concrètes de reconstruction, cette condition est vérifiée\footnote{Par exemple, en tournant autour d’un cube, il n’est pas possible d’en voir les six faces en même temps.}.
Par conséquent, nous nous intéressons dans ce projet aux heuristiques qui permettent de fournir un modèle se rapprochant au mieux de la réalité dans une majorité de situations.
Ces heuristiques pouvant se révéler coûteuses en calculs, une attention particulière est prêtée à la réduction du temps de calcul et aux possibilités de distribution du calcul.

Le projet se déroule d’octobre~2018 à mai~2019.
Ce rapport synthétise les résultats des recherches effectuées pendant les trois premiers mois pour approfondir les connaissances acquises l’an dernier, en particulier sur la comparaison des algorithmes de description du voisinage des points caractéristiques et d’association des points similaires dans un ensemble d’images.
Dans la suite de ce rapport, le processus d’acquisition de structure par mouvement est d’abord présenté, puis les méthodes de détection et d’association de points sont détaillées.
Enfin, je conclus en exposant les possibilités de poursuite possible.
