\chapter{Processus d’acquisition de structure par mouvement}
\label{chapter:reconstruction}

Pour un être humain comme pour un ordinateur, l’observation d’un objet complexe et inconnu selon un seul point de vue ne permet pas à elle seule d’appréhender sa structure tridimensionnelle.
En effet, si, dans une image, la direction dans laquelle se trouve un point par rapport à l’observateur est directement lisible, il est en général difficile d’estimer sa distance.
En revanche, lorsqu’un même point est reconnu dans plusieurs images, l’espace des positions plausibles à laquelle il pourrait se trouver est réduit, ce qui rend possible l’estimation de la structure de la scène en utilisant un appareil photographique (en l’occurrence un drone) en mouvement en son sein.
La mise au point d’algorithmes efficaces pour ce problème~\NPHardClass{} est un domaine de recherche actif depuis le début des années~1980~\parencite[p.~487]{wei-2013-applications} et nous avons décidé d’utiliser le processus suivant, inspiré des travaux de \cite{forstner-2016-photogrammetric}~(chapitre~11) et~\cite{szeliski-2011-computervision}~(section~4.1).
Ce processus est également illustré en figure~\ref{fig:reconstruction-pipeline}.

\paragraph{Étalonnage de l’appareil.} Chaque appareil photographique projette les rayons lumineux de façon différente sur son capteur.
Cette projection peut être modélisée mathématiquement par une transformation projective de l’espace de la scène vers l’espace de l’image, suivi d’une distortion radiale liée à la lentille utilisée.
Les paramètres de projection et de distorsion propres à l’appareil utilisé pour capturer le flux d’images sont initialement estimés~\parencite{zhang-2000-calibration} puis sont supposés être invariants tout au long de la capture.

\paragraph{Correction de la distorsion des images.} Les traitements réalisés sur les images dans les étapes suivantes requièrent que celles-ci soient exemptes de distorsions radiales.
Les images sont donc rectifiées en utilisant les paramètres déterminés précédemment pour annuler la distorsion.

\paragraph{Détection de points caractéristiques.} Certains points des images sont plus facilement reconnaissables sous une variété de points de vue grâce à la structure de leur voisinage.
Ces points sont dits \emph{caractéristiques} des images.
À cette étape, les images sont traitées individuellement pour y reconnaître des points susceptibles d’en être caractéristiques.
Dans la suite, ces points seront utilisés comme base pour reconstruire le modèle tridimensionnel~\parencite{lowe-2004-sift,bay-2008-surf}.

\paragraph{Association des points caractéristiques similaires.} L’enjeu de cette étape du processus est d’identifier des groupes de points caractéristiques qui, à travers le flux d’images, correspondent à l’observation du même point de la scène.
Ces groupes de correspondances doivent être formés de façon cohérente~: deux points d’une même image ne peuvent pas coïncider avec le même objet physique, un point d’une image ne peut pas être la projection de deux points de la scène et les correspondances les plus plausibles doivent être favorisées.

\paragraph{Reconstruction d’un nuage de points caractéristiques et de la trajectoire de l’appareil.} Lorsqu’un point est reconnu sur deux images différentes, sa position dans l’espace peut être estimée par le procédé de \emph{triangulation}\footnote{Ce procédé ne doit pas être confondu avec la notion homonyme de triangulation en géométrie qui consiste à partitionner un objet à $n$ dimensions en un ensemble de $n$-simplexes.} en exploitant les propriétés de la géométrie projective.~\parencite[p.~262]{hartley-2004-multiple}.
De même, lorsqu’au moins cinq points sont associés entre deux images, le déplacement relatif de l’appareil d’une image à l’autre peut être estimé.
Cette estimations n’exploitent cependant pas toute l’information du flux d’images car elles ne sont basées que sur des paires d’images.

\paragraph{Ajustement des positions des points et de la trajectoire de l’appareil.} Lorsque des points sont reconnus sur au moins trois images, les estimations de position et de trajectoire peuvent être encore affinées avec l’\emph{ajustement de faisceaux}~(connu dans la littérature anglophone comme le \emph{\enquote{bundle adjustment}}), un algorithme en programmation non-linéaire~\parencite[sections~18.1 et~18.6]{hartley-2004-multiple}.

\paragraph{Reconstruction de la surface tridimensionnelle.} À partir du nuage de points caractéristiques et des positions estimées de l’appareil, cette dernière étape consiste à estimer une surface passant par le nuage et texturisée par les photographies du flux~\parencite[section~4]{pollefeys-2001-image-based}.

\begin{figure}[htb]
    \hspace{-1.75em}
    \input{fig/reconstruction/pipeline}
    \caption{\textbf{Vue d’ensemble du processus d’acquisition de structure par mouvement} utilisé pour ce projet.}
    \label{fig:reconstruction-pipeline}
\end{figure}

Pendant l’année universitaire 2017–2018, les efforts de recherche se sont concentrés sur les méthodes d’étalonnage, de détection des points caractéristiques et de reconstruction du nuage de points.
Cette année, j’ai orienté les recherches sur l’approfondissement de la détection des points et les algorithmes d’association des points similaires à travers le flux d’images.
Ces deux axes sont développés dans les chapitres suivants.
