# Réflexion sur les sujets de projet

_Dimanche 09 septembre_

Le sujet doit avoir un rapport avec la robotique, et si possible la communication multi-agents. Idées possibles :

* protocole de communication entre les agents ;
* intelligence artificielle et imitation de la nature ;
* exploration d’un terrain inconnu.

![Carte des idées identifiées](carte-mentale.jpg)

<http://future.arte.tv/fr/lintelligence-en-essaim-inspire-la-robotique?language=fr>\
<https://tperobobee.jimdo.com/1-le-projet-d-harvard-un-insecte-robot/>

L’idée retenue est l’exploration et la cartographie d’un espace en trois dimensions.

![Idée de base exploration 3D](sections-projet-3d.jpg)

Le projet se découpe en trois grandes sections.

![Liste des tâches projet 3D](taches-projet-3d.jpg)
