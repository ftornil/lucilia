# Réunion initiale de l’équipe

_Vendredi 29 septembre, 15h30_

Au cours de cette réunion, nous présentons l’avancement actuel du document de proposition de sujet. Il s’agit de la première réunion de l’équipe avec les encadrants (il manque M. Chemori et  un sixième membre au groupe).

## Demandes à déposer

1. Déposer la demande de financement pour le drone avant le 10 - 15 octobre. Cette demande doit contenir une réorganisation de la proposition originelle avec un devis pour comparer deux ou trois drones. S’il n’est pas possible d’avoir un devis, voir avec le secrétariat informatique ou trouver simplement les références.

2. Prévoir de faire une demande d’autorisation pour le survol du lieu. Il faut faire une demande à la préfecture, et à la fac s’il s’agit d’une demande sur le campus.

3. Faire une convention de projet, à voir avec AEB.

## Revue de la proposition de sujet

Dans la partie 1, il faut aussi minimiser aussi le nombre de photos prises par le drone tout en maximisant la surface couverte.

Le sujet finalisé doit être envoyé samedi 30 septembre.

### Type de sujet

Le sujet s’oriente vers la reproduction de l’état de l’art avec en perspective une améloiration de l’existant.

### Budget allouable par le département

Au plus 600 €.

### Fonctionnement de la collecte

Dans un premier temps : séparation de la collecte des données et de leur traitement dans un premier temps. On peut ensuite envisager un traitement en parallèle avec transmission des données du drone à une station fixe. À voir en fonction du temps de calcul en partie II.

