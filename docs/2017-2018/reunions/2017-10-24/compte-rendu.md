# Réunion n°2

_Mardi 24 octobre 2017, 18 h 30_

Réunion du groupe afin de fixer les créneaux à venir, fixer l’organisation annuelle et signer les conventions.

## Réunions suivantes

**Réunions plénières planifiées :**

* mardi 21 novembre 2017, 18 h 30, bâtiment 16 (M. Chemori, Mme Bouziane) ;
* mardi 12 décembre 2017, 14 h 30, LIRMM (Mme Bouziane, Mme Rodriguez) ;
* mardi 19 décembre 2017, 14 h 30, LIRMM.

**Réunions ponctuelles :**

* avec Mme Bouziane et M. Chemori : le mardi après 18 h 30 (sauf 14 novembre et 5 décembre) ;
* avec Mme Rodriguez : à fixer.

## Fixation des créneaux de travail

L’annexe d’emploi du temps des conventions doit être réalisée d’ici la fin de la semaine. Elle doit être spécifique à chaque binôme. Elle doit contenir les créneaux de travail et de réunion et totaliser 100 heures minimum sur l’année.

### Créneaux au semestre 5

Voir des disponibilités ponctuelles.

### Créneaux au semestre 6

* Jeudi après-midi libre (14h30 – 18h30) : au total 48 heures.
* Éventuellement vendredi après-midi lorsque l’anglais n’est pas programmé.
* Pendant les vacances avec l’accord des binômes respectifs :
  * semaine 8 (19 février) ;
  * semaine 16 (16 avril).

## Autorisations de survol

Le département demande de rédiger une lettre de demande d’autorisation à destination de la présidence, qui spécifie :

* la marque du drone ;
* le type de drone (catégorie) ;
* le rappel de la législation et la mise en contexte législatif du projet ;
* les lieux envisagés, avec sortie pédagogique si lieu extérieur ;
* la présence éventuelle le week-end sur le campus, pour empêcher le survol de personne.

En annexe, le sujet du projet.

## Recherche bibliographique

Une date limite pour un premier brouillon est la première réunion plénière. Il peut être utile de préparer des slides de présentation des recherches réalisées, qui seront de toute façon présentées plus tard.
