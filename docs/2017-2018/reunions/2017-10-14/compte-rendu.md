# Réunion plénière n°1

_Vendredi 13 octobre 2017, 15h30_

Cette première réunion plénière est l’occasion de rencontrer tous les membres du groupe et les encadrants, à l’exception de Nancy Rodriguez qui n’a pas pu se libérer.

## Organisation du projet

Les binômes seront encadrés de la façon suivante :

* binôme 1 : Ahmed Chemori (avec intervention de Hinde Bouziane).
* binôme 2 : Hinde Bouziane (avec intervention de Nancy Rodriguez).
* binôme 3 : Nancy Rodriguez.

Objectif : une réunion plénière (avec tous les binômes et encadrants) par mois. Avec des réunions par binôme régulières.

Ahmed est en déplacement la semaine prochaine.

## Contenu du projet

Dans le binôme 1, la génération de trajectoire sera envisagée : le travail ne se résumera pas à un simple contrôle manuel du drone. La notation n’est pas corrélée avec la quantité de travail informatique, mais avec l’investissement total et les résultats obtenus.

Il est possible d’avoir un compte sur Grid5000 pour le calcul distribué. La seule contrainte est au niveau du transfert de données qui risque de prendre de temps.

## Conventions de projet

Une convention de projet par binôme sera faite, même si le projet est envisagé dans son ensemble. L’encadrant de chaque binôme sera signataire. Dans le week-end, les modèles de convention doivent être envoyés pour validation.

## Autorisations

Nous semblons entrer dans la catégorie Aéromodélisme - A. Voir la demande envoyée la fac et à la préfecture.

Pour les assurances : on peut envisager des sorties terrain (ordre de mission officiel pour la couverture de l’assurance).

## Dossier de demande de financement

Donner dans le dossier :

* deux ou trois choix de drone ;
* le prix de chaque drone ;
* les caractéristiques du drone.

Décider d’un modèle en groupe et envoyer par email aux encadrants qui transmettront au secrétariat. **Le département n’acceptera pas de commande tant que la préfecture n’aura pas délivré d’autorisation spécifique.** Aïda revient dans une semaine.

On a un délai supplémentaire pour le dossier de demande de financement.

Il faut vérifier la possibilité d’obtenir les informations de focale et d’ouverture. Vérifier précisément les possibilités de programmation des DJI (à documenter).

## Recherche bibliographique

On réalise un document par binôme, qui pourra ultérieurement être fusionné. Idéalement, il faut réaliser ces documents en LaTeX (voir les documents envoyés par Ahmed pour une initiation au langage).

