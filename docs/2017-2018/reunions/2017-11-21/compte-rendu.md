# Réunion plénière n°2

_Mardi 21 novembre 2017, 18 h 30_

Envisager un vol en intérieur si problème d’autorisation. Dans le pire cas, récupérer des données de source externe et répartir les équipes à nouveau.

## Recherche bibliographique

Expliquer et sourcer le choix du format DNG/RAW.

### Binôme 2

Trouver des sources plus académiques (thèses et articles) pour la recherche bibliographique.

Quantifier le nombre de données requises (nb. photos) trouver des travaux qui estiment la quantité nécessaire, dans le cahier des charges.  

## Cahier des charges

### Binôme 1
On arrache tout et on recommence. Apporter des précisions supplémentaires sur la trajectoire et la manière de gérer la caméra.
Voir avec le binôme 2 la manière la qualité et le nombre d'images nécessaires.
Dans la description du choix du drone, préciser le cheminement de recherche que nous avons eu et non la conclusion avec des justifications

## Prochaine(s) réunion(s)

__Plénière__ _vendredi 15 décembre 2017, 10 h 30_ au LIRMM __OU__ _mardi 18 décembre 2017, 14 h 30_ au LIRMM selon les disponibilités de Nancy.

### Réunions pour le binôme 1
* _mardi 28 novembre, 17 h_ au LIRMM
* _vendredi 22 décembre, 10 h 30_ au LIRMM