# Réunion binôme 2 n°1

_Mardi 14 novembre 2017, 18 h 15_

Cette première réunion du binôme 2 a été l'occasion de proposer notre cahier des charges à Hinde afin d'avoir un premier retour, ainsi que d'obtenir quelques précisions sur la direction à prendre pour le reste de la recherche bibliographique.

## Préambule

Nous avons discuté à propos du retour de la fac sur notre demande de survol et nous avons eu plusieurs éléments de réponse :

* Il y a toujours en dernier recours la possibilité de faire voler le drone en « privé » mais cela nécessite des assurances et autorisations au niveau de la faculté donc c'est pas top.
* Notre demande a probablement été mélangée à une autre concernant une demande de survol en drone lors de la journée Festisciences faite par d'autres personnes, d'où la réponse reçue par mail la semaine dernière.
* Pour l'instant, nous devons juste attendre une réponse de la présidence, tout en gardant à l'esprit que la visioconférence proposée par le CNRS pourrait être intéressante afin de parler à des gens qui connaissent le domaine.

## Critique du plan

La parallélisation est à prévoir dès l'écriture des algorithmes et ne consiste pas vraiment en une partie à part.

## Algorithmes

Il faudra bien évidemment un travail de réflexion sur les algorithmes, surtout au niveau des structures de données utilisées, afin de rester modulaire, pour pouvoir réutiliser ce projet dans d’autres contextes, mais aussi que celui-ci soit plus facile à déployer.

Il ne faut cependant pas oublier de regarder ce qui se fait déjà au niveau des algorithmes/programmes.

## Parallélisation

La parallélisation consiste à effectuer des traitements simultanés sur des données, en répartissant ces données entre plusieurs unités de calcul, puis en collectant les résultats. Il ne faut pas oublier ce principe dans la recherche bibliographique.

Il y a de nombreux paradigmes de parallélisation, dont deux à développer dans notre recherche bibliographique :

* MapReduce, Hadoop (pipeline et flux) ;
* Workflow.

Ce choix de paradigme est à effectuer au préalable, selon l'algorithme prévu, sachant qu'un mode n'exclut pas forcément l'autre.

### Bibliothèques

* MPI est une librairie de calcul distribué. Cette librrairie est disponible sur différents langages de programmation et permet de déployer le code sur des machines choisies. La lirairie gère ensuite elle-même la répartition des tâches entre les divers processeurs/machines disponibles.
* OpenMP (Open Multi-Processing) permet de faire du parallélisme en partageant la mémoire et est une alternative à MPI.

### Workflow

Le travail en workflow consiste à pouvoir à un moment du programme, arrêter l'exécution et revenir en arrière (par exemple, si on voit le programme comme un graphe de dépendances acyclique, il est possible de remonter dans le calcul). L'objectif étant de permettre l'orchestration du programme.

Plusieurs frameworks ont été évoqués, incluant :

* Bueple (?)
* Execo
* AGWL qui est utilisé pour les calculs scientifiques

Le travail en workflow est une très grosse partie qui nécessite du travail d'analyse, qu'il faut donc laisser pour plus tard.

## Grid’5000

Grid’5000 est un ensemble de machines accessible afin de réaliser des calculs longs.

Il serait possible d'effectuer des essais sur Grid’5000 afin de voir ce qu'il se passe et commencer à prendre en main cet outil.

Hinde doit nous faire parvenir dans la semaine des informations supplémentaires incluant le cours de M2 sur Grid’5000.

Un exemple jouet possible qui a été abordé est le calcul d'une somme de deux matrices, où le parcours de chaque ligne est indépendant des autres lignes. De ce fait il est possible de paralléliser le calcul sur chaque ligne.

Sur Grid’5000, on dispose de 25Go de stockage, et d’un accès physique aux machines à distance via SSH.

Il faut cependant penser à la duplication des données, qui sont stockées via un NFS (Network File System).

## Traitement des données en cours de vol du drone

Le terme de « calcul paramétrique » a été abordé pour définir le fait de calculer le modèle au fur et à mesure de la réception des images du drone. Ce type de calcul suit le principe des pipeline. Il faudra également utiliser des workflows afin de pouvoir continuer, sans tout recommencer, le calcul du modèle à l'arrivée d'une nouvelle image.

Pour la prochaine réunion, étudier les algorithmes et tenter une première modélisation sous forme de graphe de dépendances de traitement.
