# Réunion de l'équipe de projet avec Alexandre Lalanne

_Vendredi 26 janvier 14 h, bâtiment M de l'IUT de Montpellier, OBI-LAB_

## But de la visite

Nous avions pris contact avec le lab de l'IUT de Montpellier de part sa création récente d'un club de drone. Nous pensions alors leur présenter notre projet, le matériel que nous souhaitions utiliser ainsi que sa finalité et voir si l'on pourrait voler chez eux, utiliser un de leur drone ou même profiter de leurs conseils et expériences.

## Notes en vrac

On nous a conseillé de voir avec le service com de la fac afin de voir si des jeux d'images prises par drone pourrait être utilisés ou si la FAC est en contact avec des entreprises réalisant des prises de vue dans le but de leur demander de l'aide pour la trajectoire, les clichés ou autre... De plus, concernant notre maquette nous pouvons contacter une école d'art à une dizaine de minutes de l'IUT.

### Lieu de vol 

* Nous pouvons nous renseigner sur les permissions de vol au STAP : le drone resterait sur la FdS et serait en extérieur ou dans un gymnase couvert. Nous aurions alors plus d'espace qu'au sein du Lab.

* Utiliser une volière ou bien un cable reliant le drone au sol serait aussi envisageable et requerrait moins d'autorisation.

### Sites internet utilisables

* https://www.helicomicro.com/ -> actualité sur les drones, dévulgarisation des textes de loi relatifs au drone...

* Géoportail 

## Conclusion

Le déroulement de la réunion n'était pas tout à fait en phase avec nos attentes... Il a très peu été question de club drone.