# Présentation du sujet et discussion

_Vendredi 15 septembre, 11h_

Réunion de présentation du sujet de projet choisi avec Hinde Bouziane et Ahmed Chemori, chercheurs au LIRMM.

## Groupe

Trois binômes, un par partie.
Le travail peut se faire en parallèle.

Découpage du travail : le deuxième groupe risque d’avoir plus de travail que les autres ?

Utilisation de données pré-existantes comme source de chaque sous-groupe.

## Volume horaire

* Importance de bien coordonner les différents groupes
* Au premier semestre, une après-midi par semaine sur 10 semaines -> 40 heures
* Au deuxième semestre, 100 heures au total
* Au total 140 heures

## Matériel

Pas de matériel accessible a priori.

* Voir s’il faut acheter du matériel (cf AEB ou dépt. info CMI).
* Ou bien adapter le projet en sous-marin.

Si possible utiliser du matériel peu cher.

## Appel à projets

* À rendre le premier octobre.
* Appel pas encore lancé par AEB.
* Descriptif et liste des membres et répartition des binômes.

Troisième chercheur encardant ? William Puech, Nancy Rodriguez, …

Pour ce week-end, finaliser le document de présentation :

* ajouter un paragraphe pour mettre en contexte (introduction type projet Arcus) ;
* donner les objectifs ;
* montrer les différentes parties du projet et lier au nombre d’étudiant·e·s nécessaire.

