# Réunion du binôme 1 avec Ahmed CHEMORI au LIRMM

_Mardi 28 novembre 2017, 17 h_

## Suivi de la demande d'autorisation de vol avec le drone

Ahmed nous a mis au courant des dernières réponses de l'université : bien que notre demande de survol soit dans un but pédagogique, elle rentre dans la catégorie des __activités particulières__. À ce titre, il nous faudrait avoir une certification auprès de l'Aviation Civile (DGAC), une attestation de permis de vol, contacter le Fonctionnaire Défense Sécurité de la FdS... Bref, on abandonne le survol en extérieur. __En revanche__, nous pouvons toujours réaliser un vol en intérieur. Il faudra entre autre : (on ne nous a pas demandé de nous en occuper maintenant)
* __Trouver une salle__ : sûrement au sein de la FdS, nous avons demandé à Ahmed si la FdS serait préférable au LIRMM notamment à cause des assurances du drone, nous n'avons pas de réponse à ce jour mais on en saura sûrement plus lors de la __prochaine réunion plénière__. Nous devrons contacter quoi qu'il en soit le Fonctionnaire Défense Sécurité qui est en copie de tous les mails que nos encadrants envoient au CNRS (ce n'est peut-être pas nous qui le contacterons, on en saura plus le __15/12__).
* __Aménager la salle__ pendant les prises de vues : on réservera sûrement une salle pendant un large créneau (14h-18h) et l'on devra préparer la salle (retirer des tables, installer un filet de sécurité autour de l'endroit survolé par le drone, ...), porter des casques de sécurité et bien entendu réaliser la sculpture en carton que nous modéliserons.

## Relecture du Cahier des Charges

### 1.1.1 Matériel

Possibilité d'ajouter une description plus technique du drone. À développer et les composants du drone doivent une nouvelle fois être vérifiés.

_Attention !_
* Pour notre choix de prendre des photographies et non des vidéos : il faut faire attention à la stabilisation du drone, le temps qu'il se prépare pour prendre la photo avec le bon angle et tout... Si le __BINOME 2__ a besoin de centaine de photos pour sa partie, le drone risquerait de ne pas pouvoir tenir en l'air aussi longtemps. Si nousb décidons alors de prendre une vidéo, qu'est ce que ça impliquerait pour le __BINOME 2__ ? Doivent-ils penser leur programme avec des vidéos, est ce que l'on doit split les vidéos en photo avant ? Comment faire pour les données géospatiales ? __A SE RENSEIGNER__
* Vérifier la précision du GPS ? De combien est-elle ? Peut-elle varier d'un mètre (ce qui serait vraiment dommage si l'on réalise la prise de vue dans une salle d'une dizaine de mètre carré ٩(^ᴗ^)۶ ). Une solution envisageable si le GPS n'est pas assez précis serait d'utiliser 'Vicon' (?) une technique / matériel de réalité virtuelle dont le matériel est disponible au LIRMM. On mettrait des capteurs sur le drone et réaliserait la prise de vues au centre des caméras. Ahmed se renseigne sur la possibilité d'utiliser cette technologie et nous communique le résultat lors de la prochaine réunion.

### 1.1.2 Loi

Toutes cette partie doit être MAJ avec les dernières nouvelles (c'est le "Département d'enseignement de la FdS" qui paie le drone).

### 1.2.1 Calcul de trajectoire

Partie à totalement retravaillée avec l'avancement de la recherche biblio.

### 1.2.2 Programmation du drone

Le Mobile SDK étant open-source, c'est une bonne chose mais nous devons rester vigilants à d'éventuelles fonctions mal implémentées.

Nous devrions apporter plus de précision aux descriptions de ce que le Mobile SDK nous permet. Pour la partie "appareil", on devrait préciser si l'on peut contrôler la vitesse, la position (stationnaire, suivi de trajectoire...). Pour la partie "caméra", attention à ne pas confondre 'position' et 'inclinaison' : la caméra est fixée sur le drone donc sa position ne bouge pas, en revanche elle peut pivoter ; nous devrions également nous renseigner jusqu'à quels degrés elle peut pivoter, notion de "poulis, tangage, lacet". Enfin pour la partie "capteur", il serait bien de trouver les références de chacun des composants embarqués sur le drone (se renseigner notamment si le drone dispose d'un IMU (Inertial Management Unit, composé d'un accéléro, gyroscope et permet d'obtenir notamment des informations sur la vitesse ou la position angulaire du drone [sensor mems ?]).

### 1.3.1/2 Récolte des données

Ces parties doivent vraiment être développées à nouveau. Pour la partie ambitieuse de vol / modélisation combinée, il faut faire attention au temps de réalisation du modèle : le drone ne peut pas rester en vol indéfiniment. Nous devons __nécessairement__ travailler en étroite collaboration avec le __DEUXIEME BINOME__ afin de penser ensemble nos programmes et trouver un équilibre entre ce que nous pouvons réaliser avec le drone et ce qui faciliterait le travail pour le binome 2.

### Remarques diverses

On devrait ajouter la description technique du drone : un tableau récapitulatif complété par quelques liens vers une doc plus poussée. Ahmed nous conseille de faire des recherches sur la __trajectoire__, la __modélisation 3D__ et l'__acquisition des données__.

## Commentaires

Nous pouvons recontacter Ahmed si nous avons besoin d'autres thèses. Nous pouvons chercher des articles scientifiques se rapprochant de notre travail afin de voir comment eux ils ont fait. Notamment, Ahmed nous conseille d'entrer une trajectoire avec le drone (définir des points d'arrêt) : nous n'aurions qu'à établir une trajectoire en suivant la théorie étudiée grâce aux recherches bibliographiques.

Ahmed nous conseille __réellement__ de nous concentrer sur une première étape du projet où chaque groupe aurait un travail "séparé" ; ce que je veux dire c'est pas de modélisation pendant que le drone vole __pour l'instant__. Il trouve que c'est très ambitieux, difficile (il ne pense pas vraiment que l'on y arrivera) et que l'on devrait bien s'assurer de fournir quelque chose qui marche. Sinon nos encadrants ont l'air d'avoir vraiment à coeur notre projet et espère le voir réussir.

## Sources et liens utiles

Pour en savoir plus sur les drones d'intérieur ou plutôt avoir de jolis exemples de ce que l'on peut faire en intérieur : http://raffaello.name/

Lien de vidéos sur des projets de modélisation d'environnement :
* http://www.3dvf.com/actualite-7900-scan-3d-reconstruction-detaillee-d-une-montagne-a-aide-drones.html
* https://www.youtube.com/watch?v=IBaaEUbWA5M
* http://www.fotografieaeriana.eu/3d-scanning-modelling-with-drones/

Plus d'infos sur Vicon : http://www.biometrics.fr/V4/fr/logiciels-vicon/70-vicon-tracker.html

## Informations diverses

Ahmed est en déplacement du 29/11 au 6/12 et du 11/12 au 14/12.