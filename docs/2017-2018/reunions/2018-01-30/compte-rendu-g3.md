# Réunion du binôme 3 avec Mme Nancy Rodrigez

_Mercredi 30 janvier 2018, 09 h, bâtiment 16._

# Générale
Rendez-vous toute les 2 semaines pour faire le points sur l'avancement du projet.

# Correction du rapport de mi-projet

Le rapport est bien rédigé. Cepandant des petite choses a modifier :
-   Quelque correction orthographique (ils / elles)

-   Remplacer ce "déplacer" par "naviger", ce mot est plkus adapté dans le contexte de la 3D et est plus complet;
-   Présicer ou sont disponibles les modèles;
-   Completer la définition de modèle 3D. ici on ne parle que du modèle géométrique (nuage de points) alors qu'un modèles est également composé de l'apparance (information sur les matérieux);
-   Organiser les notes de bas de pages par chapitre;
-   Présicer que le relevé la position du poiteur de la souris ce fait en 2D;
-   On imite pas les intéraction physique avec les objet du quotidien;
-   Déplacer le paragraphe d'histoire avant la description d'un interface;

# Discution sur le diagrame de Gantt
Diagrame semble cohérent et réalisable. Une bonne choses est la grande zone tampon a la fin du projet afin de pouvoir gérer les imprévus et/ou amélioré l'application.
Celons Nancy les mission les plus compliquées sont la :m
-   Outils de mesure
-   Interface graphique de l'application

Nancy nous partage ces cours et TPs sur WebGL, three.js et DAT.GUi.

# Préparation de la soutenance
Recherche des information de la date de la soutenance, pour le moment le 8 février. ou? , heure ?

Le plan de la soutance semble correcte. Présenter les outils que l'on a choisie en les justifiant. évoquer les autre rapidemant dans détailler.

# Conseil sur la mise en place

Trés important de trouver un bibliothèque de test unitaire en JS.
Bien prendre le temps de faire la modélisation.
Plutot que de faire un diagrame de classe, faire un diagrame d'activité.
