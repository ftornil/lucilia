# Réunion plénière n°4

_Mardi 28 janvier 2018, 10 h, bâtiment 16._

## Ordre du jour

* Résumé de la réunion club drone.
* Présentation des diagrammes de Gantt.
* Présentation du plan de la soutenance.

* Combien de temps pour la soutenance ?
* Dans quelle salle aura-t-elle lieu, quand et à quelle heure ?

* **À la fin :** pour la soutenance avez-vous une idée de vidéo d’exemple d’application concrète du projet ?

## Vol du drone

* Mail pour vol dans les locaux du SUAPS sans réponse à ce jour.
* Utilisation éventuellement de maquettes de l’école d’architecture :
    * Nancy a des conteacts à l’école d’architecture.
* Contacts du service communication : peu de chance de succès.

Ahmed propose de discuter avec un enseignant de l’IUT ayant travaillé sur les drones aussi. Envoyer les disponibilités pour une réunion plénière au LIRMM avec tout le monde.

Faire une commande dans laquelle le mot « drone » n’apparaît pas pour éviter de se faire repérer par l’université…

## Soutenances

* Réaliser une animation pour l’introduction et/ou trouver une vidéo courte. Éventuellement utiliser les vidéos d’Ahmed.
* Date indéfinie pour le moment.
* 45 minutes de présentation + 15 minutes de questions.
    * prévoir moins de 5 minutes pour l’intro.
* Salle pas définie pour le moment.

## Rapports finaux

* Spécifier uniquement les choix, mais pas la recherche sur ce qui a été éliminé.
* Embrayer immédiatement sur l’implémentation.

