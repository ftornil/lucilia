# Réunion plénière n°5

_Mardi 3 avril 2018, 10 h, LIRMM._

## Ordre du jour

* Revue de l’avancement équipe par équipe.
* Rappel des consignes de sécurité pour l’utilisation du drone (M. Chemori).
* État des demandes de vol en gymnase ?
* Charge de travail trop importante nuit au projet (Rémi).

## Avancement du groupe 1 : génération de trajectoire et contrôle du drone

* **Alexandre :** à partir d’un objet, génération d’une série de points à suivre pour couvrir visuellement la scène. Fixation d’une distance minimum entre l’objet et le drone (fonction de la vitesse maximum du drone). Arrêts à des points fixes pour prendre les photos.
    * Importance du chevauchement d’une vue à une autre.
    * Pas nécessaire de voir l’objet en entier, angle d’incidence pas importante avec l’objet.
    * Besoin de connaître à l’avance l’envergure de l’objet si la trajectoire est planifiée en amont. Sinon, on peut se baser sur la distance pour recalculer dynamiquement la trajectoire en vol.
    * La maquette doit être positionnée dans le repère local du drone en amont (par rapport au point de départ $(0,0,0)$). Ce qui pose la question du positionnement du drone : comment déterminer la position spatiale ?
    * Question à laquelle répondre : quel protocole envisager en cas d'incohérence entre la trajectoire planifiée du drone et une détection d'un obstacle ?

* **Amandine :** récupération de la trajectoire calculée. Transmission par un format de fichier convenu. Réalisation d’une application pour mettre en œuvre la trajectoire point à point que le drone suit.
    * Il suffit d’une liste de points avec la pose (position ET orientation) du drone à chaque point.
    * En mode automatique. Décider de ce qu’il faut faire si un obstacle se manifeste sur la trajectoire. Retour à la position initiale ?

Contacter _Robin Passama_ qui a travaillé sur la robotique dans sa thèse.

## Avancement du groupe 2 :

**Question :** y a-t-il un changement significatif dans la qualité du modèle/dans le temps de calcul lorsqu’on voit le modèle entier sur toutes les photos ou seulement des parties ?

Procédure d’étalonnage implémentée. Testée sur des photos prises par Maëlle sans succès pour le moment car étalonnage erronné. Détection des points OK. Reste à implémenter le positionnement relatif des vues et des points.

Trouver le nombre optimal d’images prises telles que la couverture soit suffisante et la précision du modèle aussi. On veut minimiser le temps de calcul.

## Avancement du groupe 3

Exploration et tests sur les différents outils (Three.js, Vue) et apprentissage du JavaScript réalisée.

Modélisation du projet réalisée en UML. Implémentation en cours. Suite à l’implémentation de la modélisation.

## Rappel des consignes de sécurité

**Aspects de sécurité :**

* dégager la zone ;
* dégager les personnes ;
* dégager le chemin de la trajectoire retour en cas de perte de signal car pas d’évitement d’obstacles ;
* préférer le mode automatique au mode manuel ;
* installer un filet de protection entre la zone d’expérimentation et la zone des personnes.

**Avant le vol :**

* vérifier les hélices :
    * fixation,
    * état de dégradation ;
* vérifier la détection du signal GPS si utilisation du GPS ;
* vérifier la batterie;
* s'assurer que les capteurs détectant les obstacles soient bien activés;
* analyser l’environnement.

Penser à réaliser la maquette. Envisager une maquette de dimensions environ 5× plus grande. Maison pour enfants ? À acheter ~60 € total.
