# Réunion plénière n°3

_Vendredi 15 décembre 2017, 10 h 30_

## Autorisations de survol par le drone

Annonce « officielle » de la décision de refus de vol en extérieur. Le chef du département informatique est d’accord pour envisager ces vols mais la présidence demande le passage d’un certificat d’aptitude théorique pour le survol en extérieur.

Hinde a envoyé un mail au Service Logistique et Sécurité de la fac pour savoir ce que l'on doit clairement appliquer comme consigne de sécurité pour effectuer un vol dans une salle. Pas de réponse à ce jour, on en saura sûrement plus en janvier.

## Dates et indication de soutenance / rapport

**DATE DE RENDU DU RAPPORT :** __11/01/2018__

Le rapport concerne uniquement la recherche bibliographique. Il devrait contenir :

* contexte général du projet (*) ;
* objectifs du « sous-projet » ;
* problématique(s) ;
* état de l’art.

La partie contexte général (*) doit être commune à chacun des rapports. Ce dernier devrait faire une vingtaine de pages. Enfin, le rapport devrait se terminer sur une prise de recul (ce que l'on a choisi, pourquoi).

**DATE DE SOUTENANCE :** __19/01/2018__

Les trois groupes passeront tous ensemble lors de la soutenance (Hinde va essayer de faire en sorte que cela se passe comme ça). Chacun des binômes utiliserait le même support visuel (afin d'éviter notamment les répétitions de contexte) et chacun des binômes prendrait la parole à tour de rôle pour une durée de 10 à 15 minutes MAXIMUM (suivie de questions). Nous serions donc convoqués pour 45 minutes, 1 h.

Consignes pour la soutenance :

* présenter le contexte global ;
* prévoir 15 minutes d’oral (3 × 15 minutes pour chaque groupe) ;
* 15 minutes de questions ;
* présence minimale de Mme Baert et des encadrants de projet.

## Références annexes

### Pour le binôme 1

Pour tester le code avant de faire imploser le drone : V-REP (la vidéo est top), simulateur de robots divers dont des drones !

### Pour le binôme 2

Soumission du rapport pour relecture le 6 janvier à Mme Bouziane.

### Pour le binôme 3

Voir ce qui existe, Nancy a évoqué [dat.GUI](https://github.com/dataarts/dat.gui).

## À faire

_Pour chacun des binômes_ : faire un __plan à présenter aux encadrants dans le début / milieu de la semaine prochaine__. Ce plan ne devra pas comporter que des titres mais également des descriptions de contenus de parties.

