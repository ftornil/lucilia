# Réunion du binôme 2 avec Mme Hinde Bouziane

_Mercredi 17 janvier 2018, 14 h, bâtiment 16._

## Généralités

Il manque une espace avant chaque deux-points dans les légendes de figures.

Changement de titre global : Modélisation 3D d’une scène photographiée par un drone
Changement de sous-titre : Traitement des images et calcul d’un modèle 3D d’une scène

Remplacer le terme « environnement » par « scène »
Remplacer « du modèle » ou « de la scène » par « d’un modèle » ou « d’une scène ».

**Garder le présent lorsqu’on fait référence au reste du rapport.**

Toujours remplacer « on » par « nous ».

Remplacer « nous » par « le rapport présente » ou une forme passive si c’est une référence au rapport.

Sinon, si c’est un choix personnel, garder le « nous ». Sinon, reformuler sans le nous car ce n’est pas nous qui l’avons fait.

Faire apparaître « photogrammétrie » là où cela fait sens dans le reste du rapport, sinon le terme est isolé dans l’introduction. **Faire apparaître dans le plan tous les mots-clefs du titre.**

## Partie 1

Il s’agit d’un rapport du projet, pas d’un sous-projet. Éventuellement faire apparaître le titre « contexte » pour la première partie.

Objectif : calcul d’un modèle 3D d’une scène à partir d’images

Section 1.3 -> retirer « Détails du »

### Partie en commun

§1 : mettre à jour le titre global et sous-titre.\
Figure 1.1 : c’est une vue globale/principe. PAS PROCESSUS.

I. Description globale du projet. Remonter l’encadré à l’intérieur du premier paragraphe.
II. Description précise
III. Qui en fait partie ?

**Éliminer « sous-projet », « sous-domaine ». On s’inscrit dans le cadre d’un projet plus important.**

Remplacer « domaine »/« sous-domaine »/« sous-projet » par « projet ».

Figure 1.2 -> positionnemnet du projet dans le cadre de la modélisation … (<- reprendre le titre principal).

Section 1.1 -> retirer sous-domaine.

…observé par ce drone.

Paragraphe §2 sans contexte ! Objectif formulé positivement : représenter au mieux la scène. Or, identifier une source pour problème NP-difficile. -> solutions se rapprochant de réalité. On sait que c’est coûteux en calcul -> réduire le temps de calcul pour un temps raisonnable.

Penser à faire un transition en fin de 1.1.

Section 1.2 : trop courte pour être une section ?

Penser à faire une transition en fin de 1.2.

(Éventuellement : regrouper 1.2 et 1.3 et en faire deux sous-parties, 1.2.1 définition et 1.2.3 processus.)

§2 : « : science et ensemble des techniques … »

FIgure du processus : I. -> Phase 1)

    I. N’a pas déjà été réalisé. Dans le cadre du projet, sera réalisé par un autre binôme.
    II. « Pour détecter ces zones …, il existe …. Dans ce rapport, nous présentons.
    III. Même chose (dans ce rapport, nous présentons …)
    IV. Ajouter les statistiques dans un second temps, sinon confus.

Dans le plan, retirer « ci-dessous » pour les problématiques. Retirer « optimal ». Garder le présent.

« Nous traiterons les différentes -> traite différentes… »

Conclusions : présenter les choix faits (pas forcément les meilleurs…).

## Partie 2

### Présentation

Commencer par définir « points caractéristiques » au début de la sous-partie.
Puis décrire le processus lié aux points caractéristiques

Modifier « les » points caractéristiques -> des points caractéristiques

Retirer « nous » et futur.

La figure est un **exemple**, pas l’objectif de la partie ! Envisager une image sur laquelle les points sont plus visibles & meilleure qualité.

Regrouper les paragraphes qui sont solidaires (§1 et §2 dans 2.1.1).

Varier et réduire le nombre d’« il faut que » : « il est nécessaire », « devoir ».

Le §2 est hors contexte, à reformuler.

Le §3 doit être réordonné pour faciliter la lecture.

Dire dès le début qu’on liste des pré-requis à satisfaire pour les points caractéristiques.

« Les algorithmes les plus courants dans la littérature » + source

**Restructuration :** présenter tous les algorithmes dont on veut parler, l’un après l’autre, avant d’en discuter (notamment SURF), en expliquant les déclinaisons du processus décrit en 2.1 Points caractéristiques.

### SIFT

Deux solutions :

- regrouper les deux items dans un paragraphe de présentation
- déplacer les paragraphes de description dans les items.

« Différence pixel à pixel » -> « Calcul de la différence… »

Regrouper dans une annexe des informations sur les probabilités et la théorie de la mesure en général (eg. fonction de Gauss).

« Modifier la couleur de **chaque** pixel. »

§6 : reformuler la première phrase.

Insérer les références hahaha.

Faire référence à la description générale et dire ce qui est nouveau dans SIFT.

Faire des références à toute équation dans le corps.

Revoir la typographie mathématique.

Retirer « que voici » : faire référence à la section suivante.

### RANSAC

_Traduire les titres des algos._ Dire « cet algorithme est probabiliste. » et ne pas le prendre pour acquis.

Préciser : mauvaises correspondances de…

« il faut générer » : « il génère »

**Retirer le verbe falloir, le mot optimal ou meilleur.** (Ou dire en quoi il serait meilleur ?. Faire référence à la formule de proba.)

Dans « notre » cas -> à préciser. Ce qu’on veut faire dans le cadre du projet.

Revoir la discussion selon la restructuration.

« degré de parallélisme d’une image »

Discussion : trouver une source sur les performances ou bien faire des tests.

## Partie 3

Section 3 -> Positionnemnt - « d’une scène » - retirer « optimal » (non-garanti).
Section 3.2 -> positionnement des points - retirer « de la scène ».

Retirer les « nous ». Eg. « nous supposons que chaque » -> « où chaque ».

Introduire les acteurs avant d’introduire le formalisme.

Inconnue -> ce qui est à calculer.

**Donner une vision globale en premier lieu, puis rentrer dans le formalisme.**
