# Réunion du binôme 2 avec Mme Hinde Bouziane

_Mercredi 17 janvier 2018, 14 h, bâtiment 16._

Groupe 2 – Analyse

## Présentation des résultats

* Étalonnage
* Mise en correspondance
* Trajectoire et nuage de points
* À faire : tests de plusieurs paramétrages SURF, matcher, RANSAC

## Discussion sur le rapport

* Introduction :
    * Partie commune.
    * Détail du projet n°2.
* Détection des points
* Mise en correspondance
* Étalonnage
* Positionnement de l’appareil et triangulation des points
* Bundle adjustment
* Conclusion.

15 pages en plus et 20 pages total.
