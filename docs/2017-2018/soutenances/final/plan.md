# Plan de la soutenance

Soutenance des trois groupes à la suite.

## Introduction générale

* Se présenter.
* Description du projet : donner les objectifs principaux :
    * à partir d’un suite de photos prises par un drone ;
    * créer automatiquement un modèle d’une scène réelle ;
    * visualiser le modèle et interagir avec, à travers une application web.
* Exemple d’application concrète du projet (à trouver) :
    * _vidéo courte de 30 à 45 secondes ;_
    * _schéma interactif._
* Découpage du projet en trois équipes : donner les missions, les membres et l’encadrant associé à chaque binôme.
* Présentation du plan de la soutenance (découpage + conclusion).

## Programmation du drone et récolte de données

* **Objectif du projet :**
    * Contrôler la trajectoire d'un drone.
    * Récolter des données lors d'un vol.
* **Fonctionnement des drones :**
    * Fonctionnement et contraintes liées au drone (roulis tangage lacet).
    * Discussion sur le choix du drone : expliquer nos besoins > amener au choix du drone.
* **Calcul d'une trajectoire :**
    * Présentation des méthodes existantes (énumération).
    * Discussion sur la méthode choisie (pourquoi ce choix, comment ça fonctionne).
    * Introduction aux méthodes d'asservissement des drones (pourquoi on en a besoin, énumération de certaines).
    * Discussion sur le choix de la méthode d'asservissement : PID (pourquoi ce choix, comment ça fonctionne).
* **Programmation du drone :**
    * Une fois la trajectoire calculée, comment peut-on s'assurer que le drone fasse ce que l'on souhaite.
    * Présentation du kit de développement DJI mobile.
* **Récolte des données :**
    * Rappel de quelles données sont nécessaires (et pourquoi) IMAGES !! (et peut être un peu de coordonnées GPS).
    * Comment va-t-on s'assurer de la bonne prise de ces dernières.
* **Conclusion :**
    * Manque à : calculer trajectoire en fonction de notre salle, cible ; faire l'appli.
    * Après nos recherches, reste des zones de flou.
    * Transition vers Mattéo et Florent.

## Traitement des images et calcul d’un modèle 3D d’une scène

* **Objectifs du projet :**
    * traitement des images récoltées par le drone ;
    * calcul automatisé d’un modèle 3D de la scène visualisée.
* Photogrammétrie et processus _« structure from motion »_ en quatre phases.
* **Phases II et III :** détecter des points caractéristiques et les mettre en correspondance entre les images
    * Intuitivement, qu’est-ce qu’un point caractéristique ?
    * Détection des angles par lissage progressif de l’image : SIFT _« Scale-Invariant feature transform »._
    * Optimisation : SURF _« speeded up robust feature »._
    * Comment éliminer les correspondances aberrantes ?\
      _Le random à la rescousse du $2^n$ : RANSAC « Random Sample Consensus »._
* **Phase IV :** positionner ces points dans l’espace de la scène pour obtenir un nuage de points
    * Qu’est-ce vraiment qu’un appareil photo ?
    * Modélisation mathématique du processus de formation des images : le modèle du sténopé.
    * Paramètres intrinsèques et extrinsèques et étalonnage d’un appareil.\
      _Démonstration interactive avec WebGL._
      _(\url{http://ksimek.github.io/perspective\_camera\_toy.html})_
    * Positionnement relatif d’une paire d’appareils photo observant un point caractéristique commun.
    * Ouverture : comment faire avec $n$ images ?
* **Conclusion :**
    * Où en est-on ?
    * **Questions en suspens :**
        * Comment implémenter les algorithmes ? Modélisation objet de l’application.
        * Comment ajuster le positionnement relatif des appareils sur $n$ images ?
        * Comment gérer les distorsions non-linéaires des appareils (exemple de la banque de Roumanie) ?
        * Comment générer un maillage à partir d’un nuage de points et le texturiser ?
    * Ce qu’on va faire par la suite.
* Transition vers Maëlle et Rémi.

## Construction d’une application de visualisation interactive de modèles tridimensionnels

* **Objectifs du projet :**
    * Effectuer un rendu d'un modèle donné par l'équipe précédente.
    * Mettre en place des outils d'interaction avec le rendu.
        * Déplacement.
        * Mesures.
        * Gestion de la lumière.
    * Interface graphique :
        * Pour les outils : permet d'utiliser les outils.
        * Pour l'application : permet de naviguer dans l'application, ouvrir un modèle, …
* **Rendu sur le web :**
    * Qu'est ce qu'un rendu ?
        * Définition.
        * Exemple.
    * WebGL & Three.js _(exemple)_ (OpenGL -> compliqué -> three.js).
* **Interface graphique :**
    * Définition.
    * Présentation de ce que l'on veut faire (maquettes et explications).
    * Séparation en deux interfaces.
    * Outils permettant de réaliser une interface web :
        * mentionner les outils avec lesquels on a comparé Vue.
        * décrire Vue :
            * composants.
            * …
            * exemple.
        * décrire dat.GUI : permet de faire une interface simple permettant de modifier des variables (montrer exemple).
* **Conclusion :**
    * Il reste à coder le tout.
    * Difficile de choisir un outil car il en existe un grand nombre.
    * Cours de web (HLIN510) on a appris le JavaScript.

## Conclusion générale

* Redonner l’avancement global du projet à travers les trois projets.
* **Difficultés rencontrées :**
    * organisation d’une équipe de travail de 6 personnes ;
    * coordination avec tous les encadrants (trouver des dates communes pour les réunions) ;
    * pour chaque équipe, exploration d’un domaine totalement nouveau.
* **Apports :**
    * pour chaque équipe, exploration d’un domaine totalement nouveau ;
    * travail en équipe et coopération ;
    * lecture d’articles de loi (vive Légifrance) ;
* Transparent final : questions.
* Ajouter en extra les références bibliographiques principales.

