# SPEECH

## Introduction (1") Amandine

  Alexandre et moi nous sommes occupés de cette partie. Comme son nom l'indique, ce projet (Programmation du drone et récolte de données) a des objectifs majeurs qui sont liés : pouvoir planifier et contrôler la trajectoire d'un drone programmable dans le but de récolter les données nécessaires à la modélisation de cet environnement. En vu d'accomplir ces objectifs, plusieurs questions se sont posées, entre autre : comment pouvons nous modéliser une trajectoire ? Comment s'assurer que le drone soit autonome -c'est-à dire qu'il ne soit pas piloté manuellement- ? Comment faire pour que la trajectoire effective du drone suive au mieux la trajectoire souhaitée ? Comment le drone saura-t-il quand il doit prendre un cliché de la scène survolée (...). Notre recherche bibliographique nous a permis d'y voir un peu plus clair et d'apporter des réponses à ces questions. Maintenant Alexandre va vous présenter le fonctionnement des drones et les contraintes qui les affectent puis nous verrons comment calculer une trajectoire ainsi que comment nous allons programmer notre drone. Enfin, avant de conclure nous aborderons la récolte des données.

## Fonctionnement (2") Alexandre

    Un drone c'est d'après la législation française "un aéronef circulant sans personne à bord".
    La législation définit 2 catégorie: Soit 'A': il doit avoir une masse <= 25kg ET ne pas être motorisé ou n’avoir qu’un seul type de propulsion, sinon B. Notre drone choisi correspond à la catégorie A.
    En effet, le terme "drone" est plutôt générale. Voici quelques illustrations appliquant la définition. ...

    Voici comment le drone marche. Supposons que la puissance identique des hélices fait que le drone soit immobile: Pour monter, il suffit d'ajouter de la puissance sur l’ensemble des Hélices, et réciproquement pour descendre. On appelle ce type de mouvement le Gaz.
    Pour le second type, le lacet: Notre drone comporte 4 hélices disposé au carré. [IMPROVISATION] Le drone tourne sur lui-même sur le sens de la rotation des hélices la moins forte...
    Pour les 2 derniers, Le drone se déplacera la où la poussé sera la plus faible. On appelle roulis lorsque le drone se déplace sur l'axe Y et tanguage sur l'axe X. On peut combiner ces 2 mouvements pour se déplacer diagonalement; sur toutes les degrés. Bien sur, Ceci ne montre pas les contraintes telle que la gravité où il faut une poussée identique à l'opposée de la force gravitationnelle pour obtenir la stabilisation verticale. Le vent aussi qui pousse voire bascule le drone. Et autres aléas météorologiques.

## Trajectoire (3") Amandine

  Maintenant intéressons-nous à la planification d'une trajectoire. Lors de nos recherches nous nous sommes rendus compte qu'il existait une multitude de méthodes permettant de définir une trajectoire comme vous pouvez le voir. Celle que nous avons choisie est la méthode de planification par juxtaposition de splines polynomiales, dans cette méthode la trajectoire du drone est simulée par des polynômes juxtaposées. Contrairement aux autres méthodes listées précédemment, avec cette méthode on peut prendre en compte les contraintes cinématiques de notre drone qui sont traduites par les polynômes.
  En approfondissant nos recherches sur cette méthode, nous avons vu qu'il existait différentes manières de l'implémenter.

  Cette méthode peut être implémentée de diverses façons mais nous avons d'abord eu à décider dans quel espace nous allions générer notre trajectoire : dans l'espace articulaire ou dans l'espace cartésien. L'espace articulaire permet de représenter toutes les articulations d'un robot, or dans le cas d'un drone on peut ne considérer qu'une seule articulation. Nous avons donc choisi l'espace cartésien où on étudie la position d'un effecteur du robot (ici notre drone). Une fois que nous avions opté pour une trajectoire dans l'espace cartésien, nous avons eu le choix entre une trajectoire à mouvement continue ou à mouvement point à point. Comme le drone devra s'arrêter pour prendre des clichés, la trajectoire point à point nous a semblé être la plus utile pour nous. Enfin nous avons choisi d'utiliser le profil d'accélération bang-bang permettant une phase d'accélération et de décélération identique.
  Ainsi la méthode de planification de trajectoire choisie est la méthode de planification par juxtaposition de splines polynomiales par génération de mouvement point à point à profil d'accélération bang-bang. Cette dernière nous permet entre autre de prendre en compte les contraintes cinématiques du drone, de séquencer notre trajectoire en points d'arrêt où nous prendrons une photographie de la scène.

  Une fois que notre trajectoire a été établie, rien ne nous certifie que le drone suivra précisément cette trajectoire théorique. En effet, une marge d'erreur entre la trajectoire effective et théorique peut exister. Lors de notre recherche bibliographique nous avons vu plusieurs méthodes d'asservissement existantes telles que la stabilité de Lyapunov et le principe d'invariance de La Salle, le Principe du Maximum Pontryagin, le back-stepping, le problème de contrôle optimal et le PID (Proportionnelle Intégrale Dérivée). Nous avons choisi le PID pour sa simplicité par rapport à ce que nous voulons faire et pour son utilisation universelle.
  Nous pouvons voir le fonctionnement du PID sur ce schéma. En entrée nous avons la trajectoire théorique désirée, c'est-à-dire la position précise où nous voulons que le drone soit. Ensuite cette trajectoire théorique est mise à jour avec l'erreur : ainsi le PID prend en entrée la différence de trajectoire entre la trajectoire théorique et effective (c'est ce que représente l'erreur). Ensuite nous appliquons chacun des paramètres du PID, soit P le régulateur proportionnel, PI le régulateur proportionnel intégral et PD le régulateur proportionnel dérivé. Une fois chacun de ces paramètres appliqués, on les ajoute puis le donne au système qui prend ces données en compte afin de corriger sa trajectoire. La nouvelle trajectoire est calculée et est passée à nouveau en entrée du PID après avoir été comparée avec la trajectoire théorique.

## Programmation du drone (1") Alexandre

    Pour pouvoir faire sa mission, il est nécessaire de le programmer...
    Comme Kit de développement on a le DJI Mobile SDK. Unique moyen pour communiquer entre le drone et les instructions programmées.
    Pour cela, on va créer une application android contenant la bibliothèque du fabriquant du drone (DJI Mobile).
    Ainsi il y aura une connexion entre le téléphone et le contrôleur, puis du contrôleur au drone.
    On pourrait par exemple utiliser la caméra, tester son ergonomie, ...

## Récolte des données (2") Amandine

 Un des buts de ce projet est de récupérer les données nécessaires à la modélisation de la scène choisie. Nous allons donc voir quelle est la nature de ces données et comment elles vont être prélevées.

 Après discussion avec nos collègues, il s'est avéré qu'ils avaient besoin d'une certaine quantité de photographies de bonne qualité de la scène à modéliser ainsi que les paramètres de notre caméra.

 Concernant les clichés après discussion avec (Mattéo et Florent), il s'est avéré qu'il ne fallait pas nécessairement une quantité fixée de clichées mais qu'au plus il y avait de photographies, au plus la modélisation en serait détaillée mais le temps pris pour créer le modèle serait long.
 Il nous faut donc trouver un juste milieu entre le temps d'exécution de leur programme et la précision du modèle généré en fonction de la quantité de photographies fournie. De plus nous ne devons pas oublier de prendre en compte les contraintes techniques de notre drone telle que l'autonomie par exemple. Par ailleurs pour le déroulement de la prise de vue, comme illustré sur la figure, nous supposons que si à chacune des rotations autour de la scène nous prenons entre 15 et 20 clichés et que nous effectuons en tout 6 rotations, nous aurons entre 100 et 120 clichés ce qui nous semble plutôt raisonnable avec une autonomie de 20 minutes.

<!-- PARTIE DISCUTABLE AVEC MATTÉO-->
 Un des moyens de gagner du temps d'exécution à l'équipe en aval est de regrouper les photos de manière à ce que les angles de vues se suivent, comme les photos seront prises en suivant la trajectoire du drone, les clichés seront donc déjà ordonnés et seront communiqués dans le même ordre. Enfin concernant la qualité des photographies, l'appareil de notre drone (comme dit précédemment) est de 12 MégaPixels et par ailleurs nous avons choisi de stocker nos clichés au format DNG (Digital Negative) qui est en fait une standardisation du format RAW où les images sont le moins traitées possibles.

 Quant au paramètres de la caméra, ces informations sont nécessaires car notre appareil photo modifie légèrement les clichés (avec la longueur focale ou la sensibilité lumineuse par exemple). Il est donc nécessaire que les responsables de la modélisation possèdent ces informations afin de les prendre en compte lors de la création du modèle. Mattéo vous en parlera plus en détail plus tard.
<!--FIN DE LA PARTIE PARTICULIÈREMENT DISCUTABLE-->

## Conclusion (1") Alexandre

<!--RESUMEE DE LAVANCEMENT -->
Pour conclure sur le projet Programmation du drone et récolte des données, nos recherches nous ont non seulement permises de nous immerger dans le monde de la robotique mais aussi d'en savoir un peu plus sur le fonctionnement d'un drone et surtout d'en choisir un adapté à notre projet. De plus nous savons comment planifier une trajectoire et faire en sorte que la trajectoire effective soit la plus proche possible de la trajectoire théorique. Par ailleurs nous savons quelles sont les possibilités de programmation du drone.

<!-- PROCHAINES ETAPES -->
Ainsi il ne nous reste plus qu'à nous familiariser avec le kit de développement de DJI en attendant que le drone soit commandé. Par ailleurs nous avons des pistes solides quand au lieu de survol. En attendant d'avoir l'autorisation nécessaire nous utiliserons le simulateur fourni par DJI (entreprise drone) ainsi que le logiciel V-REP. Enfin, dans l'année nous aurons la possibilité de passer le permis drone.

<!--<TENTATIVE DE CONCLUSION>
 Pour conclure, notre drone est ...

 En l'état actuel, il reste des inconnus sur l'avenir du projet:
 - On a pas encore un lieu réservée pour ainsi faire les expérimentation du drone
 - La FDS n'a toujours pas financé l'achat du drone
 ...
 Malgré tout, on peut faire des simulations de vols (avec V-REP) pour continuer à expérimenter.-->
