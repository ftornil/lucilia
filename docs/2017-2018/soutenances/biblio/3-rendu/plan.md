
<!--
make a presentation of mi-project

goals :
-   present our works until here
-   what are the tools to realize our project

question to be answered
-   what is your works until here ?

-   what is goal of your project
 -->


# Construction d’une application de visualisation interactive de modèles 3D

## introduction

-   3 eme étape dy projet
-   réaliser par
-   Lire intitulé de notre partie.

### Objectifs

-   On a maintenant un modèle de la scène, (liste de sommet et d'arrete sous forme d'un fichier texte)
-   Objectif du projet, et de le rendre exploitable par l'utilisateur
-   Pour cela on construit une application réalisant l'interface le modéle et l'uitlisateur

qui a partir du des modéles calcule des rendu.


-   Exploite des modèle dans l'application en réalisant des rendu
-   Créer une application web permettant d'afficher les modèles fournis par Mattéo et Florent (schéma voir photo tableau)
-   application propose des outils, but de permettre à l'utilisateur d'intéragir et de personnaliser l'affichage du modèle
    -   **caméra :** Naviguer au sein du modèle;
    -   **Lumière :** modifier l'éclairage
    -   **Mesure :** mesurer des distances, des angles
-   interface graphique
    -   outils
    -   application, naviguer dans l'application et ouvrir un modèle

### Transition et lancement de la présentation
-   avant le début de ce projet nous avions peu de connaissance dans :
    -   le rendu 3D
    -   Interface graphique
    -   Les technlogie du web
-   `->` Notre recherche biblio s'est concentrée sur la découverte de ces concepts et des outils disponibles pour atteindre nos objectifs

-   Dans un premier temps nous allons définir le rendu et présenter les outils que nous avons choisi.
-   Puis nous présenterons les interfaces graphiques et les outils
> plan éventuel

## Méthodes et outils pour le rendu tridimensionnel sur le web

### Qu'est ce qu'un rendu ?

- définition
Processus qui, à partir du modèle 3D et d’un point de vue, calcule
l’image 2D représentant la vue de la scène qu’aurait un observateur en ce point.

Il a souvent pour finalité l’affichage sur un écran.
Le calcul se fait en prenant en compte les sources de lumière ainsi que différents phénomènes
physiques.

- schéma avec deux flèches du processus de rendu

- exemple de rendu dans three.js
> https://poly.google.com/view/3gIlXOyZxna

- il existe 2 principales méthodes mathématiques de rendu d’un modèle 3D : les plus communes sont la rastérisation et le lancer de rayon
Nous n'avons malheureseusement pas le temps de les présenter ici (ne pas mentionner les questions pendant la soutenance à mon avis)


Depuis 2011 il existe un standard pour calculer un rendu tridimensionnel

### Web Graphics Library (WebGL)
- présentation :
    - WebGL est une API JavaScript
    - open source
    - développée par Khronos
    - objectif l’affichage de graphismes 2D et 3D dans n’importe quel navigateur web compatible


-   permet : (schéma flèche entre le navigateur et la carte raphique)
    - d’utiliser le standard OpenGL ES (Open Graphics Library for Embedded Systems) au sein d’une page web
        - OpenGL est une interface de programmation bas niveau implémentée au dessus des pilotes graphiques
        - permet de réaliser des rendus 2D et 3D en exploitant l’accélération matérielle de la carte graphique
        - OpenGL ES est une version portable de OpenGL prévue pour fonctionner sur des appareils mobiles.
    - A l’aide de WebGl, le navigateur peut utiliser les ressources de la carte graphique pour calculer le rendu.


- avantage :
    -   Standard `->` intégré au navigateur, ne nécessite pas l'installation de plugin
    -   multiplateformes, compatible avec tous les os, les ordinateurs, téléphones et tablettes
    -   WebGl est basé sur l’API de OpenGL, ce qui lui permet d'exploiter pleinement les ressources de la carte graphique du client pour le calcul du rendu.

- Webgl est complexe `->` couches d'abstraction simplifiant l'utilisation

### Three.js

- comme toujours sur le web -> beaucoup de bibliothèque et frameworks permettant d'abstraire webGL
- Lors de la recherche bilio, nous en avons étudié plusieurs couches d'abstraction  (a-frame, babylon, three, playcavnas). (-> pourquoi mentionner des bibliothèques qu'on n'a pas mises dans la recherche biblio ?)
- On présente ici celle que nous avons choisi d'uliser pour le projet, three.js

Présentation :
- three.js est une couche d'abstraction de Webgl
- créé en 2010
- par Ricardo cabello
- écrit en JS

objectif :
- rendre plus accessible la création de graphismes 3D sur le web

Pour cela fournit des abstractions et des concepts :
- scène
- caméra
- lumière, différent type prédéfinie
- annimation
- texture
- relevé simplement le curseur de la souris
- ouvrir des fichier objet

- demos de three.js

choisie car:
- approche généraliste, les autres sont plus spécialisées dans le jeu vidéo (babylon) ou la vr(A-frame)
- Plus utilisé, grande communauté
- trés bonne documentation

## Interface graphique

### Qu'est ce qu'une interface ?
- dispositif de dialogue homme-machine
- objet dessiné sous forme de pictogramme
- manipulation de ces objet au travers un dispositf de pointage, généralement une souris

### Définition de nos besoin d'interfaces
- Présenter les 2 maquettes fonctionnelles d'interfaces
- 2 types d'inteface
   -   interface outils
   -   interface générale de l'application

### Interface outils ; DAT.GUI
Présentation :
- micro framework
- crée en 2011
- javascript

objectifs :
- api permettant de construire simplement une interface de controle

pour cela :
- définir un ensemble de widget graphique (bouttons, slider, ect)
- on associe le paramètre que l'on souhaite rendre manipulable avec un widget, et celui-ci est automatiquement ajouté à l'interface et modifiable.
- offre ausi la possiblité de sauvgarde les valeurs (pas obligatoire)

avantages :
- simple d'utilisation
- correpond au besoin pour le controle des outils

### interface général de l'application

- beaucoup beaucoup d'outils d'interface
- nous on avons étudié égalemnt plusier, angular, react, vue
- pas le temps de tous présenté, donc juste celui que l'on a choisie, vue.js

Vue JS :

- bibliotheque js
- 2014
- Evan you

Avantages :

- Permet d'organiser le code par composant dans un fichier unique (rend le composant réutilisable et la maintenance du code est simplifiée)
- Très léger donc adapté à des projets de petite taille
- API simple à maîtriser -> permet de passer moins de temps à maîtriser l'outil, donc plus de temps à développer le projet
- Egalement l'une des bibliothèques les plus performantes

---

TODO :
-   Revoire les titres des parties principale (celles qui seront affichée sur le diaporama)
