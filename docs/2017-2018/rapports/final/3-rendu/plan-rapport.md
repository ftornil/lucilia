# I) introduction

-   cadre du projet (L3 TER)
-   Expliquer le projet en 1 phrase
-   Introduire et justifier le nom
-   Présentation du groupe
-   Donner les date de debut et fin de projet

## a) Motivation du projet
    Deja fin lors du précedent rapport

## b) Découpage du projet en 3 sous groupe
-   Dire que l'on veut tout faire par nous meme du deb a la fin
-   Deja fait dans le précedent rapport

## c) Objectif et cahier des charges de notre sous groupe
<!-- -   Dire que l'on est la phase 3 -->
<!-- -   Présentaiton global de notre mission (afficher les modèle construit par les groupe précedent) -->
-   Cahier des charges
    -   Ouvrir et afficher les donne fournie par les groupe précedent

    -   Permettre de ce déplacer au sein du modèle
    -   Personaliser le rendu, modification de lumiere
        -   Luimere ambiant (couleur, intensité)
        -   Lumiere directionelle (ajouter, supprimer)
    -   Mesure de distance

    -   Interface graphique permettant e lister et selectionné un modèle
    -   Interface graphique pour les outils simple et intuitif

## d) Définition essentiel et plan du rapport

# II) Organisation du projet

## a) méthode et organisation du travail
-   travail en paire
-   Temps de travail, lesjeudis et les weekends
-   Réunion toute les 2 semaine avec notre encadrande Mme Nancy rodrigez

## b) Répartition du travail dans le Temps

-   donner la période de temps du projet
    -   1 ere phase de recherche biblio jusu'en janvier
    -   2 eme phase de dévelopement jusqu'a fin avril
        -   Préparation modélisation, découverte des outils
        -   Développement de la partie modèle 3D
        -   Dévelopement de la partie affichage
-   Gantt

# III) État de l'arts
