\input{../preamble.tex}

\begin{document}

\title{Programmation du drone et récolte de données}
\author{Amandine~\textsc{Paillard}, Alexandre~\textsc{Rouyer}}
\date{\today}

\newcommand*\encadrant{%
    \textbf{Encadrant~:}\\
    \M~Ahmed~\textsc{Chemori}\\[1em]
}

\subimport*{../}{titlepage}
\tableofcontents
\subimport*{../}{intro}

Lors du premier vol de notre drone, nous avons été surpris par le bruit qu'il faisait, bruit qui nous a rappelé celui d'une mouche. Nous avons alors pris l'habitude d'utiliser ce terme pour nous y référer. "La mouche" n'étant pas très élogieux pour un nom de projet nous avons choisi d'utiliser sa version latine : \textbf{Lucilia}.

\section{Objectifs du projet~: programmation du drone et récolte de données}

Dans ce projet, notre but est d'obtenir des données (photographies et données spatiales) à l'aide d'un drone. Ce dernier étant programmé pour effectuer une trajectoire prédéterminée autour de sa cible et stocker les données prises au cours du vol.

Nous allons donc planifier la trajectoire que le drone suivra tout en prenant en compte les potentiels écarts de trajectoire, s'assurer que le drone prenne les photographies et autres données nécessaires et enfin, qu'il puisse les stocker.

Ces objectifs soulèvent les problématiques suivantes~:

\begin{itemize}
    \item Comment planifier une trajectoire qui prenne en compte les différents facteurs liés à l'environnement dans lequel évoluera le drone ?
    \item Comment faire en sorte que l'écart entre la trajectoire prévue et effective soit minime ?
    \item Comment s'assurer que le drone puisse suivre de manière autonome la trajectoire ?
    \item Comment le drone déterminera à quel moment il doit prendre un cliché ?
    \item Comment nous assurerons-nous que le cliché soit bien pris, c'est-à-dire que la caméra du drone soit bien orientée devant son objectif ?
\end{itemize}

Dans la suite de ce rapport, nous tâcherons de préciser les procédés techniques déjà existants pour réaliser chacun des objectifs et présenter ceux que nous allons utiliser.

\chapter{État de l'art}

Le semestre précédent était consacré à la recherche bibliographique relative à notre projet. Les parties qui suivent sont donc un condensé de nos précédentes recherches et présentent les notions à comprendre et à maîtriser avant de développer notre projet.

\section{Fonctionnement d'un drone}

D’après la législation française un drone est ”un aéronef circulant sans personne à bord”. On utilise également le terme de UAV : « Unmanned Aerial Vehicle » pour qualifier les drones et inversement. Comme on peut le voir dans la figure ~\ref{fig:fonctionnement-axe}, un drone évolue dans un \textbf{plan tridimensionnel XYZ}.

\begin{figure}[!h]
    \centering
  \includegraphics[scale=0.3]{./figures/axes}
  \caption{\textbf{Drone et axes principaux}}
  \label{fig:fonctionnement-axe}
\end{figure}

L'appareil se maintient en l'air grâce à la \emph{rotation de ses rotors}. En général les hélices constituant un drone quadrirotor sont \emph{disposées en croix} afin de lui assurer la meilleure stabilité possible. Par ailleurs, deux rotors tournent dans le sens des aiguilles d'une montre alors que les deux autres tournent en sens inverse. On parle d'\emph{hélices à pas normal} et d'\emph{hélices à pas inversé}.

En variant légèrement la vitesse des rotors, notre drone peut effectuer \textbf{divers mouvements}.

\subsubsection{Gaz}

Il s'agit de la \textbf{prise ou perte d'altitude} du drone. Pour s'élever dans les airs, la vitesse des rotors doit \emph{augmenter}. De même, pour descendre dans les airs, la vitesse des rotors doit \emph{diminuer}. Ces situations sont illustrées dans les figures ~\ref{fig:fonctionnement-gaz-1} et ~\ref{fig:fonctionnement-gaz-2}.

\begin{figure}[!h]
\begin{minipage}[!h]{.5\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/gaz_monte}
    \caption{\textbf{Drone prenant de l'altitude.}}
    \label{fig:fonctionnement-gaz-1}
\end{minipage} \hfill
\begin{minipage}[!h]{.4\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/gaz_descend}
    \caption{\textbf{Drone perdant de l'altitude.}}
    \label{fig:fonctionnement-gaz-2}
\end{minipage}
\end{figure}

\subsubsection{Lacet}

Ce mouvement permet au drone d'\textbf{effectuer des rotations} sur lui-même et donc d'évoluer dans son~\textbf{axe~Z}. Cette situation est illustrée dans les figures ~\ref{fig:fonctionnement-lacet-1} et ~\ref{fig:fonctionnement-lacet-2}.

\begin{figure}[!h]
\begin{minipage}[!h]{.5\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/gaz_droite}
    \caption{\textbf{Drone effectuant une rotation sur la droite.}}
    \label{fig:fonctionnement-lacet-1}
\end{minipage} \hfill
\begin{minipage}[!h]{.4\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/gaz_gauche}
    \caption{\textbf{Drone effectuant une rotation sur la gauche.}}
    \label{fig:fonctionnement-lacet-2}
\end{minipage}
\end{figure}

\subsubsection{Roulis}

Ceci permet au drone de s'\textbf{incliner sur son axe Y}. À ce titre, le comportement de ce mouvement est assez \emph{similaire au tangage}. Cette situation est illustrée dans les figures ~\ref{fig:fonctionnement-roulis-1} et ~\ref{fig:fonctionnement-roulis-2}.

\begin{figure}[!h]
\begin{minipage}[!h]{.5\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/rouler_droite}
    \caption{\textbf{Drone s'inclinant vers la droite.}}
    \label{fig:fonctionnement-roulis-1}
\end{minipage} \hfill
\begin{minipage}[!h]{.4\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/rouler_gauche}
    \caption{\textbf{Drone s'inclinant vers la gauche.}}
    \label{fig:fonctionnement-roulis-2}
\end{minipage}
\end{figure}

\subsubsection{Tangage}

Ceci permet au drone de s'\textbf{incliner sur son axe X}. À ce titre, le comportement de ce mouvement est assez \emph{similaire au roulis}. Cette situation est illustrée dans les figures ~\ref{fig:fonctionnement-tangage-1} et ~\ref{fig:fonctionnement-tangage-2}.

\begin{figure}[!h]
\begin{minipage}[!h]{.5\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/tanguer_avant}
    \caption{\textbf{Drone s'inclinant vers l'avant.}}
    \label{fig:fonctionnement-tangage-1}
\end{minipage} \hfill
\begin{minipage}[!h]{.4\textwidth}
    \centering
    \includegraphics[scale=0.3]{./figures/tanguer_arriere}
    \caption{\textbf{Drone s'inclinant vers l'arrière.}}
    \label{fig:fonctionnement-tangage-2}
\end{minipage}
\end{figure}

Ainsi le drone peut se déplacer grâce à la \textbf{force générée par ses hélices} ; plus la vitesse des rotors est grande, plus la force de poussée du drone sera élevée et le drone se déplacera rapidement. Notons que le \textbf{type d'hélice utilisé} ainsi que leur \textbf{taille} influe également dans le \emph{calcul de force}.

De plus, un drone peut être \textbf{soumis à plusieurs autres forces}. Comme chaque objet sur Terre, un drone est soumis à la \textbf{gravité} mais il peut également être soumis à la \textbf{résistance au vent} ou même aux différences de température dans l'atmosphère qui créent des \textbf{zones de dépression}.

\section{Planification de notre trajectoire}

\subsection{Notre méthode de planification de trajectoire}

Lors de nos recherches, nous avons trouvé qu’il existait différentes façons de calculer des trajectoires en robotique. Après en avoir appris plus sur certaines, notre choix s'est fixé sur la \textbf{méthode de génération de mouvement point à point à profil d'accélération bang-bang}. Parmi les autres méthodes étudiées, celle-ci nous permet de \textbf{faire exactement ce que nous désirons} : nous arrêter à chaque point d'arrêt et rejoindre chacun des points rapidement. Ainsi avoir une phase d'accélération suivie juste après d'une phase de décélération nous parait approprié.

Concernant notre méthode de planification de trajectoire, il s'agit de la \textbf{composition de spline la plus simple}. Chacune des trajectoires point à point comportent une phase d'\textbf{accélération constante suivie d'une phase de freinage}. La loi de mouvement est composée de deux splines du second degré -ce qui permet de faciliter les calculs- et on détermine les différents coefficients à partir de nos \textbf{contraintes initiales, finales et de continuité} de la manière suivante :

\[
\left\{
\begin{array}{r l}
x(t) = at^2  +  bt  +  c  ;  0 \leq t \leq T/2\\
x(tT) = d(t - T)^2 + e (t - T) + f  ;  T/2 \leq t \leq T/2
\end{array}
\right.
\]

avec, $T$ désignant la \textbf{durée du mouvement} et $a$, $b$, $c$, $d$, $e$ et $f$ les \textbf{coefficients déterminés à partir des contraintes initiales, finales et de continuité} en :

\[
\left\{
\begin{array}{r c l}
x(t = 0) = x^d , \dot{x}(t = 0) = 0\\
x(t = T) = x^a , \dot{x}(t = T) = 0\\
x(t = (T/2)^-) = x(t = (T/2)^+)\\
\dot{x}(t = (T/2)^- =  \dot{x}(t = (T/2)^+)
\end{array}
\right.
\]

\[
\left\{
\begin{array}{r c l}
a = 2(x^a - x^d) / T^2 \\
b = 0\\
c = x^d\\
d = -2(x^a - x^d)/T^2\\
e = 0\\
f = x^a
\end{array}
\right.
\]

La figure~\ref{fig:trajectoire-illustration} illustre la trajectoire de notre drone. Il va effectuer des rotations autour d'un lieu et au cours de ses rotations, il s'arrêtera en diverses positions pour prendre des clichés d'une scène.

\begin{figure}[!h]
    \centering
  \includegraphics[scale=0.3]{./figures/trajectoire}
  \caption{\textbf{Survol d'un lieu avec un drone et prise de clichés.}}
  \label{fig:trajectoire-illustration}
\end{figure}

\subsection{Contrôle de la trajectoire}

Très souvent une différence plus ou moins grande entre la trajectoire théorique -précédemment calculée- et la trajectoire effective persiste. Dans le but d’amoindrir d’éventuels écarts entre ces deux trajectoires, nous pouvons essayer de corriger la trajectoire du drone pendant son vol afin qu’une fois sa destination atteinte, la trajectoire effectuée soit la plus proche possible de la trajectoire théorique. Pour contrôler notre trajectoire nous avons choisi le PID.

\subsubsection{Fonctionnement du PID : proportionnelle dérivée intégrale}

Afin de corriger la trajectoire effective pour qu'elle s'apparente le plus possible à la trajectoire théorique, \textbf{le PID mémorise l'écart de trajectoire, la somme des écarts ainsi que la différence entre l'écart de trajectoire courant avec les écarts précédents}. Il est composé de trois règles~:

\begin{itemize}
    \item \textbf{Régulateur proportionnel P} : proportionnel à l'erreur.

    On note : $ commande = Kp * erreur $ avec $Kp$ coefficient de proportionnalité de l'erreur.

    \item \textbf{Régulateur proportionnel intégral PI} : également proportionnel à l'erreur mais aussi proportionnel à l'intégrale de l'erreur.

    On note : $ commande = Kp * erreur  +  Ki * sommeErreurs $ avec $Kp$ coefficient de proportionnalité de l'erreur et $Ki$ coefficient de proportionnalité de la somme des erreurs.

    \item \textbf{Régulateur proportionnel dérivé PD} : également proportionnel à l'erreur mais aussi proportionnel à la dérivée de l'erreur, c'est-à-dire à la variation de l'erreur entre deux échantillons : on fait la différence entre l'erreur courante et l'erreur précédente.

    On note $ commande = Kp * erreur  +  Kd * (erreur - erreurPrécédente) $ avec $Kp$ coefficient de proportionnalité de l'erreur et $Kd$ coefficient de proportionnalité de la variation de l'erreur.

\end{itemize}

\subsubsection{Application du PID}

Comme vu précédemment nous devons fixer nos paramètres $Kp$, $Ki$ et $Kd$ avant de pouvoir appliquer le PID. Nous commençons par fixer $Kp$ en prenant un $Ki$ et un $Kd$ nuls, puis nous fixerons $Ki$ et enfin $Kd$. Une fois ces paramètres fixés nous pouvons appliquer le PID à notre système comme le décrit la figure :

\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.7]{./figures/pid}
  \caption{\textbf{Application du PID} (source : \url{https://polyengineer.wordpress.com/2014/08/08/self-balancing-robot-pid-control/}).}
  \label{fig:trajectoire-pid}
\end{figure}

Comme on peut le voir sur la figure~\ref{fig:trajectoire-pid}, en entrée nous avons la trajectoire théorique désirée, c'est-à-dire la position précise où nous voulons que le drone soit. Ensuite cette trajectoire théorique est mise à jour avec l'erreur $e(t)$ : ainsi le PID prend en entrée la différence de trajectoire entre la trajectoire théorique et effective (c'est ce que représente $e(t)$). Ensuite nous appliquons chacun des paramètres du PID, soit $Kp$ le régulateur proportionnel, $Ki$ le régulateur proportionnel intégral et $Kd$ le régulateur proportionnel dérivé. Une fois chacun de ces paramètres appliqués, on les ajoute puis les donne au système qui prend ces données en compte afin de corriger sa trajectoire. La nouvelle trajectoire est calculée et est passée à nouveau en entrée du PID après avoir été comparée avec la trajectoire théorique.

\section{Récolte et stockage des données}

Maintenant que nous savons comment calculer la trajectoire de notre drone, nous nous sommes intéressés à comment les données seront récoltées. Concernant ces données, après consultation de nos camarades chargés de la modélisation du modèle, seules les photographies du modèle leurs sont nécessaires. Ces dernières doivent être prises avec des positions et des angles différents afin d’avoir des points de vue se superposant. De plus, la bonne qualité de ces photographies est capitale pour le travail à réaliser en aval, c’est pourquoi nous avons choisi le format DNG où les images sont le moins traitées possible.

Notre trajectoire est divisée en de nombreux points d'arrêt. Concrètement, le drone va \textbf{partir d'un point de départ jusqu'à sa destination finale en s'arrêtant à chacun des points constituant sa trajectoire}. Ainsi, le drone va effectuer plusieurs rotations autour de sa cible, chacun de ses tours ayant une hauteur différente. On peut comptabiliser une vingtaine de clichés par rotation. Nous allons \textbf{profiter de chacun de ces points d'arrêt pour prendre un cliché} de l'élément à photographier. À chacune de ces étapes, le drone va \emph{effectuer les orientations de caméras et autres mises au point nécessaires avant de prendre la photographie souhaitée et de retenir sa position}. Nous pouvons \textbf{contrôler les cardans de la caméra} du drone ce qui nous permet de la diriger au niveau des \textbf{axes de tangage et lacet} comme nous le souhaitons vers sa cible.

\begin{wrapfigure}[18]{r}{8cm}
    \centering
    \includegraphics[scale=0.5]{./figures/programmation_drone.png}
    \caption{\textbf{Schéma de l'interaction entre l'application mobile que nous allons créer et le drone en passant par la radiocommande }(source : \url{https://developer.dji.com/mobile-sdk/documentation/introduction/mobile_sdk_introduction.html}).}
    \label{fig:programmation}
\end{wrapfigure} \hfill

Nous allons contrôler notre drone grâce à une \textbf{application Android}. Cette dernière utilise le \emph{Mobile SDK (kit de développement logiciel)} fourni et créé par le fabricant de notre drone. La gestion de trajectoire coupée en points d'arrêt, le positionnement de la caméra et la prise de photographies peuvent être gérés par le SDK ; ainsi ce que nous devons faire est utiliser toutes les ressources mises à notre disposition pour créer notre application.

\chapter{Organisation du projet}

Dans cette partie nous aborderons les choix faits concernant la répartition de notre travail et nous présenterons les outils utilisés pour ce dernier.

\section{Répartion du travail}

Après avoir une idée plus concrète du travail à réaliser, nous avons décidé de nous répartir le travail. En effet, notre projet est suffisamment conséquent et de part les différentes notions qui le composent, cette décision semble naturelle. À première vue, notre projet se compose de deux parties distinctes.

\begin{itemize}
  \item \textbf{Génération de trajectoire} : il s'agit de décider d'où le drone s'arrêtera en fonction de son environnement et de la cible qu'il doit photographier. Nous avons décidé d'implémenter un programme en C++ nous permettant de planifier ainsi la trajectoire selon les différents paramètres l'affectant et de générer un fichier contenant les coordonnées correspondant aux points d'arrêt du drone.
  \item \textbf{Programmation du drone} : une fois notre trajectoire déterminée, nous voulons pouvoir la communiquer à notre drone et s'assurer que ce dernier prenne bien les clichés aux positions voulues, c'est l'objectif de cette partie. Pour ce faire nous avons utilisé le kit de développement déjà existant et fourni par l'entreprise du drone, le DJI Mobile SDK.
\end{itemize}

\section{Outils et matériels utilisés}

Tout au long de notre projet nous avons utilisés différents outils et matériels.

\subsection{Outils de communication}

Dans le but de faciliter la communication et le travail des trois binômes nous avons employé plusieurs outils. Nous avons d'abord mis en place un dépot gitlab contenant le travail de chacun, que ce soit pour les documents dont certaines parties étaient communes comme nos rapports, mais également pour les compte rendus de réunion et bien sûr pour notre code aussi. Dans le but de nous tenir informé du déroulement du projet et pour communiquer nous avons utilisé une application de messagerie instantanée : Telegram. Il nous est également arrivé de faire des réunions vocales et nous utilisions alors Framatalk ainsi que Framapad pour les notes de réunion. Au sein de notre binôme et travaillant sur des choses distinctes nous nous organisions essentiellement de vive voix.

\subsection{Le drone}

Travailler avec un drone n'est \textbf{pas évident}. Le semestre précédant nos démarches nous avaient appris que nous ne pourrions pas faire voler notre drone en extérieur. Il nous restait la possibilité de faire \textbf{voler le drone en intérieur} et nous avions plusieurs pistes pour cela : utiliser un des \emph{gymnases} du SUAPS au site de la Motte Rouge situé près de l'université, ou emprunter les \emph{locaux du club de drone} de l'IUT de Montpellier. Une fois l'autorisation de faire voler le drone en intérieur, il nous restait encore à trouver la \textbf{cible que nous allons modéliser}, il fallait que cette dernière soit suffisamment solide pour résister au souffle du drone, qu'elle soit facile à transporter tout en étant dans des dimensions respectables.

De plus, l'utilisation du drone implique des \textbf{règles de sécurité} que nous ne devions pas négliger (vérifications des éléments constituant le drone avant le vol, périmètre de sécurité...).

\subsection{Code}

Pour la génération de la trajectoire, nous avons utilisé le C++. Concernant la programmation du drone, nous avons utilisé \textbf{Android Studio}, un \emph{téléphone portable compatible avec Android} et le \textbf{kit de développement fourni par DJI}.

\chapter{Génération de la trajectoire}

À partir d'un modèle, nous pouvons générer une trajectoire en forme d’hélice circulaire, illustrée ci-dessous.

\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.5]{./figures/helice.png}
  \caption{\textbf{Illustration d’une hélice circulaire} (source : \url{http://serge.mehl.free.fr/anx/helice.html}).}
  \label{fig:trajectoire-helice-circulaire}
\end{figure}

\section{Langage de programmation}

Nous avons choisi le C++ comme langage de programmation car il permet de manipuler les fichiers et il est orienté objet. De plus c'est un langage que nous connaissons bien.

\section{Le modèle requis}

Pour générer la trajectoire, nous avons seulement besoin de connaître les points.
En effet, nous cherchons à faire un tour de l'ensemble des points.
Le format fichier n'est pour l'instant pas décidé. (entrée et sortie)

\section{Création de la trajectoire}

La création de la trajectoire est faite en plusieurs étapes :

\begin{itemize}
	\item \textbf{Extraction} : Du fichier \textit{fe} à un vecteur(tableau) de points 3D: \textit{t}
	\item \textbf{Calcul de la marge hauteur} : Calcul de la hauteur utilisée.
	\item \textbf{Aplatissement de la 3D à la 2D} : Copie de \textit{t} sans la hauteur (2D): \textit{d}
	\item \textbf{Calcul du diamètre} : Calcul du cercle \textit{c} comprenant tous les points de \textit{d} avec une marge
	\item \textbf{Calcul du nombre de points sur cercle} : Calcul du nombre de points nécessaires sur \textit{c}
	\item \textbf{Création du parcours circulaire en 2D} : Création du parcours (vecteur) 2D \textit{p2} dont tous les points sont sur \textit{c}
	\item \textbf{Création du parcours hélice circulaire en 3D} : Création du parcours (vecteur) 3D \textit{p3} basé sur une suite de \textit{p2} (\textit{x} et \textit{y} depuis \textit{p2}, \textit{z} variant)
	\item \textbf{Insertion} : Du vecteur \textit{p3} à un fichier \textit{fs}.
\end{itemize}

\subsection{Extraction}

Cette partie du code n'est pas encore implémentée

\subsection{Calcul de la marge hauteur}

Pour faire l’hélice circulaire \textit{p3}, qui est une répétition de \textit{p2} en \textit{x} et \textit{y}, nous sommes contraints de respecter une hauteur maximale entre chaque tour. Nous prenons donc l'écart de la hauteur maximale \textit{h}, \textit{i = 1}, tant que \textit{h / i} dépasse la limitation, nous incrémentons \textit{i} et nous revérifions, obtenant ainsi l'écart de hauteur voulu entre chaque tour.

\subsection{Aplatissement de la 3D à la 2D}

C'est tout simplement \textit{d = t} sans la hauteur.

\subsection{Calcul du diamètre}

Nous cherchons les deux points dans d qui sont les plus éloignés (il peut y en avoir plusieurs mais un suffit). Ainsi nous trouvons le milieu entre deux points correspondant à l'origine du cercle \textit{cm} minimum comprenant tous les points de \textit{d}. Nous trouvons ainsi le cercle \textit{c} de diamètre (diamètre de \textit{cm + 2 * distance de sécurité}).

\subsection{Calcul nombre de points sur cercle}

Une fois \textit{c} trouvé, nous pouvons introduire le nombre nécessaire de points sur \textit{c} tel que
\begin{itemize}
	\item La distance entre chaque point et son suivant ne dépasse pas une certaine valeur
	\item L'angle entre chaque point et son suivant depuis l'origine de \textit{c} ne dépasse pas une autre valeur.
\end{itemize}

\subsection{Création parcours circulaire en 2D}

Une fois ces calculs réalisés, nous pouvons enfin créer \textit{p2} qui est un vecteur(tableau) de points 2D dont l'ordre à une importance. Cela donne un circuit circulaire, un tour.

Chaque point doit donner l’orientation du drone pour qu’il "regarde" l’origine de \textit{c}.

\subsection{Création parcours hélice circulaire en 3D}

Depuis \textit{p2}, nous pouvons créer \textit{p3}, vecteur de points 3D. Ce sera une suite de \textit{p2} du point de vue de l'axe \textit{x} et \textit{y}. Mais \textit{z} varie de telle sorte qu’il crée une hélice circulaire.

Dans la première boucle, tous les points ont la hauteur minimale de la zone de vol du drone ou le minimum de \textit{t} s’il est supérieur à cette première limite (on ne peut pas faire voler un drone trop près ou dans le sol.)
Dans la dernière boucle tous les points ont la hauteur maximale de \textit{t}.

Sur les autres boucles, on obtient une part d’hélice circulaire : au fur et à mesure qu'on avance dans la boucle, \textit{z} augmente constamment. On a une distance égale entre chaque point et la même sur la boucle suivante : on appelle cela le pas.

\subsection{Insertion}

Cette partie du code n'est pas encore implémentée.

\textbf{Remarque}: Si une erreur ou exception se lève à n'importe laquelle des étapes précédentes, ce ficher ne sera pas créé ou modifié.

\section{À propos des méthodes étudiées sur la génération de trajectoire...}

L’étude sur la génération de trajectoire est pratique si on veut programmer un drone.
Or on ne fait que l’utiliser pour appliquer notre trajectoire.
Il s’avère donc que cette partie de l’étude n’est pas utile au projet.

%Faire commentaire sur le fait que les méthodes vues précédemment ne concerne que les robots que l'on veut programmer, nous on ne fait pas ça. Introduire la forme que le fichier des trajectoires aura.

\chapter{Application}

Dans cette partie, nous présenterons les raisons qui nous ont poussées à réaliser cette application, dans quel but et surtout, comment nous l'avons développée.

\section{But de l'application}

Pour rappel, la finalité de ce projet est de récolter des clichés d'une cible afin d'en établir un modèle tridimensionnel. Nous voulons photographier la cible avec un drone, en effet ces appareils sont assez utiles de part leur mobilité. Nous souhaitons également que nos photos couvrent à 180 degrés la cible, la trajectoire que nous avons générée dans la partie précédente ayant alors une forme de spirale. Une fois que nous avons notre cible et notre trajectoire, nous voulons que le drone "comprenne" cette trajectoire et l'effectue tout en s'arrêtant aux endroits indiqués par la trajectoire et prenne une photo qu'il stockera.

Nous avons choisi de réaliser une application car c'est le moyen le plus simple de communiquer avec notre modèle de drone. En effet, son fabricant -DJI- propose un kit de développement que nous avons déjà brièvement présenté auparavant. Ainsi cette application devra être capable de lire le fichier précedemment créé contenant les coordonnées des points d'arrêt du drone qui constituent sa trajectoire ainsi que ses angles sur ses trois axes : roulis, tangage et lacet. À partir de ces points, notre drone devra construire une trajectoire (liste de points à atteindre) et une fois sa pose correcte prendre un cliché de la cible -cliché qu'il stockera sur une mémoire externe (carte SD)-.

\section{Réalisation}

Avec le recul, cette phase de réalisation s'est composée de deux étapes différentes : une phase de découverte et compréhension des technologies utilisées puis une phase d'implémentation de l'application.

\subsection{Phase de découverte}

Cette partie du projet nécessite d'utiliser des outils et technologies avec lesquels nous n'avions jamais travaillé auparavant. De ce fait, il a fallu un certain temps avant que nous maîtrisions ces notions.

\subsubsection{Android et Android Studio}

N'ayant jamais réalisé d'application mobile nous ne connaissions ni l'Android ni son environnement de développement : Android Studio. Nous avons pu comprendre progressivement comment Android fonctionnait grâce à des camarades ayant déjà eu à travailler avec ces notions. Par ailleurs en regardant des exemples fournis par DJI et autres tutoriels nous avons fini par comprendre la façon dont les choses s'organisent entre elles. Là où les choses se sont un peu compliquées c'est quand nous avons voulu importer notre propre application, basée sur le code fourni par le SDK Mobile. En effet nous ignorions que pour pouvoir fonctionner, une application Android avait besoin d'un identifiant unique suivant l'architecture du projet et nous nous retrouvions devant la même erreur sans trop savoir quelle partie du code péchait.

Concernant Android Studio, c'est un logiciel assez facile à prendre en main et ressemblant à d'autres environnements de développement. Son utilisation ne nous a donc pas posé de problème.

Globalement, pour acquérir ces notions de bases, les ressources universitaires que l'on nous a donné mais aussi les documentations en ligne du Mobile SDK et le Guide du développeur Android Google nous ont bien aidés.

\subsubsection{Kit de développement de DJI : Mobile SDK}

Une fois que nous maitrîsions les bases d'Android, nous nous sommes intéréssés de près au kit de développement fourni par DJI : à sa documentation, ses exemples d'applications~\parencite{dji-sdk-sample} ainsi qu'à ces tutoriels. Nous nous sommes donc vite retrouvés devant grande quantité d'informations, toutes pas forcément utiles pour notre projet.

La documentation du kit de développement~\parencite{dji-sdk-doc} est suffisamment fournie et le rôle de chacune des classes est bien expliquée, cependant la façon dont les classes sont organisées entre elles ne se retrouvent pas forcément dans les exemples de projet donnés.

Il existe de nombreux tutoriels illustrant ce que les applications permettent de réaliser avec le drone et comment le faire~\parencite{dji-tutoriels-git}.

\begin{itemize}
 \item \textbf{GSDemo-GoogleMap} : permet de générer des trajectoires avec des points d'arrêts en utilisant GoogleMap.
 \item \textbf{FPV-Demo} : permet de faire une vue à la première personne.
 \item \textbf{Simulator-Demo} : montre comment utiliser le simulateur de DJI.
 \item \textbf{Media-Manager} : montre comment interagir entre la caméra et les périphériques de stockage (carte SD) du drone.
 \item ...
\end{itemize}

Parmi ces divers tutoriels, nous en avons retenu deux qui nous ont parus particulièrement pertinents : le \textbf{Media-Manager} et le \textbf{GSDemo-GoogleMap}. Par conséquent c'est en utilisant ces tutoriels que nous allons pouvoir faire notre application.

\subsection{Implémentation}

Pour ce faire on utilisera le "projet type" fourni par DJI ainsi que deux des tutoriels précedemment vus. Tout l'enjeu de notre travail est donc d'analyser le code existant et le modifier afin que nous puissions faire ce que nous désirons.

\subsubsection{Suivi de la trajectoire}

Les drones de DJI peuvent être automatisés par ce qu'on appelle des "missions". On peut approcher cela à des modes de vols tels que :

\begin{itemize}
  \item \textbf{Follow Me} : le drone va suivre la télécommande en restant à une altitude et distance constante ;
  \item \textbf{Tap Fly} : le drone se dirige aux endroits désignés par le pilote lorsqu'il appuie sur l'application ;
  \item \textbf{Waypoint} : le drone se dirige vers des points d'arrêts précisés. Il peut également effectuer une action une fois arrivé à un point précis.
  \item ...
\end{itemize}

Nous utilisons le "\emph{Waypoint Mission}". Ainsi on crée une nouvelle mission à partir des coordonnées présentes sur le fichier généré précédemment. Une mission ne peut contenir que quatre-vingt-dix-neuf points d'arrêt.

\subsubsection{Prise des clichés et stockage}

Maintenant que nous savons où nous déplacer et où nous arrêter, il nous faut savoir quoi faire à chaque point d'arrêt, c'est-à-dire dans notre cas prendre une photographie. Le type de mission précédemment choisi nous permet de faire une action à chaque arrêt : nous bougeons alors légérement la caméra en accord avec ce qui a été calculé précédemment pour être bien en face de notre objectif.

Par ailleurs les paramètres de la caméra tels que le focus et l'ouverture sont contrôlables, nos images correspondent donc à ce que nos camarades en aval attendent.

\vspace*{1cm}

À ce jour l'implémentation du programme n'est pas terminée et nous continuons à travailler sur son aboutissement.

\chapter{Conclusion}

Lors de ce semestre notre projet ne s'est pas déroulé exactement comme nous l'avions espéré et de ce fait, l'avancement n'est pas aussi abouti que ce qu'il devrait l'être à ce jour.

Ce retard est notamment dû à plusieurs difficultés auxquelles nous avons dû faire face. La première est le \textbf{manque de temps}, nous avons l'impression que ce semestre a été plus court et plus dense que le précédent et comparativement à la charge de travail que ce projet nécessitait, le temps allouable à ce dernier semblait bien faible. Cette charge de travail est d'autant plus importante que notre projet était particulièrement ambitieux. Ainsi une autre difficulté que nous avons rencontrée est l'\textbf{audace de notre projet} : nous n'avions jamais réalisé d'applications mobile, ni n'avions travaillé avec des robots et nous avons découvert le kit de développement de DJI qui est plutôt conséquent. Au début du semestre, nous avons également rencontré quelques difficultés à nous \textbf{organiser et à communiquer au sein de notre binôme}. Nous avons choisi de surmonter cette difficulté en nous répartissant le travail afin de donner à chacun de l'autonomie dans ses tâches tout en nous tenant informé de notre avancement. Enfin, nous avons à nouveau eu quelques difficultés à trouver un endroit où faire voler notre drone ainsi qu'à trouver une cible à photographier.

Parallèlement, ce projet a été une grande aventure à laquelle nous avons été heureux de participer. Il nous a permis d'acquérir de nombreuses connaissances et a donc été très enrichissant que ce soit sur le plan technique ou humain. Travailler dans un projet de cette envergure et qui relevait un peu du défi était une bonne expérience que nous ne regrettons pas. Au sein de notre équipe de projet, nous avons eu la chance de toujours bien nous entendre et de rester soudés même dans les moments de fatigue. De plus, nos encadrants nous ont toujours bien accompagnés et nous leur en sommes reconnaissants pour cela.

Enfin au sein de notre binôme il nous reste à ce jour un regret : ne pas avoir pu faire voler le drone pour prendre les images qui auraient servi à la réalisation du modèle. Cependant, comme nous allons poursuivre notre projet au moins jusqu'à la soutenance, cet objectif pourra être atteint.

\phantomsection
\listoffigures
\newpage
\printbibliography

\end{document}
