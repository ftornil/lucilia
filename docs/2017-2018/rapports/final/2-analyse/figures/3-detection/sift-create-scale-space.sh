#!/usr/bin/bash
# Génére les couches de la pyramide SIFT à partir d’une image

# Image d’entrée sans l’extension
input="sift-detection-example"

# Nombre d’octaves à générer
octaves=2

# Nombre de couches par octave
levels=3

###

previous=""
sigma=1.6

for ((octave=0;octave<octaves;octave++))
do
    for ((level=0;level<levels;level++))
    do
        current="$input-o$octave-b$level.png"
        diff="$input-o$octave-d$level.png"

        if [ $level -eq 0 ]
        then
            if [ $octave -eq 0 ]
            then
                convert "$input.jpg" -grayscale Average "$current"
            else
                convert "$previous" -resize 50% "$current"
            fi
        else
            convert "$previous" -gaussian-blur "0x$sigma" "$current"
            composite "$previous" "$current" -compose difference "$diff"
            convert "$diff" -auto-level "$diff"
        fi

        previous="$current"
    done
done
