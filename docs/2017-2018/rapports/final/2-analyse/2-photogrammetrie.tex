\chapter[Positionnement du projet dans l’état de l’art]{Positionnement du projet\\dans l’état de l’art}
\label{chapter:state-of-the-art}

Le problème de l’estimation de la trajectoire suivie par un appareil lors de la capture d’un flux d’images et de la reconstruction d’un modèle~3D d’une scène appartient au domaine de la photogrammétrie. La photogrammétrie est la science et l’ensemble des techniques qui permettent d’obtenir de l’information sur un environnement physique à partir d’images de cet environnement~\parencite[p.~1, chapitre~1]{forstner-2016-photogrammetric}.

Dans la littérature, ce problème est appelé \emph{«~structure from motion~»} (et parfois abrégé en \emph{SfM}). Son objectif est, selon \cite{waxman-1985-sfm}, «~l’inversion du processus de création d’un flux d’images afin de déterminer la structure locale de l’objet visualisé et le déplacement relatif dans l’espace de l’observateur par rapport à l’objet~»\footnote{\emph{«~The goal is then to invert the image flow process along the line of sight and thereby to determine the local structure of the object under view and the relative motion in space between the object and the observer~»}~\parencite[p.~1, traduction personnelle]{waxman-1985-sfm}.}. Il est assorti d’un vocabulaire spécifique qu’il convient d’expliciter~:

\glossaire{Vocabulaire du problème \emph{«~structure from motion~»}}{
    \item[Image~:] surface rectangulaire et planaire sur laquelle sont projetés les points d’une scène.
    \item[Scène~:] environnement capturé en partie dans une image par un appareil photographique.
    \item[Pose~:] position et orientation d’un objet dans un espace.
    \item[Appareil photographique (ou \emph{camera})~:] dispositif numérique ou analogique, physique ou conceptuel, permettant de capturer une image (en~2D) à partir d’une pose dans une scène (en~3D).
    \item[Mouvement (ou \emph{motion})~:] suite de poses d’un appareil photographique lors de la capture d’un flux d’images dans une scène.
    \item[Modèle de la scène (ou \emph{structure})~:] discrétisation et numérisation de la scène sous forme d’un nuage de points ou d’un maillage de polygones texturisé.
    \item[Point caractéristique (ou \emph{scene/image feature})~:] zone d’une scène ou d’une image aisément localisable indépendamment de la pose de l’appareil photographique, de la luminosité ou du contraste de l’image.
}

\section{Modèle du sténopé pour les appareils photographiques}

Les appareils photographiques comprennent traditionnellement un objectif qui projette les rayons de lumière entrants sur un capteur qui enregistre l’image résultante~\parencite[chapitre~34, p.~1182]{young-2008-physics}. Ce dispositif permet le passage de l’espace~3D de la scène au plan~2D de l’image. Chaque objectif dispose de propriétés géométriques spécifiques définissant la manière dont les différents éléments de la scène sont projetés sur l’image.

Selon si les segments de la scène sont projetés sur des lignes droites dans l’image ou sur des courbes, l’objectif est dit en perspective linéaire ou curviligne. La figure~\ref{fig:position-image-distorted} montre un exemple d’image obtenue avec un objectif en perspective curviligne.

\begin{figure}[htb]
    \begin{minipage}[t]{.495\linewidth}
        \centering
        \includegraphics[width=.85\linewidth]{2-photogrammetrie/image-distorted}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.495\linewidth}
        \centering
        \includegraphics[width=.85\linewidth]{2-photogrammetrie/image-corrected}
    \end{minipage}
    \caption{\textbf{Photographie de la Banque Nationale de Roumanie prise avec un objectif \emph{fisheye}} (par Diego Delso, sous licence CC-BY-SA). À gauche, l’image originelle, sur laquelle les courbes surlignées sont en réalité des lignes droites. À droite, une version rectifiée de l’image.}
    \label{fig:position-image-distorted}
\end{figure}

Les appareils dotés d’un objectif en perspective linéaire peuvent, pour le problème \emph{«~structure from motion~»}, être réduits à l’assemblage d’un capteur et d’un sténopé\footnote{«~Petit trou percé dans une feuille mince disposée au lieu et place de l'objectif d'un appareil photographique.~»~\parencite{gdt}. En anglais, \emph{pinhole}.}, qui se substitue à l’objectif~\parencite[chapitre~6, p.~153]{hartley-2004-multiple}. Ce modèle (cf. figure~\ref{fig:position-pinhole}) met de côté tous les éléments optiques de l’appareil qui n’affectent pas la géométrie des images capturées.

\begin{figure}[htb]
    \centering
    \includegraphics[scale=.8]{2-photogrammetrie/pinhole}
    \caption{\textbf{Simplification du dispositif d’appareil photographique par le modèle du sténopé.} Une image de la scène observée se forme, à l’envers, sur un plan au fond de l’appareil. Le centre de l’image est appelé point principal. Le sténopé, également appelé ouverture, est le centre de projection de l’image. La distance entre le sténopé et l’ouverture est appelée distance focale.}
    \label{fig:position-pinhole}
\end{figure}

\section{Processus de reconstruction d’un modèle~3D}

\citeauthor{nister-2007-sfm-nphard} ont démontré en~\citeyear{nister-2007-sfm-nphard} que le problème \emph{«~structure from motion~»} est NP-difficile lorsqu’il existe des points visibles dans certaines images du flux et masqués dans d’autres. Dans le cadre de notre projet, et de la plupart des situations concrètes de reconstruction, cette condition est vérifiée\footnote{Par exemple, si le drone réalise une rotation autour d’un cube, il n’est jamais possible d’en voir les six faces dans une même image.}.

Par conséquent, nous nous sommes intéressés aux heuristiques qui permettent de fournir un modèle se rapprochant au mieux de la réalité dans une majorité de situations. Ces heuristiques peuvent se révéler coûteuses en calculs, et nous avons donc prêté une attention particulière à la réduction du temps de calcul. Par ailleurs, la plupart des heuristiques utilisent des seuils qui doivent être ajustés selon la situation où elles sont utilisées.

Pour obtenir des solutions approximatives au problème~\emph{«~structure from motion~»}, nous proposons un processus à six phases inspiré du travail de~\cite{forstner-2016-photogrammetric}~(chapitre~11), illustré par la figure~\ref{fig:sfm-pipeline}.

\begin{figure}[htb]
    \makebox[\textwidth][c]{
        \includegraphics{2-photogrammetrie/sfm-pipeline.pdf}
    }
    \caption{\textbf{Processus de reconstruction d’un modèle~3D d’une scène en six phases.}}
    \label{fig:sfm-pipeline}
\end{figure}

\begin{enumerate}[align=left]
\item[\textbf{Phase 1 ---}] \textbf{Détection de points caractéristiques dans les images,} c’est-à-dire de zones qui sont uniques dans chaque image et peuvent facilement être reconnues sous d’autres angles de vues (travaux de \cite{lowe-2004-sift}~; \cite{bay-2008-surf}).
\item[\textbf{Phase 2 ---}] \textbf{Étalonnage de l’appareil photographique} utilisé pour capturer le flux d’images afin d’obtenir les paramètres géométriques de son objectif, tels que la distance focale ou la position du point principal (travaux de~\cite{zhang-2000-calibration}).
\item[\textbf{Phase 3 ---}] Association des points caractéristiques similaires dans chaque paire d’images afin \textbf{d’estimer la trajectoire} suivie par l’appareil lors de la capture du flux d’images (travaux de \cite{hartley-2004-multiple}). Utilisation de cette trajectoire pour \textbf{reconstruire un nuage de points de la scène} en positionnant chaque point caractéristique dans l’espace (travaux de \cite{forstner-2016-photogrammetric}).
\item[\textbf{Phase 4 ---}] \textbf{Ajustement global de la position des points et de la trajectoire} de l’appareil avec pour objectif de minimiser la distance entre la position originelle des points sur chaque image et la position obtenue par re-projection du nuage de points sur les images.
\item[\textbf{Phase 5 ---}] \textbf{Génération d’un maillage texturisé} à partir du nuage de points calculé.
\end{enumerate}

Nous avons décidé de travailler sur les quatre premières phases de ce processus. Ainsi, nous ne visons pas la génération d’un maillage texturisé optimal, mais plutôt la reconstruction d’un nuage de points approximatif de la scène.

\section{Modèle objet pour le processus de reconstruction}

Nous avons conçu le modèle en figure~\ref{fig:uml-model} pour mettre en place notre solution.

\begin{figure}[htb]
    \makebox[\textwidth][c]{
        \includegraphics{../../../../projects/analyse/docs/modelisation/classes}
    }
    \caption{\textbf{Ensemble de classes modélisant les quatre premières phases du processus de reconstruction d’un modèle~3D.}}
    \label{fig:uml-model}
\end{figure}

La classe \textbf{Camera} modélise l’appareil photographique utilisé pour capturer une image. Celui-ci est réduit à ses paramètres géométriques puisque, sous le modèle du sténopé, il s’agit du seul critère qui différencie deux appareils.

La classe \textbf{Capture} regroupe les informations relatives à une des images prises de la scène par un appareil. Elle contient l’appareil utilisé pour la capture, sa pose au moment de la capture, et les données de l’image. Un point caractéristique d’une image est représenté par la classe \textbf{Feature}.

Enfin, la classe \textbf{Scene}, le point d’entrée du modèle, modélise la scène étudiée. Chaque instance contient~:

\begin{itemize}
    \item une instance de la classe \textbf{Camera} correspondant à l’appareil photographique utilisé pour la capture du flux d’images. Il est supposé que ses paramètres ne varient pas significativement d’une capture à l’autre~;
    \item une liste d’instances de la classe \textbf{Capture} qui correspondent au flux d’images capturé par l’appareil~;
    \item un ensemble de points formant le nuage de points approchant la structure de la scène. Ce sont des instances de la classe \textbf{Match} qui contiennent les points caractéristiques auxquels elles correspondent dans les images.
\end{itemize}

Les phases étudiées du processus de reconstruction peuvent être reformulées sous ce modèle. Initialement, une instance de \textbf{Scene} est créée. La détection des points caractéristiques d’une image est réalisée par la méthode \texttt{Capture::detectFeatures()}. L’étalonnage de l’appareil correspond à l’obtention des informations nécessaires à la création de l’instance de \textbf{Camera} liée à la scène. L’estimation de la trajectoire suivie par l’appareil permet d’obtenir les poses de chaque capture du flux d’images. Finalement, lors de la reconstruction du nuage de points, chaque correspondance trouvée donne lieu à la création d’une instance de \textbf{Match} dans la scène, qui peut ensuite être positionnée dans l’espace.

\section{Choix des outils pour mener à bien le projet}

Nous avons choisi d’utiliser le langage C++17\footnote{Langage C++~: \url{https://isocpp.org/}} ainsi que la bibliothèque OpenCV\footnote{Bibliothèque OpenCV~: \url{https://opencv.org/}} pour implémenter l’outil de reconstruction de modèles~3D à partir d’un flux d’images. Pour visualiser les données résultantes, nous avons utilisé l’outil gnuplot\footnote{Outil gnuplot~: \url{http://www.gnuplot.info/}}.

Cette bibliothèque regroupe des implémentations d’algorithmes pour l’analyse, la transformation ou la génération d’images. Elle inclut notamment des algorithmes pour la détection de points caractéristiques, leur mise en correspondance, l’étalonnage de l’appareil et le positionnement dans l’espace. Par ailleurs, pour la plupart des algorithmes proposés, l’implémentation est parallélisée et exploite le matériel disponible sur la machine pour accélérer les calculs lorsque cela est possible. Pour ces raisons, OpenCV est aujourd’hui considérée comme la bibliothèque de référence pour les programmes de vision par ordinateur.

Employant plusieurs heuristiques, notre outil accepte plusieurs options permettant d’ajuster les seuils utilisés. Ces options peuvent être passées sur la ligne de commande lors de l’appel à l’outil ou bien via un fichier de configuration. La gestion des options peut se révéler compliquée, et nous avons donc décidé d’utiliser la bibliothèque Boost Program Options\footnote{Boost Program Options~: \url{https://www.boost.org/doc/libs/release/doc/html/program_options.html}} pour la simplifier. Le manuel de l’outil, en annexe~\ref{appendix:manual}, présente l’ensemble des options disponibles.

La suite de ce rapport détaille, pour chaque phase sur laquelle nous avons travaillé, les recherches spécifiques que nous avons réalisées, les algorithmes conçus ou exploités, leur implémentation et les résultats obtenus. Puis, nous concluons en faisant un bilan général du déroulement de notre projet et en présentant les améliorations et ajouts futurs possibles à notre outil.
