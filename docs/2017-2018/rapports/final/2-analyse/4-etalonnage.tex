\chapter[Étalonnage de l’appareil photographique]{Étalonnage de l’appareil\\photographique}

Lorsqu’un rayon lumineux traverse le sténopé d’un appareil photographique pour frapper son capteur, une projection centrale du point émetteur ou réflecteur de ce rayon est réalisée par rapport au sténopé, noté $O$, sur le plan de l’image.

L’appareil met ainsi en relation deux espaces projectifs, l’espace de la scène (ou espace des objets) et l’espace de l’image (ou espace du capteur). Ces deux espaces sont munis de deux repères $\mathcal{R}_o(^o\vec x, ^o\vec y, ^o\vec z, ^o\vec t)$ et $\mathcal{R}_s(^s\vec x, ^s\vec y, ^s\vec z)$, où $\mathcal{R}_s$ est centré sur le point principal de l’image.

Dès lors, l’appareil peut être vu comme une application projective $\Phi~: \mathbb{P}^4 \rightarrow \mathbb{P}^3$, qui, à tout point $^ox = (x, y, z, t)^T$ de la scène associe un unique point $^sx^\prime = (x, y, z)^T$ de l’image\footnote{Ce point est potentiellement hors du cadre de l’image s’il est hors du champ de vision de l’appareil.}. Cette application est entièrement déterminée par une matrice $P \in \mathcal{M}_{3,4}$ telle que

\begin{equation}
    \fctdef{\Phi}%
        {\mathbb{P}^3}{^ox}%
        {\mathbb{P}^2}{^sx^\prime = P \times ^ox.}
    \label{equ:position-cam-trans-def}
\end{equation}

Cette partie s’intéresse à la caractérisation des paramètres encodés dans cette matrice, et en particulier aux moyens de déterminer automatiquement certains de ces paramètres de façon semi-automatique, par une procédure appelée \emph{étalonnage.}

\section{Hypothèse des paramètres intrinsèques statiques et décomposition des paramètres de l’appareil}

Afin de pouvoir exprimer simplement cette transformation, un second repère de l’espace de la scène $\mathcal{R}_k(^k\vec x, ^k\vec y, ^k\vec z)$, appelé référentiel de l’appareil photo, est défini. Il est centré sur $O$, ses axes des abscisses et des ordonnées sont alignés avec ceux de $R_s$ et l’axe des cotes est l’axe $(pO)$. Ce choix des repère est illustré par la figure~\ref{fig:position-axes}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/camera-axes}
    \caption{\textbf{Exemple de disposition des repères} de la scène $\mathcal{R}_o$ en noir, de l’appareil photo $\mathcal{R}_k$ en rouge, et du capteur $\mathcal{R}_s$ en bleu.}
    \label{fig:position-axes}
\end{figure}

Muni de ce nouveau repère, la matrice de l’application projective $\Phi$ peut être décomposée en trois transformations~: le changement de repère pour placer le point de la scène dans le référentiel de l’appareil, puis la projection centrale par le point $O$, et enfin la prise en compte des paramètres internes à l’appareil photo~\parencite[chapitre~6, p.~156]{hartley-2004-multiple}.

Les deux premières transformations sont généralement regroupées dans une matrice de paramètres dits extrinsèques à l’appareil, qui encodent la pose de l’appareil par rapport à la scène, indépendamment de sa configuration interne. La matrice des paramètres extrinsèques est définie par \begin{equation}
    W := \begin{pmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 1 & 0 \\
    \end{pmatrix} \times \begin{pmatrix}
        1 & 0 & 0 & -^oO_x \\
        0 & 1 & 0 & -^oO_y \\
        0 & 0 & 1 & -^oO_z \\
        0 & 0 & 0 &  1  \\
    \end{pmatrix} \times \begin{pmatrix}
        r_{11} & r_{12} & r_{13} & 0 \\
        r_{21} & r_{22} & r_{23} & 0 \\
        r_{31} & r_{32} & r_{33} & 0 \\
          0    &   0    &   0    & 1
    \end{pmatrix} = \begin{pmatrix}
        r_{11} & r_{12} & r_{13} & -^oO_x \\
        r_{21} & r_{22} & r_{23} & -^oO_y \\
        r_{31} & r_{32} & r_{33} & -^oO_z \\
    \end{pmatrix},
\end{equation} qui correspond à la composition d’une rotation du repère $\mathcal{R}_o$, puis d’une translation pour déplacer le centre en $O$, et enfin d’une projection sur les axes $^kx$ et $^ky$. L’appareil photo étant mobile lors d’une prise d’un flux de photographies, cette matrice est variable.

Les paramètres intrinsèques font le lien entre le repère de l’appareil et l’espace du capteur. Ces paramètres sont le décalage du point principal $p$ par rapport au centre du repère du capteur $\mathcal{R}_s$, la distance focale $c$ et la déformation verticale $m$. Ils sont regroupés dans la matrice de paramètres intrinsèques définie par \begin{equation}
    K := \begin{pmatrix}
        1 & 0 & -^sp_x \\
        0 & 1 & -^sp_y \\
        0 & 0 & 1 \\
    \end{pmatrix} \times \begin{pmatrix}
        c & 0       & 0 \\
        0 & (c + m) & 0 \\
        0 & 0       & 1 \\
    \end{pmatrix} = \begin{pmatrix}
        c & 0       & -^sp_x \\
        0 & (c + m) & -^sp_y \\
        0 & 0       &  1  \\
    \end{pmatrix},
\end{equation} qui correspond à la composition d’une mise à l’échelle par un facteur $c$ selon $^kx$ et $c + m$ selon $^ky$, puis d’une translation par $p_x$ et $p_y$, coordonnées du point principal, afin de déplacer l’origine de l’image dans le coin inférieur gauche~\parencite[chapitre~6, p.~157]{hartley-2004-multiple}.

Les matrices de paramètres intrinsèques et extrinsèques forment une décomposition de la matrice de projection de l’appareil qui décrit l’application projective $\Phi$ définie en~(\ref{equ:position-cam-trans-def}), c’est-à-dire \begin{equation}
    P = K \times W.
\end{equation}

Lorsque la matrice $K$ des paramètres intrinsèques d’un appareil photo est connue, l’appareil est dit étalonné (\emph{calibrated} en anglais). Dans le cadre de ce projet, ces paramètres ne seront pas modifiés durant la capture du flux d’images. Il est donc pertinent de calculer en avance la matrice $K$ correspondant à ces paramètres.

\section{Procédure d’étalonnage automatique}

Pour étalonner un appareil photo linéaire, une technique consiste à imprimer sur une surface plane un motif régulier, asymétrique et très contrasté, puis à photographier ce motif sous plusieurs angles avec l’appareil~\parencite{zhang-2000-calibration}. Un exemple typique, illustré par la figure~\ref{fig:position-zhang-chessboard}, est un damier, dont les points d’intersection sont détectables par l’une des méthodes présentées dans la partie~\ref{part:feature-detection}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=.9]{figures/chessboard}
    \caption{\textbf{Utilisation d’un motif de damier pour étalonner un appareil photo.}}
    \label{fig:position-zhang-chessboard}
\end{figure}

Soit $m^\prime := (^sx_{m^\prime}, ^sy_{m^\prime}, 1) \in \mathbb{P}^2$ un point caractéristique d’une des photographies du motif. Le motif visualisé étant asymétrique et de dimensions connues, la position du point du motif associé à $m^\prime$ dans le référentiel de la scène peut être retrouvée, et est notée $m := (^ox_m, ^oy_m, ^oz_m, 1) \in \mathbb{P}^3$.

Sans perte de généralité\footnote{En effet, les paramètres intrinsèques de l’appareil restent les mêmes quelle que soit sa position dans l’espace de la scène, et la position de l’appareil au moment de la prise de la photo n’est pas recherché dans ce cas.}, le repère $R_o$ peut être placé à l’origine du motif, de telle sorte que tous les points caractéristiques de ce motif soient sur le plan $z = 0$ (cf. figure~\ref{fig:position-zhang-chessboard}). En particulier, $^oz_m = 0$ car $m$ est un point caractéristique du motif. Par la définition (\ref{equ:position-cam-trans-def}),

\begin{equation}
    \begin{aligned}
        m^\prime &= \Phi(m)\\
        \begin{pmatrix}
            ^sx_{m^\prime}\\
            ^sy_{m^\prime}\\
            1\\
        \end{pmatrix} &= \lambda \times K \times \begin{pmatrix}
            r_{11} & r_{12} & r_{13} & ^oO_x \\
            r_{21} & r_{22} & r_{23} & ^oO_y \\
            r_{31} & r_{32} & r_{33} & ^oO_z \\
        \end{pmatrix} \times \begin{pmatrix}
            ^ox_m\\
            ^oy_m\\
            0\\
            1\\
        \end{pmatrix}\\
        \begin{pmatrix}
            ^sx_{m^\prime}\\
            ^sy_{m^\prime}\\
            1\\
        \end{pmatrix} &= \lambda \times K \times \begin{pmatrix}
            r_{11} & r_{12} & ^oO_x \\
            r_{21} & r_{22} & ^oO_y \\
            r_{31} & r_{32} & ^oO_z \\
        \end{pmatrix} \times \begin{pmatrix}
            ^ox_m\\
            ^oy_m\\
            1\\
        \end{pmatrix},
    \end{aligned}
\end{equation}

où $\lambda ≠ 0$ est un scalaire quelconque, qui peut être ignoré car l’équation relie des points exprimés en coordonnées homogènes. Dans ce cas particulier, la transformation projective $\Phi$ est bijective puisqu’elle relie deux espaces de même dimension. La matrice $T$, définie par

\begin{equation}
    T := \lambda \times K \times \begin{pmatrix}
        r_{11} & r_{12} & ^oO_x \\
        r_{21} & r_{22} & ^oO_y \\
        r_{31} & r_{32} & ^oO_z \\
    \end{pmatrix},
\end{equation}

caractérise ainsi une homographie, et est donc inversible. \citeauthor{zhang-2000-calibration} remarque que les vecteurs $r_1 := (r_{11}, r_{21}, r_{31})^T$ et $r_2 := (r_{12}, r_{22}, r_{32})^T$ sont orthonormaux car ils sont issus d’une matrice de rotation, d’où $r_1 \times r_2 = 0$ et $\left|r_1\right| \times \left|r_2\right| = 1$. Les deux contraintes suivantes résultent

\begin{equation}
\begin{cases}
    t_1 \times K^{-T} \times K^{-1} \times t_2 = 0,\\
    t_1 \times K^{-T} \times K^{-1} \times t_1 -
        t_2 \times K^{-T} \times K^{-1} \times t_2 = 0
\end{cases}
\label{equ:pos-constr-zhang}
\end{equation}

Pour chaque image du motif, deux contraintes sont ainsi obtenues sur l’homographie de matrice $T \in \mathcal{M}_{3}(\mathbb{R})$. Comme $T$ est définie à un facteur scalaire près, elle a 8 degrés de liberté. Pour contraindre suffisamment la matrice et ainsi déterminer sa valeur, il faut au moins 4 images différentes du motif.

En pratique, plus d’images sont nécessaires pour obtenir des valeurs précises, et ces images ne donneront pas forcément des résultats cohérents sur les contraintes (\ref{equ:pos-constr-zhang}). Pour exploiter la sur-détermination de $T$, l’erreur sur ces contraintes peut être considérée comme une fonction objectif à minimiser.

\section{Implémentation de l’étalonnage}

\section{Présentation des résultats et discussion}

