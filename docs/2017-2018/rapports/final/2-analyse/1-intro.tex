\section{Objectifs du projet~: traitement des images, estimation de la trajectoire suivie par le drone et calcul d’un modèle~3D de la scène}
\label{chapter:intro}

Notre équipe s’est intéressée en particulier au problème de l’analyse de photographies prises par le drone lors d’un vol afin d’estimer sa trajectoire et de calculer un modèle~3D de la scène observée par celui-ci.

Plus précisément, notre projet a pour objectif le développement d’un outil acceptant en entrée une séquence d’images (ou flux d’images) capturées lors d’un vol du drone et produisant en sortie d’une part un modèle du site survolé proche de la réalité sous la forme d’un maillage de polygones texturisé (ou \emph{structure} de la scène) et d’autre part une estimation de la trajectoire suivie par l’appareil (ou \emph{motion} de l’appareil).

Dans un premier temps, d’octobre~2017 à janvier~2018, nous avons dressé l’état de l’art\footnote{Nous avons produit un premier rapport sur ces recherches en janvier~2018, qui est disponible à l’adresse \url{https://gitlab.com/ftornil/lucilia/blob/master/docs/rapports/biblio/2-analyse/biblio.pdf}.} sur ce problème, qui nous a permis d’identifier les problématiques afférentes~:

\begin{itemize}
    \item comment estimer la trajectoire suivie par l’appareil à partir d’un flux d’images 2D~?
    \item comment reconstruire un modèle 3D à partir d’un flux d’images 2D~?
    \item quelles informations sont nécessaires au calcul d’un modèle et à l’estimation de trajectoire~?
    \item quelles sont les méthodes existantes et que pouvons-nous leur apporter~?
    \item comment obtenir un indicateur de qualité d’un modèle calculé~?
    \item comment faire en sorte que le calcul d’un modèle ne soit pas trop long~?
\end{itemize}

\section{Organisation du projet et outils de travail}

Nous avons mené ce projet au cours de l’année universitaire, en parallèle avec nos autres enseignements et sous la supervision de \Mme~Hinde~Bouziane.
Le temps de travail s’est divisé en trois phases~:

\begin{enumerate}
\item \textbf{Recherche bibliographique.} Réalisation de recherches pour tenter d’identifier l’état de l’art sur le problème auquel nous nous intéressons et les techniques que nous pourrions mettre en place pour concevoir une solution.
\item \textbf{Modélisation du problème.} Conception d’un modèle et choix des structures de données adaptées pour répondre au problème.
\item \textbf{Implémentation de la solution choisie.} Création d’un outil pour répondre au problème initial en utilisant les techniques découvertes dans la phase de recherche et le modèle conçu.
\end{enumerate}

L’emploi du temps du premier semestre ne prévoyant malheureusement pas de temps pour le travail sur le projet, nous avons réalisé les recherches principalement sur notre temps libre durant les week-ends.
Au second semestre, nous avons pu travailler tous les jeudis après-midis au Laboratoire d’Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM)\footnote{Site web du laboratoire~: \url{http://www.lirmm.fr/}}. Le diagramme de Gantt, en annexe~\ref{appendix:gantt}, présente le découpage précis des tâches.

Tout au long du projet, nous nous sommes réunis de façon régulière avec notre encadrante, \Mme~Hinde~Bouziane, pour faire le point sur l’avancement du projet. Afin de garder les trois équipes de travail synchronisées, nous avons également organisé des réunions plénières tous les mois, réunissant toutes les équipes et leur encadrant-e.

Nous avons choisi d’utiliser le gestionnaire de versions décentralisé Git pour faciliter la collaboration sur le même code\footnote{Adresse du dépôt Git hébergé sur GitLab~: \url{https://gitlab.com/ftornil/lucilia}}. Le code a été rédigé sur les éditeurs Vim\footnote{Éditeur Vim~: \url{https://www.vim.org/}} et Atom\footnote{Éditeur Atom~: \url{https://atom.io/}}. Le logiciel de messagerie instantanée Telegram\footnote{Telegram~: \url{https://telegram.org/}} nous a permis de communiquer tout au long du projet avec les autres étudiant-e-s participant au projet.

Pour prendre des notes pendant les réunions, nous avons utilisé le langage Markdown\footnote{Markdown~: \url{https://daringfireball.net/projects/markdown}}, et pour rédiger les rapports, y compris celui-ci, nous avons fait usage du système de composition de documents {\LaTeX}\footnote{{\LaTeX}~: \url{https://www.latex-project.org/}}.
