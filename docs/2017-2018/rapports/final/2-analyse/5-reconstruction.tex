\chapter{Estimation des poses relatives de l’appareil et reconstruction d’un nuage de points}
\label{part:feature-correspondance}

\section{Traitement par paires successives d’images}

Le flux d’images utilisé dans le cadre de ce projet, et dans la plupart des situations concrètes de reconstruction, est constitué de photographies prises à intervalle régulier et court. Ainsi, deux images prises successivement dans le flux auront dans la majorité des cas des points en commun. Ces points communs peuvent être détectés et décrits par l’une des méthodes présentées en partie~\ref{chapter:feature-detection}. Pour mettre en correspondance des points caractéristiques de deux images, il suffit de comparer le descripteur des points de la première image avec ceux de la seconde. Deux points caractéristiques sont associés si leurs descripteurs sont proches, avec un certain seuil de tolérance. La figure~\ref{fig:pair-geometry} illustre un cas simplifié de point commun.

\section{Estimation de la pose relative de l’appareil photographique dans chaque paire}

\begin{figure}[htb]
    \centering
    \includegraphics{figures/pair-geometry}
    \caption{\textbf{Propriétés géométriques d’un point commun observé par deux appareils depuis deux positions différentes.} Le point commun $m$ peut être observé en $m^\prime$ sur la première image et en $m^\dprime$ sur la seconde. Les points $\epsilon^\prime$ et $\epsilon^\dprime$ sont appelés épipôles.}
    \label{fig:pair-geometry}
\end{figure}

La principale propriété géométrique exploitable dans ce contexte de point caractéristique commun est la coplanarité des points $m$, $O^\prime$, $m^\prime$, $\epsilon^\prime$, $O^\dprime$, $m^\dprime$ et $\epsilon^\dprime$. Au moins huit points caractéristiques distincts correspondants entre les deux images sont nécessaires pour imposer suffisamment de contraintes sur la matrice de paramètres extrinsèques $W$~\parencite[chapitre~11, p.~281]{hartley-2004-multiple}.

Malgré la détermination de contraintes sur la matrice $W$, il subsiste un dernier degré de liberté inconnu~: l’échelle. En effet, à partir de deux images et d’un ensemble de points correspondants, le second appareil peut être positionné sur une ligne, mais le point de cette ligne correspondant à la réalité ne peut pas être caractérisé. Cette ambiguïté d’échelle est illustrée par la figure~\ref{fig:pair-ambiguity}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/pair-ambiguity}
    \caption{\textbf{Ambiguïté d’échelle lors du positionnement relatif de l’appareil photo $O^\dprime$.} Ici, les positions $O^\dprime$ et $O^\trprime$ sont toutes deux des solutions valides pour les contraintes exprimées.}
    \label{fig:pair-ambiguity}
\end{figure}

Cette ambiguïté peut être résolue en plaçant dans la zone observée par l’appareil photo des points caractéristiques dont la localisation absolue dans l’espace est connue à l’avance. Une fois les matrices de paramètres extrinsèques et intrinsèques connues, la position de chaque point caractéristique visible peut être retrouvée par triangulation~\parencite[chapitre~9, p.~257]{hartley-2004-multiple}.

\section{Élimination des correspondances aberrantes avec la méthode RANSAC~\emph{(Random Sample Consensus)}}

La comparaison naïve des descripteurs a pour inconvénient de générer des correspondances aberrantes, notamment entre différentes parties d’un motif qui se répète.

En supposant que ces correspondances aberrantes soient minoritaires, la méthode de consensus par échantillonnage aléatoire (ou \emph{RANdom Sample Consensus,} RANSAC) peut être utilisée~\parencite{fischler-1981-ransac}. Cette méthode permet de résoudre de façon générale les problèmes de valeurs aberrantes.

Cette méthode est basé sur une méthode d'essai et d'approche de l'erreur, c'est à dire qu'elle consiste à trouver le meilleur ensemble de points pour lesquels le calcul d'un modèle est correct. Pour cela, elle génère un nombre important de possibilités qui sont évaluées en calculant le nombre de points validant le modèle proposé à partir d'un sous ensemble de points. Concrètement, cet algorithme consiste à :

\begin{enumerate}
    \item Choisir aléatoirement un ensemble minimal de points parmi ceux détectés précédemment, quel que soit le(s) algorithme(s) utilisé(s).
    \item Calculer un modèle à partir de ce sous ensemble.
    \item Attribuer une note au modèle ainsi calculé en fonction du nombre de points (autres que ceux choisis aléatoirement dans la première phase) validant le modèle en acceptant un certain taux d'erreur,
    \item Recommencer les étapes 1 à 3 un certain nombre de fois et selectionner le modèle ayant la meilleure note parmi ceux calculés.
\end{enumerate}

La figure \ref{fig:detection-feature-match} présente un exemple d'application de cet algorithme, en utilisant un modèle simplifié basé sur une simple translation de l'image, qui permet de détecter et éliminer les correspondances aberrantes.

\begin{figure}[htb]
    \centering
    \begin{tikzpicture}[
        match/.style={
            yellow,
            fill,
            very thick
        },
        outlier/.style={
            match,
            thick,
            red
        }
    ]
        \node[inner sep=0pt] (before)
            {\includegraphics[scale=.25]{figures/feature-match-before}};
        \node[inner sep=0pt, below=.5cm of before] (after)
            {\includegraphics[scale=.25]{figures/feature-match-after}};

        \begin{scope}[shift={(0, -3.16)}]
            % Correspondances aberrantes
            \draw[outlier] (-4.2, -.275) circle (.05) -- (.45, -1.65) circle (.05);
            \draw[outlier] (-6.5, -1.6) circle (.05) -- (5.9, -1) circle (.05);
            \draw[outlier] (-5.34, -4.1) circle (.05) -- (3.05, -3.8) circle (.05);

            % Correspondances réelles
            \draw[match] (-3.3, -.9) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.35, -1.15) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.32, -1.72) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.35, -3.4) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-4, -4.8) circle (.05) -- ++(4.6, 0.35) circle (.05);
        \end{scope}
    \end{tikzpicture}
    \caption{\textbf{Mise en correspondance de points caractéristiques entre deux images.} Les deux images, à gauche et à droite, sont des portions d’une plus grande photographie de la Cour Carrée du Louvre, prise par l’utilisateur \emph{KingOfHearts} de Wikimédia et distribuée sous licence CC-BY-SA. En haut, la paire d’images originelle. En bas, la même paire en niveaux de gris sur laquelle ont été repérées des paires de points caractéristiques détectés comme correspondants par l’algorithme SIFT. En jaune, les correspondances validant le modèle et en rouge les correspondances aberrantes éliminées par l’algorithme~RANSAC.}
    \label{fig:detection-feature-match}
\end{figure}

Dans un premier temps la taille de l'ensemble minimal à choisir aléatoirement est déterminée. Cet ensemble doit permettre de calculer un modèle à lui seul. Le nombre de point est donc déterminé par le nombre de points nécessaires au calcul du modèle. Il est préférable que ce modèle utilise le moins de points possibles, tout en permettant de vérifier la conformité (ou non) des différents points à ce modèle. Le modèle utilisé dans le cadre de ce projet n'est pas celui d'une simple translation, car la précision obtenu ne serait pas suffisante et son calcul ne serait pas toujours possible, mais celui du positionnement relatif du point de vue de chaque image, décrit précédemment et dont le calcul est possible en choisissant uniquement 8 points.

Enfin, il reste à déterminer combien d'essais sont nécessaires, c'est à dire, combien de fois l'étape 4 est-elle répétée. De façon optimale, il faudrait calculer un modèle à partir de tous les sous-ensembles possibles afin d'être sûr d'obtenir le modèle avec la meilleure note et éliminer toutes les valeurs aberrantes.

En pratique et afin de réduire le temps de calcul, cet algorithme se limite à un certain pourcentage de confiance, ce qui fait qu'il est probabiliste. Les équations~\ref{equ:ransac-proba-ft} et~\ref{equ:ransac-essais-fp} permettant de calculer le nombre d’essais sont obtenues en tirant aléatoirement, et avec remise après chaque essai, les points et en notant :

\begin{itemize}
    \item $s$~: le nombre de points nécessaires au calcul du modèle~;
    \item $e$~: la proportion de valeurs aberrantes~;
    \item $T$~: le nombre d’essais à effectuer~;
    \item $p$~: la probabilité de sélectionner au moins une valeur aberrante dans chacun des $T$ essais.
\end{itemize}

\begin{equation}
    1-p = (1-(1-e)^s)^T
    \label{equ:ransac-proba-ft}
\end{equation}
\begin{equation}
    T = \frac{\log(1-p)}{\log(1-(1-e)^s)}
    \label{equ:ransac-essais-fp}
\end{equation}

On remarque donc que la valeur la plus importante à réduire afin de limiter le nombre d'essai est le nombre de points nécessaires au calcul du modèle ($s$). Dans le cadre de ce projet, en supposant un ratio de valeurs aberrantes de $30\ \%$ (il est préférable de surestimer cette valeur afin d’éviter tout problème par la suite) et avec une probabilité de succès fixée à $99,9\ \%$, 78~essais sont à réaliser. Il est possible de réduire cette première estimation en donnant par exemple une meilleure approche du ratio de valeurs aberrantes, qu'il est possible d'obtenir plus précisément en faisant des essais sur des images à disposition.

\section{Triangulation des points caractéristiques}

TODO.

La partie précédente a développé différentes méthodes permettant d’identifier des points caractéristiques de façon individuelle dans des images d’une scène, puis de mettre en correspondance des points des images liés à un même point de cette scène.

Pour obtenir un modèle 3D complet d’une scène, ses points caractéristiques doivent être positionnés dans son espace. Cela revient à inverser le processus de formation des images de la scène dans le but de retrouver les informations perdues, c’est-à-dire la profondeur de chaque point caractéristique de la scène. Pour comprendre quelles approches permettent de réaliser cette inversion, il est nécessaire de présenter le processus de formation des images en lui-même.

\section{Implémentation}

TODO

\section{Présentation des résultats et discussion}

En conclusion, la détection de points caractéristiques dans un ensemble d’images d’une scène en vue de leur mise en correspondance est un processus décomposé dans la littérature en trois phases. Dans un premier temps, les points caractéristiques sont détectés individuellement dans chaque image. Cette détection peut être faite par l’algorithme SIFT ou SURF.

Par la suite, un descripteur est calculé pour chaque point caractéristique, de telle sorte que deux points équivalents de la scène résultent en des descripteurs numériquement très proches, et inversement que deux points différents résultent en des descripteurs très différents. Nous avons étudié dans cette partie deux descripteurs~: SIFT et SURF.

Enfin, les points caractéristiques sont mis en correspondance par simple comparaison de leurs composantes. Cette mise en correspondance génère souvent des ensembles aberrants de points caractéristiques, et les résultats doivent donc être filtrés par une méthode telle que RANSAC avant d’être exploitables pour, par exemple, la reconstruction d’un modèle 3D d’une scène.

Les expérimentations réalisées par~\cite{juan-2010-comparaison}, indiquent que l’algorithme SURF est en moyenne trois fois plus rapide que l’algorithme SIFT pour la détection et la description des points caractéristiques. Cependant, l’étude de~\cite{panchal-2013-comparaison}, montre que l'algorithme SIFT donne des résultats plus cohérents que SURF, notamment dans le cas où les images présentent de forts changements de luminosité où d’angle de vue.

Un des objectifs de notre projet est de minimiser le plus possible le temps de calcul du modèle. Par ailleurs, après concertation avec l’équipe chargée de récolter les images, ces dernières ne devraient pas présenter de forts changements lumineux ou d’angle de vue. De ce fait, les problèmes de cohérence des résultats de SURF sur ces cas particuliers peuvent être négligés. Nous avons ainsi décidé, pour la suite du projet, de nous baser sur l’algorithme SURF pour détecter, décrire et mettre en correspondance les points caractéristiques des images en vue de la reconstruction d’un modèle 3D de la scène observée.

Pour conclure, les points caractéristiques détectés avec l’un des algorithmes de la partie~\ref{part:feature-detection} ont, comme tout autre point d’une image formée par un appareil photo, été projetées par celui-ci à travers son objectif. Dans le contexte de la photogrammétrie, ce modèle de projection peut être simplifié par le modèle du sténopé, qui permet de dériver deux matrices de paramètres qui caractérisent de façon unique l’appareil utilisé~: les matrices de paramètres intrinsèques~$K$ et extrinsèques~$W$. Pour pouvoir estimer la position d’un point caractéristique observé dans l’espace de la scène, ces deux matrices doivent auparavant être déterminées.

Pour étalonner un appareil photo et ainsi déterminer ses paramètres intrinsèques, un motif plan et régulier peut être utilisé. Pour déterminer la position relative d’un appareil par rapport à un autre, les propriétés géométriques des points caractéristiques correspondants peuvent être exploitées. Enfin, une fois les matrices de paramètres de chaque appareil photo connues, la position de chaque point caractéristique peut être obtenue par triangulation.

Des recherches complémentaires sont nécessaires pour déterminer comment éviter l’accumulation d’erreurs de mesures si tous les points et les appareils photos sont positionnés les uns relativement aux autres. Par ailleurs, la méthode présentée dans cette partie permet d’obtenir un nuage de points, et non pas un maillage texturisé, comme requis par l’équipe réalisant le rendu du modèle~3D.

Enfin, dans cette partie, l’utilisation d’un appareil photo linéaire a été supposée. En pratique, de nombreux appareils produisent des photographies contenant des distorsions non-linéaires qu’il est nécessaire de corriger avant de pouvoir réaliser les calculs présentés précédemment. Nous devrons donc nous intéresser à ces problématiques dans la suite du projet.
