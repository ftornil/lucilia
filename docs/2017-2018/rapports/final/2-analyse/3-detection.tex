\chapter{Détection et description de points caractéristiques dans des images}
\label{chapter:feature-detection}

Un point est dit «~caractéristique~» de la scène s’il peut être repéré dans plusieurs images de façon unique. Il n’est pas suffisant de repérer un tel point par la seule valeur d’intensité d’un pixel sur une image. En effet, ceux-ci sont généralement limités à une gamme de 256~valeurs, et il est donc certain que, dans des images contenant plus de 256~pixels, au moins deux pixels partagent la même valeur.

L’approche adoptée est de considérer un voisinage d’une taille fixe autour du point, et d’utiliser les valeurs des pixels de ce voisinage pour l’identifier. En effet, le même point aura probablement un voisinage similaire depuis deux points de vue proches. Tout voisinage d’un point dans une image peut avoir l’une des trois structures géométriques illustrées dans la figure~\ref{fig:feature-types}~\parencite[chapitre~4, p.~209]{szeliski-2011-computervision}.

\begin{figure}[htb]
    \centering
    \includegraphics{3-detection/feature-types}
    \caption{\textbf{Trois structures géométriques possibles pour le voisinage d’un point dans une image.} Image adaptée d’une photographie d’Ildar Sagdejev sous licence CC-BY-SA.}
    \label{fig:feature-types}
\end{figure}

\begin{description}
    \item[Surface~:] voisinage monotone, dans lequel tous les pixels ont une valeur proche.
    \item[Bordure~:] ligne à fort contraste séparant deux surfaces.
    \item[Angle~:] point de rencontre d’au moins trois surfaces.
\end{description}

Parmi ces trois structures possibles, seuls les angles sont uniques dans leur voisinage~: ce sont les points caractéristiques. Dès lors, le problème de détection des points caractéristiques d’une scène dans une image se ramène à celui de la détection d’angles.

Cette partie présente l’idée générale de détection et description des points caractéristiques dans une image ainsi que l’implémentation que nous avons réalisée et les résultats obtenus.

\section{Algorithmes SIFT et SURF~: détection de points caractéristiques dans une image}

L’algorithme de détection de points caractéristiques invariants par mise à l’échelle (ou \emph{Scale-Invariant Feature Transform}, SIFT) a été introduit par~\cite{lowe-2004-sift}. Cette heuristique consiste à rechercher dans une image les points dont la variation de couleur est un extremum local, et à les considérer comme étant des points caractéristiques de l’image.

Pour effectuer cette recherche de façon efficace, l’image originelle est lissée par un flou gaussien de plus en plus fort, et son échelle est réduite petit à petit. Les images de même taille sont regroupées dans un octave\footnote{Qui est appelé octave, même lorsqu’il ne contient pas exactement huit images.}, au sein duquel l’intensité de lissage augmente progressivement. Enfin, entre chaque niveau de lissage, la différence des deux images est calculée pixel par pixel~\parencite[p.~6]{lowe-2004-sift}. La figure~\ref{fig:sift-detection-example} donne un exemple d’application de ce procédé sur une photographie.

\begin{figure}[h!]
    \centering
    \makebox[\textwidth][c]{
        \includegraphics{3-detection/sift-detection-example}
    }
    \caption{\textbf{Exemple de déroulement de l’algorithme SIFT sur une photographie de la sculpture «~Hommage à Confucius~» d’Alain Jacquet.} Sur cet exemple, deux octaves avec trois niveaux de lissage sont utilisés.}
    \label{fig:sift-detection-example}
\end{figure}

Le flou gaussien a la propriété de laisser inchangées les surfaces de couleur uniforme et, au contraire, de modifier fortement les zones de bordure. Ainsi, en faisant la différence entre chaque niveau de lissage, les surfaces uniformes sont éliminées, et seuls les bordures et les angles sont conservés. Par ailleurs, la réduction progressive d’échelle de l’image permet de détecter des détails de la scène proches ou éloignés de l’appareil photo, sur les différents octaves.

L’algorithme parcourt finalement toutes les images résultant de la différenciation pour sélectionner les points qui ont une intensité supérieure à tous leurs voisins. Les voisins considérés sont les 8 dans l’image actuelle, les 9 dans la différence précédente et les 9 dans la différence suivante, s’ils existent. Les points qui satisfont ce critère sont des extremums locaux et sont candidats pour être des points caractéristiques~\parencite[p.~7]{lowe-2004-sift}.

Les paramètres de l’algorithme SIFT sont l’écart type du flou gaussien appliqué à chaque étape (également appelé $\sigma$), le nombre de niveaux de lissage par octave et le nombre d’octaves. Les expérimentations de~\citeauthor{lowe-2004-sift} montrent que de bons résultats sont généralement obtenus avec $\sigma = 1,6$, 3~niveaux de lissage et 4~octaves~\parencite[p.~8 et~10]{lowe-2004-sift}, mais que ces paramètres doivent être ajustés empiriquement pour chaque cas d’utilisation.

\cite{bay-2008-surf}, ont proposé une modification de l’algorithme SIFT, baptisée «~caractéristiques robustes accélérées~» (ou \emph{Speeded Up Robust Features,} SURF), qui rend le calcul des points caractéristiques plus rapide en ne calculant pas un lissage gaussien à chaque étape, mais en appliquant plutôt la fonction carré \begin{equation}
    S(x,y)=\sum _{i=0}^{x}\sum _{j=0}^{y}I(i,j).
    \label{equ:surf-square-filter}
\end{equation}

La matrice hessienne, dont la formule est donnée en \ref{equ:surf-determinant-hessian} est ensuite calculée. Les points pour lesquels cette valeur dépasse un certain seuil sont détectés comme étant des points caractéristiques de l'image.

\begin{equation}
    H(p,\sigma )=
        \begin{pmatrix}
            L_{xx}(p,\sigma )&L_{xy}(p,\sigma )\\
            L_{yx}(p,\sigma )&L_{yy}(p,\sigma )
        \end{pmatrix}
    \label{equ:surf-determinant-hessian}
\end{equation} où $L_{xy}(p,\sigma )$ correspond au pixel aux coordonnées $(x, y)$ de l'image $p$ obtenue en appliquant la fonction carré à l'image de départ et $\sigma$ représente la valeur de $S(x,y)$ pour ce point.

Ainsi, la transformation peut être précalculée dans une image d'entiers (ou \emph{«~integer image~»}), c’est-à-dire une image dans laquelle la valeur de chaque pixel est déterminée en fonction de celle des pixels alentour, permettant ainsi d’effectuer le calcul souhaité en temps constant au lieu de~$O(n^2)$~\parencite{cozzi-2011-sat}.

\section{Problème de l’association des points caractéristiques similaires et description du voisinage des points}

L’objectif de la détection de points caractéristiques est de pouvoir mettre en correspondance ces points entre différentes photographies d’un même objet prises depuis plusieurs points de vue. Pour ce faire, il faut disposer d’un indicateur de similarité entre deux voisinages qui permette de sélectionner les voisinages équivalents dans deux images~\parencite[p.~222]{szeliski-2011-computervision}. Entre ces deux images, la luminosité, l’orientation ou l’échelle peuvent avoir changé~: il est nécessaire que l’indicateur y soit invariant.

Le descripteur SIFT~\parencite[p.~2]{lowe-2004-sift} fournit un tel indicateur. Chaque point caractéristique est paramétré par \begin{equation}
    \left< (x, y), \sigma, \theta, f\right >,
\end{equation}
où $(x, y)$ est la position du pixel central dans l’image, $\sigma$ est l’écart-type du flou gaussien utilisé pour détecter le point considéré, $\theta$ est l’orientation du point et $f$ est un vecteur décrivant le voisinage du point. Le voisinage d’un point caractéristique est un carré de $16 \times 16$ pixels autour du pixel central. Il est divisé en seize blocs de $4 \times 4$ pixels.

Le gradient de l’image est évalué pour chaque point du voisinage selon les fonctions approchées \begin{equation}
    \begin{cases}
        m\left(x,y\right) &= {\sqrt {\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)^{2}+\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)^{2}}}\\
        \theta\left(x,y\right) &= {\mathrm{tan}^{-1}}\left(\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)/\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)\right),
    \end{cases}
\end{equation} donnant respectivement la norme et la direction du vecteur gradient d’un pixel situé aux coordonnées $(x, y)$ de l’image. Dans ces équations, $L\left(x,y\right)$ désigne le pixel $(x, y)$ de l’image obtenue en appliquant un lissage gaussien d’écart-type $\sigma$ sur l’image originelle, et $\mathrm{tan}^{-1}$ désigne la fonction arc tangente.

Pour chaque bloc du voisinage, un histogramme des gradients des points qu’il contient est calculé. Celui-ci est discrétisé sur 8~directions possibles. Chaque vecteur gradient du bloc considéré contribue à l’histogramme de façon proportionnelle à sa norme et inversement proportionnelle à sa distance au point d’intérêt~\parencite[p.~13]{lowe-2004-sift}. Enfin, les normes de chaque vecteur de l’histogramme de chaque zone sont stockées dans le vecteur $f$ de dimension $16 \times 8 = 128$, qui est le descripteur du point. La figure~\ref{fig:sift-feature-description} illustre ce processus.

\begin{figure}[htb]
    \centering
    \includegraphics{3-detection/sift-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SIFT d’un point caractéristique d’une image.} Le gradient de chaque pixel dans le voisinage du point est évalué, puis un histogramme est calculé pour chaque bloc de taille $4 \times 4$. L’ensemble des valeurs des histogrammes constitue le descripteur. Ici, seulement $8 \times 8$ pixels du voisinage sont illustrés par souci de clarté.}
    \label{fig:sift-feature-description}
\end{figure}

Les descripteurs calculés par l’algorithme SIFT sont normalisés et donc invariants par changement de luminosité. Ils sont uniquement basés sur le motif de variation d’intensité autour des points d’intérêt et sont donc invariants par rotation ou changement d’échelle~\parencite[p.~16]{lowe-2004-sift}.

\cite{bay-2008-surf}, ont également proposé une modification du descripteur SIFT~: le descripteur SURF, dont le calcul est effectué plus rapidement en utilisant l'ondelette de Haar ou \emph{«~Haar wavelet~»}, décrite par la formule \ref{equ:haar-wavelet-def}~\parencite{cheng-2011-haar}. Cette fonction permet de détecter des changements brutaux dans les valeurs. Dans le cadre de l'algorithme SURF, ces valeurs correspondent aux valeurs des pixels dans un cercle alentour au point détecté, en $x$ et en $y$.

\begin{equation}
    \psi (t)=
        \begin{cases}
            1\quad &0\leq t<{\frac {1}{2}},\\
            -1&{\frac {1}{2}}\leq t<1,\\
            0&{\mbox{sinon.}}
        \end{cases}
    \label{equ:haar-wavelet-def}
\end{equation}

Pour déterminer l'orientation générale d'un point, la somme des valeurs est effectuée et la direction générale indiquée est affectée à la direction du point. La description du voisinage est quand à elle effectuée en calculant la somme du résultat de l'ondelette de Haar dans des sous-régions autour du point, comme illustré dans l'image \ref{fig:surf-feature-description}.

\begin{figure}[htb]
    \centering
    \includegraphics{3-detection/surf-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SURF d’un point caractéristique d’une image.} Au lieu de calculer un histogramme des directions de gradient possible dans chaque bloc, la somme de tous les vecteurs d’un bloc est choisie comme étant son représentant.}
    \label{fig:surf-feature-description}
\end{figure}

\section{Implémentation de la détection des points caractéristiques et de la description de leur voisinage}

La bibliothèque OpenCV fournit une implémentation pour un grand nombre de détecteurs et de descripteurs de points caractéristiques, y compris SIFT et SURF. L’interface \texttt{cv::Feature2D} définit de façon générique les méthodes qui doivent être implémentées par ces algorithmes, notamment la méthode \texttt{cv::Feature2D::detectAndCompute} qui détecte et décrit des points dans une image quelconque. Chaque algorithme est implémenté dans une classe séparée qui réalise cette interface. L’avantage de cette architecture est qu’elle permet de remplacer aisément l’algorithme utilisé, et d’observer les différences autant en quantité de points détectés qu’en qualité.

Lors de l’analyse des options passées à l’outil (cf. annexe~\ref{appendix:manual}), une instance de la classe dans laquelle est implémentée l’algorithme demandé par l’utilisateur est créée, avec les valeurs des paramètres demandés. La méthode \texttt{Capture::detectFeatures} reçoit cette instance et n’a plus qu’à invoquer sa méthode de détection et de description.

\section{Présentation des résultats et discussion}

Les résultats qualitatifs obtenus sur des images de test sont présentés dans la figure~\ref{fig:sift-surf-qual-results}. Sur ces exemples, l’algorithme SURF a détecté plus de points caractéristiques que l’algorithme SIFT. Il a cependant répondu à des voisinages pas toujours caractéristiques, notamment sur le cube où il y a plus de points situés sur des surfaces uniformes que dans le résultat de l’algorithme SIFT.

\begin{figure}[htb]
\begin{minipage}{.485\textwidth}
    \includegraphics[width=\textwidth]{3-detection/results-img-1-sift-features}\\[.05\textwidth]
    \includegraphics[width=\textwidth]{3-detection/results-img-2-sift-features}
\end{minipage}\hfill%
\begin{minipage}{.485\textwidth}
    \includegraphics[width=\textwidth]{3-detection/results-img-1-surf-features}\\[.05\textwidth]
    \includegraphics[width=\textwidth]{3-detection/results-img-2-surf-features}
\end{minipage}\hfill%
    \caption{\textbf{Résultats de l’exécution des algorithmes de détection} SIFT~(à gauche) et SURF~(à droite) sur deux images.}
    \label{fig:sift-surf-qual-results}
\end{figure}

Dans cette partie, deux algorithmes de détection et de description de points caractéristiques dans des images ont été abordés. Les algorithmes SIFT et SURF sont parmi les plus utilisés pour le problème \emph{«~structure from motion~»}, mais il en existe d’autres, tels que \emph{Features from Accelerated Segment Test} (FAST), \emph{Good Features to Track} (GFTT), \emph{Gradient Location and Orientation Histogram} (GLOH) ou le détecteur de Harris-Laplace. Nous n’avons pas eu le temps de les étudier dans notre projet, mais il serait intéressant de comparer leur efficacité avec SIFT et SURF.

Afin de pouvoir estimer la trajectoire suivie par l’appareil lors du vol, il est nécessaire de connaître les paramètres géométriques de l’objectif utilisé. Les algorithmes de détection de points caractéristiques peuvent être utilisés pour  construire une procédure de calcul semi-automatique ces paramètres.
