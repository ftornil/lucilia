\section{Récolte des données}

\subsection{Matériel}

Notre projet étant de modéliser tridimensionnellement un environnement à l'aide d'un drone, les prises de vues relatives à cet environnement sont la base de notre travail. Ainsi la caméra du drone choisi doit être précise ; nous décidons donc de limiter nos recherches de drone à ceux ayant un \textbf{appareil photo de douze méga pixels} au moins. De plus, grâce au drone nous avons la possibilité de travailler soit avec des photographies soit avec des vidéos : nous avons choisis de travailler à partir de \textbf{photographies} de manière à ce que \emph{chacun des clichés pris dispose des informations facilitant le travail de traitement des images} en aval. Ces informations sont en fait des \emph{données géo-spatiales} ; notre drone doit alors posséder un \textbf{GPS} et un \textbf{accéléromètre}. Également, la qualité des photographies prises par le drone dépend notamment du format d'image pris en charge par son appareil photo. Nous voulons éviter le format destructeur JPEG autant que possible et souhaitons donc que le drone propose un \textbf{autre format}. De plus, lors de la récolte des données le drone devra effectuer \emph{plusieurs rotations} autour de sa cible (l'environnement à représenter) dans le but de recueillir suffisamment de clichés pour fournir aux membres en aval matière à travailler. Ainsi l'autonomie de notre drone doit être suffisante pour accomplir son travail, nous considérons qu'il doit avoir une \textbf{autonomie d'au moins quinze minutes} pour récupèrer une moyenne d'au moins X clichés. Par ailleurs, une des ambitions de notre projet est de réaliser un modèle 3D de l'environnement survolé en drone pendant que ce dernier est en vol en train de recueillir les prises de vue. Afin que cette idée puisse être réalisée, notre drone doit être en mesure de "communiquer" avec l'extérieur, c'est pourquoi un \textbf{module Wi-Fi} lui est indispensable. Enfin, comme nous souhaitons que le drone accomplisse sa trajectoire et ses prises de vues de manière autonome, il faut qu'il puisse être \textbf{programmable}.

Cette analyse préalable de nos besoins nous a permis de cibler quelques drones pouvant correspondre à nos attentes : le \emph{Yunneec Typhoon Q500 4k}, le \emph{DJI Spark}, le \emph{DJI Phantom 3 Standard} et le \emph{DJI Phantom} 3 SE. Après avoir consulté nos encadrants et leurs avoir présenté chacun des modèles étudiés, nous avons choisi le drone \textbf{Phantom SE} de \emph{DJI}. Ce drone correspond bien à nos attentes : un appareil photo de douze méga pixels disposant du format d'image DNG (Digital Negative), un GPS, un accéléromètre, la possibilité d’interagir en Wi-Fi et une autonomie supérieure à vingt minutes. De plus, la série Phantom de DJI dispose d'un kit de développement nous permettant de programmer le drone comme souhaité. Par ailleurs, son prix de 649,99 euros reste raisonnable et est en accord avec le budget du département informatique (ou LIRMM ou CNRS).

\subsubsection{Mentions légales}

La législation autour des drones est d'autant plus complexe qu'elle est imprécise. Les questions juridiques relatives aux drones sont relativement nouvelles. Nous nous sommes renseignés auprès de plusieurs organismes (Direction Générale de l'Aviation Civile, préfecture de l'Hérault, présidence de l'Université de Montpellier...) dans le but d'\textbf{obtenir une autorisation de vol}. À ce jour nous attendons encore des réponses. 

Cependant, nos recherches nous ont permis d'obtenir plusieurs informations. La législation française considère pour le cadre d'utilisation qui nous intéresse (aéromodélisme) deux catégories de drones :

\begin{itemize}
\item \textbf{Catégorie A} : le robot fait moins de vingt-cinq kilos ; non motorisé ou avec un seul type de propulsion ; capacité à reprendre le contrôle manuel à tout moment (même en vol automatique ou seulement en cas d'urgence si le drone a une masse inférieure à deux kilogrammes) ; utilisation dans des conditions de sécurité normales ; le pilote ne se trouve pas dans un véhicule en déplacement ; aucune charge n'est larguée.
\item \textbf{Catégorie B} : les drones ne correspondant pas aux critères de la catégorie A ; uniquement sur autorisations particulières.
\end{itemize}

Ainsi notre drone est rentre dans la \textbf{catégorie A}. Par ailleurs, certaines restrictions s'appliquent sur le vol de drone tel que l'obligation d'avoir le \emph{vol en vue du télépilote} (ou d'une seconde personne) ou bien l'\emph{interdiction de vol au dessus de l'espace public sans autorisation préfectorale}. La législation classifie également des scénarios de vols.

\begin{itemize}
\item \textbf{S-1} : hors zone peuplée, à 200 mètres et en vue du télépilote.
\item \textbf{S-2} : hors zone peuplée, sans survol de tiers et à une distance horizontale maximale de rayon d'un kilomètre du télépilote.
\item \textbf{S-3} : en zone peuplée, sans survol de tiers, à 100 mètres et en vue du télépilote.
\item \textbf{S-4} : à au moins cinquante mètres de rassemblement de personnes et n'entrant pas dans les scénarios 1 et 2.
\end{itemize}

Notre projet s'inscrit donc dans le \textbf{scénario 4}.

\subsection{Collecte des données}

\subsubsection{Trajectoire}

Pour l'instant nous envisageons de programmer le drone de manière à ce qu'il effectue des rotations autour de sa cible de façon indépendante. Ainsi le drone réalise par lui même (en suivant son algorithme) les nombreux clichés dont nous avons besoin et ne s'arrête qu'une fois sa trajectoire complétée.

Plus tard nous pourrions envisager d'effectuer une première rotation afin d'obtenir un premier jeu de clichés, envoyé au programme de modélisation conçu par le binôme 2. Ces premières photographies sont instantanément analysées et le drone reçoit en retour une ébauche du modèle conçu. Notre appareil utilise ainsi le modèle pour se déplacer dans son environnement en temps réel et réaliser les autres photographies.

\subsubsection{Programmation du drone}

La série \emph{Phantom de DJI} dispose d'un kit de développement nous permettant de \emph{créer notre propre application} afin de programmer notre drone : le \textbf{Mobile SDK}. Ceci, couplé à l'\textbf{UI Library de DJI} nous permettra de créer notre propre application Android afin de contrôler le drone comme nous l'entendons, tant sa trajectoire que de l'utilisation des données récoltées. Par ailleurs, nous disposerons de la documentation officielle nous permettant de partir sur des bases solides.

Le Mobile SDK nous permet de \textbf{contrôler différents composants} du drone dans notre programme : 

\begin{itemize}
\item \textbf{L'appareil} : Nous pouvons contrôler les actions du drone (allumer son moteur, s'envoler, atterir), son style de vol ou son altitude.
\item \textbf{La caméra} : de même, nous pouvons contrôler la position de la caméra, son inclinaison, son mode (photographie ou vidéo) et même sa lentille (zoom, focus...)
\item \textbf{Les capteurs} : nous avons accès aux divers capteurs du drone (GPS, altitude, baromètre et d'autres).
\end{itemize}

L'UI Library quand à elle nous permet de réaliser l'\textbf{interface graphique} de notre application. En résumé, le Mobile SDK s'occupe du \textbf{fond} de l'application (ce que va faire le drone et comment) alors que l'UI Library est la \textbf{forme}, ce à quoi l'application ressemblera.

\subsection{Stockage des données}

\subsubsection{Méthode : récolte et modélisation séparées}

%---------------------------PARTIE A RETIRER ET SUITE A IMPLEMENTER----------------
\textbf{N.B. : la partie qui suit mériterait d'être un peu plus étoffée / précisée en discutant avec le binome 2.}
%-----------------------------------FIN--------------------------------

Les données obtenues sont des photographies au format Digital Negative (DNG) couplées avec des informations géo-spatiales : coordonnées GPS relatant la position où était le drone au moment de la photographie ; altitude du drone ; inclinaison du drone et de la caméra . Toutes ces données sont capitales pour que les équipes en aval puissent réaliser au mieux leur travail.
%On stockera les données sous forme de tableau (A CHECK) que l'on donnera ensuite ... Comment ? on regroupe dans un fichier qu'on donne / envoie au B2? objet binaire ? Idk, A VOIR.

\subsubsection{Méthode : récolte et modélisation simultanée}

Les données stockées à envoyer sont les mêmes, avec cette méthode ce qui diffère c'est la façon dont les données sont transmises. On peut imaginer que le drone stocke une dizaine de photographies / données géo-spatiales avant de les envoyer en Wi-Fi au programme du binôme 2 ; ou d'envoyer les données une à une...

Par ailleurs, ici notre drone recevra également des données qu'il devra traiter : la modélisation partielle issue des premiers clichés.