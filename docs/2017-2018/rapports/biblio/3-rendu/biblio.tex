\input{../preamble.tex}

\begin{document}

\title{Construction d’une application de visualisation interactive de modèles tridimensionnels}
\author{Maëlle~\textsc{Beuret}, Rémi~\textsc{Cérès}}
\date{26~janvier~2018}

\newcommand*\encadrant{%
    \textbf{Encadrante~:}\\
    \Mme~Nancy~\textsc{Rodriguez}\\[1em]
}

\subimport*{../}{titlepage}
\tableofcontents
\subimport*{../}{intro}

\section{Objectif du projet : construction d'une application de visualisation de modèles tridimensionnels}

Ce rapport se concentre sur le troisième projet. L'objectif est de mettre en place une application web permettant de visualiser les modèles tridimensionnels construits par la deuxième équipe.
À l'aide de cette application, l'utilisateur doit pouvoir visualiser les différents modèles et manipuler et personnaliser leur rendu.

Nous avons divisé ce travail en trois missions.
La première consiste à mettre en place une application web basique qui effectue un simple rendu d'un modèle.
Celle-ci s'exécute sur un navigateur web. Le rendu qu'elle affiche est calculé à partir d'un modèle donné au format objet (\textit{Object File}), d'extension \texttt{.obj}~\parencite{wavefront-1980-obj}.

Notre deuxième mission est de développer des outils permettant la personnalisation et l'interaction avec le rendu. Avec ces outils, l'utilisateur peut :

\begin{description}
    \item[Se déplacer :] le premier outil est le plus important, il permet de modifier le point de vue à partir duquel le rendu est effectué. Au travers de cet outil, l'utilisateur peut se déplacer au sein du modèle à l'aide de son clavier et/ou de la souris.

    \item[Gérer les lumières :] le deuxième outil permet de modifier les paramètres d'éclairage~; l'utilisateur peut choisir différents types d'éclairages et modifier la position des sources de lumières.

    \item[Mesurer :] le troisième outil permet d'effectuer des mesures de distance, de surface et de volume directement sur le modèle.
\end{description}

Cette mission concerne uniquement le développement de l'aspect technique des outils et non l'interface permettant de les utiliser.

Enfin, notre troisième mission consiste à construire une interface graphique simple et intuitive.
L'interface liste les modèles disponibles et permet d'effectuer une recherche à partir de leur nom ou de mots-clés.
Elle intègre les outils mis en place lors de la précédente mission.

Ces objectifs soulèvent les problématiques suivantes : qu'est-ce qu'un rendu tridimensionnel~? Quels sont les outils permettant d'effectuer un rendu et une interface graphique sur le web~? Lesquels sont les plus adaptés à notre mission~?

Dans un premier temps, nous définissons le concept et les techniques de rendu. Nous présentons ensuite divers outils de rendu pour le web, avant de terminer par les outils permettant de créer des interfaces graphiques.

\chapter{Méthodes et outils pour le rendu tridimensionnel sur le web}

\section{Présentation et définition du concept de rendu}

Le rendu est un processus qui, à partir du modèle 3D d'une scène et d'un point de vue, calcule l'image 2D représentant la vue de la scène qu'aurait un observateur en ce point. Il a souvent pour finalité l'affichage sur un écran \parencite{methode-rendu-2012}.

Le calcul se fait en prenant en compte les sources de lumière ainsi que différents phénomènes physiques. La figure \ref{fig:rendu} est un exemple de rendu du modèle de théière de l'Utah.

\begin{figure}[htb]
    \addtocounter{footnote}{-1}
    \centering
    \includegraphics[width=.55\linewidth]{./figures/utahteapot.jpg}\\
    \caption{\textbf{Exemple de rendu moderne} du modèle de théière de l'Utah\protect\footnotemark conçu par Martin Newell en 1975.}
    \label{fig:rendu}
\end{figure}

\footnotetext{Wikipédia : \url{https://upload.wikimedia.org/wikipedia/commons/5/5f/Utah_teapot_simple_2.png} CC-BY-SA~2.0).}

\noindent
\begin{tikzpicture}
\node [text width=\textwidth - 20pt, draw, inner sep=10pt]
    (definitions) {
        \begin{description}
            \item[ Modèle 3D:] un modèle tridimensionnel (3D) est une représentation d'un corps physique à travers une collection de points dans un espace 3D, reliés par diverses entités géométriques telles que des triangles, des lignes, des courbes, etc..
            \item[ Scène :] ensemble de modèles tridimentionnels.
            \item[ Caméra :] point de vue à partir duquel le rendu de la scène est calculé.
        \end{description}
    };
\node [fill=white, inner xsep=10pt] at (definitions.north)
    {\textbf{\large Quelques définitions}};
\end{tikzpicture}

\section{Principales méthodes de rendu}
Il existe plusieurs méthodes mathématiques de rendu d'un modèle 3D : les plus communes sont la rastérisation et le lancer de rayon \parencite{ploeg-replacemment-rasterization}.

\subsection{Rastérisation}

La rastérisation consiste à projeter chaque objet de la scène 3D sur un écran 2D.
La rastérisation se déroule en plusieurs étapes schématisées sur la figure \ref{fig:rasterization}. Dans un premier temps les coordonnées des points de chaque objet sont projetées géométriquement dans le plan de l'écran. On obtient ainsi une image vectorielle en deux dimensions de la scène. Dans un second temps, l'image vectorielle obtenue est convertie en une image matricielle. C'est au cours de cette étape que l'on détermine les pixels qui doivent apparaître, leur intensité de lumière et leur couleur \parencite{mozilla-theorie-histoire-rendu-3D}.

\begin{figure}[htb]
    \addtocounter{footnote}{-1}
	\centering
	\includegraphics[width=.9\linewidth]{./figures/rasterization.png}\\
	\caption{Schéma	du principe de la rastérisation\protect\footnotemark}
	\label{fig:rasterization}
\end{figure}

\footnotetext{Scratchapixel~:~\url{https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation}}

Dans le cas où la projection de l'objet sort de l'écran, il n'est pas rendu. La profondeur, c'est-à-dire la distance par rapport à l'écran, est calculée afin de ne pas effectuer le rendu d'un pixel si un autre objet plus proche de l'écran utilise ce même pixel \parencite{rey-webgl-2014}.

Les sommets des objets sont calculés grâce à des opérations matricielles adaptées aux fonctionnalités des matériels graphiques. La simplicité de ces calculs permet de réaliser des rendus en temps réel.


\subsection{Lancer de rayon}

Le lancer de rayon consiste à reproduire le processus de la vision en inversant le trajet de la lumière. Cette méthode a un principe opposé à la rastérisation. En effet, le rendu ne s'obtient plus en projetant les objets de la scène vers la caméra, mais en tirant pour chaque pixel de l'image des rayons partant de la caméra vers les objets, puis des objets vers les sources de lumière (voir figure \ref{fig:raytracing}).

\begin{figure}[htb]
    \addtocounter{footnote}{0}
	\centering
	\includegraphics[width=.55\linewidth]{./figures/raytracing.png}\\
	\caption{Schéma illustrant le principe du lancer de rayon\protect\footnotemark.}
	\label{fig:raytracing}
\end{figure}

\footnotetext{Wikipédia : \url{https://upload.wikimedia.org/wikipedia/commons/8/83/Ray_trace_diagram.svg}}

Le calcul de la trajectoire du rayon prend en compte les règles de la physique telles que la réflexion et la réfraction \parencite{ploeg-replacemment-rasterization}.
Cette méthode permet de représenter des effets lumineux complexes et d'obtenir un rendu plus réaliste. Cependant, elle demande des calculs plus importants que la rastérisation, rendant le processus lent et coûteux. Cette méthode n'est donc pas adaptée à un rendu en temps réel.

\section{Outils de rendu du web : WebGL}

Il existe à ce jour un standard permettant de faire un rendu tridimensionnel sur le web. Il s'agit de Web Graphics Library (WebGL). Avant l'apparition de ce standard en mars 2011, \parencite{tutorials-Point-2015} il existait des solutions telles que Flash ou Java3D permettant d'y parvenir, mais tendent aujourd'hui à devenir obsolètes.

Nous avons donc décidé dans ce rapport de nous concentrer sur WebGL et ses couches d'abstraction les plus utilisées.

WebGL est une API JavaScript \textit{open source} développée par Khronos permettant l'affichage de graphismes 2D et 3D dans n'importe quel navigateur web compatible \parencite{mozilla-webgl-api}.
Elle est promue par un consortium qui regroupe à la fois des fabricants de puces (Nvidia, Intel, ARM, AMD) et des éditeurs de navigateurs (Mozilla, Opera et Chrome).

Cette API permet d'utiliser le standard OpenGL ES (\textit{Open Graphics Library for Embedded Systems}) au sein d'une page web.
OpenGL est une interface de programmation bas niveau implémentée au dessus des pilotes graphiques. Elle permet de réaliser des rendus 2D et 3D en exploitant l’accélération matérielle de la carte graphique~\parencite{bourry-2013-webgl}.


OpenGL ES est une version portable de OpenGL prévue pour fonctionner sur des appareils mobiles. WebGL a choisi d'utiliser OpenGL~ES pour son large support au sein des architectures embarquées telles que les tablettes ou les téléphones.

A l'aide de WebGl, le navigateur peut utiliser les ressources de la carte graphique pour calculer le rendu. La figure \ref{fig:nav-webgl} résume les différentes étapes de la communication entre le navigateur et la carte graphique.\\

\begin{figure}[h!]
    \addtocounter{footnote}{-3}
    \centering
    \includegraphics{./figures/communication-webgl-cg.png}
    \caption{Schéma des étapes de la communication entre le navigateur et la carte graphique (icône réalisé par Freepik\protect\footnotemark).}
    \label{fig:nav-webgl}
\end{figure}

\footnotetext{FlatIcon : \url{https://www.flaticon.com/}}

La méthode de rendu utilisée par WebGL est la rastérisation. Pour calculer le rendu, WebGL utilise le même \textit{Shading Language} que OpenGL, GLSL ES (\textit{OpenGL Shading Language Embedded Systems}).
Un \textit{Shading Language} est un langage adapté a la programmation de \textit{shaders}.
Les \textit{shaders} sont de petits programmes écrits en langage haut niveau, qui définissent comment les pixels d'un objet tridimensionnel sont dessinés. Ils s'exécutent sur la carte graphique. Un \textit{shader} est composé principalement de deux parties :
un \textit{vertex shader} et un \textit{pixel shader} (également appelé \textit{fragment shader}). Le premier transforme les coordonnées de l'objet en espace d'affichage bidimensionnel. Le second génère les couleurs finales de chaque pixel en se basant sur la texture et le matériau de l'objet, la couleur ainsi que la lumière qui y est projetée \parencite{parisi-2012-webgl}.

Cette API comporte de nombreux avantages. Tout d'abord, comme dit précédemment, il s'agit d'un standard  supporté nativement par les principaux navigateurs (Chrome, Firefox, Safari, Opera)\parencite{can-i-use-webgl}. L'utilisateur peut donc utiliser cette technologie directement sans devoir installer un \textit{plugin}. De plus, WebGL est également utilisable sur n'importe quelle plateforme, allant de l'ordinateur de bureau au téléphone \parencite{parisi-2012-webgl}, \parencite{bosh-webGL-stats}.

WebGl étant basé sur l'API de OpenGL, cela lui permet d'exploiter pleinement les ressources de la carte graphique du client pour le calcul du rendu.

WebGL est écrit en JavaScript, il est donc possible d'interagir directement avec les autres éléments du document HTML. Cela permet également d'exploiter les nombreuses bibliothèques disponibles pour ce langage de programmation.

\subsection{Couches d'abstraction de WebGL}

Programmer directement avec WebGL est assez complexe, cela nécessite une bonne connaissance de WebGL ainsi que du \textit{Shading Language} (GLSL) \parencite{dirksen2014three}.
Il existe plusieurs couches d'abstraction simplifiant l'utilisation de WebGL au travers d'API JavaScript haut niveau. Ces couches d'abstraction permettent de créer facilement des scènes 3D de bonne qualité sans avoir à connaître WebGL dans ses détails.

Pour réaliser ce rapport nous avons effectué un tri afin d'éliminer celles ne correspondant pas à nos besoins.
Nous avons choisi une technologie libre de droit, et donc exclu les bibliothèques propriétaires et payantes. De plus, il nous a semblé important d'avoir accès à l'intégralité du code source de la bibliothèque afin de pouvoir comprendre son fonctionnement.

Nous avons ensuite choisi d'éliminer les bibliothèques ne permettant pas l'importation de fichiers au format \textit{object} car il s'agit du format de stockage du modèle choisi par le groupe.
Nous en avons donc sélectionné deux, Three.js et Babylon.js.

\subsection{Three.js}

Three.js est une couche d'abstraction \textit{open source} créée par Ricardo Cabello \parencite{Ricardo-Cabello-cv} et lancée en 2010. Elle fut dans un premier temps écrite en ActionScript puis traduite en JavaScript. Elle permet de créer des scènes tridimensionnelles directement dans le navigateur en utilisant WebGL.

Three a pour objectif de simplifier et rendre plus accessible la création de graphismes 3D sur le web. Cette couche d'abstraction orientée objet réalise des abstractions des différents concepts d'un rendu. Elle définit ainsi les concepts de scène, de caméra, de lumière, etc.

Three simplifie donc la création de scènes tridimensionnelles complexes. Elle permet de réaliser plus facilement des animations et des mouvements d'objets. Cette couche d'abstraction apporte également une gestion de l'éclairage avec différents types de lumière prédéfinis.

Enfin, l'application de textures et le chargement d'objets depuis un logiciel de modélisation sont également facilités. Il est notamment possible à l'aide de Three de charger un fichier au format .obj \parencite{three-js-api-obj}.

Cette couche d'abstraction va également permettre de simplifier les interactions avec le rendu en apportant la possibilité de relever les coordonnées du curseur de la souris \parencite{parisi-2012-webgl}.

\subsection{Babylon.js}

Babylon.js est plus récent que Three, il a été lancé en juillet 2013. Cette couche d'abstraction a été développée initialement par David Cathude, David Rousset, Pierre Lagarde et Michel Rousseau \parencite{Moreau-Mathis:2142424}. C'est un projet \textit{open source}, dont le code source est disponible sur \href{https://github.com/BabylonJS/Babylon.js}{GitHub}.

Babylon est une couche d'abstraction développée en TypeScript, un langage compilé multi-plateformes qui génère des fichiers JavaScript pur.
Elle permet de créer des scènes tridimensionnelles complètes, des applications et des jeux en 3D. Elle a pour objectif de simplifier l'utilisation de WebGL et de rendre un moteur 3D accessible à tous \parencite{Moreau-Mathis:2142424}.

Babylon apporte également des abstractions des différents concepts d'un rendu très proches de ceux proposés par Three. En revanche Babylon possède un moteur physique complet et permet la gestion des collisions \parencite{baby-docs-engine}.

\section{Comparatif des outils}

Three et Babylon simplifient l'utilisation de WebGL au travers d'abstractions, ce qui nous sera utile pour développer les outils permettant d'interagir avec la scène.
Three apporte un grand éventail de fonctionnalités et d'outils pour améliorer la création et l'animation de graphismes 3D sur le web alors que
Babylon a une approche plus spécialisée. En effet, elle a été conçue comme un moteur de jeu vidéo et apporte un moteur physique complet.

Three, plus ancienne, est aujourd'hui plus couramment utilisée que Babylon. Il est difficile de trouver des statistiques précises comparant l'utilisation de ces deux couches d'abstraction. Nous pouvons cependant noter que Three est en moyenne vingt fois plus téléchargée avec npm \parencite{npm-stats-three} \parencite{npm-stats-babylone}.

Notre objectif est de construire un visualiseur de modèle tridimensionnel. Pour réaliser cet objectif, l'approche plus généraliste de Three nous semble plus adaptée. De plus, dans le cadre de notre projet, nous n'avons pas trouvé l'utilité du moteur physique de Babylon.

Nous découvrons avec ce projet l'univers du rendu tridimensionnel, il nous a semblé important de pouvoir compter sur une grande communauté pouvant nous apporter de l'aide en cas de besoin. La forte popularité de Three est une garantie de son bon fonctionnement et d'un support à long terme.

\chapter{Interface graphique de sélection et de manipulation du rendu}

\section{Principe d'une interface graphique de sélection et de manipulation du rendu}
Une interface graphique est un dispositif d'interaction homme-machine. L'objectif est de permettre un fonctionnement et un contrôle efficace de la machine par l'homme. La figure \ref{fig:interface-desktop-nuage} est un exemple d'interface graphique.

% Dans un même temps, la machine restitue des informations qui facilitent le processus de prise de décision de l'opérateur.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\linewidth]{./figures/user-interface-desktop.png}
	\addtocounter{footnote}{-1}\caption{\textbf{Interface graphique utilisateur qui suit la métaphore du bureau} Gnome 3.26\protect\footnotemark}
	\label{fig:interface-desktop-nuage}
\end{figure}

\footnotetext{Gnome : \url{https://www.gnome.org/}}

% Elle permet à l'utilisateur de contrôler le logiciel et d'interagir avec celui-ci, par exemple de modifier des paramètres ou bien d'utiliser les outils mis à disposition par le logiciel.

Une interface graphique est manipulée au travers d'objets ou \textit{widgets} dessinés sous forme de pictogrammes. Ces objets peuvent par exemple être des fenêtres ou des boutons, comme illustré par la figure \ref{fig:widgets}. L'utilisateur manipule ces objets en imitant les interactions physiques qu'il aurait avec l'objet qu'il représente. Pour cela, il utilise un dispositif de pointage, généralement une souris.

\begin{figure}[h!]
   \centering
   \includegraphics[width=0.7\linewidth]{./figures/widgets.png}
   \caption{\textbf{Exemple de widgets graphique typique\protect\footnotemark} }
   \label{fig:widgets}
\end{figure}

\footnotetext{Wikipédia : \url{https://upload.wikimedia.org/wikipedia/commons/d/d5/Widgets.png}}


Dans les années 70 les ordinateurs se manipulaient au clavier à travers une interface en lignes de commandes.
Le Xerox Star créé par Xerox en 1981 fut le premier ordinateur proposant une interface graphique \parencite{smith1975pygmalion} \parencite{Xerox-star-user-interface}.
%Le terme a été créé pour distinguer ce type d'interface des lignes de commandes.

\section{Définition des besoins d'interface pour l'application}

L'interface de notre application est constituée de deux pages :
la première, schématisée par la figure \ref{fig:vue-1}, est la page d'accueil de l'application. Cette page est découpée en trois composants : \texttt{en-tête}, \texttt{pied de page} et \texttt{liste}.
Les composants \texttt{en-tête} et \texttt{pied de page} contiennent les informations générales de l'application tels que le nom, le logo, etc.
Le composant \texttt{liste}, situé au centre, donne la liste des modèles disponibles. Chaque modèle est représenté par un composant \texttt{carte} lui-même constitué d'un titre, d'une courte description et d'un aperçu du modèle.
Lors du clic sur une \texttt{carte}, une transition a lieu vers la seconde page.

La seconde page, schématisée par la figure \ref{fig:vue-2}, a pour but d'afficher le rendu d'un modèle. Cette vue contient également les composants \texttt{en-tête} et \texttt{pied de page}. La partie centrale est constituée de trois composants : \texttt{informations}, \texttt{rendu} et \texttt{outils}. Le bandeau \texttt{informations} positionné à gauche contient le titre et une courte description du modèle. Le \texttt{rendu} au centre affiche le rendu de la scène. Enfin, le composant \texttt{outils} à droite contient l'interface des outils permettant de manipuler le rendu.

\begin{figure}[h!]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{./figures/appli-web1.png}
        \addtocounter{footnote}{-1}\caption{\textbf{Maquette fonctionnelle de la page d'acceuil de l'application} listant les modèles disponibles}.
        \label{fig:vue-1}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{./figures/appli-web2.png}
        \caption{\textbf{Maquette fonctionnelle de l'affichage du rendu d'un modèle} et de ses outils de manipulation}.
        \label{fig:vue-2}
    \end{minipage}
\end{figure}

Le projet possède deux types distincts d'interfaces graphiques. La première, utilisée dans le composant \texttt{outils}, est l'interface graphique des outils.
La seconde est l'interface de navigation qui définit le reste des composantes. Elle permet notamment de lister les modèles.

\section{Outils permettant de créer une interface}

Il existe différents \textit{frameworks} et bibliothèques JavaScript permettant de faciliter la création d'interfaces graphiques. Ces outils permettent de faciliter l'organisation du code et de séparer les données et leur affichage. Ainsi, le code obtenu est plus clair, plus compréhensible et plus simple à entretenir.

\subsection{Dat.GUI}

Dat.GUI est un \textit{micro-framework} JavaScript créé en 2011. Il définit un ensemble de \textit{widgets} graphiques (boutons, \textit{slider}, etc.) permettant de modifier dynamiquement des variables. Cette bibliothèque est très légère et simple d'utilisation. Pour rendre un paramètre contrôlable via une interface graphique, il suffit d'associer une variable à un \textit{widget} graphique et celui-ci s'affichera automatiquement dans l'interface. La valeur de la variable sera alors mise à jour en temps réel. La figure \ref{fig:datgui} montre un exemple d'interface créée avec dat.GUI.

Dans le cadre de notre application, cette bibliothèque est parfaitement adaptée pour construire l'interface graphique des outils dans le composant \texttt{outils}.

\begin{figure}[h!]
   \addtocounter{footnote}{-2}
   \centering
   \includegraphics[width=0.6\linewidth]{./figures/datgui.png}
   \caption{\textbf{Exemple d'utilisation de dat.GUI }pour modifier les paramètres de rendu d'un modèle tridimensionnel\protect\footnotemark}.
   \label{fig:datgui}
\end{figure}

\footnotetext{Codepen : \url{https://codepen.io/programking/pen/MyOQpO}}

\newpage

\subsection{Angular.JS}

Angular est un \textit{framework} développé en 2009 par Google en TypeScript .

Il permet de créer des applications web monopage. Concrètement, un seul fichier HTML est exposé au navigateur par le serveur. Toutes les données sont récupérées en AJAX et injectées dans le DOM par Angular.

Ce \textit{framework} se base sur l'architecture MVVM (\textit{Model View ViewModel}). Cette méthode permet de séparer dans des fichiers distincts les modèles, les vues et un lien entre les vues et les modèles. Les modèles représentent la structure de données, généralement un fichier au format JSON ou une base de données. Les vues forment l'interface avec l'utlisateur ; en web la vue s'inscrit dans le HTML. La communication entre la vue et les modèles est assurée par le JavaScript.

Angular utilise la liaison bidirectionnelle de données (\textit{two-way data binding}). Ce terme regroupe trois mécanismes : la détection de changements, la résolution des liaisons associées, et la mise à jour du DOM. La liaison bidirectionnelle fait le lien entre la vue et le modèle, d'une part elle permet de récupérer des événements, comme une modification sur une zone de texte, et met à jour le modèle en conséquence ; d'autre part elle met à jour la vue en fonction de l'évolution du modèle~\parencite{bindin-sylvain}.

Les avantages d'Angular sont nombreux.
Tout d'abord il permet de créer des applications web monopage, fluidifiant ainsi l'expérience utilisateur en évitant les chargements de page.
Angular, au travers de liaison bidirectionnelle des données simplifie grandement la gestion du contenu dynamique.
Enfin, Angular impose aux développeurs une architecture MVVM. Cela permet de bien séparer les données de l'affichage et donc de rendre le code plus lisible et réutilisable~\parencite{martin-2016}.

\subsection{React.js}

React.js est une bibliothèque d'interface graphique utilisateur \textit{open source} développée par Facebook depuis 2013.
Elle vise les principes suivants~: un code déclaratif, l'efficacité, la flexibilité et une expérience développeur améliorée.

Les composants sont l’une des plus puissantes fonctionnalités de React. Un composant est une unité d'interface élémentaire. Il peut lui-même être constitué d'autres composants. Par exemple dans la figure \ref{fig:vue-1}, une \texttt{liste} est constituée de plusieurs \texttt{cartes}.
Les composants permettent d’isoler, d’encapsuler du code HTML et de le rendre réutilisable.

React utilise JSX, une syntaxe proche du HTML/XML. La syntaxe est destinée à être utilisée par des préprocesseurs pour transformer du texte pseudo-HTML dans des fichiers JavaScript en des objets JavaScript standards.
En temps normal, on cherche à séparer l'affichage en HTML et la logique en JavaScript. Avec JSX, la logique et l'affichage se retrouvent à nouveau mélangés en un seul fichier. La séparation ne se fait plus au niveau des technologies utilisées, mais plutôt par composant.

Cette bibliothèque utilise la liaison unidirectionnelle des données, c'est-à-dire que la vue est mise à jour automatiquement lorsque le modèle est modifié. En revanche, contrairement à la liaison bidirectionnelle, le modèle n'est pas mis à jour en fonction des modifications de la vue \parencite{neuhaus-comparison}.

\subsection{Vue.js}
\addtocounter{footnote}{-1}

Vue.js est une bibliothèque JavaScript créée par Evan You \footnote{Evan You : \url{http://evanyou.me/}} en 2014 distribuée sous licence MIT.

Cette bibliothèque utilise par défaut la liaison unidirectionnelle des données, mais peut également utiliser la liaison bidirectionnelle.
Le but de cette bibliothèque est de combiner les avantages d'une liaison de données réactive avec une API la plus simple possible.

Vue permet d'organiser le code par composant. Chaque composant est défini dans un fichier unique, le rendant ainsi indépendant et réutilisable lors d'un autre projet \parencite{neuhaus-comparison}.

Outre son utilisation simple, Vue possède également un avantage non négligeable : il est très léger, ce qui le rend particulièrement adapté pour des projets de petite taille. \parencite{kyriakidis-2016-majesty}.

\section{Comparatif des outils d'interface}

Pour créer l'interface des outils, nous avons choisi dat.GUI. Cette bibliothèque légère propose de créer une interface graphique permettant de modifier des variables très simplement. Elle s'intègre parfaitement à Three et est couramment utilisée dans ce contexte.

Pour créer l'interface de navigation de notre application, plusieurs solutions s'offrent à nous : Angular, React et Vue. Ces outils ont un objectif similaire, cependant il existe des différences notables dans leur fonctionnement.

Premièrement, nous pouvons noter une différence de performance entre ces trois outils. Nous pouvons voir sur le schéma \ref{fig:perf} qui compare les performance de JavaScsript natif, Vue, React et Angular. Nous pouvons observer que Angular est 1.85 fois plus long que le JavaScript natif, React 1.82 fois et Vue 2.0 seulement 1.37 fois. Vue est donc la bibliothèque la plus rapide.

\begin{figure}[h!]
    \addtocounter{footnote}{-1}
   \centering
   \includegraphics[width=0.6\linewidth]{./figures/rapide.png}
   \caption{\textbf{Histogramme comparant les performances de JavaScsript natif, Vue, React et Angular}\protect\footnotemark}
   \label{fig:perf}
\end{figure}

\footnotetext{Xebia : \url{https://blog.xebia.fr/2016/11/22/vue-js-vue-js-un-challengeur-de-poids/}}

L'interface requise pour ce projet est relativement simple, les trois bibliothèques et \textit{frameworks} présentés ci-dessus nous permettent toutes de la réaliser. Afin d'éviter d'alourdir le projet, il est préférable de choisir une bibliothèque adaptée à l'envergure du projet et éviter de surcharger de fonctionnalités superflues. Un comparatif de la taille bibliothèques et \textit{frameworks} peut être un premier indicateur de leur complexité.
Sur le graphique \ref{fig:taille} nous constatons que la taille de Angular~2 est environ 3 fois supérieure à celle de React, elle-même 2 fois supérieure à la taille de Vue.

\begin{figure}[h!]
   \centering
   \includegraphics[width=0.6\linewidth]{./figures/taille.png}
   \caption{\textbf{Histogramme comparant les poids en Ko de Vue, React et Angular}}
   \label{fig:taille}
\end{figure}

De plus,  nous disposons de peu de temps pour prendre en main les outils. Angular et React sont plus complexes à maîtriser que Vue. En effet, la documentation de Vue est complète et l'apprentissage rapide, et aucun \textit{plugin} ou bibliothèque externe n'est nécessaire. Une bibliothèque plus simple d'utilisation nous permettrait de consacrer davantage de temps pour créer une interface complète \parencite{sharma-comparison}.

Par sa taille et ses performances, nous pensons que Angular n'est pas le \textit{framework} le plus adapté pour répondre aux besoins du projet. Cela nous laisse le choix entre React et Vue.

React est une bibliothèque très flexible, mais par conséquent cela implique une grande responsabilité. Chaque projet nécessite de prendre une décision sur l'architecture à utiliser. Vue est plus équilibré entre ce qui est automatique et ce que le développeur doit faire lui-même. Ainsi, il est plus simple d'utilisation.

JSX, utilisé par React, réunit la partie logique en JavaScript avec l'affichage en HTML. Les composants sont difficiles à lire et cela nécessite de séparer au maximum chaque composant. Très rapidement, le nombre de micro-composants augmente et complexifie la maintenance du code. C'est un des principaux défauts de React par rapport à Vue \parencite{neuhaus-comparison}.

Suite à ce comparatif, Vue.js nous semble être l'outil le plus adapté pour développer l'interface de l'application.

\chapter{Conclusion}

Dans un premier temps, nous avons présenté et défini le concept de rendu. Nous avons par la suite exposé deux méthodes mathématiques de calcul de rendu les plus communes, la rastérisation et le lancer de rayon.
Nous avons également étudié les différents outils permettant de réaliser un rendu tridimensionnel ainsi qu'une interface graphique sur le web. En ce qui concerne le rendu, notre choix s'est porté sur l'API WebGL avec sa bibliothèque Three.js. Pour l'interface graphique, nous avons décidé d'utiliser Vue.js pour l'interface générale couplé avec dat.GUI pour l'interface des outils.

\section{Difficultés rencontrées}

La première difficulté que nous avons rencontrée concerne l'organisation de notre recherche bibliographique elle-même. En effet, nous ne partagions pas la même vision de cet exercice. Finalement nous avons réussi à nous mettre d'accord à l'aide d'avis extérieurs, notamment de la part de nos tuteurs de projet.

Il existe un grand nombre de bibliothèques pour WebGL\parencite{wiki:Liste_de_frameworks_WebGL}, elles sont relativement récentes et évoluent rapidement. Aussi, comparer et trouver la plus adaptée à notre mission fut difficile. Il en va de même pour les bibliothèques et \textit{frameworks} permettant de créer des interfaces graphiques utilisateur.

\section{Apports personnels}

Cette première étape de projet nous a été très bénéfique. En effet, c'est la première fois que nous avons  l'occasion de travailler sur un projet d'aussi grande envergure coordonnant trois équipes de deux personnes. Cela nous a permis d'apprendre à travailler en équipe et nous a demandé une grande organisation.

Cela nous a également permis de découvrir les principes et concepts de tridimensionnalité. Nous avons également pu approfondir nos connaissances sur les technologies du web, dont les bases apprises grâce à l'unité d'enseignement «architecture et programmation du web» (HLIN510) nous ont été essentielles.

\section{Avancement du projet}

Après avoir réalisé cette recherche bibliographique, nous avons acquis les bases nécessaires afin de pouvoir entamer la mise en place de notre application web. Il nous faudra effectuer le rendu des modèles fournis par le deuxième binôme et créer les outils permettant d'interagir avec le rendu. Enfin, il nous faudra intégrer ces outils ainsi qu'un système de gestion de modèles à une interface graphique (voir annexe \ref{appendix:gantt}).

\appendix
\chapter{Diagramme de Gantt}
\label{appendix:gantt}

\thispagestyle{empty}
\scalebox{.65}{\rotatebox{-90}{
		\includegraphics[trim={0 0 28cm 0}, clip]{./figures/gantt/gantt-ter.pdf}
}}

\newpage
\thispagestyle{empty}
\scalebox{.65}{\rotatebox{-90}{
		\includegraphics[trim={20cm 0 0 0}, clip]{./figures/gantt/gantt-ter.pdf}
}}


\restoregeometry\clearpage



\phantomsection%
\printbibliography

\end{document}
