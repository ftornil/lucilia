@techreport{wavefront-1980-obj,
    author = {{Wavefront Technologies}},
    title = {Object Files (.obj)},
    year = {1980},
    url = {https://www.cs.utah.edu/~boulos/cs3505/obj_spec.pdf},
    urldate = {2017-11-20}
}
