\chapter{Détection et mise en correspondance de points caractéristiques dans des images d’une scène}
\label{part:feature-detection}

Afin de pouvoir reconstruire un modèle 3D d’une scène à partir de photographies prises depuis des points de vue différents, il est nécessaire de connaître des points apparaissant sur ces images qui correspondent en réalité à des éléments d’un même objet. Pour pouvoir être reconnus sur plusieurs images différentes, ces points doivent être décrits de façon invariante à une rotation, une déformation, une mise à l’échelle ou changement de luminosité~\parencite[p.~2]{lowe-2004-sift}. De tels points sont dits «~caractéristiques~» de l’image.

Toute zone locale d’une image peut être classifiée en l’une des trois structures géométriques illustrées dans la figure~\ref{fig:feature-types}~\parencite[chapitre~4, p.~209]{szeliski-2011-computervision}.

\begin{figure}[b!]
    \centering
    \includegraphics[scale=1.05]{figures/feature-types}
    \caption{\textbf{Trois structures géométriques possibles pour les zones locales d’une image.} Image adaptée d’une photographie d’Ildar Sagdejev sous licence CC-BY-SA.}
    \label{fig:feature-types}
\end{figure}

\begin{description}
    \item[Surface plane~:] contient peu de variation de couleur, ou des motifs répétés. Une telle zone ne contient pas de point pouvant être repéré de façon unique dans l’image, mais au contraire de nombreux points semblables.
    \item[Bordures~:] contient une ligne de différence nette entre deux surfaces. Cette ligne est distinguable dans l’image, mais pas les points individuels qui la composent car ils partagent tous un voisinage semblable.
    \item[Angles~:] contient un point de rencontre entre au moins trois surfaces de couleurs différentes. Ce point est unique dans son voisinage et peut être repéré grâce aux variations de couleur dans son entourage.
\end{description}

Parmi ces trois structures possibles, les plus caractéristiques d’une image sont les angles~; le problème de détection des points caractéristiques d’une image d’une scène se ramène donc à une détection des angles. Dans la littérature, le processus de détection est décomposé en trois étapes~\parencite[chapitre~4, p.~206]{szeliski-2011-computervision}~:

\begin{enumerate}
    \item \textbf{Détection des points caractéristiques.} Chaque image est analysée à la recherche de zones dont la structure géométrique est celle d’un angle. L’ensemble constitué d’un point sélectionné dans chaque zone détectée est l’ensemble des points caractéristiques.
    \item \textbf{Description du voisinage de chaque point caractéristique.} Afin de pouvoir repérer un point caractéristique correspondant dans une autre image ayant éventuellement subi une rotation, une déformation, une mise à l’échelle ou un changement de luminosité, une description du voisinage du point, indépendante de ces transformations, est formulée.
    \item \textbf{Mise en correspondance des points caractéristiques communs.} Les descriptions de points caractéristiques entre différentes images sont comparées, et les points ayant des descriptions proches sont considérés équivalents. Certaines correspondances peuvent être erronées et, si elles sont minoritaires, être statistiquement éliminées~\parencite{fischler-1981-ransac}.
\end{enumerate}

Cette partie présente des techniques permettant réaliser automatiquement cette détection, ainsi que des procédés permettant de calculer une correspondance entre des points caractéristiques de différentes images, pour déterminer s’il s’agit d’un unique et même point de la scène observée.

\emph{N.B.~: la suite de ce rapport fait appel à des notions de probabilités et de statistiques basiques. L’annexe~\ref{appendix:statistiques} donne un bref rappel de ces notions.}

\section{Détection des points caractéristiques d’une image}

L’algorithme de détection de points caractéristiques indépendants d’une mise à l’échelle (ou \emph{Scale-Invariant Feature Transform}, SIFT) a été introduit par~\cite{lowe-2004-sift}. Cette heuristique consiste à rechercher dans une image les points dont la variation de couleur est un extremum local, et à les considérer comme étant des points caractéristiques de l’image.

Pour effectuer cette recherche de façon efficace, l’image originelle est lissée par un flou gaussien de plus en plus fort, et son échelle est réduite petit à petit. Les images de même taille sont regroupées dans un octave\footnote{Qui est appelé octave, même lorsqu’il ne contient pas exactement huit images.}, au sein duquel le flou augmente progressivement. Enfin, entre chaque niveau de lissage, la différence des deux images est calculée pixel par pixel~\parencite[p.~6]{lowe-2004-sift}.

Le flou gaussien a la propriété de laisser inchangées les surfaces de couleur uniforme et, au contraire, de modifier fortement les zones de bordure. Ainsi, en faisant la différence entre chaque niveau de lissage, les surfaces uniformes sont éliminées, et seuls les bordures et les angles sont conservés. Par ailleurs, la réduction progressive d’échelle de l’image permet de détecter des détails de la scène proches ou éloignés de l’appareil photo, sur les différents octaves. La figure~\ref{fig:sift-example} donne un exemple d’application de l’algorithme SIFT sur une photographie, et la figure~\ref{fig:sift-feature-detection} illustre le processus de lissage progressif.

\begin{figure}[h!]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/sift-example-orig.jpg}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/sift-example-result.jpg}
    \end{minipage}
    \caption{\textbf{Exemple d’application de l’algorithme SIFT sur une photographie de la sculpture «~Hommage à Confucius~» d’Alain Jacquet.} À gauche, l’image originelle. À droite, l’image en niveaux de gris sur laquelle chaque point caractéristique détecté a été repéré par un cercle jaune. Les points caractéristiques ont été calculés par la bibliothèque OpenCV~: \url{https://opencv.org/}.}
    \label{fig:sift-example}
\end{figure}

\begin{figure}[h!]
    \centering
    \makebox[\textwidth][c]{
        \includegraphics{figures/sift-feature-detection}
    }
    \caption{\textbf{Schéma du déroulement de l’algorithme SIFT lors de la détection des points caractéristiques d’une image.} Sur cet exemple, deux octaves avec trois niveaux de lissage sont utilisés.}
    \label{fig:sift-feature-detection}
\end{figure}

L’algorithme parcourt finalement toutes les images résultant de la différenciation pour sélectionner les points qui ont une intensité supérieure à tous leurs voisins. Les voisins considérés sont les 8 dans l’image actuelle, les 9 dans la différence précédente et les 9 dans la différence suivante, s’ils existent. Les points qui satisfont ce critère sont des extremums locaux et sont choisis comme points caractéristiques~\parencite[p.~7]{lowe-2004-sift}.

Les paramètres de l’algorithme SIFT sont $\sigma$, l’écart-type du flou gaussien appliqué à chaque étape, et le nombre d’images par octave. Les expérimentations de~\citeauthor{lowe-2004-sift} montrent que de bons résultats sont généralement obtenus pour $\sigma = 1,6$ et avec 3~images par octave~\parencite[p.~8 et~10]{lowe-2004-sift}, mais ceux-ci doivent être ajustés empiriquement pour chaque cas d’utilisation.

\cite{bay-2008-surf}, ont proposé une modification de l’algorithme SIFT, baptisée «~caractéristiques robustes accélérées~» (ou \emph{Speeded Up Robust Features,} SURF), qui rend le calcul des points caractéristiques plus rapide en ne calculant pas un lissage gaussien à chaque étape, mais en appliquant plutôt la fonction carré suivante \begin{equation}
    S(x,y)=\sum _{i=0}^{x}\sum _{j=0}^{y}I(i,j).
    \label{equ:surf-square-filter}
\end{equation}

La matrice hessienne, dont la formule est donnée en \ref{equ:surf-determinant-hessian} est ensuite calculée. Les points pour lesquels cette valeur dépasse un certain seuil sont détectés comme étant des points caractéristiques de l'image.

\begin{equation}
    H(p,\sigma )=
        \begin{pmatrix}
            L_{xx}(p,\sigma )&L_{xy}(p,\sigma )\\
            L_{yx}(p,\sigma )&L_{yy}(p,\sigma )
        \end{pmatrix}
    \label{equ:surf-determinant-hessian}
\end{equation} où $L_{xy}(p,\sigma )$ correspond au pixel aux coordonnées $(x, y)$ de l'image $p$ obtenue en appliquant la fonction carré à l'image de départ et $\sigma$ représente la valeur de $S(x,y)$ pour ce point.

Ainsi, la transformation peut être précalculée dans une image d'entiers (ou \emph{«~integer image~»}), c’est-à-dire une image dans laquelle la valeur de chaque pixel est déterminée en fonction de celle des pixels alentour, permettant ainsi d’effectuer le calcul souhaité en temps constant au lieu de~$O(n^2)$~\parencite{cozzi-2011-sat}.

\section{Description des points caractéristiques d’une image}

L’objectif de la détection de points caractéristiques est de pouvoir mettre en correspondance ces points entre différentes photographies d’un même objet prises depuis plusieurs points de vue. Pour ce faire, la seule connaissance du pixel correspondant à un point caractéristique ne suffit pas, car les intensités d’un point commun ont peu de chance de correspondre exactement d’une image à une autre.

Au lieu de comparer les pixels centraux des points caractéristiques, la comparaison est faite sur une description du voisinage de chaque point, calculée de telle sorte à ce que celle-ci soit similaire pour des points équivalents~\parencite[p.~222]{szeliski-2011-computervision}. Le descripteur SIFT fournit une telle description de façon indépendante de l’échelle, d’une rotation, d’une déformation ou d’un changement de luminosité. Chaque point caractéristique est paramétré par \begin{equation}
    \left< (x, y), \sigma, \theta, f\right >,
\end{equation}

où $(x, y)$ est la position du pixel central dans l’image, $\sigma$ est l’écart-type du flou gaussien utilisé pour détecter le point considéré, $\theta$ est l’orientation du point et $f$ est un vecteur décrivant le voisinage du point.

Dans l’algorithme SIFT, le voisinage d’un point caractéristique est considéré comme étant un carré de $16 \times 16$ pixels autour du pixel central. Il est divisé en seize blocs de $4 \times 4$ pixels. Le gradient de l’image est évalué pour chaque point de ce voisinage selon les fonctions approchées \begin{equation}
    \begin{cases}
        m\left(x,y\right) &= {\sqrt {\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)^{2}+\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)^{2}}}\\
        \theta\left(x,y\right) &= {\mathrm{tan}^{-1}}\left(\left(L\left(x,y+1\right)-L\left(x,y-1\right)\right)/\left(L\left(x+1,y\right)-L\left(x-1,y\right)\right)\right),
    \end{cases}
\end{equation} donnant respectivement la norme et la direction du vecteur gradient d’un pixel situé aux coordonnées $(x, y)$ de l’image. Dans ces équations, $L\left(x,y\right)$ désigne l’image obtenue en appliquant un lissage gaussien d’écart-type $\sigma$ sur l’image originelle, et $\mathrm{tan}^{-1}$ désigne la fonction arc tangente.

Pour chaque bloc du voisinage, un histogramme des gradients des points qu’il contient est calculé. Celui-ci est discrétisé sur 8~directions possibles. Chaque vecteur gradient du bloc considéré contribue à l’histogramme de façon proportionnelle à sa norme et inversement proportionnelle à sa distance au point d’intérêt~\parencite[p.~13]{lowe-2004-sift}. Enfin, les normes de chaque vecteur de l’histogramme de chaque zone sont stockées dans le vecteur $f$ de dimension $16 \times 8 = 128$, qui est le descripteur du point. La figure~\ref{fig:sift-feature-description} illustre ce processus.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/sift-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SIFT d’un point caractéristique d’une image.} Le gradient de chaque pixel dans le voisinage du point est évalué, puis un histogramme est calculé pour chaque bloc de taille $4 \times 4$. L’ensemble des valeurs des histogrammes constitue le descripteur. Ici, seulement $8 \times 8$ pixels du voisinage sont illustrés par souci de clarté.}
    \label{fig:sift-feature-description}
\end{figure}

Les descripteurs calculés par l’algorithme SIFT sont normalisés et donc invariants par changement de luminosité. Ils sont uniquement basés sur le motif de variation d’intensité autour des points d’intérêt et sont donc invariants par rotation ou changement d’échelle~\parencite[p.~16]{lowe-2004-sift}.

\cite{bay-2008-surf}, ont également proposé une modification du descripteur SIFT~: le descripteur SURF, dont le calcul est effectué plus rapidement en utilisant l'ondelette de Haar ou \emph{«~Haar wavelet~»}, décrite par la formule \ref{equ:haar-wavelet-def}~\parencite{cheng-2011-haar}. Cette fonction permet de détecter des changements brutaux dans les valeurs. Dans le cadre de l'algorithme SURF, ces valeurs correspondent aux valeurs des pixels dans un cercle alentour au point détecté, en x et en y.

\begin{equation}
    \psi (t)=
        \begin{cases}
            1\quad &0\leq t<{\frac {1}{2}},\\
            -1&{\frac {1}{2}}\leq t<1,\\
            0&{\mbox{sinon.}}
        \end{cases}
    \label{equ:haar-wavelet-def}
\end{equation}

Pour déterminer l'orientation générale d'un point, la somme des valeurs est effectuée et la direction générale indiquée est affectée à la direction du point. La description du voisinage est quand à elle effectuée en calculant la somme du résultat de l'ondelette de Haar dans des sous-régions autour du point, comme illustré dans l'image \ref{fig:surf-feature-description}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/surf-feature-description}
    \caption{\textbf{Illustration du procédé de calcul du descripteur SURF d’un point caractéristique d’une image.} Au lieu de calculer un histogramme des directions de gradient possible dans chaque bloc, la somme de tous les vecteurs d’un bloc est choisie comme étant son représentant.}
    \label{fig:surf-feature-description}
\end{figure}

\section{Mise en correspondance des points caractéristiques de plusieurs images d’une scène et élimination des correspondances aberrantes}

Pour mettre en correspondance entre plusieurs images d’une même scène des points caractéristiques dont le descripteur SIFT ou SURF a été calculé, il suffit de comparer ces descripteurs et de choisir des ensembles de points prenant des valeurs de descripteur proches, avec un certain seuil de tolérance. Cette comparaison naïve a pour inconvénient de générer des correspondances aberrantes, notamment entre différentes parties d’un motif qui se répète.

En supposant que ces correspondances aberrantes soient minoritaires, la méthode de consensus par échantillonnage aléatoire (ou \emph{RANdom Sample Consensus,} RANSAC) peut être utilisée~\parencite{fischler-1981-ransac}. Cette méthode permet de résoudre de façon générale les problèmes de valeurs aberrantes.

Cette méthode est basé sur une méthode d'essai et d'approche de l'erreur, c'est à dire qu'elle consiste à trouver le meilleur ensemble de points pour lesquels le calcul d'un modèle est correct. Pour cela, elle génère un nombre important de possibilités qui sont évaluées en calculant le nombre de points validant le modèle proposé à partir d'un sous ensemble de points. Concrètement, cet algorithme consiste à :

\begin{enumerate}
    \item Choisir aléatoirement un ensemble minimal de points parmi ceux détectés précédemment, quel que soit le(s) algorithme(s) utilisé(s).
    \item Calculer un modèle à partir de ce sous ensemble.
    \item Attribuer une note au modèle ainsi calculé en fonction du nombre de points (autres que ceux choisis aléatoirement dans la première phase) validant le modèle en acceptant un certain taux d'erreur,
    \item Recommencer les étapes 1 à 3 un certain nombre de fois et selectionner le modèle ayant la meilleure note parmi ceux calculés.
\end{enumerate}

La figure \ref{fig:detection-feature-match} présente un exemple d'application de cet algorithme, en utilisant un modèle simplifié basé sur une simple translation de l'image, qui permet de détecter et éliminer les correspondances aberrantes.

\begin{figure}[htb]
    \centering
    \begin{tikzpicture}[
        match/.style={
            yellow,
            fill,
            very thick
        },
        outlier/.style={
            match,
            thick,
            red
        }
    ]
        \node[inner sep=0pt] (before)
            {\includegraphics[scale=.25]{figures/feature-match-before}};
        \node[inner sep=0pt, below=.5cm of before] (after)
            {\includegraphics[scale=.25]{figures/feature-match-after}};

        \begin{scope}[shift={(0, -3.16)}]
            % Correspondances aberrantes
            \draw[outlier] (-4.2, -.275) circle (.05) -- (.45, -1.65) circle (.05);
            \draw[outlier] (-6.5, -1.6) circle (.05) -- (5.9, -1) circle (.05);
            \draw[outlier] (-5.34, -4.1) circle (.05) -- (3.05, -3.8) circle (.05);

            % Correspondances réelles
            \draw[match] (-3.3, -.9) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.35, -1.15) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.32, -1.72) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-3.35, -3.4) circle (.05) -- ++(4.6, 0.35) circle (.05);
            \draw[match] (-4, -4.8) circle (.05) -- ++(4.6, 0.35) circle (.05);
        \end{scope}
    \end{tikzpicture}
    \caption{\textbf{Mise en correspondance de points caractéristiques entre deux images.} Les deux images, à gauche et à droite, sont des portions d’une plus grande photographie de la Cour Carrée du Louvre, prise par l’utilisateur \emph{KingOfHearts} de Wikimédia et distribuée sous licence CC-BY-SA. En haut, la paire d’images originelle. En bas, la même paire en niveaux de gris sur laquelle ont été repérées des paires de points caractéristiques détectés comme correspondants par l’algorithme SIFT. En jaune, les correspondances validant le modèle et en rouge les correspondances aberrantes éliminées par l’algorithme~RANSAC.}
    \label{fig:detection-feature-match}
\end{figure}

Dans un premier temps la taille de l'ensemble minimal à choisir aléatoirement est déterminée. Cet ensemble doit permettre de calculer un modèle à lui seul. Le nombre de point est donc déterminé par le nombre de points nécessaires au calcul du modèle. Il est préférable que ce modèle utilise le moins de points possibles, tout en permettant de vérifier la conformité (ou non) des différents points à ce modèle. Le modèle utilisé dans le cadre de ce projet n'est pas celui d'une simple translation, car la précision obtenu ne serait pas suffisante et son calcul ne serait pas toujours possible, mais celui du positionnement relatif du point de vue de chaque image, décrit dans la partie 3 et dont le calcul est possible en choisissant uniquement 8 points.

Enfin, il reste à déterminer combien d'essais sont nécessaires, c'est à dire, combien de fois l'étape 4 est-elle répétée. De façon optimale, il faudrait calculer un modèle à partir de tous les sous-ensembles possibles afin d'être sûr d'obtenir le modèle avec la meilleure note et éliminer toutes les valeurs aberrantes.

En pratique et afin de réduire le temps de calcul, cet algorithme se limite à un certain pourcentage de confiance, ce qui fait qu'il est probabiliste. Les équations~\ref{equ:ransac-proba-ft} et~\ref{equ:ransac-essais-fp} permettant de calculer le nombre d’essais sont obtenues en tirant aléatoirement, et avec remise après chaque essai, les points et en notant :

\begin{itemize}
    \item $s$~: le nombre de points nécessaires au calcul du modèle~;
    \item $e$~: la proportion de valeurs aberrantes~;
    \item $T$~: le nombre d’essais à effectuer~;
    \item $p$~: la probabilité de sélectionner au moins une valeur aberrante dans chacun des $T$ essais.
\end{itemize}

\begin{equation}
    1-p = (1-(1-e)^s)^T
    \label{equ:ransac-proba-ft}
\end{equation}
\begin{equation}
    T = \frac{\log(1-p)}{\log(1-(1-e)^s)}
    \label{equ:ransac-essais-fp}
\end{equation}

On remarque donc que la valeur la plus importante à réduire afin de limiter le nombre d'essai est le nombre de points nécessaires au calcul du modèle ($s$). Dans le cadre de ce projet, en supposant un ratio de valeurs aberrantes de $30\ \%$ (il est préférable de surestimer cette valeur afin d’éviter tout problème par la suite) et avec une probabilité de succès fixée à $99,9\ \%$, 78~essais sont à réaliser. Il est possible de réduire cette première estimation en donnant par exemple une meilleure approche du ratio de valeurs aberrantes, qu'il est possible d'obtenir plus précisément en faisant des essais sur des images à disposition.

\section{Discussion}

En conclusion, la détection de points caractéristiques dans un ensemble d’images d’une scène en vue de leur mise en correspondance est un processus décomposé dans la littérature en trois phases. Dans un premier temps, les points caractéristiques sont détectés individuellement dans chaque image. Cette détection peut être faite par l’algorithme SIFT ou SURF.

Par la suite, un descripteur est calculé pour chaque point caractéristique, de telle sorte que deux points équivalents de la scène résultent en des descripteurs numériquement très proches, et inversement que deux points différents résultent en des descripteurs très différents. Nous avons étudié dans cette partie deux descripteurs~: SIFT et SURF.

Enfin, les points caractéristiques sont mis en correspondance par simple comparaison de leurs composantes. Cette mise en correspondance génère souvent des ensembles aberrants de points caractéristiques, et les résultats doivent donc être filtrés par une méthode telle que RANSAC avant d’être exploitables pour, par exemple, la reconstruction d’un modèle 3D d’une scène.

Les expérimentations réalisées par~\cite{juan-2010-comparaison}, indiquent que l’algorithme SURF est en moyenne trois fois plus rapide que l’algorithme SIFT pour la détection et la description des points caractéristiques. Cependant, l’étude de~\cite{panchal-2013-comparaison}, montre que l'algorithme SIFT donne des résultats plus cohérents que SURF, notamment dans le cas où les images présentent de forts changements de luminosité où d’angle de vue.

Un des objectifs de notre projet est de minimiser le plus possible le temps de calcul du modèle. Par ailleurs, après concertation avec l’équipe chargée de récolter les images, ces dernières ne devraient pas présenter de forts changements lumineux ou d’angle de vue. De ce fait, les problèmes de cohérence des résultats de SURF sur ces cas particuliers peuvent être négligés. Nous avons ainsi décidé, pour la suite du projet, de nous baser sur l’algorithme SURF pour détecter, décrire et mettre en correspondance les points caractéristiques des images en vue de la reconstruction d’un modèle 3D de la scène observée.

