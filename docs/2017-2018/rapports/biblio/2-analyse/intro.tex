\section{Objectifs du projet~: traitement des images et calcul d’un modèle 3D d’une scène}

Ce projet concerne l’analyse de photographies prises par un drone en vue de calculer un modèle~3D de la scène observée par celui-ci. Cette scène est supposée statique pendant le temps de prise des photographies. Il a pour objectif le développement d’un programme acceptant en entrée des images capturées lors du vol du drone et produisant en sortie un modèle du site survolé proche de la réalité.

Plus précisément, à partir d’un flux d’images issu d’un appareil photo en déplacement dans une scène, le problème consiste à reconstruire un modèle~3D sous la forme d’un maillage de polygones texturisé (ou \emph{structure} de la scène) et à retrouver la position de l’appareil pour chaque cliché (ou \emph{motion} de l’appareil).

\subsection{Problème de photogrammétrie}

Ce problème fait partie de ceux examinés par la photogrammétrie~: science et ensemble des techniques qui permettent d’obtenir de l’information sur un environnement physique à partir d’images de cet environnement~\parencite[p.~1, chapitre~1]{forstner-2016-photogrammetric}. Dans la littérature, il est appelé \emph{«~structure from motion~»}. Son objectif est, selon \cite{waxman-1985-sfm}, «~l’inversion du processus de création d’un flux d’images afin de déterminer la structure locale de l’objet visualisé et le déplacement relatif dans l’espace de l’observateur par rapport à l’objet~»\footnote{\emph{«~The goal is then to invert the image flow process along the line of sight and thereby to determine the local structure of the object under view and the relative motion in space between the object and the observer~»}~\parencite[p.~1, traduction personnelle]{waxman-1985-sfm}.}. Ce problème est assorti d’un vocabulaire spécifique qu’il convient d’expliciter~:

\glossaire{Vocabulaire du problème \emph{«~structure from motion~»}}{
    \item[Image~:] surface rectangulaire et planaire sur laquelle sont projetés les points d’une scène.
    \item[Scène~:] environnement physique capturé en partie dans une image par un appareil photo.
    \item[Appareil photo (ou \emph{camera})~:] dispositif numérique ou analogique, physique ou conceptuel, permettant de capturer une image (en 2D) à partir d’une pose dans une scène (en 3D).
    \item[Pose~:] point de vue et direction d’un appareil photo lors de la capture d’une image.
    \item[Mouvement (ou \emph{motion})~:] suite de poses d’un appareil photo lors de la capture d’un flux d’images dans une scène.
    \item[Modèle de la scène (ou \emph{structure})~:] discrétisation et numérisation de la scène sous forme d’un maillage de polygones texturisé.
    \item[Point caractéristique (ou \emph{scene/image feature})~:] zone d’une scène ou d’une image aisément localisable indépendamment de la pose de l’appareil photo, de la luminosité ou du contraste de l’image.
}

\subsection{Processus de reconstruction d’un modèle}

Pour tenter de résoudre le problème~\emph{«~structure from motion~»}, \cite{forstner-2016-photogrammetric}~(chapitre~11) le décomposent en un processus à quatre phases, illustré par la figure~\ref{fig:sfm-pipeline}.

\begin{figure}[htb]
    \makebox[\textwidth][c]{
        \includegraphics[scale=1.1]{figures/sfm-pipeline.pdf}
    }
    \caption{\textbf{Les quatre principales phases du processus de reconstruction d’un modèle.}}
    \label{fig:sfm-pipeline}
\end{figure}

\noindent\textbf{Phase I.} Prise d’images 2D de la scène à modéliser.

\noindent\textbf{Phase II.} Détection de points caractéristiques dans les images, c’est-à-dire de zones qui sont uniques dans la scène et peuvent facilement être reconnues sous d’autres angles de vues.

\noindent\textbf{Phase III.} Mise en correspondance des points caractéristiques. La reconnaissance du même point caractéristique sur au moins deux images avec différents angles de vues est nécessaire pour pouvoir positionner ce point dans l’espace.

\noindent\textbf{Phase IV.} Positionnement des points dans l’espace. En exploitant les propriétés de la géométrie projective et les liens entre points caractéristiques, une estimation de la position de chaque point caractéristique dans l’espace de la scène peut être donnée, permettant de reconstruire un nuage de points de la scène.

\vspace{1em}

Dans le cadre de notre projet, nous avons dressé un état de l’art sur les phases II, III et IV du processus. En particulier, nous avons identifié et étudié des travaux sur la détection des points caractéristiques dans des images ainsi que leur mise en correspondance entre plusieurs images (\cite{lowe-2004-sift}~; \cite{bay-2008-surf}) et leur positionnement dans l’espace de la scène observée (\cite{zhang-2000-calibration}~; \cite{hartley-2004-multiple}~; \cite{forstner-2016-photogrammetric}).

Nous avons constaté que les solutions existantes répondent à des problèmes difficiles, et s’appuient donc sur des heuristiques. En conséquence, les solutions étudiées ne sont pas exactes et ont pour objectif de fournir un modèle se rapprochant au mieux de la réalité dans une majorité de situations. Les heuristiques permettant d’obtenir ce type de solution peuvent se révéler coûteuses en calculs, et nous prêtons donc une attention particulière à la réduction du temps de calcul.

À travers cet état de l’art, nous pouvons dire que l’objectif de notre projet est de répondre au mieux aux problématiques suivantes~:

\begin{itemize}
    \item comment reconstruire un modèle 3D à partir d’images 2D~?~;
    \item quelles informations sont nécessaires au calcul d’un modèle~?~;
    \item quelles sont les méthodes existantes et que pouvons-nous leur apporter~?~;
    \item comment obtenir un indicateur de qualité d’un modèle calculé~?~;
    \item comment faire en sorte que le calcul d’un modèle ne soit pas trop long~?
\end{itemize}

La suite de ce rapport commence par présenter des techniques permettant d’extraire des points caractéristiques d’une image et de les mettre en correspondance entre plusieurs images. La deuxième partie traite différentes méthodes permettant de positionner les caractéristiques extraites dans l’espace de la scène. Enfin, nous concluons en présentant les choix d’algorithmes faits pour la suite du projet.

