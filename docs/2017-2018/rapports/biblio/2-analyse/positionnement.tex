\chapter{Positionnement des points caractéristiques d’une scène dans l’espace}
\label{part:feature-position}

La partie précédente a développé différentes méthodes permettant d’identifier des points caractéristiques de façon individuelle dans des images d’une scène, puis de mettre en correspondance des points des images liés à un même point de cette scène.

Pour obtenir un modèle 3D complet d’une scène, ses points caractéristiques doivent être positionnés dans son espace. Cela revient à inverser le processus de formation des images de la scène dans le but de retrouver les informations perdues, c’est-à-dire la profondeur de chaque point caractéristique de la scène. Pour comprendre quelles approches permettent de réaliser cette inversion, il est nécessaire de présenter le processus de formation des images en lui-même.

En optique et en photogrammétrie, un dispositif permettant de former une image d’une scène physique est appelé un appareil photo. Il est constitué, entre autres, d’un objectif contenant une lentille dont le rôle est de projeter la lumière entrante sur un capteur qui enregistre l’image résultante~\parencite[chapitre~34, p.~1\,182]{young-2008-physics}.

Dans ce projet, l’appareil photo utilisé pour capturer la scène est dit en perspective linéaire~: toute ligne droite dans la réalité apparaît comme une ligne droite sur toute photographie produite par un tel appareil. La figure~\ref{fig:position-image-distorted} montre une photographie issue d’un appareil en perspective curviligne, qui n’a pas cette caractéristique.

\begin{figure}[htb]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/image-distorted.jpg}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/image-corrected.jpg}
    \end{minipage}
    \caption{\textbf{Photographie de la Banque Nationale de Roumanie prise avec un objectif \emph{fisheye}} (par Diego Delso, sous licence CC-BY-SA). À gauche, l’image originelle, sur laquelle les courbes surlignées sont en réalité des lignes droites. À droite, une version corrigée de l’image.}
    \label{fig:position-image-distorted}
\end{figure}

\section{Modèle du sténopé pour les appareils photo linéaires}

Pour cette classe d’appareil, une simplification courante est de le considérer comme l’assemblage d’un sténopé\footnote{«~Petit trou percé dans une feuille mince disposée au lieu et place de l'objectif d'un appareil photographique.~» En anglais, \emph{pinhole}.~\parencite{gdt}}, qui se substitue à l’objectif dans le rôle de projection des points de la scène~\parencite[chapitre~6, p.~153]{hartley-2004-multiple}, et d’un capteur, qui enregistre l’image projetée. Ce modèle (cf. figure~\ref{fig:position-pinhole}) met de côté tous les éléments optiques de l’appareil qui ne sont pas pertinents dans le contexte de la photogrammétrie car ils n’affectent pas la géométrie des images capturées.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/pinhole}
    \caption{\textbf{Simplification du dispositif d’appareil photo par le modèle du sténopé.} Une image de la scène observée se forme, à l’envers, sur un plan au fond de l’appareil et est enregistrée par un capteur fixé sur ce plan. Le centre de l’image est appelé point principal. Le sténopé, également appelé ouverture, est le centre de projection de l’image. La distance entre le sténopé et l’ouverture est appelée distance focale.}
    \label{fig:position-pinhole}
\end{figure}

Dans le modèle du sténopé, deux espaces sont importants~: l’espace de la scène, ou espace des objets, qui peut être muni d’un repère euclidien absolu $\mathcal{R}_o(^o\vec x, ^o\vec y, ^o\vec z)$ (la lettre $o$ faisant référence à «~objet~»)~; et l’espace du capteur, qui peut être muni d’un autre repère euclidien $\mathcal{R}_s(^s\vec x, ^s\vec y)$ (la lettre $s$ faisant référence à l’anglais \emph{sensor}). Dès lors, l’appareil photo est vu comme une application projective $\Phi$ de l’espace de la scène vers l’espace du capteur, définie par \begin{equation}
    \fctdef{\Phi}%
        {\mathbb{P}^3}{^ox}%
        {\mathbb{P}^2}{^sx^\prime = P \times ^ox}
    \label{equ:position-cam-trans-def}
\end{equation} où $P \in \mathcal{M}_{3,4}(\mathbb{R})$ est la matrice de projection de l’appareil~\parencite[chapitre~12.1.3, p.~465]{forstner-2016-photogrammetric}.

\section{Matrice de projection d’un appareil photo linéaire~: décomposition en paramètres intrinsèques et extrinsèques}

La projection d’un point d’une scène sur le plan du capteur d’un appareil photo est une projection centrale par rapport au point d’ouverture $O$. Afin de pouvoir exprimer simplement cette transformation, un second repère de l’espace de la scène $\mathcal{R}_k(^k\vec x, ^k\vec y, ^k\vec z)$, appelé référentiel de l’appareil photo, est défini. Il est centré sur $O$, ses axes des abscisses et des ordonnées sont alignés avec ceux de $R_s$ et l’axe des cotes est l’axe $(pO)$. Ce choix des repère est illustré par la figure~\ref{fig:position-axes}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/camera-axes}
    \caption{\textbf{Exemple de disposition des repères} de la scène $\mathcal{R}_o$ en noir, de l’appareil photo $\mathcal{R}_k$ en rouge, et du capteur $\mathcal{R}_s$ en bleu.}
    \label{fig:position-axes}
\end{figure}

Muni de ce nouveau repère, la matrice de l’application projective $\Phi$ peut être décomposée en trois transformations~: le changement de repère pour placer le point de la scène dans le référentiel de l’appareil, puis la projection centrale par le point $O$, et enfin la prise en compte des paramètres internes à l’appareil photo~\parencite[chapitre~6, p.~156]{hartley-2004-multiple}.

Les deux premières transformations sont généralement regroupées dans une matrice de paramètres dits extrinsèques à l’appareil, qui encodent la pose de l’appareil par rapport à la scène, indépendamment de sa configuration interne. La matrice des paramètres extrinsèques est définie par \begin{equation}
    W := \begin{pmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 1 & 0 \\
    \end{pmatrix} \times \begin{pmatrix}
        1 & 0 & 0 & -^oO_x \\
        0 & 1 & 0 & -^oO_y \\
        0 & 0 & 1 & -^oO_z \\
        0 & 0 & 0 &  1  \\
    \end{pmatrix} \times \begin{pmatrix}
        r_{11} & r_{12} & r_{13} & 0 \\
        r_{21} & r_{22} & r_{23} & 0 \\
        r_{31} & r_{32} & r_{33} & 0 \\
          0    &   0    &   0    & 1
    \end{pmatrix} = \begin{pmatrix}
        r_{11} & r_{12} & r_{13} & -^oO_x \\
        r_{21} & r_{22} & r_{23} & -^oO_y \\
        r_{31} & r_{32} & r_{33} & -^oO_z \\
    \end{pmatrix},
\end{equation} qui correspond à la composition d’une rotation du repère $\mathcal{R}_o$, puis d’une translation pour déplacer le centre en $O$, et enfin d’une projection sur les axes $^kx$ et $^ky$. L’appareil photo étant mobile lors d’une prise d’un flux de photographies, cette matrice est variable.

Les paramètres intrinsèques font le lien entre le repère de l’appareil et l’espace du capteur. Ces paramètres sont le décalage du point principal $p$ par rapport au centre du repère du capteur $\mathcal{R}_s$, la distance focale $c$ et la déformation verticale $m$. Ils sont regroupés dans la matrice de paramètres intrinsèques définie par \begin{equation}
    K := \begin{pmatrix}
        1 & 0 & -^sp_x \\
        0 & 1 & -^sp_y \\
        0 & 0 & 1 \\
    \end{pmatrix} \times \begin{pmatrix}
        c & 0       & 0 \\
        0 & (c + m) & 0 \\
        0 & 0       & 1 \\
    \end{pmatrix} = \begin{pmatrix}
        c & 0       & -^sp_x \\
        0 & (c + m) & -^sp_y \\
        0 & 0       &  1  \\
    \end{pmatrix},
\end{equation} qui correspond à la composition d’une mise à l’échelle par un facteur $c$ selon $^kx$ et $c + m$ selon $^ky$, puis d’une translation par $p_x$ et $p_y$, coordonnées du point principal, afin de déplacer l’origine de l’image dans le coin inférieur gauche~\parencite[chapitre~6, p.~157]{hartley-2004-multiple}.

Les matrices de paramètres intrinsèques et extrinsèques forment une décomposition de la matrice de projection de l’appareil qui décrit l’application projective $\Phi$ définie en~(\ref{equ:position-cam-trans-def}), c’est-à-dire \begin{equation}
    P = K \times W.
\end{equation}

Lorsque la matrice $K$ des paramètres intrinsèques d’un appareil photo est connue, l’appareil est dit étalonné (\emph{calibrated} en anglais). Dans le cadre de ce projet, ces paramètres ne seront pas modifiés durant la capture du flux d’images. Il est donc pertinent de calculer en avance la matrice $K$ correspondant à ces paramètres.

\section{Étalonnage d’un appareil photo~: obtention des paramètres intrinsèques}

Pour étalonner un appareil photo linéaire, une technique consiste à imprimer sur une surface plane un motif régulier, asymétrique et très contrasté, puis à photographier ce motif sous plusieurs angles avec l’appareil~\parencite{zhang-2000-calibration}. Un exemple typique, illustré par la figure~\ref{fig:position-zhang-chessboard}, est un damier, dont les points d’intersection sont détectables par l’une des méthodes présentées dans la partie~\ref{part:feature-detection}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=.9]{figures/chessboard}
    \caption{\textbf{Utilisation d’un motif de damier pour étalonner un appareil photo.}}
    \label{fig:position-zhang-chessboard}
\end{figure}

Soit $m^\prime := (^sx_{m^\prime}, ^sy_{m^\prime}, 1) \in \mathbb{P}^2$ un point caractéristique d’une des photographies du motif. Le motif visualisé étant asymétrique et de dimensions connues, la position du point du motif associé à $m^\prime$ dans le référentiel de la scène peut être retrouvée, et est notée $m := (^ox_m, ^oy_m, ^oz_m, 1) \in \mathbb{P}^3$.

Sans perte de généralité\footnote{En effet, les paramètres intrinsèques de l’appareil restent les mêmes quelle que soit sa position dans l’espace de la scène, et la position de l’appareil au moment de la prise de la photo n’est pas recherché dans ce cas.}, le repère $R_o$ peut être placé à l’origine du motif, de telle sorte que tous les points caractéristiques de ce motif soient sur le plan $z = 0$ (cf. figure~\ref{fig:position-zhang-chessboard}). En particulier, $^oz_m = 0$ car $m$ est un point caractéristique du motif. Par la définition (\ref{equ:position-cam-trans-def}),

\begin{equation}
    \begin{aligned}
        m^\prime &= \Phi(m)\\
        \begin{pmatrix}
            ^sx_{m^\prime}\\
            ^sy_{m^\prime}\\
            1\\
        \end{pmatrix} &= \lambda \times K \times \begin{pmatrix}
            r_{11} & r_{12} & r_{13} & ^oO_x \\
            r_{21} & r_{22} & r_{23} & ^oO_y \\
            r_{31} & r_{32} & r_{33} & ^oO_z \\
        \end{pmatrix} \times \begin{pmatrix}
            ^ox_m\\
            ^oy_m\\
            0\\
            1\\
        \end{pmatrix}\\
        \begin{pmatrix}
            ^sx_{m^\prime}\\
            ^sy_{m^\prime}\\
            1\\
        \end{pmatrix} &= \lambda \times K \times \begin{pmatrix}
            r_{11} & r_{12} & ^oO_x \\
            r_{21} & r_{22} & ^oO_y \\
            r_{31} & r_{32} & ^oO_z \\
        \end{pmatrix} \times \begin{pmatrix}
            ^ox_m\\
            ^oy_m\\
            1\\
        \end{pmatrix},
    \end{aligned}
\end{equation}

où $\lambda ≠ 0$ est un scalaire quelconque, qui peut être ignoré car l’équation relie des points exprimés en coordonnées homogènes. Dans ce cas particulier, la transformation projective $\Phi$ est bijective puisqu’elle relie deux espaces de même dimension. La matrice $T$, définie par

\begin{equation}
    T := \lambda \times K \times \begin{pmatrix}
        r_{11} & r_{12} & ^oO_x \\
        r_{21} & r_{22} & ^oO_y \\
        r_{31} & r_{32} & ^oO_z \\
    \end{pmatrix},
\end{equation}

caractérise ainsi une homographie, et est donc inversible. \citeauthor{zhang-2000-calibration} remarque que les vecteurs $r_1 := (r_{11}, r_{21}, r_{31})^T$ et $r_2 := (r_{12}, r_{22}, r_{32})^T$ sont orthonormaux car ils sont issus d’une matrice de rotation, d’où $r_1 \times r_2 = 0$ et $\left|r_1\right| \times \left|r_2\right| = 1$. Les deux contraintes suivantes résultent

\begin{equation}
\begin{cases}
    t_1 \times K^{-T} \times K^{-1} \times t_2 = 0,\\
    t_1 \times K^{-T} \times K^{-1} \times t_1 -
        t_2 \times K^{-T} \times K^{-1} \times t_2 = 0
\end{cases}
\label{equ:pos-constr-zhang}
\end{equation}

Pour chaque image du motif, deux contraintes sont ainsi obtenues sur l’homographie de matrice $T \in \mathcal{M}_{3}(\mathbb{R})$. Comme $T$ est définie à un facteur scalaire près, elle a 8 degrés de liberté. Pour contraindre suffisamment la matrice et ainsi déterminer sa valeur, il faut au moins 4 images différentes du motif.

En pratique, plus d’images sont nécessaires pour obtenir des valeurs précises, et ces images ne donneront pas forcément des résultats cohérents sur les contraintes (\ref{equ:pos-constr-zhang}). Pour exploiter la sur-détermination de $T$, l’erreur sur ces contraintes peut être considérée comme une fonction objectif à minimiser.

\section{Positionnement relatif de deux appareils~: obtention des paramètres extrinsèques}

Le flux d’images utilisé dans le cadre de ce projet, est constitué de photographies prises à intervalle régulier et court. Ainsi, deux images prises successivement dans le flux auront dans la majorité des cas des points en commun. Ces points communs peuvent être détectés et mis en correspondance par l’une des méthodes présentées en partie~\ref{part:feature-detection}. La figure~\ref{fig:pair-geometry} illustre un cas simplifié de point commun.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/pair-geometry}
    \caption{\textbf{Propriétés géométriques d’un point commun observé par deux appareils depuis deux positions différentes.} Le point commun $m$ peut être observé en $m^\prime$ sur la première image et en $m^\dprime$ sur la seconde. Les points $\epsilon^\prime$ et $\epsilon^\dprime$ sont appelés épipôles.}
    \label{fig:pair-geometry}
\end{figure}

La principale propriété géométrique exploitable dans ce contexte de point caractéristique commun est la coplanarité des points $m$, $O^\prime$, $m^\prime$, $\epsilon^\prime$, $O^\dprime$, $m^\dprime$ et $\epsilon^\dprime$. Au moins huit points caractéristiques distincts correspondants entre les deux images sont nécessaires pour imposer suffisamment de contraintes sur la matrice de paramètres extrinsèques $W$~\parencite[chapitre~11, p.~281]{hartley-2004-multiple}.

Malgré la détermination de contraintes sur la matrice $W$, il subsiste un dernier degré de liberté inconnu~: l’échelle. En effet, à partir de deux images et d’un ensemble de points correspondants, le second appareil peut être positionné sur une ligne, mais le point de cette ligne correspondant à la réalité ne peut pas être caractérisé. Cette ambiguïté d’échelle est illustrée par la figure~\ref{fig:pair-ambiguity}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/pair-ambiguity}
    \caption{\textbf{Ambiguïté d’échelle lors du positionnement relatif de l’appareil photo $O^\dprime$.} Ici, les positions $O^\dprime$ et $O^\trprime$ sont toutes deux des solutions valides pour les contraintes exprimées.}
    \label{fig:pair-ambiguity}
\end{figure}

Cette ambiguïté peut être résolue en plaçant dans la zone observée par l’appareil photo des points caractéristiques dont la localisation absolue dans l’espace est connue à l’avance. Une fois les matrices de paramètres extrinsèques et intrinsèques connues, la position de chaque point caractéristique visible peut être retrouvée par triangulation~\parencite[chapitre~9, p.~257]{hartley-2004-multiple}.

\section{Discussion}

Pour conclure, les points caractéristiques détectés avec l’un des algorithmes de la partie~\ref{part:feature-detection} ont, comme tout autre point d’une image formée par un appareil photo, été projetées par celui-ci à travers son objectif. Dans le contexte de la photogrammétrie, ce modèle de projection peut être simplifié par le modèle du sténopé, qui permet de dériver deux matrices de paramètres qui caractérisent de façon unique l’appareil utilisé~: les matrices de paramètres intrinsèques~$K$ et extrinsèques~$W$. Pour pouvoir estimer la position d’un point caractéristique observé dans l’espace de la scène, ces deux matrices doivent auparavant être déterminées.

Pour étalonner un appareil photo et ainsi déterminer ses paramètres intrinsèques, un motif plan et régulier peut être utilisé. Pour déterminer la position relative d’un appareil par rapport à un autre, les propriétés géométriques des points caractéristiques correspondants peuvent être exploitées. Enfin, une fois les matrices de paramètres de chaque appareil photo connues, la position de chaque point caractéristique peut être obtenue par triangulation.

Des recherches complémentaires sont nécessaires pour déterminer comment éviter l’accumulation d’erreurs de mesures si tous les points et les appareils photos sont positionnés les uns relativement aux autres. Par ailleurs, la méthode présentée dans cette partie permet d’obtenir un nuage de points, et non pas un maillage texturisé, comme requis par l’équipe réalisant le rendu du modèle~3D.

Enfin, dans cette partie, l’utilisation d’un appareil photo linéaire a été supposée. En pratique, de nombreux appareils produisent des photographies contenant des distorsions non-linéaires qu’il est nécessaire de corriger avant de pouvoir réaliser les calculs présentés précédemment. Nous devrons donc nous intéresser à ces problématiques dans la suite du projet.
