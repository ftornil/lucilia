# Planification de trajectoire pour drones de combats - Thibault MAILLOT

_S'intéresse uniquement aux drones HALE (haute altitude et longue endurance) et MALE (moyenne altitude et longue endurance) : ici on s'intéresse surtout aux problèmes de planification de trajectoires pour le système composé du vecteur (trajectoire) et de sa charge utile._ Le but de ce projet est de construire des algos de trajectoires : en fonction de la situation dans laquelle se trouve le drone, il va alors adopter tel ou tel comportement. Cet article nous est utile pour la partie théorique de détermination de trajectoire.

__La partie nous intéressant particulièrement est : "Observer un point fixe en utilisant un pattern autour d'un point d'intérêt" / "Effectuer un balayage d'une zone d'intérêt fixe avec la caméra".__ Par ailleurs, nous devrons établir une liste des contraintes s'appliquant sur notre drone et sur son matériel embarqué ainsi qu'à l'environnement. Nous pouvons également lister les différents modes affectant les appareils (drone / camera) et définir ce que nous attendons CONCRETEMENT de notre algo (survoler batiment, tourner autour, combien de fois...).

_Simule les équations du mouvement du drone HALE avec le_ __système de la Voiture de Dubins__ _: (trajectoire) x = vitesse * cos(theta) ; (trajectoire) y = vitesse * sin (theta) ; cap  = u (variable de controle)
Pour le drone MALE les équations de mouvements sont considérées comme l'évolution de son repère cinétique. Pour la suite, la thèse se divise en deux partie : l'étude des ces différentes équations par rapport aux précédents objectifs fixés ; essayer d'approcher les trajectoires à celles que pourraient réaliser un être humain (non utile pour nous)._

## Méthodes utilisées pour planifier des trajectoires

Deux aspects du problème : __le drone doit se déplacer d'un point à l'autre__ et __la caméra doit remplir sa mission__.

__Le problème de couplage vecteur / capteur__ : étant donnée une position initiale du vecteur + sa caméra (q0) comment calculer trajectoire q(t) définie sur [0,T] qui permette d'atteindre un point voulu qcible ? _Seul le cas du point final fixe nous intéresse_ Quoi qu'il en soit, pour définir une trajectoire on cherche à optimiser un critère (minimisation du temps pour rejoindre deux points, de la longueur). On peut planifier de telles trajectoires (optimisant un critère) grâce au Principe du Maximum Pontryagin (PMP).
On peut également _discréditer l'espace et appliquer des algorithmes de cheminement_ tels que Dijkstra ou A*. Il existe des patterns de trajectoires selon la mission à réaliser.

### Modélisation des drones (basée sur modèles de comportement cinématique)
On associe à des modèles cinématiques des équations formant un système. La controlabilité d'un système : pour tout couple (q0, qcible) il existe un temps T et une fonction localement intégrable u : [0 , T]-> U satisfaisant q(T) = qcible. Il existe des méthodes d'analyse de la contrabilité telles que le critère de Kalman pour les syst linéaires. En revanche si le système est non linéaire, on ne peut pas toujours prouver la controlabilité (si non linéaire et sans drift : théorème de Rashevski-Chow).
__HALE__ : altitude / vitesse constante ; suit système de Dubins, d'une forme dite "affine dans le controle"
__MALE__ : modifie cap / altitude ; suit modèle avion de Dubins : x = v cos(theta) ; y = v sin(theta) ; z = uz (altitude) ; theta = u0 (cap). un état peut aussi s'écrire q = (X,R) (matrice de rotation R : R[iuav, juav, kuav]). Au final le système cinématique peut s'écrire d'une forme légèrement différentes notamment en précisant la vitesse angulaire du drone (matrice antisymétriques avec les vitesses angulaires de roulis, tangage et lacet).

### Planif point par point, init et final fixés, aperçu méthode de stabilisation et controle



## Méthodes utilisées dans la thèse


# Principes à voir plus en profondeur

## système de la Voiture de Dubins

## évolution de son repère cinétique

## Principe du Maximum Pontryagin

## Dijkstra / A*

## Critère de Kalman (Théorème de Rashevski-Chow)

## Algèbre de Lie

## Pour en savoir plus : livres de LaValle et Laumond. (planif trajectoire)
