%%
%% Contenu introductif commun aux rapports
%%

\chapter{Introduction}

\section{Contexte général du projet~: modélisation 3D d’une scène photographiée par un drone}

Dans le cadre du projet annuel du L3 Cursus Master en Ingénierie (CMI) informatique, nous nous sommes fixé pour objectif la reconstruction d’un modèle en trois dimensions d’une scène à partir de photographies prises par un drone, et la réalisation d’un rendu dynamique de ce modèle.

Un modèle en trois dimensions d’une scène est une représentation numérique de son espace à un moment précis. À l’inverse des photographies, qui ne sont qu’une projection des objets visibles dans une scène depuis un point de vue~—~sans notion de profondeur~—, les modèles 3D conservent des informations sur la disposition spatiale des éléments de cette scène. Ces informations peuvent ensuite être analysées et manipulées par des outils numériques.

De nombreuses situations réelles peuvent tirer parti de ces informations. Par exemple, ils peuvent se révéler cruciaux pour l’analyse \emph{a posteriori} d’un accident de voiture, servir un intérêt plus historique en permettant de conserver une copie numérique d’un objet ou d’un bâtiment fragile du patrimoine~\parencite[p.~11]{remondino-2016-3d}, comme dans la figure~\ref{fig:application-chambord-nuage}, ou encore permettre de découvrir et de se repérer dans une ville, comme avec le projet Google Earth\footnote{Google Earth~: \url{https://www.google.com/earth}} présenté dans la figure~\ref{fig:application-earth}.

\begin{figure}[h!]
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/application-chambord-nuage.jpg}
        \addtocounter{footnote}{-1}\caption{\textbf{Nuage de points issu de la numérisation du château de Chambord} réalisée par des étudiants de l’École Nationale des Sciences Géographiques\protect\footnotemark.}
        \label{fig:application-chambord-nuage}
    \end{minipage}\hfill%
    \begin{minipage}[t]{.475\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/application-earth.jpg}
        \caption{\textbf{Rendu d’un modèle du cœur de Montpellier issu du logiciel Google Earth,} avec notamment le Carré Saint Anne, les Jardins du Peyrou et l’Arc de Triomphe.}
        \label{fig:application-earth}
    \end{minipage}
\end{figure}

\footnotetext{Détails sur le projet~: \url{http://micmac.ensg.eu/index.php/Presentation}}

Pour mener à bien ce projet conséquent, qui se déroule sur la période d’octobre~2017 à avril~2018, nous avons constitué trois équipes de deux étudiants affectées chacune à une partie du processus de modélisation et de rendu d’une scène, illustré par la figure~\ref{fig:processus-et-affectation}.

\begin{figure}[htb]
\makebox[\textwidth][c]{
    \begin{tikzpicture}
    \tikzset{
        dom/.style={
            align=left,
            font=\small,
            text width=4cm,
            inner sep=15pt,
            draw
        },
        label/.style={
            above=5pt, align=center,
            font=\small\itshape
        }
    }

    \node[dom] (dom-1) {
        \textbf{Équipe de projet n°~1}\\
        Programmation du drone et récolte de données\\[1em]
        Amandine~\textsc{Paillard}\\
        Alexandre~\textsc{Rouyer}\\[1em]
        \emph{Encadrant~:}\\
        M.~Ahmed~\textsc{Chemori}
    };

    \node[dom, right=1cm of dom-1.north east, anchor=north west] (dom-2) {
        \textbf{Équipe de projet n°~2}\\
        Traitement des images et calcul d’un modèle 3D d’une scène\\[1em]
        Mattéo~\textsc{Delabre}\\
        Florent~\textsc{Tornil}\\[1em]
        \emph{Encadrante~:}\\
        \Mme~Hinde~\textsc{Bouziane}
    };

    \node[dom, text width=4.5cm, right=1cm of dom-2.north east, anchor=north west] (dom-3) {
        \textbf{Équipe de projet n°~3}\\
        Construction d’une application de visualisation interactive de modèles tridimensionnels\\[1em]
        Maëlle~\textsc{Beuret}\\
        Rémi~\textsc{Cérès}\\[1em]
        \emph{Encadrante~:}\\
        \Mme~Nancy~\textsc{Rodriguez}
    };

    \path[draw, ->, >=latex, bend left]
        (dom-1.60)
        to node[label] {images}
        (dom-2.120);

    \path[draw, ->, >=latex, bend left]
        (dom-2.60)
        to node[label] {modèle 3D,\\textures}
        (dom-3.120);

    \end{tikzpicture}
}
    \caption{\textbf{Positionnement des trois projets et de leur équipe dans le processus de modélisation et de rendu d’une scène.} Cette figure montre notamment les dépendances entre équipes et les données échangées de l’une à l’autre.}
    \label{fig:processus-et-affectation}
\end{figure}

