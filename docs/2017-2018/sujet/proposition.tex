\documentclass[french]{article}

% Police
\usepackage{lmodern}
\usepackage{fontspec}
\usepackage{xunicode}

% Marges
\usepackage[a4paper, margin=3cm]{geometry}

% Francisations
\usepackage{csquotes}
\usepackage{babel}

% Métadonnées
\newcommand\thetitle{Modélisation tridimensionnelle d'un environnement par drone}
\newcommand\theauthor{Maëlle~\textsc{Beuret}, Rémi~\textsc{Cérès}, Mattéo~\textsc{Delabre}, Amandine~\textsc{Paillard}, Alexandre~\textsc{Rouyer}, Florent~\textsc{Tornil}}
\newcommand\thedate{1\textsuperscript{er} octobre 2017}

\title{\thetitle}
\author{\theauthor}
\date{\thedate}

\usepackage[unicode=true]{hyperref}
\hypersetup{
    pdftitle=\thetitle,
    pdfauthor={Maëlle Beuret, Rémi Cérès, Mattéo Delabre, Amandine Paillard, Alexandre Rouyer, Florent Tornil},
    colorlinks=true,
    linkcolor=blue,
    citecolor=blue,
    urlcolor=blue,
    breaklinks=true
}
\urlstyle{same}

% Figures
\usepackage{graphicx, grffile}

% Cadres
\usepackage{framed}

% Espacement inter-paragraphes
\setlength{\parskip}{.5em plus .2em minus .2em}

% Bibliographie
\usepackage[
    style=authortitle,
    citestyle=authoryear,
    maxbibnames=99,
    maxcitenames=5,
    backend=biber
]{biblatex}

\setlength\bibitemsep{1em}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{proposition.bib}

\DefineBibliographyStrings{french}{%
    urlseen = {consulté le},
    techreport = {Rapport technique},
}

\begin{document}

% Titre du document
\noindent
\begin{center}
    {\huge Modélisation tridimensionnelle}\\[5pt]
    {\huge d’un environnement par drone}\\[15pt]
    {\large Proposition de projet CMI annuel}\\[3pt]
    {\large \thedate}\\[15pt]
\end{center}

\noindent
\begin{minipage}[t]{.45\linewidth}
    \centering
    \textbf{Étudiant-e-s\\}
    \theauthor
\end{minipage}\hfill%
\begin{minipage}[t]{.45\linewidth}
    \centering
    \textbf{Encadrant-e-s}\\
    Hinde~\textsc{Bouziane}, Ahmed~\textsc{Chemori}, Nancy~\textsc{Rodriguez}
\end{minipage}

\vspace{15pt}
\noindent\hfil\rule{0.5\textwidth}{.4pt}\hfil

\section*{Introduction}

Un modèle en trois dimensions d’un environnement est une représentation numérique de cet espace à un moment précis. À l’inverse des photographies, qui ne représentent qu’une projection des objets de la scène, les modèles 3D conservent des informations sur la disposition spatiale de ces objets. Ces informations peuvent ensuite être analysées et manipulées par des outils numériques.

De nombreux cas réels peuvent bénéficier des informations des modèles tridimensionnels. Par exemple, ils peuvent se révéler cruciaux pour l’analyse \emph{a posteriori} d’un accident de voiture, servir un intérêt plus historique en permettant de conserver une copie numérique d’un objet ou d’un bâtiment historique fragile~\parencite[p.~11]{remondino-2016-3d}, ou encore permettre de découvrir et de se repérer dans une ville, comme avec le projet Google Earth.

Il existe différentes méthodes pour créer automatiquement ces modèles. Il est notamment possible d’utiliser un ensemble de photographies dont les points de vue se chevauchent, comme illustré dans la figure~\ref{fig:illustration}. Des techniques photogrammétriques, permettant de mesurer des objets sur des photographies, sont ensuite mobilisées pour évaluer les distances entre les objets et créer le modèle.

Ces techniques nécessitent des photographies avec une grande variété de points de vue de l’objet à modéliser, qui peut potentiellement être très grand (statue, bâtiment, ville, …). Pour réaliser ces photographies, nous pouvons tirer parti de la très grande mobilité des drones.

\begin{framed}
\noindent\textbf{Objectif du projet :} calcul d’un modèle en trois dimensions d’un environnement à partir des données récoltées par un drone et réalisation d’un rendu dynamique de ce modèle.
\end{framed}

\begin{figure}[bh]
    \centering
    \includegraphics[width=.6\linewidth]{figures/illustration}\\
    \caption{\textbf{Illustration du processus de modélisation} tridimensionnelle d’un environnement à partir d’un ensemble de photographies (source : \url{http://techni-drone.com/}).}
    \label{fig:illustration}
\end{figure}

\section*{Analyse des besoins du projet}

Le projet est divisé en trois domaines de travail, illustrés en figure~\ref{fig:decoupage}.

\begin{figure}[htb]
    \centering
    \includegraphics{figures/decoupage}
    \caption{\textbf{Résumé du découpage du projet en sous-domaines.}}
    \label{fig:decoupage}
\end{figure}

\input{parties/1-recolte}
\input{parties/2-analyse}
\input{parties/3-rendu}

\section*{Effectif}

Ce projet sera réalisé avec 6 étudiant-e-s réparti-e-s en trois binômes selon les domaines définis dans la section précédente.

\phantomsection%
\printbibliography

\end{document}
